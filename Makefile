## Provide at least `PROJ_NAME`
include config.mk

#+ how to use overring of system config but not of 'config.mk' ?
C := gcc
CPP := g++
# CPP := clang++

AR := ar -rcs

LN := ln -rfs

INSTALL ?= install
INSTALL_DIR ?= /usr

SHELL := /bin/bash

###################################################

locase = $(shell printf '$1' | tr '[:upper:]' '[:lower:]')
upcase = $(shell printf '$1' | tr '[:lower:]' '[:upper:]')

###################################################

ROOT_DIR   := $(abspath .)

INCL_DIR  := include
SRC_DIR   := src
BUILD_DIR_BASE := build
LIB_DIR_BASE   := lib
BIN_DIR_BASE   := bin
TEST_DIR_BASE  := test

INCL_LIB_DIR  := $(INCL_DIR)/$(PROJ_NAME)
SRC_LIB_DIR   := $(SRC_DIR)/$(PROJ_NAME)
BUILD_LIB_DIR  = $(BUILD_DIR)/$(PROJ_NAME)

_MAIN_DIR := main
SRC_MAIN_DIR := $(SRC_DIR)/$(_MAIN_DIR)
BUILD_MAIN_DIR = $(BUILD_DIR)/$(_MAIN_DIR)

INCL_TEST_LIB_DIR := $(INCL_LIB_DIR)
SRC_TEST_LIB_DIR  := $(SRC_LIB_DIR)
BUILD_TEST_LIB_DIR = $(BUILD_LIB_DIR)

SRC_TEST_MAIN_DIR := $(SRC_DIR)/$(TEST_DIR_BASE)
BUILD_TEST_MAIN_DIR = $(BUILD_DIR)/$(TEST_DIR_BASE)
BIN_TEST_DIR = $(TEST_DIR)

TOOLS_DIR := tools
DOC_DIR   := doc
DATA_DIR  := data
LOCAL_DIR  := local
EXTERNAL_DIR  := external

PROJ_CMD +=
PROJ_TEST_CMD +=
PROJ_SCRIPT +=

#+ no dynamic library, which would require compiling all objects with `-fPIC`
# LIB_BASE := lib$(PROJ_NAME).so
LIB_BASE = lib$(PROJ_NAME).a

###################################################

RELEASE_DIR := release
DEBUG_DIR := debug

ifdef DEBUG
  BUILD_DIR := $(BUILD_DIR_BASE)/$(DEBUG_DIR)
  LIB_DIR   := $(LIB_DIR_BASE)/$(DEBUG_DIR)
  BIN_DIR   := $(BIN_DIR_BASE)/$(DEBUG_DIR)
  TEST_DIR  := $(TEST_DIR_BASE)/$(DEBUG_DIR)
else
  BUILD_DIR := $(BUILD_DIR_BASE)/$(RELEASE_DIR)
  LIB_DIR   := $(LIB_DIR_BASE)/$(RELEASE_DIR)
  BIN_DIR   := $(BIN_DIR_BASE)/$(RELEASE_DIR)
  TEST_DIR  := $(TEST_DIR_BASE)/$(RELEASE_DIR)
endif

###################################################

override FLAGS += -Wall -Wextra -pedantic -Wshadow
override FLAGS += -I $(INCL_DIR) -I $(SRC_DIR)
override FLAGS += -fopenmp
ifeq ($(CPP), g++)
override FLAGS += -pipe
endif
ifeq ($(CPP), clang++)
override FLAGS +=
endif
override FLAGS += $(PROJ_FLAGS)

CPPFLAGS += $(FLAGS) -std=c++20
CFLAGS   += $(FLAGS) -std=gnu99
ifeq ($(CPP), g++)
CPPFLAGS +=
CFLAGS +=
endif
ifeq ($(CPP), clang++)
CPPFLAGS += -Wno-dtor-name -Wno-mismatched-tags -Wno-unused-lambda-capture -Wno-unqualified-std-cast-call
CFLAGS +=
endif
CPPFLAGS += $(PROJ_CPPFLAGS)
CFLAGS   += $(PROJ_CFLAGS)

RLS_FLAGS += -DRELEASE
RLS_FLAGS += -O3
# RLS_FLAGS += -flto
RLS_CPPFLAGS += $(RLS_FLAGS)
RLS_CFLAGS += $(RLS_FLAGS)

DBG_FLAGS += -DDEBUG
DBG_FLAGS += -g -ftrapv
DBG_FLAGS += -O0
ifeq ($(CPP), g++)
DBG_FLAGS += -fmax-errors=3
DBG_FLAGS += -fno-inline
DBG_FLAGS += -fconcepts-diagnostics-depth=2
# DBG_FLAGS += -pg
endif
ifeq ($(CPP), clang++)
DBG_FLAGS += -ferror-limit=3
endif
DBG_CPPFLAGS += $(DBG_FLAGS) #-fvtable-verify=std
DBG_CFLAGS += $(DBG_FLAGS)

ifndef DEBUG
  ifeq ($(CPP), clang++)
  CPPFLAGS += -Wno-unused-parameter -Wno-unused-variable
  endif
endif

ifdef VERBOSE
  CPPFLAGS += -DVERBOSE=$(VERBOSE)
endif

ifdef QUIET
  CPPFLAGS += -DQUIET
endif

# LIB_FLAGS += -fPIC
LIB_FLAGS +=
MAIN_FLAGS +=
TEST_FLAGS +=

LIB_CFLAGS += $(CFLAGS) $(LIB_FLAGS)
LIB_CPPFLAGS += $(CPPFLAGS) $(LIB_FLAGS)
LIB_CPPFLAGS += -Weffc++ -Wno-non-virtual-dtor -Wold-style-cast -Woverloaded-virtual -Wmissing-declarations
# LIB_CPPFLAGS += -Wsign-conversion -Wfloat-conversion
ifeq ($(CPP), g++)
LIB_CPPFLAGS +=
CPPFLAGS += -Wno-terminate -Wuseless-cast -Wsuggest-override
# CPPFLAGS += -Wsuggest-final-types -Wsuggest-final-methods
endif
ifeq ($(CPP), clang++)
LIB_CPPFLAGS +=
CPPFLAGS +=
endif
# LIB_CPPFLAGS += -Wnoexcept

MAIN_CPPFLAGS += $(CPPFLAGS) $(MAIN_FLAGS)
MAIN_CFLAGS += $(CFLAGS) $(MAIN_FLAGS)
TEST_CPPFLAGS += $(CPPFLAGS) $(TEST_FLAGS)

ifndef STATIC
  LINK_MODE := dynamic
else
  LINK_MODE := static
  override LDFLAGS += -static
  ifeq ($(CPP), g++)
    override LDFLAGS += -static-libgcc -static-libstdc++
  endif
endif

## Use both absolute and relative runtime paths for dynamic libraries
override LDFLAGS += -Wl,--no-undefined,-rpath,$(ROOT_DIR)/$(LIB_DIR)
override LDFLAGS += -Wl,--no-undefined,-rpath,$(LIB_DIR)
override LDFLAGS += -L $(ROOT_DIR)/$(LIB_DIR)
override LDFLAGS += -Wl,--no-undefined,-rpath,$(ROOT_DIR)/$(LIB_DIR_BASE)
override LDFLAGS += -Wl,--no-undefined,-rpath,$(LIB_DIR_BASE)
override LDFLAGS += -L $(ROOT_DIR)/$(LIB_DIR_BASE)
override LDFLAGS += -lm
override LDFLAGS += -lgomp
override LDFLAGS += $(PROJ_LDFLAGS)

ifndef DEBUG
  LIB_CPPFLAGS += $(RLS_CPPFLAGS)
  MAIN_CPPFLAGS += $(RLS_CPPFLAGS)
  LIB_CFLAGS += $(RLS_CFLAGS)
  MAIN_CFLAGS += $(RLS_CFLAGS)
  TEST_CPPFLAGS += $(RLS_CPPFLAGS) $(DBG_CPPFLAGS)
else
  LIB_CPPFLAGS += $(DBG_CPPFLAGS)
  MAIN_CPPFLAGS += $(DBG_CPPFLAGS)
  LIB_CFLAGS += $(DBG_CFLAGS)
  MAIN_CFLAGS += $(DBG_CFLAGS)
  TEST_CPPFLAGS += $(DBG_CPPFLAGS)
endif

###################################################

DIRS_CMD := $(TOOLS_DIR)/dev/dirs.sh

MKDIR := mkdir -p
MKDIR_DIRS := $(LIB_DIR_BASE)/ $(LIB_DIR)/ $(LOCAL_DIR)/
MKDIR_DIRS += $(shell $(DIRS_CMD) $(SRC_LIB_DIR) $(BUILD_LIB_DIR))
MKDIR_DIRS += $(shell $(DIRS_CMD) $(SRC_MAIN_DIR) $(BUILD_MAIN_DIR))
MKDIR_DIRS += $(shell $(DIRS_CMD) $(SRC_MAIN_DIR) $(BIN_DIR))
ifneq ($(SRC_LIB_DIR), $(SRC_TEST_LIB_DIR))
MKDIR_DIRS += $(shell $(DIRS_CMD) $(SRC_TEST_LIB_DIR) $(BUILD_TEST_LIB_DIR))
endif
ifneq ($(SRC_MAIN_DIR), $(SRC_TEST_MAIN_DIR))
MKDIR_DIRS += $(shell $(DIRS_CMD) $(SRC_TEST_MAIN_DIR) $(BUILD_TEST_MAIN_DIR))
MKDIR_DIRS += $(shell $(DIRS_CMD) $(SRC_TEST_MAIN_DIR) $(BIN_TEST_DIR))
endif

###################################################

LIB_HEADER := $(wildcard $(INCL_DIR)/$(PROJ_NAME).hpp)

FIND_CMD = [[ -d $(1) ]] && find $(1)
FIND_FLAGS := -not -path '*/\.*' -type f -path

ALL_HEADERS     := $(shell $(call FIND_CMD,$(INCL_DIR)) $(FIND_FLAGS) *.hpp)
ALL_INLINES     := $(shell $(call FIND_CMD,$(INCL_DIR)) $(FIND_FLAGS) *.inl)
ALL_INCLUDES := $(ALL_HEADERS) $(ALL_INLINES)

LIB_HEADERS     := $(shell $(call FIND_CMD,$(INCL_LIB_DIR)) $(FIND_FLAGS) *.hpp)
LIB_INLINES     := $(shell $(call FIND_CMD,$(INCL_LIB_DIR)) $(FIND_FLAGS) *.inl)
#+ do not include test sources in case of:
#+ ifeq ($(SRC_LIB_DIR), $(SRC_TEST_LIB_DIR))
LIB_CPP_SOURCES := $(shell $(call FIND_CMD,$(SRC_LIB_DIR))  $(FIND_FLAGS) *.cpp)
LIB_C_SOURCES   := $(shell $(call FIND_CMD,$(SRC_LIB_DIR))  $(FIND_FLAGS) *.c)

LIB_HEADERS += $(LIB_HEADER)

LIB_INCLUDES := $(LIB_HEADERS) $(LIB_INLINES)
LIB_SOURCES  := $(LIB_CPP_SOURCES) $(LIB_C_SOURCES)

MAIN_CPP_SOURCES := $(shell $(call FIND_CMD,$(SRC_MAIN_DIR)) $(FIND_FLAGS) *.cpp)
MAIN_C_SOURCES   := $(shell $(call FIND_CMD,$(SRC_MAIN_DIR)) $(FIND_FLAGS) *.c)
MAIN_SOURCES := $(MAIN_CPP_SOURCES) $(MAIN_C_SOURCES)

ifeq ($(SRC_LIB_DIR), $(SRC_TEST_LIB_DIR))
TEST_LIB_SOURCES := $(shell $(call FIND_CMD,$(SRC_LIB_DIR)) $(FIND_FLAGS) $(TEST_DIR).cpp)
TEST_LIB_SOURCES := $(shell $(call FIND_CMD,$(SRC_LIB_DIR)/$(TEST_DIR)) $(FIND_FLAGS) *.cpp)
else
TEST_LIB_SOURCES := $(shell $(call FIND_CMD,$(SRC_TEST_LIB_DIR))   $(FIND_FLAGS) *.cpp)
endif
TEST_MAIN_SOURCES := $(shell $(call FIND_CMD,$(SRC_TEST_MAIN_DIR)) $(FIND_FLAGS) *.cpp)

LIB_CPP_OBJECTS := $(patsubst $(SRC_LIB_DIR)/%, $(BUILD_LIB_DIR)/%, $(LIB_CPP_SOURCES:.cpp=.o))
LIB_C_OBJECTS := $(patsubst $(SRC_LIB_DIR)/%, $(BUILD_LIB_DIR)/%, $(LIB_C_SOURCES:.c=.o))
LIB_OBJECTS := $(LIB_CPP_OBJECTS) $(LIB_C_OBJECTS)

MAIN_OBJECTS := $(patsubst $(SRC_MAIN_DIR)/%, $(BUILD_MAIN_DIR)/%, $(MAIN_CPP_SOURCES:.cpp=.o))
MAIN_OBJECTS += $(patsubst $(SRC_MAIN_DIR)/%, $(BUILD_MAIN_DIR)/%, $(MAIN_C_SOURCES:.c=.o))

TEST_LIB_OBJECTS := $(patsubst $(SRC_TEST_LIB_DIR)/%, $(BUILD_TEST_LIB_DIR)/%, $(TEST_LIB_SOURCES:.cpp=.o))
TEST_MAIN_OBJECTS := $(patsubst $(SRC_TEST_MAIN_DIR)/%, $(BUILD_TEST_MAIN_DIR)/%, $(TEST_MAIN_SOURCES:.cpp=.o))

OBJECTS := $(LIB_OBJECTS) $(MAIN_OBJECTS)
TEST_OBJECTS := $(TEST_LIB_OBJECTS) $(TEST_MAIN_OBJECTS)
ALL_OBJECTS := $(OBJECTS) $(TEST_OBJECTS)

CMDS := $(patsubst $(SRC_MAIN_DIR)/%, $(BIN_DIR)/%, $(MAIN_CPP_SOURCES:.cpp=))
CMDS += $(patsubst $(SRC_MAIN_DIR)/%, $(BIN_DIR)/%, $(MAIN_C_SOURCES:.c=))
TEST_CMDS := $(patsubst $(SRC_TEST_MAIN_DIR)/%, $(BIN_TEST_DIR)/%, $(TEST_MAIN_SOURCES:.cpp=))
ALL_CMDS := $(CMDS) $(TEST_CMDS)

LIB := $(LIB_DIR)/$(LIB_BASE)
# LIB_LN := $(LIB_DIR_BASE)/$(LIB_BASE)

###################################################

EXTERNALS := $(wildcard $(EXTERNAL_DIR)/*)

###################################################
###################################################

### Main rules

.PHONY: default all main tests install clean print

default: external $(PROJ_CMD) $(PROJ_SCRIPT) local tools

all: default lib main test

lib: external $(LIB)

main: external $(MAIN_OBJECTS) $(CMDS)

test: external $(PROJ_TEST_CMD)

tests: external $(TEST_LIB_OBJECTS) $(TEST_MAIN_OBJECTS) $(TEST_CMDS)

local:

tools:

###################################################

### External rules

external: $(EXTERNALS)

###################################################

### Particular build files rules

$(BUILD_LIB_DIR)/%.o: $(SRC_LIB_DIR)/%.cpp
	$(CPP) -c $< $(LIB_CPPFLAGS) -o $@

$(BUILD_LIB_DIR)/%.o: $(SRC_LIB_DIR)/%.c
	$(C) -c $< $(LIB_CFLAGS) -o $@

$(BUILD_MAIN_DIR)/%.o: $(SRC_MAIN_DIR)/%.cpp
	$(CPP) -c $< $(MAIN_CPPFLAGS) -o $@

$(BUILD_MAIN_DIR)/%.o: $(SRC_MAIN_DIR)/%.c
	$(C) -c $< $(MAIN_CFLAGS) -o $@

ifneq ($(BUILD_LIB_DIR), $(BUILD_TEST_LIB_DIR))
$(BUILD_TEST_LIB_DIR)/%.o: $(SRC_TEST_LIB_DIR)/%.cpp
	$(CPP) -c $< $(LIB_CPPFLAGS) $(TEST_CPPFLAGS) -o $@
endif

$(BUILD_TEST_MAIN_DIR)/%.o: $(SRC_TEST_MAIN_DIR)/%.cpp
	$(CPP) -c $< $(TEST_CPPFLAGS) -o $@

$(BIN_DIR)/%: $(BUILD_MAIN_DIR)/%.o
	$(CPP) $^ $(LDFLAGS) -o $@

$(BIN_TEST_DIR)/%: $(BUILD_TEST_MAIN_DIR)/%.o
	$(CPP) $^ $(LDFLAGS) -o $@

###################################################

### Particular library files rules

$(LIB): $(LIB_OBJECTS) | $(LIB_DIR)
	$(AR) $@ $^

# $(LIB_LN): $(LIB)
# 	$(LN) $(LIB) $@

###################################################

### Symbolic link rules

$(PROJ_SCRIPT): | $(PROJ_CMD)
	$(LN) $(TOOLS_DIR)/bin/$(PROJ_NAME).sh $@

###################################################

### Install rules

#+ "Nothing to be done" never applies, I do not understand why
install: install-include install-lib install-bin

install-include: $(ALL_INCLUDES)
	@for h in $^; do \
		$(INSTALL) -Dm644 $$h $(INSTALL_DIR)/$$h; \
	done

install-lib: lib
# If it was dynamic library: -m 755
	@$(INSTALL) -Dm644 $(LIB) $(INSTALL_DIR)/$(LIB)

install-bin: main
	@for b in $(CMDS); do \
		$(INSTALL) -Dm755 $$b $(INSTALL_DIR)/$$b; \
	done

###################################################

.SECONDEXPANSION:

### External general rules

$(EXTERNALS): | $(LIB_DIR)/lib$$(@F).a

## General rule for external modules (e.g. the ones that use this same Makefile)
#+ not using these (would update with external) because the external file does not exist yet at the beginning
# $(LIB_DIR)/lib%.a: $(EXTERNAL_DIR)/%/$$@
# $(EXTERNAL_DIR)/%/$(LIB_DIR)/lib%.a:
$(LIB_DIR)/lib%.a:
	@$(MAKE) -C $(EXTERNAL_DIR)/$* install INSTALL_DIR=../..

## Provide specific rules for '$(LIB_DIR)/lib<module>.a' if they differ from the general one:
-include external.mk

###################################################

### Clean rules

clean: clean-lib clean-main

clean-lib:
	rm -fr $(BUILD_LIB_DIR)/* $(LIB_DIR)/*

clean-main:
	rm -fr $(BUILD_MAIN_DIR)/* $(BIN_DIR)/*

clean-test:
	rm -fr $(BUILD_TEST_LIB_DIR)/* $(BUILD_TEST_MAIN_DIR)/* $(BIN_TEST_DIR)/*

clean-all: clean clean-test

###################################################

## Print internal variables
print:
	@echo LIB_INCLUDES $(LIB_INCLUDES)
	@echo LIB_SOURCES $(LIB_SOURCES)
	@echo LIB_OBJECTS $(LIB_OBJECTS)
	@echo MAIN_SOURCES $(MAIN_SOURCES)
	@echo MAIN_OBJECTS $(MAIN_OBJECTS)
	@echo OBJECTS $(OBJECTS)
	@echo CMDS $(CMDS)
	@echo TEST_LIB_SOURCES $(TEST_LIB_SOURCES)
	@echo TEST_LIB_OBJECTS $(TEST_LIB_OBJECTS)
	@echo TEST_MAIN_SOURCES $(TEST_MAIN_SOURCES)
	@echo TEST_MAIN_OBJECTS $(TEST_MAIN_OBJECTS)
	@echo TEST_OBJECTS $(TEST_OBJECTS)
	@echo TEST_CMDS $(TEST_CMDS)
	@echo ALL_OBJECTS $(ALL_OBJECTS)
	@echo ALL_CMDS $(ALL_CMDS)
	@echo MKDIR_DIRS $(MKDIR_DIRS)
	@echo EXTERNALS $(EXTERNALS)
	@echo LIB_CPPFLAGS $(LIB_CPPFLAGS)
	@echo LIB_CFLAGS $(LIB_CFLAGS)
	@echo MAIN_CPPFLAGS $(MAIN_CPPFLAGS)
	@echo MAIN_CFLAGS $(MAIN_CFLAGS)
	@echo TEST_CPPFLAGS $(TEST_CPPFLAGS)
	@echo LDFLAGS $(LDFLAGS)

## Print internal variables
print-config:
	@echo PROJ_NAME $(PROJ_NAME)
	@echo C $(C)
	@echo CPP $(CPP)
	@echo AR $(AR)
	@echo LN $(LN)
	@echo INSTALL $(INSTALL)
	@echo SHELL $(SHELL)
	@echo BUILD_DIR $(BUILD_DIR)
	@echo LIB_DIR $(LIB_DIR)
	@echo BIN_DIR $(BIN_DIR)
	@echo TEST_DIR $(TEST_DIR)

$(MKDIR_DIRS):
	@$(MKDIR) $@

## Create directory if necessary
$(ALL_OBJECTS) $(ALL_CMDS): | $$(@D)/
$(LIB_DIR_BASE) $(LIB_DIR) $(LOCAL_DIR): | $$@/

###################################################
###################################################

.depend: | external
	$(TOOLS_DIR)/dev/deps.sh

include .depend
include .obj_depend
