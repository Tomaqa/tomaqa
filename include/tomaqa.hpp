#pragma once

/// Includes the most important headers of the library

#include "tomaqa/core.hpp"
#include "tomaqa/util.hpp"
#include "tomaqa/util/string.hpp"
#include "tomaqa/util/string/alg.hpp"
#include "tomaqa/util/ptr.hpp"
#include "tomaqa/util/fun.hpp"
#include "tomaqa/util/alg.hpp"
#include "tomaqa/util/optional.hpp"
#include "tomaqa/util/array.hpp"
#include "tomaqa/util/flag.hpp"
#include "tomaqa/util/set.hpp"
#include "tomaqa/util/map.hpp"
#include "tomaqa/util/hash.hpp"
#include "tomaqa/util/numeric.hpp"
#include "tomaqa/util/numeric/alg.hpp"
#include "tomaqa/util/scope_guard.hpp"
#include "tomaqa/util/union.hpp"
#include "tomaqa/util/path.hpp"
#include "tomaqa/math.hpp"
