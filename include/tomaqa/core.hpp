#pragma once

#include <utility>
#include <tuple>
#include <functional>
#include <compare>

#define EMPTY_CMD do {} while(0)
#define UNUSED(x) do {(void)(x);} while(0)

#define FORWARD(arg) forward<decltype(arg)>(arg)
#define MAKE_TUPLE(args) tuple{FORWARD(args)...}
#define FORWARD_AS_TUPLE(args) forward_as_tuple(FORWARD(args)...)

#define NOEXCEPT(expr) noexcept(noexcept(expr))

#ifdef __clang__
#ifndef NO_EXPLICIT_TP_INST
#define NO_EXPLICIT_TP_INST
#endif
#endif

// TODO:
//+ use `inline` even for template functions - it clarifies that it is located at 'inl'

namespace tomaqa {
    struct Dummy final {};
    struct Ignore;
    struct Absorb;

    struct Empty {};

    using std::size_t;

    using std::nullptr_t;

    using std::pair;
    using std::tuple;

    using std::initializer_list;

    using std::partial_ordering;
    using std::weak_ordering;
    using std::strong_ordering;

    using std::move;
    using std::move_if_noexcept;
    using std::forward;
    using std::swap;
    using std::exchange;

    using std::get;
    using std::make_from_tuple;
    using std::forward_as_tuple;
    using std::tie;
    using std::tuple_cat;
    using std::tuple_size_v;
    using std::apply;

    using std::invoke;

    using std::is_eq;
    using std::is_neq;
    using std::is_lt;
    using std::is_lteq;
    using std::is_gt;
    using std::is_gteq;

    decltype(auto) call(auto&& t, auto fn, auto&&... args);

    template <typename T> decltype(auto) tuple_car(T&&);
    template <typename T> decltype(auto) tuple_cdr(T&&);
    template <typename T> decltype(auto) tuple_front(T&&);
    template <typename T> decltype(auto) tuple_back(T&&);
}

namespace tomaqa {
    struct Ignore {
        explicit Ignore()                                                                 = default;
    };

    struct Absorb {
        Absorb(auto&&...)                                                                        { }
    };

    constexpr Dummy dummy;
    constexpr Ignore ignore;
    inline Dummy rdummy;
}

#include "tomaqa/core/debug.hpp"
#include "tomaqa/core/meta.hpp"
#include "tomaqa/core/string.hpp"
#include "tomaqa/core/object.hpp"
#include "tomaqa/core/vector.hpp"
#include "tomaqa/core/error.hpp"

namespace tomaqa {
    template <typename T, typename U>
    constexpr bool operator ==(const pair<T, U>&, const tuple<T, U>&);
    template <typename T, typename U>
    constexpr bool operator ==(const tuple<T, U>& lhs, const pair<T, U>& rhs) { return rhs == lhs; }

    template <typename T, typename U = T> requires Has_equals_with<T, U>
    constexpr bool operator ==(const T&, const U&);
    template <typename T, typename U = T> requires (Has_equals_with<U, T> && !Has_equals_with<T, U>)
    constexpr bool operator ==(const T&, const U&);
    template <typename T, typename U = T> requires Has_equals_with<T, U>
    constexpr bool operator !=(const T& lhs, const U& rhs)              { return !lhs.equals(rhs); }

    template <typename T, typename U = T> requires Has_compare_with<T, U>
    constexpr auto operator <=>(const T&, const U&);

    template <typename T, typename U = T> requires Swappable_with<T, U>
    void swap(T&, U&);

    /// Uses `deep_copy` if present and copy constructor otherwise
    template <copy_constructible T> T deep_copy(const T&);

    /// Generic function to get raw pointer
    template <typename T> T* get(T* ptr)                                             { return ptr; }
    auto get(auto&& ptr) requires requires { {ptr.get()} -> Is_ptr; };
}

#include "tomaqa/core.inl"

namespace tomaqa {
    static_assert(indirectly_readable<Iter_t<Vector<int>>>);
    static_assert(indirectly_readable<int*>);
    static_assert(!indirectly_readable<int>);
    static_assert(indirectly_writable<Iter_t<Vector<int>>, int>);
    static_assert(indirectly_writable<int*, int>);
    static_assert(!indirectly_writable<int, int>);
    static_assert(indirectly_writable<Iter_t<Vector<int>>, double>);
    static_assert(indirectly_writable<int*, double>);
    static_assert(!indirectly_writable<Iter_t<Vector<int>>, String>);
    static_assert(!indirectly_writable<int*, String>);
    static_assert(input_or_output_iterator<Iter_t<Vector<int>>>);
    static_assert(input_or_output_iterator<int*>);
    static_assert(!input_or_output_iterator<int>);
    static_assert(input_iterator<Iter_t<Vector<int>>>);
    static_assert(input_iterator<int*>);
    static_assert(!input_iterator<int>);
    static_assert(output_iterator<Iter_t<Vector<int>>, int>);
    static_assert(output_iterator<int*, int>);
    static_assert(!output_iterator<int, int>);
    static_assert(output_iterator<Iter_t<Vector<int>>, double>);
    static_assert(output_iterator<int*, double>);
    static_assert(!output_iterator<Iter_t<Vector<int>>, String>);
    static_assert(!output_iterator<int*, String>);
    static_assert(forward_iterator<Iter_t<Vector<int>>>);
    static_assert(forward_iterator<int*>);
    static_assert(!forward_iterator<int>);
    static_assert(bidirectional_iterator<Iter_t<Vector<int>>>);
    static_assert(bidirectional_iterator<int*>);
    static_assert(!bidirectional_iterator<int>);
    static_assert(random_access_iterator<Iter_t<Vector<int>>>);
    static_assert(random_access_iterator<int*>);
    static_assert(!random_access_iterator<int>);
    static_assert(contiguous_iterator<Iter_t<Vector<int>>>);
    static_assert(contiguous_iterator<int*>);
    static_assert(!contiguous_iterator<int>);

    static_assert(indirectly_unary_invocable<identity, Iter_t<Vector<int>>>);
    static_assert(indirectly_unary_invocable<identity, int*>);
    static_assert(!indirectly_unary_invocable<identity, int>);
    static_assert(!indirectly_unary_invocable<identity, int&>);
    static_assert(!Indirectly_binary_invocable<identity, Iter_t<Vector<int>>, Iter_t<Vector<int>>>);
    static_assert(indirectly_unary_invocable<std::negate<>, Iter_t<Vector<int>>>);
    static_assert(!indirectly_unary_invocable<std::negate<>, Iter_t<Vector<String>>>);
    static_assert(!Indirectly_binary_invocable<std::negate<>, Iter_t<Vector<int>>, Iter_t<Vector<int>>>);
    static_assert(indirectly_unary_invocable<std::logical_not<>, Iter_t<Vector<int>>>);
    static_assert(!indirectly_unary_invocable<std::logical_not<>, Iter_t<Vector<String>>>);
    static_assert(indirectly_unary_invocable<std::logical_not<>, Iter_t<Vector<istream>>>);
    static_assert(!Indirectly_binary_invocable<std::logical_not<>, Iter_t<Vector<int>>, Iter_t<Vector<int>>>);
    static_assert(!Indirectly_binary_invocable<std::logical_not<>, Iter_t<Vector<istream>>, Iter_t<Vector<istream>>>);
    static_assert(!indirectly_unary_invocable<std::plus<>, Iter_t<Vector<int>>>);
    static_assert(Indirectly_binary_invocable<std::plus<>, Iter_t<Vector<int>>, Iter_t<Vector<int>>>);
    static_assert(Indirectly_binary_invocable<std::plus<>, Iter_t<Vector<String>>, Iter_t<Vector<String>>>);
    static_assert(Indirectly_binary_invocable<std::plus<>, Iter_t<Vector<int>>, Iter_t<Vector<double>>>);
    static_assert(Indirectly_binary_invocable<std::plus<>, Iter_t<Vector<double>>, Iter_t<Vector<int>>>);
    static_assert(!Indirectly_binary_invocable<std::plus<>, Iter_t<Vector<int>>, Iter_t<Vector<String>>>);
    static_assert(!Indirectly_binary_invocable<std::plus<>, Iter_t<Vector<String>>, Iter_t<Vector<int>>>);
    static_assert(!indirectly_unary_invocable<std::less<>, Iter_t<Vector<int>>>);
    static_assert(Indirectly_binary_invocable<std::less<>, Iter_t<Vector<int>>, Iter_t<Vector<int>>>);
    static_assert(Indirectly_binary_invocable<std::less<>, Iter_t<Vector<int>>, Iter_t<Vector<double>>>);
    static_assert(Indirectly_binary_invocable<std::less<>, Iter_t<Vector<double>>, Iter_t<Vector<int>>>);
    static_assert(!Indirectly_binary_invocable<std::less<>, Iter_t<Vector<int>>, Iter_t<Vector<String>>>);
    static_assert(!Indirectly_binary_invocable<std::less<>, Iter_t<Vector<String>>, Iter_t<Vector<int>>>);

    // `negate` is `-`, but it still works here with `int`, and similarly for `plus`
    static_assert(indirect_unary_predicate<identity, Iter_t<Vector<int>>>);
    static_assert(!indirect_unary_predicate<identity, Iter_t<Vector<String>>>);
    static_assert(indirect_unary_predicate<identity, int*>);
    static_assert(!indirect_unary_predicate<identity, int>);
    static_assert(!indirect_unary_predicate<identity, int&>);
    static_assert(indirect_unary_predicate<std::negate<>, Iter_t<Vector<int>>>);
    static_assert(!indirect_unary_predicate<std::negate<>, Iter_t<Vector<String>>>);
    static_assert(!indirect_binary_predicate<std::negate<>, Iter_t<Vector<int>>, Iter_t<Vector<int>>>);
    static_assert(indirect_unary_predicate<std::logical_not<>, Iter_t<Vector<int>>>);
    static_assert(!indirect_unary_predicate<std::logical_not<>, Iter_t<Vector<String>>>);
    static_assert(indirect_unary_predicate<std::logical_not<>, Iter_t<Vector<istream>>>);
    static_assert(!indirect_binary_predicate<std::logical_not<>, Iter_t<Vector<int>>, Iter_t<Vector<int>>>);
    static_assert(!indirect_binary_predicate<std::logical_not<>, Iter_t<Vector<istream>>, Iter_t<Vector<istream>>>);
    static_assert(indirect_binary_predicate<std::plus<>, Iter_t<Vector<int>>, Iter_t<Vector<int>>>);
    static_assert(!indirect_binary_predicate<std::plus<>, Iter_t<Vector<String>>, Iter_t<Vector<String>>>);
    static_assert(!indirect_binary_predicate<std::plus<>, Iter_t<Vector<int>>, Iter_t<Vector<String>>>);
    static_assert(!indirect_binary_predicate<std::plus<>, Iter_t<Vector<String>>, Iter_t<Vector<int>>>);
    static_assert(indirect_binary_predicate<std::less<>, Iter_t<Vector<int>>, Iter_t<Vector<int>>>);
    static_assert(indirect_binary_predicate<std::less<>, Iter_t<Vector<int>>, Iter_t<Vector<double>>>);
    static_assert(indirect_binary_predicate<std::less<>, Iter_t<Vector<double>>, Iter_t<Vector<int>>>);
    static_assert(!indirect_binary_predicate<std::less<>, Iter_t<Vector<int>>, Iter_t<Vector<String>>>);
    static_assert(!indirect_binary_predicate<std::less<>, Iter_t<Vector<String>>, Iter_t<Vector<int>>>);

    static_assert(Unary_invocable_for_cont<identity, Vector<int>>);
    static_assert(!Binary_invocable_for_cont<identity, Vector<int>>);
    static_assert(Unary_invocable_for_cont<std::negate<>, Vector<int>>);
    static_assert(!Unary_invocable_for_cont<std::negate<>, Vector<String>>);
    static_assert(!Binary_invocable_for_cont<std::negate<>, Vector<int>>);
    static_assert(Unary_invocable_for_cont<std::logical_not<>, Vector<int>>);
    static_assert(!Unary_invocable_for_cont<std::logical_not<>, Vector<String>>);
    static_assert(Unary_invocable_for_cont<std::logical_not<>, Vector<istream>>);
    static_assert(!Binary_invocable_for_cont<std::logical_not<>, Vector<int>>);
    static_assert(!Binary_invocable_for_cont<std::logical_not<>, Vector<istream>>);
    static_assert(!Unary_invocable_for_cont<std::plus<>, Vector<int>>);
    static_assert(Binary_invocable_for_cont<std::plus<>, Vector<int>>);
    static_assert(!Unary_invocable_for_cont<std::plus<>, Vector<String>>);
    static_assert(Binary_invocable_for_cont<std::plus<>, Vector<String>>);
    static_assert(!Unary_invocable_for_cont<std::less<>, Vector<int>>);
    static_assert(Binary_invocable_for_cont<std::less<>, Vector<int>>);

    static_assert(Unary_predicate_for_cont<identity, Vector<int>>);
    static_assert(!Unary_predicate_for_cont<identity, Vector<String>>);
    static_assert(!Binary_predicate_for_cont<identity, Vector<int>>);
    static_assert(Unary_predicate_for_cont<std::negate<>, Vector<int>>);
    static_assert(!Unary_predicate_for_cont<std::negate<>, Vector<String>>);
    static_assert(!Binary_predicate_for_cont<std::negate<>, Vector<int>>);
    static_assert(Unary_predicate_for_cont<std::logical_not<>, Vector<int>>);
    static_assert(!Unary_predicate_for_cont<std::logical_not<>, Vector<String>>);
    static_assert(Unary_predicate_for_cont<std::logical_not<>, Vector<istream>>);
    static_assert(!Binary_predicate_for_cont<std::logical_not<>, Vector<int>>);
    static_assert(!Binary_predicate_for_cont<std::logical_not<>, Vector<istream>>);
    static_assert(!Unary_predicate_for_cont<std::plus<>, Vector<int>>);
    static_assert(Binary_predicate_for_cont<std::plus<>, Vector<int>>);
    static_assert(!Unary_predicate_for_cont<std::plus<>, Vector<double>>);
    static_assert(Binary_predicate_for_cont<std::plus<>, Vector<double>>);
    static_assert(!Unary_predicate_for_cont<std::plus<>, Vector<String>>);
    static_assert(!Binary_predicate_for_cont<std::plus<>, Vector<String>>);
    static_assert(!Unary_predicate_for_cont<std::less<>, Vector<int>>);
    static_assert(Binary_predicate_for_cont<std::less<>, Vector<int>>);

    static_assert(Unary_predicate_for_cont<identity, Vector<int>> ==
                  Unary_invocable_for_cont_r<identity, Vector<int>, bool>);
    static_assert(Unary_predicate_for_cont<identity, Vector<String>> ==
                  Unary_invocable_for_cont_r<identity, Vector<String>, bool>);
    static_assert(Binary_predicate_for_cont<identity, Vector<int>> ==
                  Binary_invocable_for_cont_r<identity, Vector<int>, bool>);
    static_assert(Unary_predicate_for_cont<std::negate<>, Vector<int>> ==
                  Unary_invocable_for_cont_r<std::negate<>, Vector<int>, bool>);
    static_assert(Unary_predicate_for_cont<std::negate<>, Vector<String>> ==
                  Unary_invocable_for_cont_r<std::negate<>, Vector<String>, bool>);
    static_assert(Binary_predicate_for_cont<std::negate<>, Vector<int>> ==
                  Binary_invocable_for_cont_r<std::negate<>, Vector<int>, bool>);
    static_assert(Unary_predicate_for_cont<std::logical_not<>, Vector<int>> ==
                  Unary_invocable_for_cont_r<std::logical_not<>, Vector<int>, bool>);
    static_assert(Unary_predicate_for_cont<std::logical_not<>, Vector<String>> ==
                  Unary_invocable_for_cont_r<std::logical_not<>, Vector<String>, bool>);
    static_assert(Unary_predicate_for_cont<std::logical_not<>, Vector<istream>> ==
                  Unary_invocable_for_cont_r<std::logical_not<>, Vector<istream>, bool>);
    static_assert(Binary_predicate_for_cont<std::logical_not<>, Vector<int>> ==
                  Binary_invocable_for_cont_r<std::logical_not<>, Vector<int>, bool>);
    static_assert(Binary_predicate_for_cont<std::logical_not<>, Vector<istream>> ==
                  Binary_invocable_for_cont_r<std::logical_not<>, Vector<istream>, bool>);
    static_assert(Unary_predicate_for_cont<std::plus<>, Vector<int>> ==
                  Unary_invocable_for_cont_r<std::plus<>, Vector<int>, bool>);
    static_assert(Binary_predicate_for_cont<std::plus<>, Vector<int>> ==
                  Binary_invocable_for_cont_r<std::plus<>, Vector<int>, bool>);
    static_assert(Unary_predicate_for_cont<std::plus<>, Vector<double>> ==
                  Unary_invocable_for_cont_r<std::plus<>, Vector<double>, bool>);
    static_assert(Binary_predicate_for_cont<std::plus<>, Vector<double>> ==
                  Binary_invocable_for_cont_r<std::plus<>, Vector<double>, bool>);
    static_assert(Unary_predicate_for_cont<std::plus<>, Vector<String>> ==
                  Unary_invocable_for_cont_r<std::plus<>, Vector<String>, bool>);
    static_assert(Binary_predicate_for_cont<std::plus<>, Vector<String>> ==
                  Binary_invocable_for_cont_r<std::plus<>, Vector<String>, bool>);
    static_assert(Unary_predicate_for_cont<std::less<>, Vector<int>> ==
                  Unary_invocable_for_cont_r<std::less<>, Vector<int>, bool>);
    static_assert(Binary_predicate_for_cont<std::less<>, Vector<int>> ==
                  Binary_invocable_for_cont_r<std::less<>, Vector<int>, bool>);

    static_assert(Unary_predicate_for_indirectly_invocable<identity, Iter_t<Vector<int>>, identity>);
    static_assert(!Unary_predicate_for_indirectly_invocable<identity, Iter_t<Vector<String>>, identity>);
    static_assert(!Unary_predicate_for_indirectly_invocable<identity, Iter_t<Vector<istream>>, identity>);
    static_assert(Unary_predicate_for_indirectly_invocable<identity, Iter_t<Vector<int>>, std::negate<>>);
    static_assert(!Unary_predicate_for_indirectly_invocable<identity, Iter_t<Vector<String>>, std::negate<>>);
    static_assert(Unary_predicate_for_indirectly_invocable<identity, Iter_t<Vector<int>>, std::logical_not<>>);
    static_assert(!Unary_predicate_for_indirectly_invocable<identity, Iter_t<Vector<String>>, std::logical_not<>>);
    static_assert(Unary_predicate_for_indirectly_invocable<identity, Iter_t<Vector<istream>>, std::logical_not<>>);
    static_assert(!Binary_predicate_for_indirectly_invocable<identity, Iter_t<Vector<int>>, identity>);
    static_assert(!Binary_predicate_for_indirectly_invocable<identity, Iter_t<Vector<String>>, identity>);
    static_assert(!Binary_predicate_for_indirectly_invocable<identity, Iter_t<Vector<istream>>, identity>);
    static_assert(!Binary_predicate_for_indirectly_invocable<identity, Iter_t<Vector<int>>, std::negate<>>);
    static_assert(!Binary_predicate_for_indirectly_invocable<identity, Iter_t<Vector<String>>, std::negate<>>);
    static_assert(!Binary_predicate_for_indirectly_invocable<identity, Iter_t<Vector<int>>, std::logical_not<>>);
    static_assert(!Binary_predicate_for_indirectly_invocable<identity, Iter_t<Vector<String>>, std::logical_not<>>);
    static_assert(!Binary_predicate_for_indirectly_invocable<identity, Iter_t<Vector<istream>>, std::logical_not<>>);
    static_assert(Unary_predicate_for_indirectly_invocable<std::negate<>, Iter_t<Vector<int>>, identity>);
    static_assert(!Unary_predicate_for_indirectly_invocable<std::negate<>, Iter_t<Vector<String>>, identity>);
    static_assert(!Unary_predicate_for_indirectly_invocable<std::negate<>, Iter_t<Vector<istream>>, identity>);
    static_assert(Unary_predicate_for_indirectly_invocable<std::negate<>, Iter_t<Vector<int>>, std::negate<>>);
    static_assert(!Unary_predicate_for_indirectly_invocable<std::negate<>, Iter_t<Vector<String>>, std::negate<>>);
    static_assert(Unary_predicate_for_indirectly_invocable<std::negate<>, Iter_t<Vector<int>>, std::logical_not<>>);
    static_assert(!Unary_predicate_for_indirectly_invocable<std::negate<>, Iter_t<Vector<String>>, std::logical_not<>>);
    static_assert(Unary_predicate_for_indirectly_invocable<std::negate<>, Iter_t<Vector<istream>>, std::logical_not<>>);
    static_assert(!Binary_predicate_for_indirectly_invocable<std::negate<>, Iter_t<Vector<int>>, identity>);
    static_assert(!Binary_predicate_for_indirectly_invocable<std::negate<>, Iter_t<Vector<String>>, identity>);
    static_assert(!Binary_predicate_for_indirectly_invocable<std::negate<>, Iter_t<Vector<istream>>, identity>);
    static_assert(!Binary_predicate_for_indirectly_invocable<std::negate<>, Iter_t<Vector<int>>, std::negate<>>);
    static_assert(!Binary_predicate_for_indirectly_invocable<std::negate<>, Iter_t<Vector<String>>, std::negate<>>);
    static_assert(!Binary_predicate_for_indirectly_invocable<std::negate<>, Iter_t<Vector<int>>, std::logical_not<>>);
    static_assert(!Binary_predicate_for_indirectly_invocable<std::negate<>, Iter_t<Vector<String>>, std::logical_not<>>);
    static_assert(!Binary_predicate_for_indirectly_invocable<std::negate<>, Iter_t<Vector<istream>>, std::logical_not<>>);
    static_assert(Unary_predicate_for_indirectly_invocable<std::logical_not<>, Iter_t<Vector<int>>, identity>);
    static_assert(!Unary_predicate_for_indirectly_invocable<std::logical_not<>, Iter_t<Vector<String>>, identity>);
    static_assert(Unary_predicate_for_indirectly_invocable<std::logical_not<>, Iter_t<Vector<istream>>, identity>);
    static_assert(Unary_predicate_for_indirectly_invocable<std::logical_not<>, Iter_t<Vector<int>>, std::negate<>>);
    static_assert(!Unary_predicate_for_indirectly_invocable<std::logical_not<>, Iter_t<Vector<String>>, std::negate<>>);
    static_assert(Unary_predicate_for_indirectly_invocable<std::logical_not<>, Iter_t<Vector<int>>, std::logical_not<>>);
    static_assert(!Unary_predicate_for_indirectly_invocable<std::logical_not<>, Iter_t<Vector<String>>, std::logical_not<>>);
    static_assert(Unary_predicate_for_indirectly_invocable<std::logical_not<>, Iter_t<Vector<istream>>, std::logical_not<>>);
    static_assert(!Binary_predicate_for_indirectly_invocable<std::logical_not<>, Iter_t<Vector<int>>, identity>);
    static_assert(!Binary_predicate_for_indirectly_invocable<std::logical_not<>, Iter_t<Vector<String>>, identity>);
    static_assert(!Binary_predicate_for_indirectly_invocable<std::logical_not<>, Iter_t<Vector<istream>>, identity>);
    static_assert(!Binary_predicate_for_indirectly_invocable<std::logical_not<>, Iter_t<Vector<int>>, std::negate<>>);
    static_assert(!Binary_predicate_for_indirectly_invocable<std::logical_not<>, Iter_t<Vector<String>>, std::negate<>>);
    static_assert(!Binary_predicate_for_indirectly_invocable<std::logical_not<>, Iter_t<Vector<int>>, std::logical_not<>>);
    static_assert(!Binary_predicate_for_indirectly_invocable<std::logical_not<>, Iter_t<Vector<String>>, std::logical_not<>>);
    static_assert(!Binary_predicate_for_indirectly_invocable<std::logical_not<>, Iter_t<Vector<istream>>, std::logical_not<>>);
    static_assert(!Unary_predicate_for_indirectly_invocable<std::plus<>, Iter_t<Vector<int>>, identity>);
    static_assert(!Unary_predicate_for_indirectly_invocable<std::plus<>, Iter_t<Vector<String>>, identity>);
    static_assert(!Unary_predicate_for_indirectly_invocable<std::plus<>, Iter_t<Vector<istream>>, identity>);
    static_assert(!Unary_predicate_for_indirectly_invocable<std::plus<>, Iter_t<Vector<int>>, std::negate<>>);
    static_assert(!Unary_predicate_for_indirectly_invocable<std::plus<>, Iter_t<Vector<String>>, std::negate<>>);
    static_assert(!Unary_predicate_for_indirectly_invocable<std::plus<>, Iter_t<Vector<int>>, std::logical_not<>>);
    static_assert(!Unary_predicate_for_indirectly_invocable<std::plus<>, Iter_t<Vector<String>>, std::logical_not<>>);
    static_assert(!Unary_predicate_for_indirectly_invocable<std::plus<>, Iter_t<Vector<istream>>, std::logical_not<>>);
    static_assert(Binary_predicate_for_indirectly_invocable<std::plus<>, Iter_t<Vector<int>>, identity>);
    static_assert(!Binary_predicate_for_indirectly_invocable<std::plus<>, Iter_t<Vector<String>>, identity>);
    static_assert(!Binary_predicate_for_indirectly_invocable<std::plus<>, Iter_t<Vector<istream>>, identity>);
    static_assert(Binary_predicate_for_indirectly_invocable<std::plus<>, Iter_t<Vector<int>>, std::negate<>>);
    static_assert(!Binary_predicate_for_indirectly_invocable<std::plus<>, Iter_t<Vector<String>>, std::negate<>>);
    static_assert(Binary_predicate_for_indirectly_invocable<std::plus<>, Iter_t<Vector<int>>, std::logical_not<>>);
    static_assert(!Binary_predicate_for_indirectly_invocable<std::plus<>, Iter_t<Vector<String>>, std::logical_not<>>);
    static_assert(Binary_predicate_for_indirectly_invocable<std::plus<>, Iter_t<Vector<istream>>, std::logical_not<>>);
    static_assert(!Unary_predicate_for_indirectly_invocable<std::less<>, Iter_t<Vector<int>>, identity>);
    static_assert(!Unary_predicate_for_indirectly_invocable<std::less<>, Iter_t<Vector<String>>, identity>);
    static_assert(!Unary_predicate_for_indirectly_invocable<std::less<>, Iter_t<Vector<istream>>, identity>);
    static_assert(!Unary_predicate_for_indirectly_invocable<std::less<>, Iter_t<Vector<int>>, std::negate<>>);
    static_assert(!Unary_predicate_for_indirectly_invocable<std::less<>, Iter_t<Vector<String>>, std::negate<>>);
    static_assert(!Unary_predicate_for_indirectly_invocable<std::less<>, Iter_t<Vector<int>>, std::logical_not<>>);
    static_assert(!Unary_predicate_for_indirectly_invocable<std::less<>, Iter_t<Vector<String>>, std::logical_not<>>);
    static_assert(!Unary_predicate_for_indirectly_invocable<std::less<>, Iter_t<Vector<istream>>, std::logical_not<>>);
    static_assert(Binary_predicate_for_indirectly_invocable<std::less<>, Iter_t<Vector<int>>, identity>);
    static_assert(Binary_predicate_for_indirectly_invocable<std::less<>, Iter_t<Vector<String>>, identity>);
    static_assert(!Binary_predicate_for_indirectly_invocable<std::less<>, Iter_t<Vector<istream>>, identity>);
    static_assert(Binary_predicate_for_indirectly_invocable<std::less<>, Iter_t<Vector<int>>, std::negate<>>);
    static_assert(!Binary_predicate_for_indirectly_invocable<std::less<>, Iter_t<Vector<String>>, std::negate<>>);
    static_assert(Binary_predicate_for_indirectly_invocable<std::less<>, Iter_t<Vector<int>>, std::logical_not<>>);
    static_assert(!Binary_predicate_for_indirectly_invocable<std::less<>, Iter_t<Vector<String>>, std::logical_not<>>);
    static_assert(Binary_predicate_for_indirectly_invocable<std::less<>, Iter_t<Vector<istream>>, std::logical_not<>>);

    static_assert(Unary_predicate_for_cont_invocable<identity, Vector<int>, identity>);
    static_assert(!Unary_predicate_for_cont_invocable<identity, Vector<String>, identity>);
    static_assert(!Unary_predicate_for_cont_invocable<identity, Vector<istream>, identity>);
    static_assert(Unary_predicate_for_cont_invocable<identity, Vector<int>, std::negate<>>);
    static_assert(!Unary_predicate_for_cont_invocable<identity, Vector<String>, std::negate<>>);
    static_assert(Unary_predicate_for_cont_invocable<identity, Vector<int>, std::logical_not<>>);
    static_assert(!Unary_predicate_for_cont_invocable<identity, Vector<String>, std::logical_not<>>);
    static_assert(Unary_predicate_for_cont_invocable<identity, Vector<istream>, std::logical_not<>>);
    static_assert(!Binary_predicate_for_cont_invocable<identity, Vector<int>, identity>);
    static_assert(!Binary_predicate_for_cont_invocable<identity, Vector<String>, identity>);
    static_assert(!Binary_predicate_for_cont_invocable<identity, Vector<istream>, identity>);
    static_assert(!Binary_predicate_for_cont_invocable<identity, Vector<int>, std::negate<>>);
    static_assert(!Binary_predicate_for_cont_invocable<identity, Vector<String>, std::negate<>>);
    static_assert(!Binary_predicate_for_cont_invocable<identity, Vector<int>, std::logical_not<>>);
    static_assert(!Binary_predicate_for_cont_invocable<identity, Vector<String>, std::logical_not<>>);
    static_assert(!Binary_predicate_for_cont_invocable<identity, Vector<istream>, std::logical_not<>>);
    static_assert(Unary_predicate_for_cont_invocable<std::negate<>, Vector<int>, identity>);
    static_assert(!Unary_predicate_for_cont_invocable<std::negate<>, Vector<String>, identity>);
    static_assert(!Unary_predicate_for_cont_invocable<std::negate<>, Vector<istream>, identity>);
    static_assert(Unary_predicate_for_cont_invocable<std::negate<>, Vector<int>, std::negate<>>);
    static_assert(!Unary_predicate_for_cont_invocable<std::negate<>, Vector<String>, std::negate<>>);
    static_assert(Unary_predicate_for_cont_invocable<std::negate<>, Vector<int>, std::logical_not<>>);
    static_assert(!Unary_predicate_for_cont_invocable<std::negate<>, Vector<String>, std::logical_not<>>);
    static_assert(Unary_predicate_for_cont_invocable<std::negate<>, Vector<istream>, std::logical_not<>>);
    static_assert(!Binary_predicate_for_cont_invocable<std::negate<>, Vector<int>, identity>);
    static_assert(!Binary_predicate_for_cont_invocable<std::negate<>, Vector<String>, identity>);
    static_assert(!Binary_predicate_for_cont_invocable<std::negate<>, Vector<istream>, identity>);
    static_assert(!Binary_predicate_for_cont_invocable<std::negate<>, Vector<int>, std::negate<>>);
    static_assert(!Binary_predicate_for_cont_invocable<std::negate<>, Vector<String>, std::negate<>>);
    static_assert(!Binary_predicate_for_cont_invocable<std::negate<>, Vector<int>, std::logical_not<>>);
    static_assert(!Binary_predicate_for_cont_invocable<std::negate<>, Vector<String>, std::logical_not<>>);
    static_assert(!Binary_predicate_for_cont_invocable<std::negate<>, Vector<istream>, std::logical_not<>>);
    static_assert(Unary_predicate_for_cont_invocable<std::logical_not<>, Vector<int>, identity>);
    static_assert(!Unary_predicate_for_cont_invocable<std::logical_not<>, Vector<String>, identity>);
    static_assert(Unary_predicate_for_cont_invocable<std::logical_not<>, Vector<istream>, identity>);
    static_assert(Unary_predicate_for_cont_invocable<std::logical_not<>, Vector<int>, std::negate<>>);
    static_assert(!Unary_predicate_for_cont_invocable<std::logical_not<>, Vector<String>, std::negate<>>);
    static_assert(Unary_predicate_for_cont_invocable<std::logical_not<>, Vector<int>, std::logical_not<>>);
    static_assert(!Unary_predicate_for_cont_invocable<std::logical_not<>, Vector<String>, std::logical_not<>>);
    static_assert(Unary_predicate_for_cont_invocable<std::logical_not<>, Vector<istream>, std::logical_not<>>);
    static_assert(!Binary_predicate_for_cont_invocable<std::logical_not<>, Vector<int>, identity>);
    static_assert(!Binary_predicate_for_cont_invocable<std::logical_not<>, Vector<String>, identity>);
    static_assert(!Binary_predicate_for_cont_invocable<std::logical_not<>, Vector<istream>, identity>);
    static_assert(!Binary_predicate_for_cont_invocable<std::logical_not<>, Vector<int>, std::negate<>>);
    static_assert(!Binary_predicate_for_cont_invocable<std::logical_not<>, Vector<String>, std::negate<>>);
    static_assert(!Binary_predicate_for_cont_invocable<std::logical_not<>, Vector<int>, std::logical_not<>>);
    static_assert(!Binary_predicate_for_cont_invocable<std::logical_not<>, Vector<String>, std::logical_not<>>);
    static_assert(!Binary_predicate_for_cont_invocable<std::logical_not<>, Vector<istream>, std::logical_not<>>);
    static_assert(!Unary_predicate_for_cont_invocable<std::plus<>, Vector<int>, identity>);
    static_assert(!Unary_predicate_for_cont_invocable<std::plus<>, Vector<String>, identity>);
    static_assert(!Unary_predicate_for_cont_invocable<std::plus<>, Vector<istream>, identity>);
    static_assert(!Unary_predicate_for_cont_invocable<std::plus<>, Vector<int>, std::negate<>>);
    static_assert(!Unary_predicate_for_cont_invocable<std::plus<>, Vector<String>, std::negate<>>);
    static_assert(!Unary_predicate_for_cont_invocable<std::plus<>, Vector<int>, std::logical_not<>>);
    static_assert(!Unary_predicate_for_cont_invocable<std::plus<>, Vector<String>, std::logical_not<>>);
    static_assert(!Unary_predicate_for_cont_invocable<std::plus<>, Vector<istream>, std::logical_not<>>);
    static_assert(Binary_predicate_for_cont_invocable<std::plus<>, Vector<int>, identity>);
    static_assert(!Binary_predicate_for_cont_invocable<std::plus<>, Vector<String>, identity>);
    static_assert(!Binary_predicate_for_cont_invocable<std::plus<>, Vector<istream>, identity>);
    static_assert(Binary_predicate_for_cont_invocable<std::plus<>, Vector<int>, std::negate<>>);
    static_assert(!Binary_predicate_for_cont_invocable<std::plus<>, Vector<String>, std::negate<>>);
    static_assert(Binary_predicate_for_cont_invocable<std::plus<>, Vector<int>, std::logical_not<>>);
    static_assert(!Binary_predicate_for_cont_invocable<std::plus<>, Vector<String>, std::logical_not<>>);
    static_assert(Binary_predicate_for_cont_invocable<std::plus<>, Vector<istream>, std::logical_not<>>);
    static_assert(!Unary_predicate_for_cont_invocable<std::less<>, Vector<int>, identity>);
    static_assert(!Unary_predicate_for_cont_invocable<std::less<>, Vector<String>, identity>);
    static_assert(!Unary_predicate_for_cont_invocable<std::less<>, Vector<istream>, identity>);
    static_assert(!Unary_predicate_for_cont_invocable<std::less<>, Vector<int>, std::negate<>>);
    static_assert(!Unary_predicate_for_cont_invocable<std::less<>, Vector<String>, std::negate<>>);
    static_assert(!Unary_predicate_for_cont_invocable<std::less<>, Vector<int>, std::logical_not<>>);
    static_assert(!Unary_predicate_for_cont_invocable<std::less<>, Vector<String>, std::logical_not<>>);
    static_assert(!Unary_predicate_for_cont_invocable<std::less<>, Vector<istream>, std::logical_not<>>);
    static_assert(Binary_predicate_for_cont_invocable<std::less<>, Vector<int>, identity>);
    static_assert(Binary_predicate_for_cont_invocable<std::less<>, Vector<String>, identity>);
    static_assert(!Binary_predicate_for_cont_invocable<std::less<>, Vector<istream>, identity>);
    static_assert(Binary_predicate_for_cont_invocable<std::less<>, Vector<int>, std::negate<>>);
    static_assert(!Binary_predicate_for_cont_invocable<std::less<>, Vector<String>, std::negate<>>);
    static_assert(Binary_predicate_for_cont_invocable<std::less<>, Vector<int>, std::logical_not<>>);
    static_assert(!Binary_predicate_for_cont_invocable<std::less<>, Vector<String>, std::logical_not<>>);
    static_assert(Binary_predicate_for_cont_invocable<std::less<>, Vector<istream>, std::logical_not<>>);
}
