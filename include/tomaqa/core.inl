#pragma once

namespace tomaqa {
    decltype(auto) call(auto&& t, auto fn, auto&&... args)
    {
        return (FORWARD(t).*move(fn))(FORWARD(args)...);
    }

    template <typename T>
    decltype(auto) tuple_car(T&& t)
    {
        return tuple_front(FORWARD(t));
    }

    namespace aux {
        template <size_t... is, typename T>
        auto tuple_cdr_impl(std::index_sequence<is...>, T&& t)
        {
            return tuple{get<is+1>(FORWARD(t))...};
        }
    }

    template <typename T>
    decltype(auto) tuple_cdr(T&& t)
    {
        // `auto&& t` and `decltype(t)` would fail with "incomplete type `std::tuple_size` ..."
        constexpr auto size_ = tuple_size_v<T>;
        auto index_seq = std::make_index_sequence<size_-1>();
        return aux::tuple_cdr_impl(move(index_seq), FORWARD(t));
    }

    template <typename T>
    decltype(auto) tuple_front(T&& t)
    {
        return get<0>(FORWARD(t));
    }

    template <typename T>
    decltype(auto) tuple_back(T&& t)
    {
        // `auto&& t` and `decltype(t)` would fail with "incomplete type `std::tuple_size` ..."
        constexpr auto size_ = tuple_size_v<T>;
        return get<size_-1>(FORWARD(t));
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace tomaqa {
    template <typename T, typename U>
    constexpr bool operator ==(const pair<T, U>& lhs, const tuple<T, U>& rhs)
    {
        return lhs.first == get<0>(rhs) && lhs.second == get<1>(rhs);
    }

    template <typename T, typename U> requires Has_equals_with<T, U>
    constexpr bool operator ==(const T& lhs, const U& rhs)
    {
        return lhs.equals(rhs);
    }

    template <typename T, typename U> requires (Has_equals_with<U, T> && !Has_equals_with<T, U>)
    constexpr bool operator ==(const T& lhs, const U& rhs)
    {
        return rhs.equals(lhs);
    }

    template <typename T, typename U> requires Has_compare_with<T, U>
    constexpr auto operator <=>(const T& lhs, const U& rhs)
    {
        return lhs.compare(rhs);
    }

    template <typename T, typename U> requires Swappable_with<T, U>
    void swap(T& lhs, U& rhs)
    {
        lhs.swap(rhs);
    }

    template <copy_constructible T>
    T deep_copy(const T& t)
    {
        if constexpr (requires { t.deep_copy(); }) return t.deep_copy();
        else return t;
    }

    auto get(auto&& ptr) requires requires { {ptr.get()} -> Is_ptr; }
    {
        return FORWARD(ptr).get();
    }
}

#include "tomaqa/core/string.inl"
#include "tomaqa/core/object.inl"
#include "tomaqa/core/vector.inl"
#include "tomaqa/core/error.inl"
