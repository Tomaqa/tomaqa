#pragma once

#ifdef NDEBUG
#undef DEBUG
#elif !defined(DEBUG)
#define NDEBUG
#endif

#ifdef DEBUG
#include <cxxabi.h>
#endif

#include <cassert>

namespace tomaqa {
    constexpr bool do_assert = true;
    constexpr bool no_assert = false;

    #ifdef DEBUG
    constexpr bool _debug_ = true;
    #else
    constexpr bool _debug_ = false;
    #endif

    #ifdef RELEASE
    constexpr bool _release_ = true;
    #else
    constexpr bool _release_ = false;
    #endif

    inline bool _prefer_assert_over_throw_ = _debug_;
}
