#pragma once

#include "tomaqa/core/string.hpp"
#include "tomaqa/core/object.hpp"

#ifndef DEBUG
#define THROW(msg) throw Error(msg)
#else
#define THROW(msg) \
    throw Error(tomaqa::to_string(__FILE__)) \
        + ":" + tomaqa::to_string(__LINE__) \
        + ":\n" + tomaqa::to_string(__PRETTY_FUNCTION__) \
        + ":\n" + msg
#endif

#define expect(condition, msg) { if (condition); else THROW(msg); }

#define EXIT(msg) { CSTREAMLN(std::cerr, msg); exit(1); }

#define _NOT_IMPLEMENTED(msg) THROW(msg)
#define NOT_IMPLEMENTED _NOT_IMPLEMENTED("Not implemented!")
#define X_NOT_IMPLEMENTED(msg) _NOT_IMPLEMENTED(msg " not implemented!")
#define NOT_IMPLEMENTED_YET _NOT_IMPLEMENTED("Not implemented yet.")

#define UNREACHABLE { assert(false); THROW(error); }

namespace tomaqa {
    class [[nodiscard]] Error;

    template <Class, typename> class Item_error_mixin;

    template <typename T> using Item_error = Item_error_mixin<Error, T>;
}

namespace tomaqa {
    class Error : public Static<Error> {
    public:
        Error(String msg_ = "")                                               : _msg(move(msg_)) { }
        ~Error()                                                                          = default;
        Error(const Error&)                                                               = default;
        Error& operator =(const Error&)                                                   = default;
        Error(Error&&)                                                                    = default;
        Error& operator =(Error&&)                                                        = default;

        const String& cmsg() const noexcept                                         { return _msg; }
        const String& msg() const& noexcept                                       { return cmsg(); }
        String msg()&& noexcept                                              { return move(msg()); }

        Error operator +(const Error&) const;
        template <typename S>
        requires (!derived_from<S, Error> && requires (String str, S s) { {str + s} -> String_c; })
        Error operator +(const S&) const;
        // Cannot(?) use more general requires-constraint, it would "depend on itself"
        friend Error operator +(const String_c auto&, const Error&);
        Error& operator +=(const Error&);
        template <typename S>
        requires (!derived_from<S, Error> && requires (String str, S s) { {str += s} -> String_c; })
        Error& operator +=(const S&);

        const String& to_string() const                                           { return cmsg(); }
        friend ostream& operator <<(ostream&, const Error&);
    private:
        String& msg()& noexcept                                                     { return _msg; }

        String _msg;
        mutable bool _printed{};
    };

    template <Class B, typename T>
    class Item_error_mixin : public Inherit<B, Item_error_mixin<B, T>> {
    public:
        using Inherit = tomaqa::Inherit<B, Item_error_mixin<B, T>>;
        using typename Inherit::This;

        Item_error_mixin(T&& item_, Error = {});
        ~Item_error_mixin()                                                               = default;
        Item_error_mixin(const Item_error_mixin&)                                         = default;
        Item_error_mixin& operator =(const Item_error_mixin&)                             = default;
        Item_error_mixin(Item_error_mixin&&)                                              = default;
        Item_error_mixin& operator =(Item_error_mixin&&)                                  = default;

        const T& citem() const noexcept                                            { return _item; }
        T& item() noexcept                                                         { return _item; }
    private:
        T _item;
    };

    const Error error;
}
