#pragma once

namespace tomaqa {
    template <typename S> requires (!derived_from<S, Error> && requires (String str, S s) { {str + s} -> String_c; })
    Error Error::operator +(const S& rhs) const
    {
        return {cmsg() + rhs};
    }

    template <typename S> requires (!derived_from<S, Error> && requires (String str, S s) { {str += s} -> String_c; })
    Error& Error::operator +=(const S& rhs)
    {
        msg() += rhs;
        return *this;
    }

    ////////////////////////////////////////////////////////////////

    template <Class B, typename T>
    Item_error_mixin<B, T>::Item_error_mixin(T&& item_, Error err)
        : Inherit(move(err)), _item(FORWARD(item_))
    { }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace tomaqa {
    Error operator +(const String_c auto& lhs, const Error& rhs)
    {
        using tomaqa::to_string;
        return {to_string(lhs) + rhs.cmsg()};
    }
}
