#pragma once

#include <utility>
#include <type_traits>
#include <concepts>
#include <iterator>
#include <functional>

#define DECAY(obj) decay_t<decltype(obj)>

#define TUPLE_WITH_SEQ(tup) FORWARD(tup), std::make_index_sequence<tuple_size_v<DECAY(tup)>>{}
#define FORWARD_AS_TUPLE_WITH_SEQ(args) TUPLE_WITH_SEQ(FORWARD_AS_TUPLE(args))

#define TUPLE_WITH_SEQ_TYPENAMES typename TArgs, size_t... I
#define TUPLE_WITH_SEQ_PARAMS(tup) TArgs&& tup, std::index_sequence<I...>
#define UNPACK_TUPLE(tup) get<I>(FORWARD(tup))...

namespace tomaqa {
    struct Morph;

    template <typename = void> struct Tag {};
    struct Explicit_tag : Tag<> {};

    struct Copy_tag : Tag<> {};
    struct Shallow_copy_tag : Copy_tag {};
    struct Deep_copy_tag : Copy_tag {};

    template <template<typename...> typename X, typename T, typename = void>
    struct Enabled_bool;

    template <template<typename...> typename T, template<typename...> typename U, typename... Args>
    class Connect;

    using std::integral_constant;
    using std::bool_constant;
    using std::true_type;
    using std::false_type;

    using std::type_identity_t;
    using std::identity;

    using std::conditional_t;

    using std::decay_t;
    template <typename T> using Rm_const = std::remove_const_t<T>;
    template <typename T> using Rm_cv = std::remove_cv_t<T>;
    template <typename T> using Rm_ref = std::remove_reference_t<T>;
    template <typename T> using Rm_cvref = std::remove_cvref_t<T>;
    template <typename T> using Rm_ptr = std::remove_pointer_t<T>;
    template <typename T> using Rm_cptr = Rm_const<Rm_ptr<T>>;
    template <typename T> using Add_const = std::add_const_t<T>;
    template <typename T> using Add_cv = std::add_cv_t<T>;
    template <typename T> using Add_lref = std::add_lvalue_reference_t<T>;
    template <typename T> using Add_rref = std::add_rvalue_reference_t<T>;
    template <typename T> using Add_cref = Add_lref<Add_const<T>>;
    template <typename T> using Add_ptr = std::add_pointer_t<T>;
    template <typename T> using Add_cptr = Add_ptr<Add_const<T>>;

    using std::is_const_v;
    template <typename T> constexpr bool is_ref_v = std::is_reference_v<T>;
    template <typename T> constexpr bool is_lref_v = std::is_lvalue_reference_v<T>;
    template <typename T> constexpr bool is_rref_v = std::is_rvalue_reference_v<T>;
    template <typename T> constexpr bool is_cref_v = is_lref_v<T> && is_const_v<Rm_ref<T>>;
    template <typename T> constexpr bool is_ptr_v = std::is_pointer_v<T>;
    template <typename T> constexpr bool is_cptr_v = is_ptr_v<T> && is_const_v<Rm_ptr<T>>;

    template <typename T, typename U>
    using Const_as = conditional_t<is_const_v<U>, Add_const<T>,
                     conditional_t<is_cref_v<U> && is_ref_v<T>, Add_cref<Rm_ref<T>>,
                     conditional_t<is_cref_v<U> && is_ptr_v<T>, Add_cptr<Rm_ptr<T>>,
                     conditional_t<is_cptr_v<U> && is_ref_v<T>, Add_cref<Rm_ref<T>>,
                     conditional_t<is_cptr_v<U> && is_ptr_v<T>, Add_cptr<Rm_ptr<T>>,
                     T>>>>>;
    /// `Forward_as<T, T>` should be equivalent with `T`
    template <typename T, typename U>
    using Forward_as = conditional_t<is_cref_v<U>, Add_cref<T>,
                       conditional_t<is_cptr_v<U>, Add_cptr<T>,
                       conditional_t<is_lref_v<U>, Add_lref<T>,
                       conditional_t<is_rref_v<U>, Add_rref<T>,
                       conditional_t<is_ptr_v<U>, Add_ptr<T>,
                       T>>>>>;
    template <typename T, typename U = T>
    using Forward_as_ref = conditional_t<is_cref_v<U>, Add_cref<T>,
                           conditional_t<is_lref_v<U>, Add_lref<T>,
                           Add_rref<T>>>;
    template <typename T, typename U = T>
    using Forward_as_ptr = conditional_t<is_cptr_v<U>, Add_cptr<T>, Add_ptr<T>>;

    using std::invoke_result_t;

    using std::is_void_v;
    using std::is_member_object_pointer_v;
    using std::is_member_function_pointer_v;

    using std::is_default_constructible_v;
    using std::is_nothrow_default_constructible_v;
    using std::is_copy_constructible_v;
    using std::is_nothrow_copy_constructible_v;
    using std::is_move_constructible_v;
    using std::is_nothrow_move_constructible_v;

    using std::is_same_v;
    using std::is_base_of_v;

    template <typename T>
    constexpr bool is_dummy_v = is_same_v<Rm_ref<T>, Dummy>;

    template <template<typename...> typename X, typename T>
    constexpr bool enabled_bool_v = Enabled_bool<X, T>::value;

    using std::same_as;
    using std::derived_from;
    using std::convertible_to;
    using std::constructible_from;
    using std::assignable_from;
    using std::swappable;
    using std::swappable_with;
    using std::destructible;
    using std::default_initializable;
    using std::move_constructible;
    using std::copy_constructible;
    template <typename T, typename U> concept Derived_from = derived_from<Rm_ref<T>, Rm_ref<U>>;
    template <typename T, typename U> concept Base_of = Derived_from<U, T>;

    using std::integral;
    using std::signed_integral;
    using std::unsigned_integral;
    using std::floating_point;
    template <typename T> concept Arithmetic = std::is_arithmetic_v<T>;

    template <typename T> concept Void = is_void_v<T>;
    template <typename T> concept Member_object_pointer = is_member_object_pointer_v<T>;
    template <typename T> concept Member_function_pointer = is_member_function_pointer_v<T>;

    template <typename T> concept Class = std::is_class_v<T>;
    template <typename T> concept Polymorphic = std::is_polymorphic_v<T>;
    template <typename T> concept Abstract = std::is_abstract_v<T>;
    template <typename T> concept Non_abstract = !Abstract<T>;
    template <typename T> concept Final = std::is_final_v<T>;
    template <typename T> concept Aggregate = std::is_aggregate_v<T>;

     template <typename T> concept Is_ref = is_ref_v<T>;
     template <typename T> concept Is_lref = is_lref_v<T>;
     template <typename T> concept Is_rref = is_rref_v<T>;
     template <typename T> concept Is_cref = is_cref_v<T>;
     template <typename T> concept Is_ptr = is_ptr_v<T>;
     template <typename T> concept Is_cptr = is_cptr_v<T>;

    using std::equality_comparable;
    using std::equality_comparable_with;
    using std::totally_ordered;
    using std::totally_ordered_with;
    using std::three_way_comparable;
    using std::three_way_comparable_with;

    template <typename T, typename U>
    concept Has_equals_with = requires (T& t, U& u) { t.equals(u); };
    template <typename T> concept Has_equals = Has_equals_with<T, T>;
    template <typename T, typename U>
    concept Has_compare_with = requires (T& t, U& u) { t.compare(u); };
    template <typename T> concept Has_compare = Has_compare_with<T, T>;

    using std::movable;
    using std::copyable;
    using std::semiregular;
    using std::regular;

    //? relation to std::swappable
    template <typename T, typename U>
    concept Swappable_with = requires (T& t, U& u) { t.swap(u); };
    template <typename T> concept Swappable = Swappable_with<T, T>;

    using std::invocable;
    using std::predicate;
    using std::relation;
    using std::equivalence_relation;
    using std::strict_weak_order;
    template <typename T, typename R, typename... Args>
    concept Invocable_r = invocable<T, Args...> && convertible_to<invoke_result_t<T, Args...>, R>;
    template <typename T> concept Unary_invocable = invocable<T, Morph&>;
    template <typename T> concept Binary_invocable = invocable<T, Morph&, Morph&>;
    template <typename T> concept Unary_predicate = predicate<T, Morph&>;
    template <typename T> concept Binary_predicate = predicate<T, Morph&, Morph&>;

    using std::indirectly_readable;
    using std::indirectly_writable;
    using std::input_or_output_iterator;
    using std::input_iterator;
    using std::output_iterator;
    using std::forward_iterator;
    using std::bidirectional_iterator;
    using std::random_access_iterator;
    using std::contiguous_iterator;

    namespace detail {
        template <typename T>
        struct Value {};

        template <input_iterator T>
        struct Value<T> {
            using Type = std::iter_value_t<T>;
        };

        template <typename T>
        requires (!input_iterator<T> && requires { typename Rm_ref<T>::value_type; })
        struct Value<T> {
            using Type = typename Rm_ref<T>::value_type;
        };

        template <typename T>
        struct Ref {};

        template <input_iterator T>
        struct Ref<T> {
            using Type = std::iter_reference_t<T>;
        };

        template <typename T>
        requires (!input_iterator<T> && requires { typename Rm_ref<T>::reference; })
        struct Ref<T> {
            using Type = typename Rm_ref<T>::reference;
        };
    }

    template <typename T> using Value_t = typename detail::Value<T>::Type;
    template <typename T> using Ref_t = typename detail::Ref<T>::Type;
    template <typename T> using Iter_t = typename Rm_ref<T>::iterator;

    //? is it still useful along with C++20 ranges?
    /// References not included - e.g. strings do not have them
    template <typename T> concept Container = !input_or_output_iterator<T> && requires {
        typename Iter_t<T>;
        typename Value_t<T>;
    };

    template <typename T, typename C>
    concept Output_iterator_for_cont = Container<C> && output_iterator<T, Value_t<C>>;

    using std::indirectly_unary_invocable;
    using std::indirect_unary_predicate;
    using std::indirect_binary_predicate;
    template <typename F, typename I1, typename I2>
    concept Indirectly_binary_invocable = indirectly_readable<I1> && indirectly_readable<I2>
                                       && copy_constructible<F>
                                       && invocable<F&, Value_t<I1>&, Value_t<I2>&>
                                       && invocable<F&, Value_t<I1>&, Ref_t<I2>>
                                       && invocable<F&, Ref_t<I1>, Value_t<I2>&>
                                       && invocable<F&, Ref_t<I1>, Ref_t<I2>>
                                       && std::common_reference_with<
                                           invoke_result_t<F&, Value_t<I1>&, Value_t<I2>&>,
                                           invoke_result_t<F&, Value_t<I1>&, Ref_t<I2>> >
                                       && std::common_reference_with<
                                           invoke_result_t<F&, Value_t<I1>&, Ref_t<I2>>,
                                           invoke_result_t<F&, Ref_t<I1>, Value_t<I2>&> >
                                       && std::common_reference_with<
                                           invoke_result_t<F&, Ref_t<I1>, Value_t<I2>&>,
                                           invoke_result_t<F&, Ref_t<I1>, Ref_t<I2>>>;

    template <typename T, typename I, typename ConvF>
    concept Output_iterator_for_indirectly_invocable = output_iterator<T, invoke_result_t<ConvF, Ref_t<I>>>
                                                    || output_iterator<T, invoke_result_t<ConvF, Ref_t<I>, Ref_t<I>>>;
    template <typename T, typename C, typename ConvF>
    concept Output_iterator_for_cont_invocable = Container<C> && Output_iterator_for_indirectly_invocable<T, Iter_t<C>, ConvF>;

    template <typename F, typename C>
    concept Unary_invocable_for_cont = Container<C> && indirectly_unary_invocable<F, Iter_t<C>>;
    template <typename F, typename C>
    concept Binary_invocable_for_cont = Container<C>
                                     && Indirectly_binary_invocable<F, Iter_t<C>, Iter_t<C>>;
    template <typename F, typename C>
    concept Unary_predicate_for_cont = Container<C> && indirect_unary_predicate<F, Iter_t<C>>;
    template <typename F, typename C>
    concept Binary_predicate_for_cont = Container<C>
                                     && indirect_binary_predicate<F, Iter_t<C>, Iter_t<C>>;

    template <typename F, typename C, typename R>
    concept Unary_invocable_for_cont_r = Unary_invocable_for_cont<F, C>
                                      && convertible_to<invoke_result_t<F&, Ref_t<C>>, R>;
    template <typename F, typename C, typename R>
    concept Binary_invocable_for_cont_r = Binary_invocable_for_cont<F, C>
                                       && convertible_to<invoke_result_t<F&, Ref_t<C>, Ref_t<C>>, R>;
    template <typename F, typename C, typename R>
    concept Unary_predicate_for_cont_r = Unary_predicate_for_cont<F, C>
                                      && convertible_to<invoke_result_t<F&, Ref_t<C>>, R>;
    template <typename F, typename C, typename R>
    concept Binary_predicate_for_cont_r = Binary_predicate_for_cont<F, C>
                                       && convertible_to<invoke_result_t<F&, Ref_t<C>, Ref_t<C>>, R>;

    template <typename F, typename I, typename ConvF>
    concept Unary_predicate_for_indirectly_invocable = indirectly_unary_invocable<ConvF, I>
                                                    && predicate<F, invoke_result_t<ConvF, Ref_t<I>>>;
    template <typename F, typename I, typename ConvF>
    concept Binary_predicate_for_indirectly_invocable = indirectly_unary_invocable<ConvF, I>
                                                     && predicate<F, invoke_result_t<ConvF, Ref_t<I>>, invoke_result_t<ConvF, Ref_t<I>>>;

    template <typename F, typename C, typename ConvF>
    concept Unary_predicate_for_cont_invocable = Unary_invocable_for_cont<ConvF, C>
                                              && predicate<F, invoke_result_t<ConvF, Ref_t<C>>>;
    template <typename F, typename C, typename ConvF>
    concept Binary_predicate_for_cont_invocable = Unary_invocable_for_cont<ConvF, C>
                                               && predicate<F, invoke_result_t<ConvF, Ref_t<C>>, invoke_result_t<ConvF, Ref_t<C>>>;

    template <typename> constexpr bool true_tp = true;
    template <typename> constexpr bool false_tp = false;

    template <typename T> constexpr Tag<T> tag;
    constexpr Explicit_tag explicit_tag;

    constexpr Copy_tag copy_tag;
    constexpr Shallow_copy_tag shallow_copy_tag;
    constexpr Deep_copy_tag deep_copy_tag;

    using std::as_const;
}

namespace tomaqa {
    struct Morph {
        template <typename T> operator T&();
        template <typename T> operator T&&();

        template <typename T> Morph operator +(T) const;
        template <typename T> Morph operator -(T) const;
        template <typename T> Morph operator *(T) const;
        template <typename T> Morph operator /(T) const;
        template <typename T> Morph operator &(T) const;
        template <typename T> Morph operator |(T) const;
        template <typename T> Morph operator ^(T) const;

        template <typename T> partial_ordering operator <=>(T) const;
    };

    template <template<typename...> typename X, typename T, typename>
    struct Enabled_bool final : false_type {};

    template <template<typename...> typename X, typename T>
    struct Enabled_bool<X, T, type_identity_t<X<T>>> final : X<T> {};

    template <template<typename...> typename T, template<typename...> typename U, typename... Args>
    class Connect final {
    private:
        template <typename C, bool first>
        struct Other {
            using Type = conditional_t<first, typename C::T1, typename C::T2>;
        };
    public:
        using T1 = T<Args..., Other<Connect, false>>;
        using T2 = U<Args..., Other<Connect, true>>;
    };
}

namespace tomaqa {
    static_assert(is_base_of_v<Dummy, Dummy>);
    static_assert(!is_base_of_v<Dummy, Dummy&>);
    static_assert(!is_base_of_v<Dummy&, Dummy>);
    static_assert(!is_base_of_v<Dummy&, Dummy&>);
    static_assert(derived_from<Dummy, Dummy>);
    static_assert(!derived_from<Dummy, Dummy&>);
    static_assert(!derived_from<Dummy&, Dummy>);
    static_assert(!derived_from<Dummy&, Dummy&>);
    static_assert(Derived_from<Dummy, Dummy>);
    static_assert(Derived_from<Dummy, Dummy&>);
    static_assert(Derived_from<Dummy, Dummy&&>);
    static_assert(Derived_from<Dummy, const Dummy&>);
    static_assert(Derived_from<Dummy&, Dummy>);
    static_assert(Derived_from<Dummy&, Dummy&>);
    static_assert(Derived_from<Dummy&, Dummy&&>);
    static_assert(Derived_from<Dummy&, const Dummy&>);
    static_assert(Derived_from<Dummy&&, Dummy>);
    static_assert(Derived_from<Dummy&&, Dummy&>);
    static_assert(Derived_from<Dummy&&, Dummy&&>);
    static_assert(Derived_from<Dummy&&, const Dummy&>);
    static_assert(Derived_from<const Dummy&, Dummy>);
    static_assert(Derived_from<const Dummy&, Dummy&>);
    static_assert(Derived_from<const Dummy&, Dummy&&>);
    static_assert(Derived_from<const Dummy&, const Dummy&>);

    static_assert(Unary_invocable<std::logical_not<int>>);
    static_assert(Unary_invocable<std::logical_not<>>);
    static_assert(Binary_invocable<std::less<int>>);
    static_assert(Binary_invocable<std::less<>>);

    static_assert(Unary_predicate<std::logical_not<int>>);
    static_assert(Unary_predicate<std::logical_not<>>);
    static_assert(Binary_predicate<std::less<int>>);
    static_assert(Binary_predicate<std::less<>>);
}
