#pragma once

#include "tomaqa/core/meta.hpp"

namespace tomaqa {
    /// Curiously recurring template pattern - for static polymorphism
    template <Class> class Crtp;

    template <Class> struct Object;
    /// Object with support of static functions
    template <Class> struct Static;
    /// Object with support of virtual functions
    template <Class T, indirectly_readable = T*> struct Dynamic;

    template <Class, typename = void> struct Inherit;

    template <bool cond, template<typename...> typename MixinT, Class T>
    using Cond_mixin = conditional_t<cond, MixinT<T>, T>;
}

#include "tomaqa/core/object/detail.hpp"

namespace tomaqa {
    template <Class T>
    class Crtp {
    public:
        friend T;

        using That = T;

        const auto& cthat() const noexcept;
        auto& that() noexcept;
        auto cthat_l() const noexcept;
        auto that_l() noexcept;
    protected:
        Crtp()                                                      = default;
        ~Crtp()                                                     = default;
        Crtp(const Crtp&)                                           = default;
        Crtp& operator =(const Crtp&)                               = default;
        Crtp(Crtp&&)                                                = default;
        Crtp& operator =(Crtp&&)                                    = default;
    };

    using detail::Object_c;
    using detail::Static_c;
    using detail::Dynamic_c;

    using detail::Has_base;

    template <Class T>
    struct Object : detail::Object_mixin<detail::Object_base, T> {};

    template <Class T>
    struct Static : detail::String_mixin<detail::Static_mixin<detail::Static_base, T>> {};

    template <Class T, indirectly_readable P>
    struct Dynamic : detail::String_mixin<detail::Dynamic_mixin<detail::Dynamic_base<T, P>, T>> {};

    //+ Such specialization works only in GCC ...
    //+ template <Class B, typename T>
    //+ struct Inherit : detail::Inherit_mixin<B, T> {
    //+     using detail::Inherit_mixin<B, T>::Inherit_mixin;
    template <Class B, typename T>
    struct Inherit : detail::Inherit_mixin<B, T, Class<T>, Static_c<B>, Dynamic_c<B>> {
        using detail::Inherit_mixin<B, T, Class<T>, Static_c<B>, Dynamic_c<B>>::Inherit_mixin;
    };
}
