#pragma once

namespace tomaqa {
    template <Class T>
    const auto& Crtp<T>::cthat() const noexcept
    {
        return static_cast<const That&>(*this);
    }

    template <Class T>
    auto& Crtp<T>::that() noexcept
    {
        return static_cast<That&>(*this);
    }

    template <Class T>
    auto Crtp<T>::cthat_l() const noexcept
    {
        return static_cast<const That*>(this);
    }

    template <Class T>
    auto Crtp<T>::that_l() noexcept
    {
        return static_cast<That*>(this);
    }
}

#include "tomaqa/core/object/detail.inl"
