#pragma once

#include "tomaqa/core/string.hpp"

namespace tomaqa::detail {
    struct Object_base;
    struct Static_base;
    class Dynamic_base_base;
    template <Class BaseT, indirectly_readable> class Dynamic_base;

    template <typename T> concept Object_c = Class<T> && derived_from<T, Object_base>;
    template <typename T> concept Static_c = Object_c<T> && derived_from<T, Static_base>;
    template <typename T> concept Dynamic_c = Object_c<T> && derived_from<T, Dynamic_base_base>;

    template <typename T> concept Has_base = requires { typename T::Base; };

    template <Class B, Class T> class Object_mixin;
    template <Class B, Class T> struct Static_mixin;
    template <Class B, Class T> class Dynamic_mixin;

    //+ Specialization works only in GCC ...
    //+ template <Class B, typename T = void> struct Inherit_mixin;
    template <Class B, typename T, bool t_is_class = false,
              bool b_is_static = false, bool b_is_dynamic = false>
    struct Inherit_mixin;
}

namespace tomaqa::detail {
    struct Object_base {
        bool equals(Dummy) const noexcept                                          { return false; }
    };

    struct Static_base : Object_base {};

    class Dynamic_base_base : public Object_base {
    public:
        bool initialized() const noexcept                                   { return _initialized; }

        virtual void virtual_init();
        void maybe_virtual_init();

        template <Dynamic_c T> static T cons_tp(auto&&...);

        virtual String to_string() const&;
    protected:
        virtual void recover_virtual_init()                                                      { }
        virtual bool valid_pre_init() const noexcept                                { return true; }
        virtual void check_pre_init() const                                                      { }
        virtual void recover_pre_init()                                                          { }
        virtual void init()                                                                      { }
        virtual void recover_init()                                                              { }
        virtual bool valid_post_init() const noexcept                               { return true; }
        virtual void check_post_init() const                                                     { }
        virtual void recover_post_init()                                                         { }
    private:
        /// (zero-initialization)
        bool _initialized{};
    };

    template <Class BaseT, indirectly_readable P>
    class Dynamic_base : public Dynamic_base_base {
    public:
        using Base = BaseT;
        using Ptr = P;

        /// calls `T::cons_tp` (!)
        template <Dynamic_c T> static Ptr new_tp(auto&&...);

        virtual Ptr to_ptr() const&                                                             = 0;
        virtual Ptr to_ptr()&&                                                                  = 0;

        virtual Ptr shallow_copy_to_ptr() const                                                 = 0;
        virtual Ptr deep_copy_to_ptr() const                                                    = 0;
    protected:
        virtual bool equals_base(const Base&) const noexcept                                    = 0;
        bool equals_this(const Base&) const noexcept                                { return true; }
        virtual bool equals_base_anyway(const Base&) const noexcept                { return false; }
    };

    template <Class B>
    class Parent_mixin : public B {
    public:
        using Parent = B;

        using Parent::Parent;
        Parent_mixin()                                                                    = default;
        Parent_mixin(const Parent&);
        Parent_mixin(Parent&&) noexcept;

        const Parent& cparent() const noexcept;
        const Parent& parent() const& noexcept                                 { return cparent(); }
        Parent& parent()& noexcept;
        Parent&& parent()&& noexcept                                      { return move(parent()); }
    };

    template <Class B>
    struct String_mixin : B {
        using B::B;

        explicit operator String() const                               { return this->to_string(); }
    };

    template <Class B, Class T>
    class Object_mixin : public B {
    public:
        using This = T;

        static_assert(!is_same_v<This, B>);

        using B::B;

        const auto& cthis() const noexcept;
        auto&& mthis() noexcept;
        auto cthis_l() const noexcept;

        template <Has_base = This> const auto& cbase() const noexcept;
        template <Has_base = This> auto& base() noexcept;
        template <Has_base = This> auto&& mbase() noexcept;
        template <Has_base = This> auto cbase_l() const noexcept;
        template <Has_base = This> auto base_l() noexcept;
    protected:
        struct Access;

        /// these variants are not useful outside of `This` class
        auto& rthis() noexcept;
        auto this_l() noexcept;
    };

    template <Class B, Class T>
    struct Object_mixin<B, T>::Access : Parent_mixin<This> {
        using This = Parent_mixin<Object_mixin::This>;

        friend Object_mixin;

        friend Dynamic_base_base;
        template <Class, indirectly_readable> friend class Dynamic_base;
        template <Class, Class> friend struct Static_mixin;
        template <Class, Class> friend class Dynamic_mixin;

        using This::This;
    };

    template <Class B, Class T>
    struct Static_mixin : Object_mixin<Parent_mixin<B>, T> {
        using typename Object_mixin<Parent_mixin<B>, T>::This;
        using typename Object_mixin<Parent_mixin<B>, T>::Parent;

        using Object_mixin<Parent_mixin<B>, T>::Object_mixin;

        String to_string() const&;
    };

    template <Class B, Class T>
    class Dynamic_mixin : public Object_mixin<Parent_mixin<B>, T> {
    public:
        using typename Object_mixin<Parent_mixin<B>, T>::This;
        using typename Object_mixin<Parent_mixin<B>, T>::Parent;
        using typename Object_mixin<Parent_mixin<B>, T>::Base;
        using typename Object_mixin<Parent_mixin<B>, T>::Ptr;

        static constexpr bool is_base_v = is_same_v<This, Base>;

        template <indirectly_readable P_>
        static constexpr bool is_ptr_v = is_same_v<Rm_cvref<P_>, Ptr>;

        using Object_mixin<Parent_mixin<B>, T>::Object_mixin;

        void virtual_init() override;

        /// calls `virtual_init` after construction (!)
        template <Non_abstract = This> static This cons(auto&&...);
        template <copy_constructible = This> static This cons(const This&);
        template <move_constructible = This> static This cons(This&&);
        template <typename U, constructible_from<initializer_list<U>> = This>
        static This cons(initializer_list<U>);

        //++ use concept for this?
        /// define `make_ptr` in class hierarchy (~ e.g. `make_unique`)
        /// calls `new_tp`
        template <Non_abstract = This> static Ptr new_me(auto&&...);
        template <copy_constructible = This> static Ptr new_me(const This&);
        template <move_constructible = This> static Ptr new_me(This&&);
        template <typename U, constructible_from<initializer_list<U>> = This>
            static Ptr new_me(initializer_list<U>);

        static bool is_me(const Ptr&) noexcept;
        static void check_is_me(const Ptr&);
        static void check_is_not_me(const Ptr&);
        static String is_me_msg(const Ptr&);
        static String is_not_me_msg(const Ptr&);

        template <typename U, Dynamic_c T_ = This>
        requires (convertible_to<U, T_> || indirectly_writable<U, T_>)
        static Forward_as_ref<This, U> cast(U&&) noexcept;
        template <indirectly_readable P_, Dynamic_c T_ = This> requires (T_::template is_ptr_v<P_>)
        static Forward_as_ref<This, P_> cast_check(P_&&);
        template <indirectly_readable P_, Dynamic_c T_ = This> requires (T_::template is_ptr_v<P_>)
        static Forward_as_ptr<This, P_> dcast_l(P_&&) noexcept;
        template <typename U, Dynamic_c T_ = This>
        requires (convertible_to<U, T_> || indirectly_writable<U, T_>)
        static Forward_as_ref<This, U> dcast(U&&);

        Ptr to_ptr() const& override;
        Ptr to_ptr()&& override;

        Ptr shallow_copy_to_ptr() const override;
        Ptr deep_copy_to_ptr() const override;

        using Parent::equals;
        bool equals(const This&) const noexcept;
        template <Class U, Class T_ = This>
        requires (derived_from<U, T_> || derived_from<T_, U>)
        bool dynamic_equals(const U&) const noexcept;
    protected:
        friend Dynamic_base_base;
        template <Class, indirectly_readable> friend class Dynamic_base;
        template <Class, Class> friend class Dynamic_mixin;

        using typename Object_mixin<Parent_mixin<B>, T>::Access;

        void recover_virtual_init() override;
        void lrecover_virtual_init()                                                             { }
        bool valid_pre_init() const noexcept override;
        void check_pre_init() const override;
        void recover_pre_init() override;
        bool lvalid_pre_init() const noexcept                                       { return true; }
        void lcheck_pre_init() const                                                             { }
        void lrecover_pre_init()                                                                 { }
        void init() override;
        void recover_init() override;
        void learly_init()                                                                       { }
        void linit()                                                                             { }
        void lrecover_init()                                                                     { }
        bool valid_post_init() const noexcept override;
        void check_post_init() const override;
        void recover_post_init() override;
        bool lvalid_post_init() const noexcept                                      { return true; }
        void lcheck_post_init() const                                                            { }
        void lrecover_post_init()                                                                { }

        static Ptr to_ptr_tp(auto&&, auto conv_f);

        bool equals_base(const Base&) const noexcept override;
        bool equals_this(const This&) const noexcept;
        bool lequals(const This&) const noexcept                                    { return true; }
    private:
        static void to_ptr_throw(const String& msg);
    };

    //+ template <Class B>
    //+ struct Inherit_mixin<B, void> : Parent_mixin<B> {
    //? This must be default, otherwise such specialization doesn't work, don't know why ...
    template <Class B, typename T, bool t_is_class, bool b_is_static, bool b_is_dynamic>
    struct Inherit_mixin : Parent_mixin<B> {
        using Parent_mixin<B>::Parent_mixin;
    };

    template <Class B, typename T>
    //+ struct Inherit_mixin : Object_mixin<Parent_mixin<B>, T> {
    struct Inherit_mixin<B, T, true> : Object_mixin<Parent_mixin<B>, T> {
        using Object_mixin<Parent_mixin<B>, T>::Object_mixin;
    };

    //+ template <Static_c B, typename T>
    //+ struct Inherit_mixin<B, T> : String_mixin<Static_mixin<B, T>> {
    template <Class B, typename T>
    struct Inherit_mixin<B, T, true, true> : String_mixin<Static_mixin<B, T>> {
        using typename String_mixin<Static_mixin<B, T>>::Parent;

        using String_mixin<Static_mixin<B, T>>::String_mixin;

        String to_string() const&;
    };

    /// no need for `String_mixin` here (virtuals are used)
    //+ template <Dynamic_c B, typename T>
    //+ struct Inherit_mixin<B, T> : Dynamic_mixin<B, T> {
    template <Class B, typename T>
    struct Inherit_mixin<B, T, true, false, true> : Dynamic_mixin<B, T> {
        static_assert(!Dynamic_mixin<B, T>::is_base_v);

        using Dynamic_mixin<B, T>::Dynamic_mixin;
    };
}
