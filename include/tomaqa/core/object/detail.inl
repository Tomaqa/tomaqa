#pragma once

namespace tomaqa::detail {
    template <Dynamic_c T>
    T Dynamic_base_base::cons_tp(auto&&... args)
    {
        /// To allow protected constructors too
        typename T::Dynamic_mixin::Access t(FORWARD(args)...);
        t.virtual_init();
        return static_cast<T&&>(t);
    }

    ////////////////////////////////////////////////////////////////

    template <Class BaseT, indirectly_readable P>
    template <Dynamic_c T>
    typename Dynamic_base<BaseT, P>::Ptr Dynamic_base<BaseT, P>::new_tp(auto&&... args)
    {
        return T::make_ptr(T::template cons_tp<T>(FORWARD(args)...));
    }

    ////////////////////////////////////////////////////////////////

    template <Class B>
    Parent_mixin<B>::Parent_mixin(const Parent& rhs)
        : Parent(rhs)
    { }

    template <Class B>
    Parent_mixin<B>::Parent_mixin(Parent&& rhs) noexcept
        : Parent(move(rhs))
    { }

    template <Class B>
    const typename Parent_mixin<B>::Parent&
    Parent_mixin<B>::cparent() const noexcept
    {
        return static_cast<const Parent&>(*this);
    }

    template <Class B>
    typename Parent_mixin<B>::Parent& Parent_mixin<B>::parent()& noexcept
    {
        return static_cast<Parent&>(*this);
    }

    ////////////////////////////////////////////////////////////////

    template <Class B, Class T>
    const auto& Object_mixin<B, T>::cthis() const noexcept
    {
        return static_cast<const This&>(*this);
    }

    template <Class B, Class T>
    auto& Object_mixin<B, T>::rthis() noexcept
    {
        return static_cast<This&>(*this);
    }

    template <Class B, Class T>
    auto&& Object_mixin<B, T>::mthis() noexcept
    {
        return static_cast<This&&>(move(*this));
    }

    template <Class B, Class T>
    auto Object_mixin<B, T>::cthis_l() const noexcept
    {
        return static_cast<const This*>(this);
    }

    template <Class B, Class T>
    auto Object_mixin<B, T>::this_l() noexcept
    {
        return static_cast<This*>(this);
    }

    template <Class B, Class T>
    template <Has_base>
    const auto& Object_mixin<B, T>::cbase() const noexcept
    {
        return static_cast<const typename This::Base&>(*this);
    }

    template <Class B, Class T>
    template <Has_base>
    auto& Object_mixin<B, T>::base() noexcept
    {
        return static_cast<typename This::Base&>(*this);
    }

    template <Class B, Class T>
    template <Has_base>
    auto&& Object_mixin<B, T>::mbase() noexcept
    {
        return static_cast<typename This::Base&&>(move(*this));
    }

    template <Class B, Class T>
    template <Has_base>
    auto Object_mixin<B, T>::cbase_l() const noexcept
    {
        return static_cast<const typename This::Base*>(this);
    }

    template <Class B, Class T>
    template <Has_base>
    auto Object_mixin<B, T>::base_l() noexcept
    {
        return static_cast<typename This::Base*>(this);
    }

    ////////////////////////////////////////////////////////////////

    template <Class B, Class T>
    String Static_mixin<B, T>::to_string() const&
    {
        return This::cthis().to_string();
    }

    ////////////////////////////////////////////////////////////////

    template <Class B, Class T>
    void Dynamic_mixin<B, T>::virtual_init()
    try {
        Parent::virtual_init();
    }
    catch (Error& err) {
        // if `This` has deleted both copy and move constructor, it fails here
        // I did not find out how to check this explicitly with static_assert
        // (it fails even for classes that are OK, even using tp. function like `static_test`)
        if constexpr (Abstract<This>) throw;
        else throw Item_error<This>(this->mthis(), move(err));
    }

    template <Class B, Class T>
    void Dynamic_mixin<B, T>::recover_virtual_init()
    {
        Parent::recover_virtual_init();
        call(this->rthis(), &Access::lrecover_virtual_init);
    }

    template <Class B, Class T>
    bool Dynamic_mixin<B, T>::valid_pre_init() const noexcept
    {
        return Parent::valid_pre_init()
            && call(this->cthis(), &Access::lvalid_pre_init);
    }

    template <Class B, Class T>
    void Dynamic_mixin<B, T>::check_pre_init() const
    {
        Parent::check_pre_init();
        call(this->cthis(), &Access::lcheck_pre_init);
    }

    template <Class B, Class T>
    void Dynamic_mixin<B, T>::recover_pre_init()
    {
        Parent::recover_pre_init();
        call(this->rthis(), &Access::lrecover_pre_init);
    }

    template <Class B, Class T>
    void Dynamic_mixin<B, T>::init()
    {
        call(this->rthis(), &Access::learly_init);
        Parent::init();
        call(this->rthis(), &Access::linit);
    }

    template <Class B, Class T>
    void Dynamic_mixin<B, T>::recover_init()
    {
        Parent::recover_init();
        call(this->rthis(), &Access::lrecover_init);
    }

    template <Class B, Class T>
    bool Dynamic_mixin<B, T>::valid_post_init() const noexcept
    {
        return Parent::valid_post_init()
            && call(this->cthis(), &Access::lvalid_post_init);
    }

    template <Class B, Class T>
    void Dynamic_mixin<B, T>::check_post_init() const
    {
        Parent::check_post_init();
        call(this->cthis(), &Access::lcheck_post_init);
    }

    template <Class B, Class T>
    void Dynamic_mixin<B, T>::recover_post_init()
    {
        Parent::recover_post_init();
        call(this->rthis(), &Access::lrecover_post_init);
    }

    template <Class B, Class T>
    template <Non_abstract T_>
    typename Dynamic_mixin<B, T>::This Dynamic_mixin<B, T>::cons(auto&&... args)
    {
        return T_::template cons_tp<T_>(FORWARD(args)...);
    }

    template <Class B, Class T>
    template <copy_constructible T_>
    typename Dynamic_mixin<B, T>::This Dynamic_mixin<B, T>::cons(const This& rhs)
    {
        T_ t(rhs);
        t.maybe_virtual_init();
        return t;
    }

    template <Class B, Class T>
    template <move_constructible T_>
    typename Dynamic_mixin<B, T>::This Dynamic_mixin<B, T>::cons(This&& rhs)
    {
        T_ t(move(rhs));
        t.maybe_virtual_init();
        return t;
    }

    template <Class B, Class T>
    template <typename U, constructible_from<initializer_list<U>> T_>
    typename Dynamic_mixin<B, T>::This Dynamic_mixin<B, T>::cons(initializer_list<U> ils)
    {
        return T_::template cons_tp<T_>(move(ils));
    }

    template <Class B, Class T>
    template <Non_abstract T_>
    typename Dynamic_mixin<B, T>::Ptr Dynamic_mixin<B, T>::new_me(auto&&... args)
    {
        return T_::template new_tp<T_>(FORWARD(args)...);
    }

    template <Class B, Class T>
    template <copy_constructible T_>
    typename Dynamic_mixin<B, T>::Ptr Dynamic_mixin<B, T>::new_me(const This& rhs)
    {
        return T_::make_ptr(T_::cons(rhs));
    }

    template <Class B, Class T>
    template <move_constructible T_>
    typename Dynamic_mixin<B, T>::Ptr Dynamic_mixin<B, T>::new_me(This&& rhs)
    {
        return T_::make_ptr(T_::cons(move(rhs)));
    }

    template <Class B, Class T>
    template <typename U, constructible_from<initializer_list<U>> T_>
    typename Dynamic_mixin<B, T>::Ptr Dynamic_mixin<B, T>::new_me(initializer_list<U> ils)
    {
        return T_::template new_tp<T_>(move(ils));
    }

    template <Class B, Class T>
    bool Dynamic_mixin<B, T>::is_me(const Ptr& ptr) noexcept
    {
        assert(ptr);
        return This::dcast_l(ptr);
    }

    template <Class B, Class T>
    void Dynamic_mixin<B, T>::check_is_me(const Ptr& ptr)
    {
        expect(This::is_me(ptr), is_not_me_msg(ptr));
    }

    template <Class B, Class T>
    void Dynamic_mixin<B, T>::check_is_not_me(const Ptr& ptr)
    {
        expect(!This::is_me(ptr), is_me_msg(ptr));
    }

    template <Class B, Class T>
    String Dynamic_mixin<B, T>::is_me_msg(const Ptr& ptr)
    {
        using tomaqa::to_string;
        return "Unexpected type "s + typeid(This).name() + ": " + to_string(*ptr);
    }

    template <Class B, Class T>
    String Dynamic_mixin<B, T>::is_not_me_msg(const Ptr& ptr)
    {
        using tomaqa::to_string;
        return "Expected type:\n"s + typeid(This).name()
             + "\nGot:\n" + typeid(*ptr).name()
             + "\nObject: " + to_string(*ptr);
    }

    template <Class B, Class T>
    template <typename U, Dynamic_c T_>
    requires (convertible_to<U, T_> || indirectly_writable<U, T_>)
    Forward_as_ref<typename Dynamic_mixin<B, T>::This, U>
    Dynamic_mixin<B, T>::cast(U&& t) noexcept
    {
        if constexpr (!is_ptr_v<U>) return Forward_as_ref<T_, U>(t);
        else {
            auto&& ptr = FORWARD(t);
            if constexpr (_debug_) {
                if (!T_::is_me(ptr)) CDBGLN(T_::is_not_me_msg(ptr));
            }
            assert(T_::is_me(ptr));
            return Forward_as_ref<T_, U>(*ptr);
        }
    }

    template <Class B, Class T>
    template <indirectly_readable P_, Dynamic_c T_> requires (T_::template is_ptr_v<P_>)
    Forward_as_ref<typename Dynamic_mixin<B, T>::This, P_>
    Dynamic_mixin<B, T>::cast_check(P_&& ptr)
    {
        T_::check_is_me(ptr);
        return T_::cast(FORWARD(ptr));
    }

    template <Class B, Class T>
    template <indirectly_readable P_, Dynamic_c T_> requires (T_::template is_ptr_v<P_>)
    Forward_as_ptr<typename Dynamic_mixin<B, T>::This, P_>
    Dynamic_mixin<B, T>::dcast_l(P_&& ptr) noexcept
    {
        return dynamic_cast<Forward_as_ptr<T_, P_>>(get(FORWARD(ptr)));
    }

    template <Class B, Class T>
    template <typename U, Dynamic_c T_>
    requires (convertible_to<U, T_> || indirectly_writable<U, T_>)
    Forward_as_ref<typename Dynamic_mixin<B, T>::This, U>
    Dynamic_mixin<B, T>::dcast(U&& t)
    {
        if constexpr (!is_ptr_v<U>) return dynamic_cast<Forward_as_ref<T_, U>>(t);
        else return dynamic_cast<Forward_as_ref<T_, U>>(*t);
    }

    template <Class B, Class T>
    typename Dynamic_mixin<B, T>::Ptr Dynamic_mixin<B, T>::to_ptr() const&
    {
        return to_ptr_tp(this->cthis(), [](auto& t) -> auto& { return t; });
    }

    template <Class B, Class T>
    typename Dynamic_mixin<B, T>::Ptr Dynamic_mixin<B, T>::to_ptr()&&
    {
        return to_ptr_tp(this->mthis(), [](auto&& t) -> auto&& { return move(t); });
    }

    template <Class B, Class T>
    typename Dynamic_mixin<B, T>::Ptr Dynamic_mixin<B, T>::to_ptr_tp(auto&& t, auto conv_f)
    {
        constexpr bool copy = is_cref_v<decltype(t)>;
        constexpr bool constructible = copy ? copy_constructible<This> : move_constructible<This>;
        if constexpr (requires { This::make_ptr(conv_f(FORWARD(t))); } && constructible)
            return This::make_ptr(conv_f(FORWARD(t)));
        else if constexpr (!constructible)
            to_ptr_throw("not "s + (copy ? "copyable" : "movable"));
        else to_ptr_throw("make_ptr not implemented");
        UNREACHABLE;
    }

    template <Class B, Class T>
    void Dynamic_mixin<B, T>::to_ptr_throw(const String& msg)
    {
        THROW(typeid(This).name()) + "::to_ptr: " + msg + "!";
    }

    template <Class B, Class T>
    typename Dynamic_mixin<B, T>::Ptr Dynamic_mixin<B, T>::shallow_copy_to_ptr() const
    {
        if constexpr (requires { this->cthis().shallow_copy(); })
            return to_ptr_tp(this->cthis(), [](auto& t){ return t.shallow_copy(); });
        else to_ptr_throw("shallow_copy not implemented");
        UNREACHABLE;
    }

    template <Class B, Class T>
    typename Dynamic_mixin<B, T>::Ptr Dynamic_mixin<B, T>::deep_copy_to_ptr() const
    {
        using tomaqa::deep_copy;
        //+ Maybe it would be better if it threw "not copyable", like in `to_ptr_tp` ..
        if constexpr (requires { deep_copy(this->cthis()); })
            return to_ptr_tp(this->cthis(), [](auto& t){ return deep_copy(t); });
        else to_ptr_throw("deep_copy not implemented");
        UNREACHABLE;
    }

    template <Class B, Class T>
    bool Dynamic_mixin<B, T>::equals(const This& rhs) const noexcept
    {
        if constexpr (is_base_v) return equals_base(rhs);
        else return equals_this(rhs);
    }

    template <Class B, Class T>
    bool Dynamic_mixin<B, T>::equals_base(const Base& rhs) const noexcept
    {
        if constexpr (is_base_v) return equals_this(rhs);
        else return dynamic_equals(rhs);
    }

    template <Class B, Class T>
    bool Dynamic_mixin<B, T>::equals_this(const This& rhs) const noexcept
    {
        return Parent::equals_this(rhs)
            && call(this->cthis(), &Access::lequals, rhs);
    }

    template <Class B, Class T>
    template <Class U, Class T_>
    requires (derived_from<U, T_> || derived_from<T_, U>)
    bool Dynamic_mixin<B, T>::dynamic_equals(const U& rhs) const noexcept
    {
        static_assert(is_same_v<T_, This>);
        /// `equals` is not virtual, need to call it precisely on `This` !
        if constexpr (is_same_v<This, U>) return this->cthis().equals(rhs);
        if constexpr (is_base_of_v<This, U>) return rhs.dynamic_equals(this->cthis());
        if (auto derived_l = dynamic_cast<const This*>(&rhs))
            return this->cthis().equals(*derived_l);

        return this->equals_base_anyway(rhs) || rhs.equals_base_anyway(this->cthis());
    }

    ////////////////////////////////////////////////////////////////

    //+ template <Static_c B, typename T>
    //+ String Inherit_mixin<B, T>::to_string() const&
    template <Class B, typename T>
    String Inherit_mixin<B, T, true, true>::to_string() const&
    {
        if constexpr (Has_to_string<Parent>)
            return Parent::to_string();
        else static_assert(false_tp<Parent>, "Attempt to call not implemented `to_string`");
    }
}
