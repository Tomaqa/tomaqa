#pragma once

#include "tomaqa/core/meta.hpp"

#include <string>
#include <cstring>

#define _CSTREAM(cstream, x) cstream << x
#define _CSTREAMLN(cstream, x) _CSTREAM(cstream, x) << std::endl

#ifndef QUIET
#define CSTREAM(cstream, x) _CSTREAM(cstream, x)
#define CSTREAMLN(cstream, x) _CSTREAMLN(cstream, x)
#else
#define CSTREAM(cstream, x) UNUSED(_CSTREAM(ignore, x))
#define CSTREAMLN(cstream, x) CSTREAM(cstream, x)
#endif

#ifdef DEBUG
#include <iostream>
#include <iomanip>

using std::cout;
using std::cerr;
using std::clog;
using std::endl;

#define CDBG(x) CSTREAM(cerr, x)
#define CDBGLN(x) CSTREAMLN(cerr, x)
#else
#define CDBG(x) EMPTY_CMD
#define CDBGLN(x) EMPTY_CMD
#endif

#ifndef VERBOSE
#include <iosfwd>
#else
#include <iostream>

#define _CVERB_BODY(x, l, r) l << " " << x << " " << r
#define _CVERB(x, l, r) CSTREAM(std::cerr, _CVERB_BODY(x, l, r))
#define _CVERBLN(x, l, r) CSTREAMLN(std::cerr, _CVERB_BODY(x, l, r))

constexpr int _verbose_ = VERBOSE;
#endif

#if VERBOSE >= 1
#define CVERB(x) _CVERB(x, "<<<<", ">>>>")
#define CVERBLN(x) _CVERBLN(x, "<<<<", ">>>>")
#else
#define CVERB(x) EMPTY_CMD
#define CVERBLN(x) EMPTY_CMD
#endif

#if VERBOSE >= 2
#define CVERB2(x) _CVERB(x, " <<<", ">>> ")
#define CVERB2LN(x) _CVERBLN(x, " <<<", ">>> ")
#else
#define CVERB2(x)
#define CVERB2LN(x)
#endif

#if VERBOSE >= 3
#define CVERB3(x) _CVERB(x, "  <<", ">>  ")
#define CVERB3LN(x) _CVERBLN(x, "  <<", ">>  ")
#else
#define CVERB3(x)
#define CVERB3LN(x)
#endif

namespace tomaqa {
    using String = std::string;

    using std::ostream;
    using std::istream;
    using std::ofstream;
    using std::ifstream;
    using std::stringstream;
    using std::istringstream;
    using std::ostringstream;
    using std::streampos;
    using std::streamsize;

    using namespace std::string_literals;

    template <typename T>
    concept String_c = same_as<decay_t<T>, char*>
                    || same_as<decay_t<T>, const char*>
                    || same_as<decay_t<T>, String>;

    template <typename T> concept Printable = requires (ostream& os, T& t) { os << t; };

    using std::to_string;

    String to_string(char);
    inline String to_string(const char* str)                                         { return str; }
    inline const String& to_string(const String& str)                                { return str; }
    String to_string(input_iterator auto it)                              { return to_string(*it); }
    String to_string(istream&);
    String to_string(istream&, streampos end_pos);
    String to_string(istream&, streamsize);
    template <typename T1, typename T2> String to_string(const pair<T1, T2>&);
    template <typename... Args> String to_string(const tuple<Args...>&);

    String to_string(floating_point auto, int precision);

    extern template String to_string(float, int precision);
    extern template String to_string(double, int precision);

    String to_string(const Ignore&);

    String to_string(partial_ordering);

    template <typename T> concept Has_to_string = requires (T& t) { t.to_string(); };
    template <typename T>
    concept Has_to_string_with_precision = requires (T& t, int p) { t.to_string(p); };

    String to_string(const Has_to_string auto& rhs)                      { return rhs.to_string(); }
    String to_string(const Has_to_string_with_precision auto& rhs, int precision)
                                                                { return rhs.to_string(precision); }

    /*-
    // Using `aux` in req. to avoid requiring itself; the order of req. does matter (recursion)
    template <typename T> requires (!requires (T& t) { aux::to_string(t); } && Printable<T>)
        String to_string(const T&);
    */

    template <typename T> concept Enabled_to_string = requires (T t) { to_string(t); };

    //+ it does not work for `to_string`s that are declared later
    template <Enabled_to_string T> requires (!String_c<T>)
        ostream& operator <<(ostream& os, const T& rhs)           { return (os << to_string(rhs)); }

    /// `Ignore` absorbs any arguments
    constexpr auto& operator <<(const Ignore& i, auto&&) noexcept                      { return i; }
    constexpr auto& operator <<(const Ignore& i, ostream&(*)(ostream&)) noexcept       { return i; }
    constexpr auto& operator <<(const Ignore& i, std::ios&(*)(std::ios&)) noexcept     { return i; }
    constexpr auto& operator <<(const Ignore& i, std::ios_base&(*)(std::ios_base&)) noexcept
                                                                                       { return i; }

    template <Arithmetic Arg> bool is_value(const String&) noexcept;
    bool get_value(Arithmetic auto&, String_c auto&&) noexcept;
    bool get_value(Arithmetic auto&, istream&) noexcept;

    int istream_remain_size(istream&);
}

namespace tomaqa {
    static_assert(!Container<char*>);
    static_assert(Container<String>);
}
