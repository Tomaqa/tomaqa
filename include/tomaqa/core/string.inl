#pragma once

#include <sstream>

namespace tomaqa {
    template <typename T1, typename T2>
    String to_string(const pair<T1, T2>& rhs)
    {
        return to_string(rhs.first) + " " + to_string(rhs.second);
    }

    template <typename... Args>
    String to_string(const tuple<Args...>& rhs)
    {
        String str;
        // using fold expression
        apply([&str](auto&&... args){ ((str += to_string(FORWARD(args)) + " "), ...); }, rhs);
        return str;
    }

    /*-
    template <typename T> requires (!requires (T t) { aux::to_string(t); } && Printable<T>)
    String to_string(const T& rhs)
    {
        ostringstream oss;
        oss << rhs;
        return oss.str();
    }
    */

    template <Arithmetic Arg>
    bool is_value(const String& str) noexcept
    {
        Arg val;
        return get_value(val, str);
    }

    bool get_value(Arithmetic auto& val, String_c auto&& str) noexcept
    {
        istringstream iss(FORWARD(str));
        return get_value(val, iss);
    }

    bool get_value(Arithmetic auto& val, istream& is) noexcept
    {
        is >> val;
        return is && is.eof();
    }
}
