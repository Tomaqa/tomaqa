#pragma once

#include "tomaqa/core/object.hpp"

#include <vector>

namespace tomaqa {
    template <typename> class Vector;
}

namespace tomaqa {
    template <typename T>
    class Vector : public Inherit<std::vector<T>, Vector<T>> {
    public:
        using Inherit = tomaqa::Inherit<std::vector<T>, Vector<T>>;
        using typename Inherit::This;
        using Base = typename Inherit::Parent;

        using typename Inherit::value_type;
        using typename Inherit::size_type;
        using typename Inherit::reference;
        using typename Inherit::const_reference;
        using typename Inherit::iterator;
        using typename Inherit::const_iterator;

        using Inherit::Inherit;

        using Inherit::size;

        const_reference operator [](size_type) const;
        reference operator [](size_type);

        /// Like `resize`, but never reduces size
        using Inherit::resize;
        void expand(size_type);
        void expand(size_type, const value_type&);

        bool equals(const This&) const;

        String to_string() const;
    };
}
