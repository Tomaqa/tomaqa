#pragma once

namespace tomaqa {
    template <typename T>
    typename Vector<T>::const_reference Vector<T>::operator [](size_type idx) const
    {
        assert(idx < size());
        return Inherit::operator [](idx);
    }

    template <typename T>
    typename Vector<T>::reference Vector<T>::operator [](size_type idx)
    {
        assert(idx < size());
        return Inherit::operator [](idx);
    }

    template <typename T>
    void Vector<T>::expand(size_type size_)
    {
        if (size_ < size()) return;
        resize(size_);
    }

    template <typename T>
    void Vector<T>::expand(size_type size_, const value_type& val)
    {
        if (size_ < size()) return;
        resize(size_, val);
    }

    template <typename T>
    bool Vector<T>::equals(const This& rhs) const
    {
        return std::operator ==(*this, rhs);
    }

    template <typename T>
    String Vector<T>::to_string() const
    {
        using tomaqa::to_string;
        String str;
        for (const auto& e : *this) {
            str += to_string(e) + " ";
        }
        return str;
    }
}
