#pragma once

#include "tomaqa/core.hpp"

#include "tomaqa/util.hpp"
#include "tomaqa/util/optional.hpp"
#include "tomaqa/util/numeric.hpp"
#include "tomaqa/util/array.hpp"

#include <cmath>
#include <limits>
#include <numbers>

#include <boost/rational.hpp>

namespace tomaqa::math {
    using namespace util;

    using Float = double_t;

    template <typename T>
    concept Coord_c = requires (T t) { t.x; t.y; };

    /// 2D
    //+ make these templated, do not stick only to `Float`
    struct Coord;
    struct Vec;

    class Interval;

    template <typename T>
    concept Polynom_c = requires { {T::deg_v} -> integral; };

    template <size_t degV, typename CoefT = Float> class Polynom;

    template <typename T> concept Rational_c = requires (T t) {
        typename T::int_type;
        requires integral<typename T::int_type>;
        {t.numerator()} -> convertible_to<typename T::int_type>;
        {t.denominator()} -> convertible_to<typename T::int_type>;
    };

    template <integral I> using Rational_tp = boost::rational<I>;
    template <floating_point FloatT>
    using Rational_for = Rational_tp<Int_tp<sizeof(FloatT)>>;
    using Rational = Rational_for<Float>;

    static_assert(is_same_v<Rational_for<Float>, Rational>);

    template <Rational_c RationalT>
    using Float_for = Float_tp<sizeof(typename RationalT::int_type)>;

    static_assert(is_same_v<Float_for<Rational>, Float>);

    template <typename T> using limits = std::numeric_limits<T>;

    constexpr Float e = std::numbers::e_v<Float>;
    constexpr Float log2e = std::numbers::log2e_v<Float>;
    constexpr Float log10e = std::numbers::log10e_v<Float>;
    constexpr Float pi = std::numbers::pi_v<Float>;
    constexpr Float inv_pi = std::numbers::inv_pi_v<Float>;
    constexpr Float ln2 = std::numbers::ln2_v<Float>;
    constexpr Float ln10 = std::numbers::ln10_v<Float>;
    constexpr Float sqrt2 = std::numbers::sqrt2_v<Float>;
    constexpr Float sqrt3 = std::numbers::sqrt3_v<Float>;

    constexpr Float inf = limits<Float>::infinity();
    constexpr Float eps = limits<Float>::epsilon();
    constexpr Float nan = limits<Float>::quiet_NaN();
    constexpr Float snan = limits<Float>::signaling_NaN();

    using util::abs;
    using std::fmod;
    using std::fma;

    using std::exp;
    using std::exp2;
    using std::log;
    using std::log2;
    using std::log10;

    using std::pow;
    using std::sqrt;
    using std::cbrt;
    using std::hypot;

    using std::sin;
    using std::cos;
    using std::tan;
    using std::asin;
    using std::acos;
    using std::atan;
    using std::atan2;

    using util::ceil;
    using util::floor;
    using util::trunc;
    using util::round;
    using util::lround;
    using util::llround;

    using std::frexp;
    using std::ldexp;
    using std::modf;
    using std::ilogb;
    using std::logb;
    using std::nextafter;
    using std::copysign;

    template <Rational_c RationalT, floating_point FloatT = Float_for<RationalT>>
    constexpr FloatT to_float(const RationalT&);

    template <floating_point FloatT, Rational_c RationalT = Rational_for<FloatT>>
    RationalT to_rational(FloatT);

    extern template Rational_tp<int64_t> to_rational(double);
    extern template Rational_tp<int32_t> to_rational(double);
    extern template Rational_tp<int64_t> to_rational(float);
    extern template Rational_tp<int32_t> to_rational(float);

    template <Side = Side::center, Precision_c = precision::Medium,
              floating_point FloatT, Rational_c RationalT = Rational_for<FloatT>>
    RationalT to_apx_rational(FloatT);

    extern template Rational_tp<int64_t> to_apx_rational<Side::left, precision::Low>(double);
    extern template Rational_tp<int32_t> to_apx_rational<Side::left, precision::Low>(double);
    extern template Rational_tp<int64_t> to_apx_rational<Side::left, precision::Low>(float);
    extern template Rational_tp<int32_t> to_apx_rational<Side::left, precision::Low>(float);
    extern template Rational_tp<int64_t> to_apx_rational<Side::left, precision::Medium>(double);
    extern template Rational_tp<int32_t> to_apx_rational<Side::left, precision::Medium>(double);
    extern template Rational_tp<int64_t> to_apx_rational<Side::left, precision::Medium>(float);
    extern template Rational_tp<int32_t> to_apx_rational<Side::left, precision::Medium>(float);
    extern template Rational_tp<int64_t> to_apx_rational<Side::left, precision::High>(double);
    extern template Rational_tp<int32_t> to_apx_rational<Side::left, precision::High>(double);
    extern template Rational_tp<int64_t> to_apx_rational<Side::left, precision::High>(float);
    extern template Rational_tp<int32_t> to_apx_rational<Side::left, precision::High>(float);
    extern template Rational_tp<int64_t> to_apx_rational<Side::right, precision::Low>(double);
    extern template Rational_tp<int32_t> to_apx_rational<Side::right, precision::Low>(double);
    extern template Rational_tp<int64_t> to_apx_rational<Side::right, precision::Low>(float);
    extern template Rational_tp<int32_t> to_apx_rational<Side::right, precision::Low>(float);
    extern template Rational_tp<int64_t> to_apx_rational<Side::right, precision::Medium>(double);
    extern template Rational_tp<int32_t> to_apx_rational<Side::right, precision::Medium>(double);
    extern template Rational_tp<int64_t> to_apx_rational<Side::right, precision::Medium>(float);
    extern template Rational_tp<int32_t> to_apx_rational<Side::right, precision::Medium>(float);
    extern template Rational_tp<int64_t> to_apx_rational<Side::right, precision::High>(double);
    extern template Rational_tp<int32_t> to_apx_rational<Side::right, precision::High>(double);
    extern template Rational_tp<int64_t> to_apx_rational<Side::right, precision::High>(float);
    extern template Rational_tp<int32_t> to_apx_rational<Side::right, precision::High>(float);
    extern template Rational_tp<int64_t> to_apx_rational<Side::center, precision::Low>(double);
    extern template Rational_tp<int32_t> to_apx_rational<Side::center, precision::Low>(double);
    extern template Rational_tp<int64_t> to_apx_rational<Side::center, precision::Low>(float);
    extern template Rational_tp<int32_t> to_apx_rational<Side::center, precision::Low>(float);
    extern template Rational_tp<int64_t> to_apx_rational<Side::center, precision::Medium>(double);
    extern template Rational_tp<int32_t> to_apx_rational<Side::center, precision::Medium>(double);
    extern template Rational_tp<int64_t> to_apx_rational<Side::center, precision::Medium>(float);
    extern template Rational_tp<int32_t> to_apx_rational<Side::center, precision::Medium>(float);
    extern template Rational_tp<int64_t> to_apx_rational<Side::center, precision::High>(double);
    extern template Rational_tp<int32_t> to_apx_rational<Side::center, precision::High>(double);
    extern template Rational_tp<int64_t> to_apx_rational<Side::center, precision::High>(float);
    extern template Rational_tp<int32_t> to_apx_rational<Side::center, precision::High>(float);

    template <Addable T> Vector<T>& operator +=(Vector<T>&, const Vector<T>&);
    template <Addable T> Vector<T> operator +(Vector<T>, const Vector<T>&);
    template <Multipliable T> Vector<T>& operator *=(Vector<T>&, const T&);
    template <Multipliable T> Vector<T> operator *(Vector<T>, const T&);
    template <Multipliable T> Vector<T> operator *(const T&, Vector<T>);

    template <Coord_c C> constexpr C operator -(C) noexcept;

    template <Coord_c C> constexpr C operator +(C, Float) noexcept;
    template <Coord_c C> constexpr C operator -(C, Float) noexcept;
    template <Coord_c C> constexpr C operator *(C, Float) noexcept;
    template <Coord_c C> constexpr C operator /(C, Float) noexcept;
    template <Coord_c C> constexpr C operator *(Float, C) noexcept;

    template <Coord_c C> constexpr C operator +(C, Coord_c auto) noexcept;
    template <Coord_c C> constexpr C operator -(C, Coord_c auto) noexcept;

    constexpr Vec transposed(Vec) noexcept;
    inline Vec normal(Vec) noexcept;

    /// Scalar product
    Float operator *(Vec, Vec) noexcept;

    Float angle(Vec);
    Float dist(Vec);
    Vec rotate(Vec, Float);

    inline Interval operator -(Interval);

    inline Interval operator +(Float, Interval);
    inline Interval operator +(Interval, Float);
    inline Interval operator -(Float, Interval);
    inline Interval operator -(Interval, Float);
    inline Interval operator *(Float, Interval) noexcept;
    inline Interval operator *(Interval, Float) noexcept;
    inline Interval operator /(Interval, Float) noexcept;

    inline Interval operator +(Interval, Interval);
    inline Interval operator -(Interval, Interval);
    inline Interval operator *(Interval, Interval);
    inline Interval operator /(Interval, Interval);
    inline Interval operator &(Interval, Interval) noexcept;
    inline Interval operator |(Interval, Interval) noexcept;

    consteval size_t deg(const Polynom_c auto&) noexcept;

    template <size_t degV, typename CoefT>
    constexpr Polynom<degV-1, CoefT> deriv(const Polynom<degV, CoefT>&) noexcept;
}

namespace tomaqa {
    String to_string(const math::Rational_c auto&);

    extern template String to_string(const math::Rational_for<double>&);
    extern template String to_string(const math::Rational_for<float>&);
}

namespace tomaqa::math {
    struct Coord {
        Float x, y;

        constexpr Coord& transpose() noexcept;

        constexpr Coord& operator +=(Float) noexcept;
        constexpr Coord& operator -=(Float) noexcept;
        constexpr Coord& operator *=(Float) noexcept;
        constexpr Coord& operator /=(Float) noexcept;

        constexpr Coord& operator +=(Coord) noexcept;
        constexpr Coord& operator -=(Coord) noexcept;

        constexpr bool equals(const Coord&) const noexcept;
        template <Precision_c = precision::Medium>
        bool apx_equals(const Coord&) const noexcept;

        String to_string() const;
        String to_string(int precision) const;
    };

    static_assert(Aggregate<Coord>);

    struct Vec : Coord {
        Vec()                                                                             = default;
        constexpr Vec(Float x_, Float y_) noexcept                               : Coord{x_, y_} { }
        explicit constexpr Vec(Coord base)                                         : Coord(base) { }
        constexpr Vec(Coord from, Coord to);

        constexpr Vec& transpose() noexcept;

        constexpr Vec& operator +=(Float) noexcept;
        constexpr Vec& operator -=(Float) noexcept;
        constexpr Vec& operator *=(Float) noexcept;
        constexpr Vec& operator /=(Float) noexcept;

        constexpr Vec& operator +=(Coord) noexcept;
        constexpr Vec& operator -=(Coord) noexcept;

        String to_string() const;
        String to_string(int precision) const;
    };

    static_assert(!Aggregate<Vec>);

    constexpr Vec zero_vec{0., 0.};

    class Interval : protected Inherit<Optional<pair<Float, Float>>, Interval> {
    public:
        using Range = Inherit::Value;

        using Tag = tomaqa::Tag<Interval>;
        struct Not_empty_tag : Tag {};
        struct Not_point_tag : Tag {};

        static inline Not_empty_tag not_empty_tag;
        static inline Not_point_tag not_point_tag;

        Interval()                                                                        = default;
        Interval(const Interval&)                                                         = default;
        Interval& operator =(const Interval&)                                             = default;
        Interval(Interval&&)                                                              = default;
        Interval& operator =(Interval&&)                                                  = default;
        Interval(Float);
        /// The bounds must form a correct interval
        Interval(Float, Float);
        /// Attempts to construct valid interval, possibly swapping the bounds
        Interval(Float, Float, Not_empty_tag);
        Interval(Float, Float, Not_point_tag);

        using Inherit::valid;
        using Inherit::invalid;
        /// Does *not* mean that the size is zero
        constexpr bool empty() const noexcept                                  { return invalid(); }

        constexpr const auto& crange() const noexcept                           { return cvalue(); }
        constexpr Float clower() const noexcept                           { return crange().first; }
        constexpr Float cupper() const noexcept                          { return crange().second; }

        void set_range(Float, Float) noexcept;
        void set_lower(Float) noexcept;
        void set_upper(Float) noexcept;
        void set_range_check(Float, Float);
        void set_lower_check(Float);
        void set_upper_check(Float);

        void inc_lower(Float) noexcept;
        void inc_upper(Float) noexcept;
        void mul_lower(Float) noexcept;
        void mul_upper(Float) noexcept;
        void inc_lower_check(Float);
        void inc_upper_check(Float);
        void mul_lower_check(Float);
        void mul_upper_check(Float);

        using Inherit::operator bool;
        using Inherit::operator !;

        inline bool infinite() const noexcept;
        inline bool is_point() const noexcept;

        template <Precision_c P = precision::Medium>
        bool apx_valid() const noexcept                                  { return !apx_empty<P>(); }
        template <Precision_c = precision::Medium>
        bool apx_empty() const noexcept;
        template <Precision_c = precision::Medium>
        bool is_apx_point() const noexcept;

        /// Assumes that `is_point` holds, otherwise it is UB
        inline Float point() const noexcept;
        Float middle() const;
        explicit operator Float() const                                         { return middle(); }

        Float size() const noexcept;
        Float radius() const noexcept;

        using Inherit::invalidate;

        Interval& neg();

        Interval& operator +=(Float);
        Interval& operator -=(Float);
        Interval& operator *=(Float) noexcept;
        Interval& operator /=(Float) noexcept;

        Interval& operator +=(Interval);
        Interval& operator -=(Interval);
        Interval& operator *=(Interval);
        Interval& operator /=(Interval);
        Interval& operator &=(Interval) noexcept;
        Interval& operator |=(Interval) noexcept;

        bool contains(Float) const noexcept;
        bool contains(Interval) const noexcept;

        bool equals(Float) const noexcept;
        partial_ordering compare(Float) const;
        inline bool equals(const Interval&) const noexcept;
        inline partial_ordering compare(const Interval&) const;
        template <Precision_c = precision::Medium>
        bool apx_equals(Float) const noexcept;
        template <Precision_c = precision::Medium>
        partial_ordering apx_compare(Float) const;
        template <Precision_c = precision::Medium>
        bool apx_equals(const Interval&) const noexcept;
        template <Precision_c = precision::Medium>
        partial_ordering apx_compare(const Interval&) const;

        String to_string() const;
        String to_string(int precision) const;
    protected:
        constexpr auto& range() noexcept                                         { return value(); }
        constexpr Float& lower() noexcept                                  { return range().first; }
        constexpr Float& upper() noexcept                                 { return range().second; }

        bool valid_range() const noexcept;
        void check_range() const;

        void maybe_invalidate() noexcept;
    private:
        bool valid_range_impl() const noexcept;
    };
    extern template bool Interval::apx_empty<precision::Low>() const noexcept;
    extern template bool Interval::apx_empty<precision::Medium>() const noexcept;
    extern template bool Interval::apx_empty<precision::High>() const noexcept;
    extern template bool Interval::apx_empty<precision::Huge>() const noexcept;
    extern template bool Interval::apx_empty<precision::Max>() const noexcept;
    extern template bool Interval::is_apx_point<precision::Low>() const noexcept;
    extern template bool Interval::is_apx_point<precision::Medium>() const noexcept;
    extern template bool Interval::is_apx_point<precision::High>() const noexcept;
    extern template bool Interval::is_apx_point<precision::Huge>() const noexcept;
    extern template bool Interval::is_apx_point<precision::Max>() const noexcept;
    extern template partial_ordering Interval::apx_compare<precision::Low>(const Interval&) const;
    extern template partial_ordering Interval::apx_compare<precision::Medium>(const Interval&) const;
    extern template partial_ordering Interval::apx_compare<precision::High>(const Interval&) const;
    extern template partial_ordering Interval::apx_compare<precision::Huge>(const Interval&) const;
    extern template partial_ordering Interval::apx_compare<precision::Max>(const Interval&) const;

    inline const Interval inf_interval{-inf, inf};
    inline const Interval pos_inf_interval{0., inf};
    inline const Interval neg_inf_interval{-inf, 0.};

    template <size_t degV, typename CoefT>
    class Polynom : public Container_types<CoefT> {
    public:
        static constexpr size_t deg_v = degV;
        static constexpr size_t size_v = deg_v+1;

        using Coef = CoefT;

        static_assert(is_same_v<typename Polynom::value_type, Coef>);

        using Coefs = array<Coef, size_v>;

        using const_iterator = typename Coefs::const_iterator;
        using iterator = typename Coefs::iterator;

        Polynom()                                                                         = default;
        ~Polynom()                                                                        = default;
        Polynom(const Polynom&)                                                           = default;
        Polynom& operator =(const Polynom&)                                               = default;
        Polynom(Polynom&&)                                                                = default;
        Polynom& operator =(Polynom&&)                                                    = default;
        constexpr Polynom(auto&&...);
        explicit constexpr Polynom(Coef);

        static consteval size_t size() noexcept                                   { return size_v; }
        static consteval size_t deg() noexcept                                     { return deg_v; }

        template <size_t IdxV>
        constexpr Coef get() const noexcept                     { return std::get<IdxV>(ccoefs()); }
        template <size_t IdxV>
        constexpr Coef& get() noexcept                           { return std::get<IdxV>(coefs()); }

        constexpr Coef cconstant() const noexcept                             { return constant(); }
        constexpr Coef constant() const noexcept;
        constexpr Coef& constant() noexcept;

        const_iterator cbegin() const noexcept                         { return ccoefs().cbegin(); }
        const_iterator cend() const noexcept                             { return ccoefs().cend(); }
        const_iterator begin() const noexcept                                   { return cbegin(); }
        const_iterator end() const noexcept                                       { return cend(); }
        iterator begin() noexcept                                        { return coefs().begin(); }
        iterator end() noexcept                                            { return coefs().end(); }

        constexpr Coef operator [](Idx idx) const noexcept                 { return ccoefs()[idx]; }
        constexpr Coef& operator [](Idx idx) noexcept                       { return coefs()[idx]; }

        static constexpr int deg_at(Idx) noexcept;

        static_assert(deg_at(0) == deg_v);
        static_assert(deg_at(size_v-1) == 0);

        constexpr Float operator ()(Float) const;

        String to_string() const;
    protected:
        explicit constexpr Polynom(Explicit_tag, auto&&...);

        constexpr const auto& ccoefs() const noexcept                            { return coefs(); }
        constexpr auto& coefs() const noexcept                                    { return _coefs; }
        constexpr auto& coefs() noexcept                                          { return _coefs; }
    private:
        Coefs _coefs{};
    };
}

namespace std {
    template <size_t degV, typename CoefT>
    struct tuple_size<tomaqa::math::Polynom<degV, CoefT>>
        : public integral_constant<size_t, tomaqa::math::Polynom<degV, CoefT>::size_v> {};

    template<size_t IdxV, size_t degV, typename CoefT>
    struct tuple_element<IdxV, tomaqa::math::Polynom<degV, CoefT>> {
        using type = typename tomaqa::math::Polynom<degV, CoefT>::value_type;
    };
}

#include "tomaqa/math.inl"

namespace tomaqa::math {
    static_assert(is_nothrow_default_constructible_v<Coord>);
    static_assert(is_copy_constructible_v<Coord>);
    static_assert(is_nothrow_move_constructible_v<Coord>);
    static_assert(is_nothrow_default_constructible_v<Vec>);
    static_assert(is_copy_constructible_v<Vec>);
    static_assert(is_nothrow_move_constructible_v<Vec>);
    static_assert(is_nothrow_default_constructible_v<Interval>);
    static_assert(is_copy_constructible_v<Interval>);
    static_assert(is_nothrow_move_constructible_v<Interval>);

    static_assert(constructible_from<Coord, Vec>);
    static_assert(convertible_to<Vec, Coord>);
    static_assert(constructible_from<Vec, Coord>);
    static_assert(!convertible_to<Coord, Vec>);

    static_assert(zero_vec == Vec());

    static_assert(Interval().invalid());
    static_assert(Interval().empty());

    static_assert(Addable<Coord>);
    static_assert(Addable<Vec>);
    static_assert(Addable<Interval>);
    static_assert(Subtractable<Coord>);
    static_assert(Subtractable<Vec>);
    static_assert(Subtractable<Interval>);
    static_assert(!Multipliable<Coord>);
    static_assert(!Multipliable<Vec>);
    static_assert(Multipliable<Interval>);
    static_assert(!Divisible<Coord>);
    static_assert(!Divisible<Vec>);
    static_assert(Divisible<Interval>);

    static_assert(0. == -0.);
    static_assert(inf == inf);
    static_assert(inf != -inf);
    static_assert(inf+1. == inf);
    static_assert(inf*2. == inf);
    static_assert(inf-1. == inf);
    static_assert(inf*-2. == -inf);
    static_assert(inf+inf == inf);
    static_assert(inf*inf == inf);
    static_assert(nan != nan);
    static_assert(nan != -nan);
    // static_assert(nan+1. != nan);
    // static_assert(nan*2. != nan);
}
