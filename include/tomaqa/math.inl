#pragma once

#include "tomaqa/util/alg.hpp"

namespace tomaqa::math {
    template <Rational_c RationalT, floating_point FloatT>
    constexpr FloatT to_float(const RationalT& x)
    {
        return boost::rational_cast<FloatT>(x);
    }

    template <Addable T>
    Vector<T>& operator +=(Vector<T>& lhs, const Vector<T>& rhs)
    {
        for_each(lhs, begin(rhs), [](T& l, const T& r){ l += r; });
        return lhs;
    }

    template <Addable T>
    Vector<T> operator +(Vector<T> lhs, const Vector<T>& rhs)
    {
        return (lhs += rhs);
    }

    template <Multipliable T>
    Vector<T>& operator *=(Vector<T>& lhs, const T& rhs)
    {
        for_each(lhs, [&rhs](T& l){ l *= rhs; });
        return lhs;
    }

    template <Multipliable T>
    Vector<T> operator *(Vector<T> lhs, const T& rhs)
    {
        return (lhs *= rhs);
    }

    template <Multipliable T>
    Vector<T> operator *(const T& lhs, Vector<T> rhs)
    {
        return (rhs *= lhs);
    }

    ////////////////////////////////////////////////////////////////

    template <Coord_c C>
    constexpr C operator -(C c) noexcept
    {
        return {-c.x, -c.y};
    }

    template <Coord_c C>
    constexpr C operator +(C c, Float a) noexcept
    {
        return c += a;
    }

    template <Coord_c C>
    constexpr C operator -(C c, Float a) noexcept
    {
        return c -= a;
    }

    template <Coord_c C>
    constexpr C operator *(C c, Float a) noexcept
    {
        return c *= a;
    }

    template <Coord_c C>
    constexpr C operator /(C c, Float a) noexcept
    {
        return c /= a;
    }

    template <Coord_c C>
    constexpr C operator *(Float a, C c) noexcept
    {
        return c*a;
    }

    template <Coord_c C>
    constexpr C operator +(C lhs, Coord_c auto rhs) noexcept
    {
        return lhs += rhs;
    }

    template <Coord_c C>
    constexpr C operator -(C lhs, Coord_c auto rhs) noexcept
    {
        return lhs -= rhs;
    }

    constexpr Vec transposed(Vec v) noexcept
    {
        return v.transpose();
    }

    Vec normal(Vec v) noexcept
    {
        return {-v.y, v.x};
    }

    ////////////////////////////////////////////////////////////////

    Interval operator -(Interval i)
    {
        return i.neg();
    }

    Interval operator +(Interval i, Float a)
    {
        return i += a;
    }

    Interval operator +(Float a, Interval i)
    {
        return i += a;
    }

    Interval operator -(Interval i, Float a)
    {
        return i -= a;
    }

    Interval operator -(Float a, Interval i)
    {
        return i.neg() += a;
    }

    Interval operator *(Interval i, Float a) noexcept
    {
        return i *= a;
    }

    Interval operator *(Float a, Interval i) noexcept
    {
        return move(i)*a;
    }

    Interval operator /(Interval i, Float a) noexcept
    {
        return i /= a;
    }

    Interval operator +(Interval lhs, Interval rhs)
    {
        return lhs += move(rhs);
    }

    Interval operator -(Interval lhs, Interval rhs)
    {
        return lhs -= move(rhs);
    }

    Interval operator *(Interval lhs, Interval rhs)
    {
        return lhs *= move(rhs);
    }

    Interval operator /(Interval lhs, Interval rhs)
    {
        return lhs /= move(rhs);
    }

    Interval operator &(Interval lhs, Interval rhs) noexcept
    {
        return lhs &= move(rhs);
    }

    Interval operator |(Interval lhs, Interval rhs) noexcept
    {
        return lhs |= move(rhs);
    }

    ////////////////////////////////////////////////////////////////

    consteval size_t deg(const Polynom_c auto& p) noexcept
    {
        return p.deg_v;
    }

    template <size_t degV, typename CoefT>
    constexpr Polynom<degV-1, CoefT> deriv(const Polynom<degV, CoefT>& p) noexcept
    {
        Polynom<degV-1, CoefT> dp;
        static_assert(degV == dp.size());
        for (Idx i = 0; i < degV; ++i) {
            const int deg = p.deg_at(i);
            dp[i] = deg*p[i];
        }
        assert(p.deg_at(degV) == 0);
        return dp;
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace tomaqa::math {
    constexpr Coord& Coord::transpose() noexcept
    {
        swap(x, y);
        return *this;
    }

    constexpr Coord& Coord::operator +=(Float a) noexcept
    {
        x += a;
        y += a;
        return *this;
    }

    constexpr Coord& Coord::operator -=(Float a) noexcept
    {
        x -= a;
        y -= a;
        return *this;
    }

    constexpr Coord& Coord::operator *=(Float a) noexcept
    {
        x *= a;
        y *= a;
        return *this;
    }

    constexpr Coord& Coord::operator /=(Float a) noexcept
    {
        x /= a;
        y /= a;
        return *this;
    }

    constexpr Coord& Coord::operator +=(Coord rhs) noexcept
    {
        x += rhs.x;
        y += rhs.y;
        return *this;
    }

    constexpr Coord& Coord::operator -=(Coord rhs) noexcept
    {
        x -= rhs.x;
        y -= rhs.y;
        return *this;
    }

    constexpr bool Coord::equals(const Coord& rhs) const noexcept
    {
        return x == rhs.x && y == rhs.y;
    }

    template <Precision_c P>
    bool Coord::apx_equals(const Coord& rhs) const noexcept
    {
        return apx_equal<P>(x, rhs.x) && apx_equal<P>(y, rhs.y);
    }

    ////////////////////////////////////////////////////////////////

    constexpr Vec::Vec(Coord from, Coord to)
        : Coord(to)
    {
        operator -=(from);
    }

    constexpr Vec& Vec::transpose() noexcept
    {
        Coord::transpose();
        return *this;
    }

    constexpr Vec& Vec::operator +=(Float a) noexcept
    {
        Coord::operator +=(a);
        return *this;
    }

    constexpr Vec& Vec::operator -=(Float a) noexcept
    {
        Coord::operator -=(a);
        return *this;
    }

    constexpr Vec& Vec::operator *=(Float a) noexcept
    {
        Coord::operator *=(a);
        return *this;
    }

    constexpr Vec& Vec::operator /=(Float a) noexcept
    {
        Coord::operator /=(a);
        return *this;
    }

    constexpr Vec& Vec::operator +=(Coord rhs) noexcept
    {
        Coord::operator +=(rhs);
        return *this;
    }

    constexpr Vec& Vec::operator -=(Coord rhs) noexcept
    {
        Coord::operator -=(rhs);
        return *this;
    }

    ////////////////////////////////////////////////////////////////

    bool Interval::infinite() const noexcept
    {
        const bool ret = valid() && clower() != cupper() && isinf(clower()) && isinf(cupper());
        assert(!ret || equals(inf_interval));
        return ret;
    }

    bool Interval::is_point() const noexcept
    {
        return valid() && clower() == cupper();
    }

    Float Interval::point() const noexcept
    {
        assert(is_point());
        return clower();
    }

    bool Interval::equals(const Interval& rhs) const noexcept
    {
        return Inherit::equals(rhs);
    }

    template <Precision_c P>
    bool Interval::apx_equals(Float a) const noexcept
    {
        if (!is_apx_point()) return false;

        return apx_equal<P>(middle(), a);
    }

    template <Precision_c P>
    partial_ordering Interval::apx_compare(Float a) const
    {
        return apx_compare<P>(Interval(a));
    }

    template <Precision_c P>
    bool Interval::apx_equals(const Interval& rhs) const noexcept
    {
        return apx_equal<P>(cparent(), rhs.cparent());
    }

    ////////////////////////////////////////////////////////////////

    template <size_t degV, typename CoefT>
    constexpr Polynom<degV, CoefT>::Polynom(auto&&... args)
        : Polynom(explicit_tag, FORWARD(args)...)
    { }

    template <size_t degV, typename CoefT>
    constexpr Polynom<degV, CoefT>::Polynom(Coef a)
        : Polynom(explicit_tag, a)
    { }

    template <size_t degV, typename CoefT>
    constexpr Polynom<degV, CoefT>::Polynom(Explicit_tag, auto&&... args)
        : _coefs{FORWARD(args)...}
    {
        assert(none_of(ccoefs(), [](Coef c){ return isnan(c); }));
    }

    template <size_t degV, typename CoefT>
    constexpr typename Polynom<degV, CoefT>::Coef Polynom<degV, CoefT>::constant() const noexcept
    {
        return get<deg_v>();
    }

    template <size_t degV, typename CoefT>
    constexpr typename Polynom<degV, CoefT>::Coef& Polynom<degV, CoefT>::constant() noexcept
    {
        return get<deg_v>();
    }

    template <size_t degV, typename CoefT>
    constexpr int Polynom<degV, CoefT>::deg_at(Idx idx) noexcept
    {
        return deg_v - idx;
    }

    template <size_t degV, typename CoefT>
    constexpr Float Polynom<degV, CoefT>::operator ()(Float x) const
    {
        int k = 0;
        return accumulate(ccoefs(), 0., [x, &k](Float y, Float c) constexpr {
            const int ord = deg_at(k);
            for (int i = 0; i < ord; ++i) c *= x;
            ++k;
            return y + c;
        });
    }

    template <size_t degV, typename CoefT>
    String Polynom<degV, CoefT>::to_string() const
    {
        using tomaqa::to_string;
        String str;
        bool first_print = true;
        for (Idx i = 0; i < deg_v; ++i) {
            const Float c = ccoefs()[i];
            if (c == 0) continue;
            if (!first_print && c > 0) str += "+";
            first_print = false;
            str += to_string(c) + "x^" + to_string(deg_at(i)) + " ";
        }

        const Float c = cconstant();
        if (c != 0 || first_print) {
            if (!first_print && c > 0) str += "+";
            str += to_string(c);
        }

        return str;
    }
}
