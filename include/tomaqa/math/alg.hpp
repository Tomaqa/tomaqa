#pragma once

#include "tomaqa/math.hpp"

#include <boost/math/tools/roots.hpp>
#include <boost/math/tools/cubic_roots.hpp>
#include <boost/math/tools/quartic_roots.hpp>

namespace tomaqa::math {
    using boost::math::tools::quadratic_roots;
    using boost::math::tools::cubic_roots;
    using boost::math::tools::quartic_roots;

    /// c <= 0
    template <bool lessV = true>
    Interval const_ineq(Float);
    template <bool lessV = true>
    inline Interval const_ineq(const Polynom<0>&);
    /// bx + c <= 0
    template <bool lessV = true>
    Interval linear_ineq(Float, Float);
    template <bool lessV = true>
    inline Interval linear_ineq(const Polynom<1>&);
    /// ax^2 + bx + c <= 0; a >= 0 (and analogously for '>=' ineq)
    template <bool lessV = true>
    Interval quadratic_ineq(Float, Float, Float);
    template <bool lessV = true>
    inline Interval quadratic_ineq(const Polynom<2>&);

    extern template Interval const_ineq<true>(Float);
    extern template Interval const_ineq<false>(Float);
    extern template Interval linear_ineq<true>(Float, Float);
    extern template Interval linear_ineq<false>(Float, Float);
    extern template Interval quadratic_ineq<true>(Float, Float, Float);
    extern template Interval quadratic_ineq<false>(Float, Float, Float);
}

#include "tomaqa/math/alg.inl"
