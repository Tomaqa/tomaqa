#pragma once

namespace tomaqa::math {
    template <bool lessV>
    Interval const_ineq(const Polynom<0>& coefs)
    {
        const auto [a] = coefs;
        return const_ineq<lessV>(a);
    }

    template <bool lessV>
    Interval linear_ineq(const Polynom<1>& coefs)
    {
        const auto [a, b] = coefs;
        return linear_ineq<lessV>(a, b);
    }

    template <bool lessV>
    Interval quadratic_ineq(const Polynom<2>& coefs)
    {
        const auto [a, b, c] = coefs;
        return quadratic_ineq<lessV>(a, b, c);
    }
}
