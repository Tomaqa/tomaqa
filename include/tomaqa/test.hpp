#pragma once

#include "tomaqa/util.hpp"

namespace tomaqa::test {
    using namespace util;

    template <typename C>
    concept Case_c = requires (C& c) { c.test(); };

    template <typename P = Ignore>
    class Void_case;
    template <typename I, typename P = Ignore>
    class Input_case;
    template <typename I, typename O = I, typename P = Ignore>
    class Case;
    template <typename T, typename I = T, typename O = T, typename P = Ignore>
    class Cons_case;

    class Suite;

    struct Config;
}

namespace tomaqa::test {
    template <typename P>
    class Void_case : public Object<Void_case<P>> {
    public:
        using typename Object<Void_case<P>>::This;
        using Params = P;

        static_assert(is_default_constructible_v<Params>);

        virtual ~Void_case()                                                              = default;
        Void_case(const Void_case&)                                                       = default;
        Void_case& operator =(const Void_case&)                                           = default;
        Void_case(Void_case&&)                                                            = default;
        Void_case& operator =(Void_case&&)                                                = default;
        // It is important to use the default type as a hint for type deduction
        template <convertible_to<P> P_ = P>
        Void_case(P_&& params_ = P_());

        const auto& cparams() const                                              { return _params; }

        virtual void pre_init(const Suite*);

        virtual void test();

        static consteval bool params_printable() noexcept;
        String lto_string() const;
        virtual String to_string() const;
    protected:
        bool should_throw() const;
        const String& msg() const;

        auto& params() const                                                     { return _params; }

        virtual void init()                                                                      { }
        virtual void finish()                                                                    { }
    private:
        virtual void do_stuff()                                                                 = 0;

        const Suite* _link{};
        mutable Params _params{};
    };

    template <typename I, typename P>
    class Input_case : public Inherit<Void_case<P>, Input_case<I, P>> {
    public:
        using Inherit = tomaqa::Inherit<Void_case<P>, Input_case<I, P>>;
        using typename Inherit::This;
        using typename Inherit::Params;
        using Input = I;

        static constexpr bool use_input_cp = !is_ref_v<Input>;
        using Input_copy = conditional_t<use_input_cp, Input, Dummy>;

        Input_case()                                                                       = delete;
        virtual ~Input_case()                                                             = default;
        Input_case(const Input_case&)                                                     = default;
        Input_case& operator =(const Input_case&)                                         = default;
        Input_case(Input_case&&)                                                          = default;
        Input_case& operator =(Input_case&&)                                              = default;
        // `!T_::use_input_cp` is necessary, I don't know why..
        template <convertible_to<I> I_ = I, convertible_to<P> P_ = P, Case_c T_ = This>
        requires (!T_::use_input_cp)
        Input_case(I_&& input_, P_&& params_ = P_());
        template <convertible_to<I> I_ = I, convertible_to<P> P_ = P, Case_c T_ = This>
        requires T_::use_input_cp
        Input_case(I_&& input_, P_&& params_ = P_());

        const auto& cinput() const                                                { return _input; }

        const auto& cinput_cp() const                                          { return _input_cp; }

        void test() override;

        String lto_string() const;
        String to_string() const override;
    protected:
        const auto& input() const&                                              { return cinput(); }
        auto& input()&                                                            { return _input; }
        auto input()&&                                                     { return move(input()); }

        auto& input_cp()                                                       { return _input_cp; }

        void init() override;
    private:
        Input _input;
        Input_copy _input_cp{};
    };

    template <typename I, typename O, typename P>
    class Case : public Inherit<Input_case<I, P>, Case<I, O, P>> {
    public:
        using Inherit = tomaqa::Inherit<Input_case<I, P>, Case<I, O, P>>;
        using typename Inherit::This;
        using typename Inherit::Params;
        using typename Inherit::Input;
        using Output = O;

        Case()                                                                             = delete;
        virtual ~Case()                                                                   = default;
        Case(const Case&)                                                                 = default;
        Case& operator =(const Case&)                                                     = default;
        Case(Case&&)                                                                      = default;
        Case& operator =(Case&&)                                                          = default;
        // This should be more viable than with `Ignore` for a default rvalue of the second arg.
        template <convertible_to<I> I_ = I, convertible_to<O> O_ = O, convertible_to<P> P_ = P>
        Case(I_&& input_, O_&& expected_, P_&& params_ = P_());
        template <convertible_to<I> I_ = I, convertible_to<P> P_ = P>
        Case(I_&& input_, const convertible_to<Ignore> auto&, P_&& params_ = P_());

        const Output& cexpected() const;
        const Output& expected() const&                                      { return cexpected(); }

        virtual bool ignored_result() const noexcept;

        virtual const char* cond_msg() const noexcept;

        void pre_init(const Suite* link) override;

        void test() override;

        String lto_string() const;
        String to_string() const override;
    protected:
        Output& expected()&;
        Output&& expected()&&                                           { return move(expected()); }

        const Output& cresult() const;

        void do_stuff() override;

        virtual bool condition(const Output& expected_, const Output& result_) const;
        /// Moves `input` by default
        virtual Output result();
    private:
        using Expected_ptr = Shared_ptr<Output>;
        using Result_ptr = Shared_ptr<Output>;

        Expected_ptr _expected_ptr{};
        Result_ptr _result_ptr{};
    };

    template <typename T, typename I, typename O, typename P>
    class Cons_case : public Inherit<Case<I, O, P>, Cons_case<I, O, P>> {
    public:
        using Inherit = tomaqa::Inherit<Case<I, O, P>, Cons_case<I, O, P>>;
        using typename Inherit::This;
        using typename Inherit::Params;
        using typename Inherit::Input;
        using typename Inherit::Output;
        using Cons = T;

        using Inherit::Inherit;
    protected:
        virtual Cons ccons() const;
        /// Be careful, this will destroy the input !
        virtual Cons cons();

        /// Calls `cons` by default
        Output result() override;
    private:
        template <typename C> static Cons cons_impl(C&&);
    };

    class Suite {
    public:
        template <Case_c C> using Data = Vector<C>;

        ~Suite()                                                                          = default;
        Suite(const Suite&)                                                               = default;
        Suite& operator =(const Suite&)                                                   = default;
        Suite(Suite&&)                                                                    = default;
        Suite& operator =(Suite&&)                                                        = default;
        Suite(String msg_ = "", bool should_throw_ = false);

        bool should_throw() const                                          { return _should_throw; }

        const auto& cmsg() const                                                    { return _msg; }
        auto& msg()                                                                 { return _msg; }

        template <Case_c C> void test(Data<C> data) const;
    private:
        String _msg{};
        bool _should_throw{};
    };

    struct Config {
        bool silent{false};
    };

    inline Config config;
}

#include "tomaqa/test.inl"
