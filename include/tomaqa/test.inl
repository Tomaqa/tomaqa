#pragma once

#include "tomaqa/util/string/alg.hpp"
#include "tomaqa/util/scope_guard.hpp"

#include <iostream>

namespace tomaqa::test {
    using std::cout;
    using std::cerr;
    using std::endl;

    template <typename P>
    template <convertible_to<P> P_>
    Void_case<P>::Void_case(P_&& params_)
        : _params(FORWARD(params_))
    { }

    template <typename P>
    void Void_case<P>::pre_init(const Suite* link)
    {
        _link = link;
    }

    template <typename P>
    bool Void_case<P>::should_throw() const
    {
        return _link->should_throw();
    }

    template <typename P>
    const String& Void_case<P>::msg() const
    {
        return _link->cmsg();
    }

    template <typename P>
    void Void_case<P>::test()
    try {
        [[maybe_unused]] bool pref_assert_tmp = false;
        if constexpr (_debug_) swap(pref_assert_tmp, _prefer_assert_over_throw_);
        auto sg = Scope_guard([pref_assert_tmp]{
            if constexpr (_debug_) _prefer_assert_over_throw_ = pref_assert_tmp;
        });
        try {
            init();
            do_stuff();
            finish();
        }
        catch (const Error&) {
            if (should_throw()) return;
            throw;
        }
        catch (const std::runtime_error&) {
            if (should_throw()) return;
            throw;
        }
        catch (Ignore) { }

        if (should_throw()) THROW("Expected thrown exception");
    }
    catch (const Error& err) {
        String str;
        if constexpr (params_printable())
            str += "\n!! With " + lto_string() + ":";
        throw move(str) + "\n!! "s + err + " !!\n";
    }

    template <typename P>
    consteval bool Void_case<P>::params_printable() noexcept
    {
        return !is_same_v<Params, Ignore> && Enabled_to_string<Params>;
    }

    template <typename P>
    String Void_case<P>::lto_string() const
    {
        using tomaqa::to_string;
        if constexpr (!params_printable()) return "";
        else return "params '" + to_string(cparams()) + "'";
    }

    template <typename P>
    String Void_case<P>::to_string() const
    {
        return lto_string();
    }

    ////////////////////////////////////////////////////////////////

    template <typename I, typename P>
    template <convertible_to<I> I_, convertible_to<P> P_, Case_c T_>
        requires (!T_::use_input_cp)
    Input_case<I, P>::Input_case(I_&& input_, P_&& params_)
        : Inherit(FORWARD(params_)), _input(FORWARD(input_))
    { }

    template <typename I, typename P>
    template <convertible_to<I> I_, convertible_to<P> P_, Case_c T_>
        requires T_::use_input_cp
    Input_case<I, P>::Input_case(I_&& input_, P_&& params_)
        : Inherit(FORWARD(params_)), _input(FORWARD(input_)), _input_cp(deep_copy(cinput()))
    { }

    template <typename I, typename P>
    void Input_case<I, P>::init()
    {
        Inherit::init();
        if constexpr (Dynamic_c<Input>) {
            This::input().maybe_virtual_init();
            if constexpr (use_input_cp) This::input_cp().maybe_virtual_init();
        }
    }

    template <typename I, typename P>
    void Input_case<I, P>::test()
    try {
        Inherit::test();
    }
    catch (const Error& err) {
        throw "\n!! At "s + lto_string() + ":" + err;
    }

    template <typename I, typename P>
    String Input_case<I, P>::lto_string() const
    {
        using tomaqa::to_string;
        String str("input '");
        /// Possibly use the copy since the `_input` could have possibly been destroyed
        if constexpr (use_input_cp) str += to_string(cinput_cp());
        else str += to_string(cinput());
        return str + "'";
    }

    template <typename I, typename P>
    String Input_case<I, P>::to_string() const
    {
        String str = Inherit::to_string();
        if constexpr (Inherit::params_printable()) str += " ";
        str += lto_string();
        return str;
    }

    ////////////////////////////////////////////////////////////////

    template <typename I, typename O, typename P>
    template <convertible_to<I> I_, convertible_to<O> O_, convertible_to<P> P_>
    Case<I, O, P>::Case(I_&& input_, O_&& expected_, P_&& params_)
        : Inherit(FORWARD(input_), FORWARD(params_)),
          _expected_ptr(make_shared<Output>(FORWARD(expected_)))
    { }

    template <typename I, typename O, typename P>
    template <convertible_to<I> I_, convertible_to<P> P_>
    Case<I, O, P>::Case(I_&& input_, const convertible_to<Ignore> auto&, P_&& params_)
        : Inherit(FORWARD(input_), FORWARD(params_))
    { }

    template <typename I, typename O, typename P>
    const typename Case<I, O, P>::Output& Case<I, O, P>::cexpected() const
    {
        return *_expected_ptr;
    }

    template <typename I, typename O, typename P>
    typename Case<I, O, P>::Output& Case<I, O, P>::expected()&
    {
        return *_expected_ptr;
    }

    template <typename I, typename O, typename P>
    const typename Case<I, O, P>::Output& Case<I, O, P>::cresult() const
    {
        assert(_result_ptr);
        return *_result_ptr;
    }

    template <typename I, typename O, typename P>
    bool Case<I, O, P>::ignored_result() const noexcept
    {
        return _expected_ptr == nullptr;
    }

    template <typename I, typename O, typename P>
    const char* Case<I, O, P>::cond_msg() const noexcept
    {
        return "==";
    }

    template <typename I, typename O, typename P>
    void Case<I, O, P>::pre_init(const Suite* link)
    {
        Inherit::pre_init(link);
        if constexpr (Dynamic_c<Output>) if (!This::ignored_result())
            This::expected().maybe_virtual_init();
    }

    template <typename I, typename O, typename P>
    void Case<I, O, P>::do_stuff()
    {
        using tomaqa::to_string;
        Output result_ = result();
        expect(ignored_result() || condition(cexpected(), result_),
               "Condition 'result "s + cond_msg() + " expected'"s
               + " not met:\n!! Got:      '" + to_string(result_) + "'");
        _result_ptr = MAKE_UNIQUE(move(result_));
    }

    template <typename I, typename O, typename P>
    bool Case<I, O, P>::condition(const Output& expected_, const Output& result_) const
    {
        if constexpr (equality_comparable<Output>) return result_ == expected_;
        else X_NOT_IMPLEMENTED("`Case::==`");
    }

    template <typename I, typename O, typename P>
    typename Case<I, O, P>::Output Case<I, O, P>::result()
    {
        if constexpr (convertible_to<Input, Output>) return This::mthis().input();
        else X_NOT_IMPLEMENTED("`Case::result`");
    }

    template <typename I, typename O, typename P>
    void Case<I, O, P>::test()
    try {
        Inherit::test();
    }
    catch (const Error& err) {
        if (ignored_result()) throw;
        throw err + "!! Expected "s + lto_string() + " !!\n";
    }

    template <typename I, typename O, typename P>
    String Case<I, O, P>::lto_string() const
    {
        using tomaqa::to_string;
        if (ignored_result()) return "";
        return "output '" + to_string(cexpected()) + "'";
    }

    template <typename I, typename O, typename P>
    String Case<I, O, P>::to_string() const
    {
        return Inherit::to_string() + " " + lto_string();
    }

    ////////////////////////////////////////////////////////////////

    template <typename T, typename I, typename O, typename P>
    typename Cons_case<T, I, O, P>::Cons Cons_case<T, I, O, P>::ccons() const
    {
        return cons_impl(*this);
    }

    template <typename T, typename I, typename O, typename P>
    typename Cons_case<T, I, O, P>::Cons Cons_case<T, I, O, P>::cons()
    {
        return cons_impl(move(*this));
    }

    template <typename T, typename I, typename O, typename P>
    template <typename C>
    typename Cons_case<T, I, O, P>::Cons
    Cons_case<T, I, O, P>::cons_impl(C&& case_)
    {
        if constexpr (constructible_from<Cons, Forward_as<Input, C>>) {
            auto&& in = FORWARD(case_).input();
            if constexpr (!Dynamic_c<Cons>) return Cons(FORWARD(in));
            else return Cons::cons(FORWARD(in));
        }
        else X_NOT_IMPLEMENTED("`Cons_case::cons`");
    }

    template <typename T, typename I, typename O, typename P>
    typename Cons_case<T, I, O, P>::Output Cons_case<T, I, O, P>::result()
    {
        if constexpr (convertible_to<Cons, Output>) return cons();
        else NOT_IMPLEMENTED;
    }

    ////////////////////////////////////////////////////////////////

    template <Case_c C>
    void Suite::test(Data<C> data) const
    {
        for (auto& c : data) {
            c.pre_init(this);
        }

        String head_msg, tail_msg;
        String line;
        if (!empty(cmsg())) {
            head_msg = "// Testing "s + cmsg() + " with " + (should_throw() ? "in" : "")
                     + "valid inputs" + R"(...   \\)";
            line = "  " + repeat('-', size(head_msg)-4) + "  ";
            if (!config.silent) cout << line << endl << head_msg << endl;
        }

        for (auto& c : data) {
            if constexpr (_debug_) {
                /// Printing even before possibly needed initialization of the input !
                if (!config.silent) cout << "|| Testing case with " << c << " ...";
                if (!config.silent) cout.flush();
            }
            c.test();
            if constexpr (_debug_) if (!config.silent) cout << " done." << endl;
        }

        if (!empty(cmsg())) {
            tail_msg = R"(\\ Testing )" + cmsg() + " done.";
            tail_msg += repeat(' ', size(head_msg) - size(tail_msg) - 3) + " //";
            if (!config.silent) cout << tail_msg << endl << line << endl << endl;
        }
    }
}
