#pragma once

#include "tomaqa/core.hpp"

#include <cmath>
#include <bit>
#include <limits>

namespace tomaqa::util {
    enum class Side { left, center, right };

    template <typename> class Wrap;

    struct Priority;

    /// It does *not* include definitions of iterator types
    template <typename ValueT, integral = size_t, typename KeyT = void>
    struct Container_types;

    template <Class> class Lock_mixin;
    using Lock = Lock_mixin<Empty>;

    using Mask = Vector<bool>;

    using Idx = Wrap<int>;

    constexpr auto nop = [](auto&&...){};
    using Nop = decltype(nop);

    template <typename RetT> constexpr auto ret_def = [](auto&&...) -> RetT { return {}; };
    template <typename RetT> using Ret_def = decltype(ret_def<RetT>);

    constexpr auto true_pred = [](auto&&...){ return true; };
    constexpr auto false_pred = [](auto&&...){ return false; };
    using True_pred = decltype(true_pred);
    using False_pred = decltype(false_pred);

    template <typename T> concept Integral = integral<T> || same_as<T, Idx>;

    using std::abs;

    using std::ceil;
    using std::floor;
    using std::trunc;
    using std::round;
    using std::lround;
    using std::llround;

    using std::isfinite;
    using std::isinf;
    using std::isnan;
    using std::isnormal;

    using std::bit_width;

    auto to_pointer(input_iterator auto) noexcept;

    template <Container ContT>
    typename ContT::iterator to_iterator(typename ContT::const_iterator) noexcept;

    ostream& operator <<(ostream&, const Side&);

    void mask(Mask&, Idx);
    void unmask(Mask&, Idx);
    inline bool masked(const Mask&, Idx);

    constexpr Idx operator +(Idx, Integral auto);
    constexpr Idx operator -(Idx, Integral auto);
    constexpr Idx operator *(Idx, Integral auto);
    constexpr Idx operator /(Idx, Integral auto);
    constexpr Idx operator %(Idx, Integral auto);
    constexpr Idx operator &(Idx, Integral auto);
    constexpr Idx operator |(Idx, Integral auto);
    constexpr Idx operator ^(Idx, Integral auto);
    constexpr Idx operator <<(Idx, Integral auto);
    constexpr Idx operator >>(Idx, Integral auto);
    constexpr Idx operator +(Integral auto, Idx);
    constexpr Idx operator -(Integral auto, Idx);
    constexpr Idx operator *(Integral auto, Idx);
    constexpr Idx operator /(Integral auto, Idx);
    constexpr Idx operator ^(Integral auto, Idx);
    constexpr auto operator +(Idx, floating_point auto);
    constexpr auto operator -(Idx, floating_point auto);
    constexpr auto operator *(Idx, floating_point auto);
    constexpr auto operator /(Idx, floating_point auto);
    constexpr auto operator +(floating_point auto, Idx);
    constexpr auto operator -(floating_point auto, Idx);
    constexpr auto operator *(floating_point auto, Idx);
    constexpr auto operator /(floating_point auto, Idx);
    constexpr Idx& operator +=(Idx&, Integral auto);
    constexpr Idx& operator -=(Idx&, Integral auto);
    constexpr Idx& operator *=(Idx&, Integral auto);
    constexpr Idx& operator /=(Idx&, Integral auto);
    constexpr Idx& operator %=(Idx&, Integral auto);
    constexpr Idx& operator &=(Idx&, Integral auto);
    constexpr Idx& operator |=(Idx&, Integral auto);
    constexpr Idx& operator ^=(Idx&, Integral auto);
    constexpr Idx& operator <<=(Idx&, Integral auto);
    constexpr Idx& operator >>=(Idx&, Integral auto);
    constexpr Idx& operator ++(Idx&);
    constexpr Idx operator ++(Idx&, int);
    constexpr Idx& operator --(Idx&);
    constexpr Idx operator --(Idx&, int);
}

namespace tomaqa::util {
    template <typename T>
    class Wrap : public Static<Wrap<T>> {
    public:
        using Item = T;

        Wrap()                                                                            = default;
        ~Wrap()                                                                           = default;
        Wrap(const Wrap&)                                                                 = default;
        Wrap& operator =(const Wrap&)                                                     = default;
        Wrap(Wrap&&)                                                                      = default;
        Wrap& operator =(Wrap&&)                                                          = default;
        constexpr Wrap(convertible_to<Item> auto&& val)                    : _item(FORWARD(val)) { }

        constexpr const auto& citem() const noexcept                               { return _item; }
        constexpr const auto& item() const& noexcept                             { return citem(); }
        constexpr auto& item()& noexcept                                           { return _item; }
        constexpr auto item()&& noexcept                                    { return move(item()); }

        constexpr operator const Item&() const& noexcept                         { return citem(); }
        constexpr operator Item&()& noexcept                                      { return item(); }
        constexpr operator Item()&& noexcept                          { return move(*this).item(); }

        constexpr bool equals(const Wrap&) const noexcept;
        constexpr auto compare(const Wrap&) const noexcept;

        String to_string() const                                { return tomaqa::to_string(_item); }
    private:
        Item _item{};
    };

    struct Priority : Inherit<Wrap<unsigned char>> {
        static constexpr Item min = 0;
        static constexpr Item low = 10;
        static constexpr Item medium = 20;
        static constexpr Item high = 30;
        static constexpr Item max = Item(~0U);

        using Inherit::Inherit;
    };

    template <typename ValueT, integral SizeT, typename KeyT>
    struct Container_types : Container_types<ValueT, SizeT> {
        using key_type = KeyT;
    };

    template <typename ValueT, integral SizeT>
    struct Container_types<ValueT, SizeT, void> {
        using value_type = ValueT;
        using reference = value_type&;
        using const_reference = const value_type&;
        using pointer = value_type*;
        using const_pointer = const value_type*;
        using size_type = SizeT;
    };

    template <Class B>
    class Lock_mixin : public Inherit<B, Lock_mixin<B>> {
    public:
        using Inherit = tomaqa::Inherit<B, Lock_mixin<B>>;

        using Inherit::Inherit;

        bool locked() const noexcept                                             { return _locked; }
        bool unlocked() const noexcept                                         { return !locked(); }
        void lock() noexcept                                                     { _locked = true; }
        void unlock() noexcept                                                  { _locked = false; }
    private:
        bool _locked{};
    };
}

namespace std {
    template <typename T>
    struct numeric_limits<tomaqa::util::Wrap<T>> : numeric_limits<T> {};

    //? hash is probably not included here yet
    template <typename T>
    struct hash<tomaqa::util::Wrap<T>> {
        size_t operator ()(const tomaqa::util::Wrap<T>& in) const          { return hash<T>{}(in); }
    };
}

#include "tomaqa/util/ptr.hpp"

#include "tomaqa/util.inl"

namespace tomaqa::util {
    constexpr Idx invalid_idx = -1;
}
