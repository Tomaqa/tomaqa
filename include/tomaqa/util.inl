#pragma once

namespace tomaqa::util {
    template <typename T>
    constexpr bool Wrap<T>::equals(const Wrap& rhs) const noexcept
    {
        return citem() == rhs.citem();
    }

    template <typename T>
    constexpr auto Wrap<T>::compare(const Wrap& rhs) const noexcept
    {
        return citem() <=> rhs.citem();
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace tomaqa::util {
    auto to_pointer(input_iterator auto it) noexcept
    {
        return &*it;
    }

    template <Container ContT>
    typename ContT::iterator to_iterator(typename ContT::const_iterator it) noexcept
    {
        ContT c;
        return c.erase(it, it);
    }

    bool masked(const Mask& mask, Idx idx)
    {
        return mask[idx];
    }

    constexpr Idx operator +(Idx idx, Integral auto x)
    {
        return idx += x;
    }

    constexpr Idx operator -(Idx idx, Integral auto x)
    {
        return idx -= x;
    }

    constexpr Idx operator *(Idx idx, Integral auto x)
    {
        return idx *= x;
    }

    constexpr Idx operator /(Idx idx, Integral auto x)
    {
        return idx /= x;
    }

    constexpr Idx operator %(Idx idx, Integral auto x)
    {
        return idx %= x;
    }

    constexpr Idx operator &(Idx idx, Integral auto x)
    {
        return idx &= x;
    }

    constexpr Idx operator |(Idx idx, Integral auto x)
    {
        return idx |= x;
    }

    constexpr Idx operator ^(Idx idx, Integral auto x)
    {
        return idx ^= x;
    }

    constexpr Idx operator <<(Idx idx, Integral auto x)
    {
        return idx <<= x;
    }

    constexpr Idx operator >>(Idx idx, Integral auto x)
    {
        return idx >>= x;
    }

    constexpr Idx operator +(Integral auto x, Idx idx)
    {
        return move(idx) + x;
    }

    constexpr Idx operator -(Integral auto x, Idx idx)
    {
        return x - idx.item();
    }

    constexpr Idx operator *(Integral auto x, Idx idx)
    {
        return move(idx)*x;
    }

    constexpr Idx operator /(Integral auto x, Idx idx)
    {
        return x/idx.item();
    }

    constexpr Idx operator ^(Integral auto x, Idx idx)
    {
        return move(idx) ^ x;
    }

    constexpr auto operator +(Idx idx, floating_point auto x)
    {
        return idx.item() + x;
    }

    constexpr auto operator -(Idx idx, floating_point auto x)
    {
        return idx.item() - x;
    }

    constexpr auto operator *(Idx idx, floating_point auto x)
    {
        return idx.item()*x;
    }

    constexpr auto operator /(Idx idx, floating_point auto x)
    {
        return idx.item()/x;
    }

    constexpr auto operator +(floating_point auto x, Idx idx)
    {
        return x + idx.item();
    }

    constexpr auto operator -(floating_point auto x, Idx idx)
    {
        return x - idx.item();
    }

    constexpr auto operator *(floating_point auto x, Idx idx)
    {
        return x*idx.item();
    }

    constexpr auto operator /(floating_point auto x, Idx idx)
    {
        return x/idx.item();
    }

    constexpr Idx& operator +=(Idx& idx, Integral auto x)
    {
        idx.item() += x;
        return idx;
    }

    constexpr Idx& operator -=(Idx& idx, Integral auto x)
    {
        idx.item() -= x;
        return idx;
    }

    constexpr Idx& operator *=(Idx& idx, Integral auto x)
    {
        idx.item() *= x;
        return idx;
    }

    constexpr Idx& operator /=(Idx& idx, Integral auto x)
    {
        idx.item() /= x;
        return idx;
    }

    constexpr Idx& operator %=(Idx& idx, Integral auto x)
    {
        idx.item() %= x;
        return idx;
    }

    constexpr Idx& operator &=(Idx& idx, Integral auto x)
    {
        idx.item() &= x;
        return idx;
    }

    constexpr Idx& operator |=(Idx& idx, Integral auto x)
    {
        idx.item() |= x;
        return idx;
    }

    constexpr Idx& operator ^=(Idx& idx, Integral auto x)
    {
        idx.item() ^= x;
        return idx;
    }

    constexpr Idx& operator <<=(Idx& idx, Integral auto x)
    {
        idx.item() <<= x;
        return idx;
    }

    constexpr Idx& operator >>=(Idx& idx, Integral auto x)
    {
        idx.item() >>= x;
        return idx;
    }

    constexpr Idx& operator ++(Idx& idx)
    {
        ++idx.item();
        return idx;
    }

    constexpr Idx operator ++(Idx& idx, int)
    {
        Idx tmp = idx;
        ++idx;
        return tmp;
    }

    constexpr Idx& operator --(Idx& idx)
    {
        --idx.item();
        return idx;
    }

    constexpr Idx operator --(Idx& idx, int)
    {
        Idx tmp = idx;
        --idx;
        return tmp;
    }
}
