#pragma once

#include "tomaqa/util.hpp"

#include "tomaqa/util/fun.hpp"

#include <algorithm>
#include <iterator>

namespace tomaqa::util {
    template <Container, Binary_invocable> struct Container_f;

    using std::move_iterator;

    template <Container ContT>
    using Iterator_tp = conditional_t<is_cref_v<ContT>, typename Rm_ref<ContT>::const_iterator,
                        conditional_t<is_rref_v<ContT>, move_iterator<Iter_t<ContT>>,
                        Iter_t<ContT>>>;

    using std::size;
    using std::ssize;
    using std::empty;
    using std::begin;
    using std::end;
    using std::cbegin;
    using std::cend;
    using std::data;

    using std::back_inserter;
    using std::inserter;

    using std::min;
    using std::max;

    using std::clamp;

    /// Returns appropriate iterator based on reference type of the container
    auto begin_tp(Container auto&&);
    auto end_tp(Container auto&&);

    template <typename F, typename WrapF> auto wrap(F, WrapF);
    auto as_unary(Binary_invocable auto, input_iterator auto first2);

    //++ c++20 ranges can probably replace many of these ...?
    template <Container ContT> bool all_of(const ContT&, Unary_predicate_for_cont<ContT> auto);
    template <Container ContT, input_iterator ItT2>
    bool all_of(const ContT&, ItT2 first2, indirect_binary_predicate<Iter_t<ContT>, ItT2> auto);
    template <typename... Args>
    constexpr bool all_of(const tuple<Args...>&, auto pred);
    template <typename... Args>
    constexpr bool all_of(const tuple<Args...>&, const tuple<Args...>&, auto pred);
    template <typename T> constexpr bool all_of(initializer_list<T>, auto pred);
    template <Container ContT> bool any_of(const ContT&, Unary_predicate_for_cont<ContT> auto);
    template <Container ContT, input_iterator ItT2>
    bool any_of(const ContT&, ItT2 first2, indirect_binary_predicate<Iter_t<ContT>, ItT2> auto);
    template <typename... Args>
    constexpr bool any_of(const tuple<Args...>&, auto pred);
    template <typename... Args>
    constexpr bool any_of(const tuple<Args...>&, const tuple<Args...>&, auto pred);
    template <typename T> constexpr bool any_of(initializer_list<T>, auto pred);
    template <Container ContT> bool none_of(const ContT&, Unary_predicate_for_cont<ContT> auto);
    template <Container ContT, input_iterator ItT2>
    bool none_of(const ContT&, ItT2 first2, indirect_binary_predicate<Iter_t<ContT>, ItT2> auto);
    template <typename... Args>
    constexpr bool none_of(const tuple<Args...>&, auto pred);
    template <typename... Args>
    constexpr bool none_of(const tuple<Args...>&, const tuple<Args...>&, auto pred);
    template <typename T> constexpr bool none_of(initializer_list<T>, auto pred);
    // Using `P` right in template args for default arg. deduction reasons
    template <input_iterator ItT, indirect_binary_predicate<ItT, ItT> P = equal_to<>>
    bool all_equal(ItT first, ItT last, P = {});
    template <Container ContT, Binary_predicate_for_cont<ContT> P = equal_to<>>
    bool all_equal(const ContT&, P = {});
    template <input_iterator ItT, indirect_binary_predicate<ItT, ItT> P = equal_to<>>
    bool any_equal(ItT first, ItT last, P = {});
    template <Container ContT, Binary_predicate_for_cont<ContT> P = equal_to<>>
    bool any_equal(const ContT&, P = {});
    template <input_iterator ItT, indirect_binary_predicate<ItT, ItT> P = equal_to<>>
    bool none_equal(ItT first, ItT last, P = {});
    template <Container ContT, Binary_predicate_for_cont<ContT> P = equal_to<>>
    bool none_equal(const ContT&, P = {});
    template <Container ContT1, Container ContT2,
              indirect_binary_predicate<Iter_t<ContT1>, Iter_t<ContT2>> P = equal_to<>>
    bool equal(const ContT1&, const ContT2&, P = {});
    template <Container ContT, input_iterator ItT2,
              indirect_binary_predicate<Iter_t<ContT>, ItT2> P = equal_to<>>
    bool equal(const ContT&, ItT2 first2, P = {});
    template <typename... Args>
    constexpr bool equal(const tuple<Args...>&, const tuple<Args...>&, auto pred);

    auto find(Container auto&&, const auto& val);
    template <Container ContT> auto find_if(ContT&&, Unary_predicate_for_cont<ContT> auto);
    template <Container ContT> auto find_if_not(ContT&&, Unary_predicate_for_cont<ContT> auto);
    template <input_iterator ItT> bool contains(ItT first, ItT last, const auto& val);
    bool contains(const Container auto&, const auto& val);
    template <typename T, typename U = T> bool contains(initializer_list<U>, const T& val);

    auto count(const Container auto&, const auto& val);
    template <Container ContT> auto count_if(const ContT&, Unary_predicate_for_cont<ContT> auto);

    template <Container ContT, Output_iterator_for_cont<ContT> OutputIt>
    OutputIt copy(const ContT&, OutputIt d_first);
    template <Container ContT, Output_iterator_for_cont<ContT> OutputIt>
    OutputIt move(ContT&&, OutputIt d_first);
    template <input_iterator ItT, output_iterator<ItT> OutputIt>
    OutputIt move_n(ItT first1, integral auto n, OutputIt d_first);

    template <Container ContT, Unary_invocable_for_cont<ContT> F> F for_each(ContT&&, F);
    template <input_iterator ItT1, input_iterator ItT2, Indirectly_binary_invocable<ItT1, ItT2> F>
    F for_each(ItT1 first1, ItT1 last1, ItT2 first2, F);
    template <Container ContT, input_iterator ItT2, Indirectly_binary_invocable<Iter_t<ContT>, ItT2> F>
    F for_each(ContT&&, ItT2 first2, F);
    template <input_iterator ItT1, input_iterator ItT2>
    ItT1 for_each_n(ItT1 first1, integral auto n, ItT2 first2,
                    Indirectly_binary_invocable<ItT1, ItT2> auto);
    template <Container ContT, Unary_invocable_for_cont<ContT> ConvF,
              Output_iterator_for_cont_invocable<ContT, ConvF> OutputIt>
    OutputIt transform(ContT&&, OutputIt d_first, ConvF);
    template <Container ContT, input_iterator ItT2, Binary_invocable_for_cont<ContT> ConvF,
              Output_iterator_for_cont_invocable<ContT, ConvF> OutputIt>
    OutputIt transform(ContT&&, ItT2 first2, OutputIt d_first, ConvF);

    template <Container ContT, Unary_invocable_for_cont<ContT> F> F for_each_cdr(ContT&&, F);

    template <input_iterator ItT, indirect_unary_predicate<ItT> P>
    P for_each_while(ItT first, ItT last, P);
    template <Container ContT, Unary_predicate_for_cont<ContT> P>
    P for_each_while(ContT&&, P);
    template <input_iterator ItT1, input_iterator ItT2,
              indirect_binary_predicate<ItT1, ItT2> P>
    P for_each_while(ItT1 first1, ItT1 last1, ItT2 first2, P);
    template <Container ContT, input_iterator ItT2,
              indirect_binary_predicate<Iter_t<ContT>, ItT2> P>
    P for_each_while(ContT&&, ItT2 first2, P);

    template <input_iterator ItT, indirectly_unary_invocable<ItT> F>
    F for_each_if(ItT first, ItT last, indirect_unary_predicate<ItT> auto, F);
    template <Container ContT, Unary_invocable_for_cont<ContT> F>
    F for_each_if(ContT&&, indirect_unary_predicate<Iter_t<ContT>> auto, F);
    template <input_iterator ItT1, input_iterator ItT2, Indirectly_binary_invocable<ItT1, ItT2> F>
    F for_each_if(ItT1 first1, ItT1 last1, ItT2 first2, indirect_binary_predicate<ItT1, ItT2> auto, F);
    template <Container ContT, input_iterator ItT2, Indirectly_binary_invocable<Iter_t<ContT>, ItT2> F>
    F for_each_if(ContT&&, ItT2 first2, indirect_binary_predicate<Iter_t<ContT>, ItT2> auto, F);
    template <input_iterator ItT, indirectly_unary_invocable<ItT> ThenF, indirectly_unary_invocable<ItT> ElseF>
    ThenF for_each_ite(ItT first, ItT last, indirect_unary_predicate<ItT> auto, ThenF, ElseF);
    template <Container ContT, Unary_invocable_for_cont<ContT> ThenF, Unary_invocable_for_cont<ContT> ElseF>
    ThenF for_each_ite(ContT&&, indirect_unary_predicate<Iter_t<ContT>> auto, ThenF, ElseF);
    template <input_iterator ItT1, input_iterator ItT2, Indirectly_binary_invocable<ItT1, ItT2> ThenF,
              Indirectly_binary_invocable<ItT1, ItT2> ElseF>
    ThenF for_each_ite(ItT1 first1, ItT1 last1, ItT2 first2,
                       indirect_binary_predicate<ItT1, ItT2> auto, ThenF, ElseF);
    template <Container ContT, input_iterator ItT2, Indirectly_binary_invocable<Iter_t<ContT>, ItT2> ThenF,
              Indirectly_binary_invocable<Iter_t<ContT>, ItT2> ElseF>
    ThenF for_each_ite(ContT&&, ItT2 first2,
                       indirect_binary_predicate<Iter_t<ContT>, ItT2> auto, ThenF, ElseF);

    template <Container To, input_iterator FromIt, indirectly_unary_invocable<FromIt> ConvF = identity>
    [[nodiscard]] To to(FromIt first, FromIt last, integral auto size_, ConvF = {});
    template <Container To, input_iterator FromIt, indirectly_unary_invocable<FromIt> ConvF = identity>
    [[nodiscard]] To to(FromIt first, FromIt last, ConvF = {});
    template <Container To, Container From, Unary_invocable_for_cont<From> ConvF = identity>
    [[nodiscard]] To to(From&&, integral auto size_, ConvF = {});
    template <Container To, Container From, Unary_invocable_for_cont<From> ConvF = identity>
    [[nodiscard]] To to(From&&, ConvF = {});
    template <input_iterator FromIt, indirectly_unary_invocable<FromIt> ConvF = identity>
    void to(Container auto&, FromIt first, FromIt last, integral auto size_, ConvF = {});
    template <input_iterator FromIt, indirectly_unary_invocable<FromIt> ConvF = identity>
    void to(Container auto&, FromIt first, FromIt last, ConvF = {});
    template <Container From, Unary_invocable_for_cont<From> ConvF = identity>
    void to(Container auto&, From&&, integral auto size_, ConvF = {});
    template <Container From, Unary_invocable_for_cont<From> ConvF = identity>
    void to(Container auto&, From&&, ConvF = {});
    template <Container To, input_iterator FromIt1, input_iterator FromIt2, Indirectly_binary_invocable<FromIt1, FromIt2> ConvF>
    [[nodiscard]] To to(FromIt1 first1, FromIt1 last1, FromIt2 first2, integral auto size_, ConvF);
    template <Container To, input_iterator FromIt1, input_iterator FromIt2, Indirectly_binary_invocable<FromIt1, FromIt2> ConvF>
    [[nodiscard]] To to(FromIt1 first1, FromIt1 last1, FromIt2 first2, ConvF);
    template <Container To, Container From1, input_iterator FromIt2, Indirectly_binary_invocable<Iter_t<From1>, FromIt2> ConvF>
    [[nodiscard]] To to(From1&&, FromIt2 first2, integral auto size_, ConvF);
    template <Container To, Container From1, input_iterator FromIt2, Indirectly_binary_invocable<Iter_t<From1>, FromIt2> ConvF>
    [[nodiscard]] To to(From1&&, FromIt2 first2, ConvF);
    template <input_iterator FromIt1, input_iterator FromIt2, Indirectly_binary_invocable<FromIt1, FromIt2> ConvF>
    void to(Container auto&, FromIt1 first1, FromIt1 last1, FromIt2 first2, integral auto size_, ConvF);
    template <input_iterator FromIt1, input_iterator FromIt2, Indirectly_binary_invocable<FromIt1, FromIt2> ConvF>
    void to(Container auto&, FromIt1 first1, FromIt1 last1, FromIt2 first2, ConvF);
    template <Container From1, input_iterator FromIt2, Indirectly_binary_invocable<Iter_t<From1>, FromIt2> ConvF>
    void to(Container auto&, From1&&, FromIt2 first2, integral auto size_, ConvF);
    template <Container From1, input_iterator FromIt2, Indirectly_binary_invocable<Iter_t<From1>, FromIt2> ConvF>
    void to(Container auto&, From1&&, FromIt2 first2, ConvF);

    template <Container OutputCont, input_iterator ItT2>
    OutputCont& append(OutputCont&, ItT2 first2, ItT2 last2, integral auto size_);
    template <Container OutputCont, input_iterator ItT2>
    OutputCont& append(OutputCont&, ItT2 first2, ItT2 last2);
    template <Container OutputCont>
    OutputCont& append(OutputCont&, Container auto&&);
    template <Container OutputCont, input_iterator ItT2>
    [[nodiscard]] OutputCont cat(OutputCont, ItT2 first2, ItT2 last2, integral auto size_);
    template <Container OutputCont, input_iterator ItT2>
    [[nodiscard]] OutputCont cat(OutputCont, ItT2 first2, ItT2 last2);
    template <Container OutputCont>
    [[nodiscard]] OutputCont cat(OutputCont, Container auto&&);
    template <input_iterator ItT1, Container ContT2>
    [[nodiscard]] Rm_cvref<ContT2> cat(ItT1 first1, ItT1 last1, integral auto size_, ContT2&&);
    template <input_iterator ItT1, Container ContT2>
    [[nodiscard]] Rm_cvref<ContT2> cat(ItT1 first1, ItT1 last1, ContT2&&);

    template <Container OutputCont, input_iterator ItT>
    requires convertible_to<Value_t<ItT>, Value_t<OutputCont>>
    [[nodiscard]] OutputCont split_if(ItT first, ItT last, indirect_unary_predicate<ItT> auto);
    template <Container ContT>
    [[nodiscard]] Rm_cvref<ContT> split_if(ContT&&, Unary_predicate_for_cont<ContT> auto);
    template <Container OutputCont, Container ContT>
    [[nodiscard]] OutputCont split_if(ContT&&, Unary_predicate_for_cont<ContT> auto);
    template <Container ContT>
    [[nodiscard]] Rm_cvref<ContT> inplace_split_if(ContT&&, Unary_predicate_for_cont<ContT> auto);
    template <Container OutputCont, Container ContT>
    [[nodiscard]] OutputCont inplace_split_if(ContT&&, Unary_predicate_for_cont<ContT> auto);

    template <Container ContT> ContT& reverse(ContT&);
    template <Container ContT> ContT reverse(ContT&&);
    template <Container ContT> ContT reverse(const ContT&)                                 = delete;
    template <Container ContT> ContT reverse_copy(const ContT&);

    template <input_iterator ItT, indirect_binary_predicate<ItT, ItT> P = less<>>
    bool is_sorted(ItT first, ItT last, indirectly_unary_invocable<ItT> auto, P = {});
    template <Container ContT, Binary_predicate_for_cont<ContT> P = less<>>
    bool is_sorted(const ContT&, P = {});
    template <Container ContT, Binary_predicate_for_cont<ContT> P = less<>>
    bool is_sorted(const ContT&, Unary_invocable_for_cont<ContT> auto, P = {});
    template <input_iterator ItT, indirect_binary_predicate<ItT, ItT> P = less<>>
    void sort(ItT first, ItT last, indirectly_unary_invocable<ItT> auto, P = {});
    template <Container ContT, Binary_predicate_for_cont<ContT> P = less<>>
    ContT& sort(ContT&, P = {});
    template <Container ContT, Binary_predicate_for_cont<ContT> P = less<>>
    ContT sort(ContT&&, P = {});
    template <Container ContT, Binary_predicate_for_cont<ContT> P>
    ContT sort(const ContT&, P)                                                            = delete;
    template <Container ContT, Binary_predicate_for_cont<ContT> P = less<>>
    ContT& sort(ContT&, Unary_invocable_for_cont<ContT> auto, P = {});
    template <Container ContT, Binary_predicate_for_cont<ContT> P = less<>>
    ContT sort(ContT&&, Unary_invocable_for_cont<ContT> auto, P = {});
    template <Container ContT, Binary_predicate_for_cont<ContT> P>
    ContT sort(const ContT&, Unary_invocable_for_cont<ContT> auto, P)                      = delete;
    template <input_iterator ItT, indirect_binary_predicate<ItT, ItT> P = less<>>
    void stable_sort(ItT first, ItT last, indirectly_unary_invocable<ItT> auto, P = {});
    template <Container ContT, Binary_predicate_for_cont<ContT> P = less<>>
    ContT& stable_sort(ContT&, P = {});
    template <Container ContT, Binary_predicate_for_cont<ContT> P = less<>>
    ContT stable_sort(ContT&&, P = {});
    template <Container ContT, Binary_predicate_for_cont<ContT> P>
    ContT stable_sort(const ContT&, P)                                                     = delete;
    template <Container ContT, Binary_predicate_for_cont<ContT> P = less<>>
    ContT& stable_sort(ContT&, Unary_invocable_for_cont<ContT> auto, P = {});
    template <Container ContT, Binary_predicate_for_cont<ContT> P = less<>>
    ContT stable_sort(ContT&&, Unary_invocable_for_cont<ContT> auto, P = {});
    template <Container ContT, Binary_predicate_for_cont<ContT> P>
    ContT stable_sort(const ContT&, Unary_invocable_for_cont<ContT> auto, P)               = delete;
    template <Container ContT, Binary_predicate_for_cont<ContT> P = less<>>
    ContT sort_copy(const ContT&, P = {});
    template <Container ContT, Binary_predicate_for_cont<ContT> P = less<>>
    ContT sort_copy(const ContT&, Unary_invocable_for_cont<ContT> auto, P = {});

    // Using fwd. ref. to avoid ambuigity with `std::min` which has generic `const T&` args
    template <input_iterator ItT, indirect_binary_predicate<ItT, ItT> P = less<>>
    constexpr const auto& min(ItT&& first, ItT&& last, P = {});
    template <input_iterator ItT, indirectly_unary_invocable<ItT> ConvF,
              Binary_predicate_for_indirectly_invocable<ItT, ConvF> P = less<>>
    auto min(ItT first, ItT last, ConvF, P = {});
    template <Container ContT, Binary_predicate_for_cont<ContT> P = less<>>
    constexpr const auto& min(ContT&&, P = {});
    template <Container ContT, Unary_invocable_for_cont<ContT> ConvF,
              Binary_predicate_for_cont_invocable<ContT, ConvF> P = less<>>
    auto min(ContT&&, ConvF, P = {});
    template <input_iterator ItT, indirect_binary_predicate<ItT, ItT> P = less<>>
    constexpr const auto& max(ItT&& first, ItT&& last, P = {});
    template <input_iterator ItT, indirectly_unary_invocable<ItT> ConvF,
              Binary_predicate_for_indirectly_invocable<ItT, ConvF> P = less<>>
    auto max(ItT first, ItT last, ConvF, P = {});
    template <Container ContT, Binary_predicate_for_cont<ContT> P = less<>>
    constexpr const auto& max(ContT&&, P = {});
    template <Container ContT, Unary_invocable_for_cont<ContT> ConvF,
              Binary_predicate_for_cont_invocable<ContT, ConvF> P = less<>>
    auto max(ContT&&, ConvF, P = {});
}

namespace tomaqa::util {
    template <Container ContT, Binary_invocable BinF = Bin_f<Value_t<ContT>>>
    struct Container_f : Inherit<BinF> {
        using Inherit = tomaqa::Inherit<BinF>;

        using Cont = ContT;

        using Ret = typename Inherit::result_type;

        using Inherit::Inherit;

        using Inherit::operator ();
        Ret operator ()(const Cont&) const;
        Ret operator ()(const Cont&, Unary_invocable_for_cont<Cont> auto) const;
    };
}

#include "tomaqa/util/alg.inl"
