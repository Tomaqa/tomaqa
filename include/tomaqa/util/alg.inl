#pragma once

#include "tomaqa/util/numeric/alg.hpp"

namespace tomaqa::util {
    auto begin_tp(Container auto&& cont)
    {
        using Cont = decltype(cont);
        if constexpr (is_cref_v<Cont>) return cbegin(cont);
        else if constexpr (is_rref_v<Cont>) return move_iterator(begin(cont));
        else return begin(cont);
    }

    auto end_tp(Container auto&& cont)
    {
        using Cont = decltype(cont);
        if constexpr (is_cref_v<Cont>) return cend(cont);
        else if constexpr (is_rref_v<Cont>) return move_iterator(end(cont));
        else return end(cont);
    }


    template <typename F, typename WrapF>
    auto wrap(F f, WrapF wrap_f)
    {
        return [CAPTURE_RVAL2(f, wrap_f)](auto&&... args) mutable -> decltype(auto) {
            if constexpr (is_member_function_pointer_v<decltype(f)>) {
                auto tup = MAKE_TUPLE(args);
                auto car = std::move(tuple_car(tup));
                auto cdr = tuple_cdr(std::move(tup));
                return invoke(std::move(f), std::move(car),
                              apply(std::move(wrap_f), std::move(cdr)));
            }
            else return invoke(std::move(f), invoke(std::move(wrap_f), FORWARD(args))...);
        };
    }

    auto as_unary(Binary_invocable auto f, input_iterator auto first2)
    {
        //+ generalize to n-ary functions, not only binary?
        return [CAPTURE_RVAL(f), it2{first2}](auto&& elem1) mutable {
            return invoke(f, FORWARD(elem1), *it2++);
        };
    }

    template <Container ContT>
    bool all_of(const ContT& cont, Unary_predicate_for_cont<ContT> auto pred)
    {
        return std::all_of(cbegin(cont), cend(cont), std::move(pred));
    }

    template <Container ContT, input_iterator ItT2>
    bool all_of(const ContT& cont1, ItT2 first2,
                indirect_binary_predicate<Iter_t<ContT>, ItT2> auto pred)
    {
        return equal(cont1, first2, std::move(pred));
    }

    template <typename... Args>
    constexpr bool all_of(const tuple<Args...>& tup, auto pred)
    {
        bool eq = true;
        apply([&eq, CAPTURE_RVAL(pred)](auto&&... args){
            // using fold expression
            //+ lazy evaluation of `&&` but the "loop" is still not terminated
            ((eq = eq && pred(FORWARD(args))), ...);
        }, tup);
        return eq;
    }

    template <typename... Args>
    constexpr bool all_of(const tuple<Args...>& tup1, const tuple<Args...>& tup2, auto pred)
    {
        bool eq = true;
        apply([&eq, &tup2, CAPTURE_RVAL(pred)](auto&&... args1){
            return apply([&](auto&&... args2){
                // using fold expression
                //+ lazy evaluation of `&&` but the "loop" is still not terminated
                ((eq = eq && pred(FORWARD(args1), FORWARD(args2))), ...);
            }, tup2);
        }, tup1);
        return eq;
    }

    template <typename T>
    constexpr bool all_of(initializer_list<T> ils, auto pred)
    {
        return std::all_of(ils.begin(), ils.end(), std::move(pred));
    }

    template <Container ContT>
    bool any_of(const ContT& cont, Unary_predicate_for_cont<ContT> auto pred)
    {
        return std::any_of(cbegin(cont), cend(cont), std::move(pred));
    }

    template <Container ContT, input_iterator ItT2>
    bool any_of(const ContT& cont1, ItT2 first2,
                indirect_binary_predicate<Iter_t<ContT>, ItT2> auto pred)
    {
        /// \exists . x <=> \not \forall . \not x
        return !none_of(cont1, first2, std::move(pred));
    }

    template <typename... Args>
    constexpr bool any_of(const tuple<Args...>& tup, auto pred)
    {
        return !none_of(tup, std::move(pred));
    }

    template <typename... Args>
    constexpr bool any_of(const tuple<Args...>& tup1, const tuple<Args...>& tup2, auto pred)
    {
        return !none_of(tup1, tup2, std::move(pred));
    }

    template <typename T>
    constexpr bool any_of(initializer_list<T> ils, auto pred)
    {
        return std::any_of(ils.begin(), ils.end(), std::move(pred));
    }

    template <Container ContT>
    bool none_of(const ContT& cont, Unary_predicate_for_cont<ContT> auto pred)
    {
        return std::none_of(cbegin(cont), cend(cont), std::move(pred));
    }

    template <Container ContT, input_iterator ItT2>
    bool none_of(const ContT& cont1, ItT2 first2,
                 indirect_binary_predicate<Iter_t<ContT>, ItT2> auto pred)
    {
        /// \forall . \not x
        return all_of(cont1, first2, [&pred](const auto& arg1, const auto& arg2){
            return !pred(arg1, arg2);
        });
    }

    template <typename... Args>
    constexpr bool none_of(const tuple<Args...>& tup, auto pred)
    {
        return all_of(tup, [&pred](auto& arg){
            return !pred(arg);
        });
    }

    template <typename... Args>
    constexpr bool none_of(const tuple<Args...>& tup1, const tuple<Args...>& tup2, auto pred)
    {
        return all_of(tup1, tup2, [&pred](auto& arg1, auto& arg2){
            return !pred(arg1, arg2);
        });
    }

    template <typename T>
    constexpr bool none_of(initializer_list<T> ils, auto pred)
    {
        return std::none_of(ils.begin(), ils.end(), std::move(pred));
    }

    template <input_iterator ItT, indirect_binary_predicate<ItT, ItT> P>
    bool all_equal(ItT first, ItT last, P pred)
    {
        if (first == last) return true;
        const auto& e0 = *first;
        return std::all_of(++first, last, [&e0, &pred](const auto& e){
            return pred(e0, e);
        });
    }

    template <Container ContT, Binary_predicate_for_cont<ContT> P>
    bool all_equal(const ContT& cont, P pred)
    {
        return all_equal(cbegin(cont), cend(cont), std::move(pred));
    }

    template <input_iterator ItT, indirect_binary_predicate<ItT, ItT> P>
    bool any_equal(ItT first, ItT last, P pred)
    {
        for (auto it = first; it != last; ) {
            const auto& e0 = *it;
            if (std::any_of(++it, last, [&e0, &pred](const auto& e){
                return pred(e0, e);
            })) return true;
        }
        return false;
    }

    template <Container ContT, Binary_predicate_for_cont<ContT> P>
    bool any_equal(const ContT& cont, P pred)
    {
        return any_equal(cbegin(cont), cend(cont), std::move(pred));
    }

    template <input_iterator ItT, indirect_binary_predicate<ItT, ItT> P>
    bool none_equal(ItT first, ItT last, P pred)
    {
        return !any_equal(first, last, std::move(pred));
    }

    template <Container ContT, Binary_predicate_for_cont<ContT> P>
    bool none_equal(const ContT& cont, P pred)
    {
        return none_equal(cbegin(cont), cend(cont), std::move(pred));
    }

    template <Container ContT1, Container ContT2,
              indirect_binary_predicate<Iter_t<ContT1>, Iter_t<ContT2>> P>
    bool equal(const ContT1& cont1, const ContT2& cont2, P pred)
    {
        if (size(cont1) != size(cont2)) return false;
        return equal(cont1, cbegin(cont2), std::move(pred));
    }

    template <Container ContT, input_iterator ItT2,
              indirect_binary_predicate<Iter_t<ContT>, ItT2> P>
    bool equal(const ContT& cont1, ItT2 first2, P pred)
    {
        return std::equal(cbegin(cont1), cend(cont1), first2, std::move(pred));
    }

    template <typename... Args>
    constexpr bool equal(const tuple<Args...>& tup1, const tuple<Args...>& tup2, auto pred)
    {
        return all_of(tup1, tup2, std::move(pred));
    }

    auto find(Container auto&& cont, const auto& val)
    {
        if constexpr (requires { cont.find(val); })
            return cont.find(val);
        else return std::find(begin_tp(cont), end_tp(cont), val);
    }

    template <Container ContT>
    auto find_if(ContT&& cont, Unary_predicate_for_cont<ContT> auto pred)
    {
        return std::find_if(begin_tp(cont), end_tp(cont), std::move(pred));
    }

    template <Container ContT>
    auto find_if_not(ContT&& cont, Unary_predicate_for_cont<ContT> auto pred)
    {
        return std::find_if_not(begin_tp(cont), end_tp(cont), std::move(pred));
    }

    template <input_iterator ItT>
    bool contains(ItT first, ItT last, const auto& val)
    {
        return std::find(first, last, val) != last;
    }

    bool contains(const Container auto& cont, const auto& val)
    {
        if constexpr (requires { cont.contains(val); })
            return cont.contains(val);
        else return contains(cbegin(cont), cend(cont), val);
    }

    template <typename T, typename U>
    bool contains(initializer_list<U> ils, const T& val)
    {
        return contains(ils.begin(), ils.end(), val);
    }

    auto count(const Container auto& cont, const auto& val)
    {
        return std::count(cbegin(cont), cend(cont), val);
    }

    template <Container ContT>
    auto count_if(const ContT& cont, Unary_predicate_for_cont<ContT> auto pred)
    {
        return std::count_if(cbegin(cont), cend(cont), std::move(pred));
    }

    template <Container ContT, Output_iterator_for_cont<ContT> OutputIt>
    OutputIt copy(const ContT& cont, OutputIt d_first)
    {
        return std::copy(cbegin(cont), cend(cont), d_first);
    }

    template <Container ContT, Output_iterator_for_cont<ContT> OutputIt>
    OutputIt move(ContT&& cont, OutputIt d_first)
    {
        return std::move(begin_tp(cont), end_tp(cont), d_first);
    }

    template <input_iterator ItT, output_iterator<ItT> OutputIt>
    OutputIt move_n(ItT first1, integral auto n, OutputIt d_first)
    {
        return std::copy_n(move_iterator(first1), n, d_first);
    }

    template <Container ContT, Unary_invocable_for_cont<ContT> F>
    F for_each(ContT&& cont, F f)
    {
        return std::for_each(begin_tp(cont), end_tp(cont), std::move(f));
    }

    template <input_iterator ItT1, input_iterator ItT2, Indirectly_binary_invocable<ItT1, ItT2> F>
    F for_each(ItT1 first1, ItT1 last1, ItT2 first2, F f)
    {
        std::for_each(first1, last1, as_unary(f, first2));
        return f;
    }

    template <Container ContT, input_iterator ItT2, Indirectly_binary_invocable<Iter_t<ContT>, ItT2> F>
    F for_each(ContT&& cont1, ItT2 first2, F f)
    {
        return for_each(begin_tp(cont1), end_tp(cont1), first2, std::move(f));
    }

    template <input_iterator ItT1, input_iterator ItT2>
    ItT1 for_each_n(ItT1 first1, integral auto n, ItT2 first2,
                    Indirectly_binary_invocable<ItT1, ItT2> auto f)
    {
        return std::for_each_n(first1, n, as_unary(std::move(f), first2));
    }

    template <Container ContT, Unary_invocable_for_cont<ContT> ConvF,
              Output_iterator_for_cont_invocable<ContT, ConvF> OutputIt>
    OutputIt transform(ContT&& cont, OutputIt d_first, ConvF f)
    {
        return std::transform(begin_tp(cont), end_tp(cont), d_first, std::move(f));
    }

    template <Container ContT, input_iterator ItT2, Binary_invocable_for_cont<ContT> ConvF,
              Output_iterator_for_cont_invocable<ContT, ConvF> OutputIt>
     OutputIt transform(ContT&& cont1, ItT2 first2, OutputIt d_first, ConvF f)
    {
        return std::transform(begin_tp(cont1), end_tp(cont1), first2, d_first, std::move(f));
    }

    template <Container ContT, Unary_invocable_for_cont<ContT> F>
    F for_each_cdr(ContT&& cont, F f)
    {
        if (empty(cont)) return f;
        return std::for_each(++begin_tp(cont), end_tp(cont), std::move(f));
    }

    template <input_iterator ItT, indirect_unary_predicate<ItT> P>
    P for_each_while(ItT first, ItT last, P pred)
    {
        std::find_if_not(first, last, pred);
        return pred;
    }

    template <Container ContT, Unary_predicate_for_cont<ContT> P>
    P for_each_while(ContT&& cont, P pred)
    {
        find_if_not(FORWARD(cont), pred);
        return pred;
    }

    template <input_iterator ItT1, input_iterator ItT2,
              indirect_binary_predicate<ItT1, ItT2> P>
    P for_each_while(ItT1 first1, ItT1 last1, ItT2 first2, P pred)
    {
        for_each_while(first1, last1, as_unary(pred, first2));
        return pred;
    }

    template <Container ContT, input_iterator ItT2,
              indirect_binary_predicate<Iter_t<ContT>, ItT2> P>
    P for_each_while(ContT&& cont1, ItT2 first2, P pred)
    {
        return for_each_while(begin_tp(cont1), end_tp(cont1), first2, std::move(pred));
    }

    template <input_iterator ItT, indirectly_unary_invocable<ItT> F>
    F for_each_if(ItT first, ItT last, indirect_unary_predicate<ItT> auto pred, F f)
    {
        for (; first != last; ++first) {
            if (auto& e = *first; pred(e)) f(e);
        }
        return f;
    }

    template <Container ContT, Unary_invocable_for_cont<ContT> F>
    F for_each_if(ContT&& cont, indirect_unary_predicate<Iter_t<ContT>> auto pred, F f)
    {
        return for_each_if(begin_tp(cont), end_tp(cont), std::move(pred), std::move(f));
    }

    template <input_iterator ItT1, input_iterator ItT2, Indirectly_binary_invocable<ItT1, ItT2> F>
    F for_each_if(ItT1 first1, ItT1 last1, ItT2 first2, indirect_binary_predicate<ItT1, ItT2> auto pred, F f)
    {
        for (; first1 != last1; ++first1, ++first2) {
            if (auto &e1 = *first1, &e2 = *first2; pred(e1, e2)) f(e1, e2);
        }
        return f;
    }

    template <Container ContT, input_iterator ItT2, Indirectly_binary_invocable<Iter_t<ContT>, ItT2> F>
    F for_each_if(ContT&& cont1, ItT2 first2, indirect_binary_predicate<Iter_t<ContT>, ItT2> auto pred, F f)
    {
        return for_each_if(begin_tp(cont1), end_tp(cont1), first2, std::move(pred), std::move(f));
    }

    template <input_iterator ItT, indirectly_unary_invocable<ItT> UnThenF, indirectly_unary_invocable<ItT> ElseF>
    UnThenF for_each_ite(ItT first, ItT last, indirect_unary_predicate<ItT> auto pred,
                         UnThenF then_f, ElseF else_f)
    {
        for (; first != last; ++first) {
            if (auto& e = *first; pred(e)) then_f(e);
            else else_f(e);
        }
        return then_f;
    }

    template <Container ContT, Unary_invocable_for_cont<ContT> UnThenF, Unary_invocable_for_cont<ContT> ElseF>
    UnThenF for_each_ite(ContT&& cont, indirect_unary_predicate<Iter_t<ContT>> auto pred,
                         UnThenF then_f, ElseF else_f)
    {
        return for_each_ite(begin_tp(cont), end_tp(cont), std::move(pred),
                            std::move(then_f), std::move(else_f));
    }

    template <input_iterator ItT1, input_iterator ItT2, Indirectly_binary_invocable<ItT1, ItT2> ThenF,
              Indirectly_binary_invocable<ItT1, ItT2> ElseF>
    ThenF for_each_ite(ItT1 first1, ItT1 last1, ItT2 first2,
                       indirect_binary_predicate<ItT1, ItT2> auto pred, ThenF then_f, ElseF else_f)
    {
        for (; first1 != last1; ++first1, ++first2) {
            if (auto &e1 = *first1, &e2 = *first2; pred(e1, e2))
                then_f(e1, e2);
            else else_f(e1, e2);
        }
        return then_f;
    }

    template <Container ContT, input_iterator ItT2, Indirectly_binary_invocable<Iter_t<ContT>, ItT2> ThenF,
              Indirectly_binary_invocable<Iter_t<ContT>, ItT2> ElseF>
    ThenF for_each_ite(ContT&& cont1, ItT2 first2,
                       indirect_binary_predicate<Iter_t<ContT>, ItT2> auto pred, ThenF then_f, ElseF else_f)
    {
        return for_each_ite(begin_tp(cont1), end_tp(cont1), first2, std::move(pred),
                            std::move(then_f), std::move(else_f));
    }

    template <Container To, input_iterator FromIt, indirectly_unary_invocable<FromIt> ConvF>
    To to(FromIt first, FromIt last, integral auto size_, ConvF conv)
    {
        To t;
        to(t, first, last, size_, std::move(conv));
        return t;
    }

    template <Container To, input_iterator FromIt, indirectly_unary_invocable<FromIt> ConvF>
    To to(FromIt first, FromIt last, ConvF conv)
    {
        To t;
        to(t, first, last, std::move(conv));
        return t;
    }

    template <Container To, Container From, Unary_invocable_for_cont<From> ConvF>
    To to(From&& from, integral auto size_, ConvF conv)
    {
        To t;
        to(t, FORWARD(from), size_, std::move(conv));
        return t;
    }

    template <Container To, Container From, Unary_invocable_for_cont<From> ConvF>
    To to(From&& from, ConvF conv)
    {
        To t;
        to(t, FORWARD(from), std::move(conv));
        return t;
    }

    namespace aux {
        void to_impl(auto& t, auto first, auto last, auto conv)
        {
            std::transform(first, last, back_inserter(t), [&conv](auto&& e){
                return conv(FORWARD(e));
            });
        }
    }

    template <input_iterator FromIt, indirectly_unary_invocable<FromIt> ConvF>
    void to(Container auto& t, FromIt first, FromIt last, integral auto size_, ConvF conv)
    {
        if (size_ == 0) return;
        t.reserve(size_);
        aux::to_impl(t, first, last, std::move(conv));
    }

    template <input_iterator FromIt, indirectly_unary_invocable<FromIt> ConvF>
    void to(Container auto& t, FromIt first, FromIt last, ConvF conv)
    {
        if constexpr (requires { t.reserve(1); }) {
            const long size_ = last - first;
            return to(t, first, last, size_, std::move(conv));
        }
        aux::to_impl(t, first, last, std::move(conv));
    }

    namespace aux {
        void to_impl(auto& t, auto&& from, auto conv)
        {
            transform(FORWARD(from), back_inserter(t), [&conv](auto&& e){
                return conv(FORWARD(e));
            });
        }
    }

    template <Container From, Unary_invocable_for_cont<From> ConvF>
    void to(Container auto& t, From&& from, integral auto size_, ConvF conv)
    {
        if (size_ == 0) return;
        t.reserve(size_);
        aux::to_impl(t, FORWARD(from), std::move(conv));
    }

    template <Container From, Unary_invocable_for_cont<From> ConvF>
    void to(Container auto& t, From&& from, ConvF conv)
    {
        if constexpr (requires { t.reserve(1); }) {
            const size_t size_ = size(t) + size(from);
            return to(t, FORWARD(from), size_, std::move(conv));
        }
        aux::to_impl(t, FORWARD(from), std::move(conv));
    }

    template <Container To, input_iterator FromIt1, input_iterator FromIt2, Indirectly_binary_invocable<FromIt1, FromIt2> ConvF>
    To to(FromIt1 first1, FromIt1 last1, FromIt2 first2, integral auto size_, ConvF conv)
    {
        To t;
        to(t, first1, last1, first2, size_, std::move(conv));
        return t;
    }

    template <Container To, input_iterator FromIt1, input_iterator FromIt2, Indirectly_binary_invocable<FromIt1, FromIt2> ConvF>
    To to(FromIt1 first1, FromIt1 last1, FromIt2 first2, ConvF conv)
    {
        To t;
        to(t, first1, last1, first2, std::move(conv));
        return t;
    }

    template <Container To, Container From1, input_iterator FromIt2, Indirectly_binary_invocable<Iter_t<From1>, FromIt2> ConvF>
    To to(From1&& from1, FromIt2 first2, integral auto size_, ConvF conv)
    {
        To t;
        to(t, FORWARD(from1), first2, size_, std::move(conv));
        return t;
    }

    template <Container To, Container From1, input_iterator FromIt2, Indirectly_binary_invocable<Iter_t<From1>, FromIt2> ConvF>
    To to(From1&& from1, FromIt2 first2, ConvF conv)
    {
        To t;
        to(t, FORWARD(from1), first2, std::move(conv));
        return t;
    }

    namespace aux {
        void to_impl(auto& t, auto first1, auto last1, auto first2, auto conv)
        {
            std::transform(first1, last1, first2, back_inserter(t), [&conv](auto&& e1, auto&& e2){
                return conv(FORWARD(e1), FORWARD(e2));
            });
        }
    }

    template <input_iterator FromIt1, input_iterator FromIt2, Indirectly_binary_invocable<FromIt1, FromIt2> ConvF>
    void to(Container auto& t, FromIt1 first1, FromIt1 last1, FromIt2 first2, integral auto size_, ConvF conv)
    {
        if (size_ == 0) return;
        t.reserve(size_);
        aux::to_impl(t, first1, last1, first2, std::move(conv));
    }

    template <input_iterator FromIt1, input_iterator FromIt2, Indirectly_binary_invocable<FromIt1, FromIt2> ConvF>
    void to(Container auto& t, FromIt1 first1, FromIt1 last1, FromIt2 first2, ConvF conv)
    {
        if constexpr (requires { t.reserve(1); }) {
            const long size_ = last1 - first1;
            return to(t, first1, last1, first2, size_, std::move(conv));
        }
        aux::to_impl(t, first1, last1, first2, std::move(conv));
    }

    namespace aux {
        void to_impl(auto& t, auto&& from1, auto first2, auto conv)
        {
            transform(FORWARD(from1), first2, back_inserter(t), [&conv](auto&& e1, auto&& e2){
                return conv(FORWARD(e1), FORWARD(e2));
            });
        }
    }

    template <Container From1, input_iterator FromIt2, Indirectly_binary_invocable<Iter_t<From1>, FromIt2> ConvF>
    void to(Container auto& t, From1&& from1, FromIt2 first2, integral auto size_, ConvF conv)
    {
        if (size_ == 0) return;
        t.reserve(size_);
        aux::to_impl(t, FORWARD(from1), first2, std::move(conv));
    }

    template <Container From1, input_iterator FromIt2, Indirectly_binary_invocable<Iter_t<From1>, FromIt2> ConvF>
    void to(Container auto& t, From1&& from1, FromIt2 first2, ConvF conv)
    {
        if constexpr (requires { t.reserve(1); }) {
            const size_t size_ = size(t) + size(from1);
            return to(t, FORWARD(from1), first2, size_, std::move(conv));
        }
        aux::to_impl(t, FORWARD(from1), first2, std::move(conv));
    }

    template <Container OutputCont, input_iterator ItT2>
    OutputCont& append(OutputCont& cont1, ItT2 first2, ItT2 last2,
                       [[maybe_unused]] integral auto size_)
    {
        if constexpr (constructible_from<OutputCont, ItT2, ItT2>) {
            if (empty(cont1)) {
                return cont1 = OutputCont(first2, last2);
            }
        }

        if constexpr (requires { cont1.reserve(1); })
            cont1.reserve(size(cont1) + size_);
        std::copy(first2, last2, back_inserter(cont1));
        return cont1;
    }

    template <Container OutputCont, input_iterator ItT2>
    OutputCont& append(OutputCont& cont1, ItT2 first2, ItT2 last2)
    {
        const long size_ = last2 - first2;
        return append(cont1, first2, last2, size_);
    }

    template <Container OutputCont>
    OutputCont& append(OutputCont& cont1, Container auto&& cont2)
    {
        return append(cont1, begin_tp(cont2), end_tp(cont2), size(cont2));
    }

    template <Container OutputCont, input_iterator ItT2>
    OutputCont cat(OutputCont cont1, ItT2 first2, ItT2 last2, integral auto size_)
    {
        return append(cont1, first2, last2, size_);
    }

    template <Container OutputCont, input_iterator ItT2>
    OutputCont cat(OutputCont cont1, ItT2 first2, ItT2 last2)
    {
        return append(cont1, first2, last2);
    }

    template <Container OutputCont>
    OutputCont cat(OutputCont cont1, Container auto&& cont2)
    {
        return append(cont1, FORWARD(cont2));
    }

    template <input_iterator ItT1, Container ContT2>
    Rm_cvref<ContT2> cat(ItT1 first1, ItT1 last1, integral auto size_, ContT2&& cont2)
    {
        using Output_cont = Rm_ref<ContT2>;
        Output_cont out;
        if constexpr (requires { out.reserve(1); })
            out.reserve(size_ + size(cont2));
        out.assign(first1, last1);
        return append(out, FORWARD(cont2));
    }

    template <input_iterator ItT1, Container ContT2>
    Rm_cvref<ContT2> cat(ItT1 first1, ItT1 last1, ContT2&& cont2)
    {
        const long size_ = last1 - first1;
        return cat(first1, last1, size_, FORWARD(cont2));
    }

    template <Container OutputCont, input_iterator ItT>
    requires convertible_to<Value_t<ItT>, Value_t<OutputCont>>
    OutputCont split_if(ItT first, ItT last, indirect_unary_predicate<ItT> auto pred)
    {
        OutputCont d_cont;
        if (first == last) return d_cont;
        auto first2 = first;
        for (++first2; first2 != last; ++first2) {
            if (!pred(*first2)) continue;
            d_cont.emplace_back(move_iterator(first), move_iterator(first2));
            first = first2;
        }
        if (first != first2) {
            d_cont.emplace_back(move_iterator(first), move_iterator(first2));
        }
        return d_cont;
    }

    template <Container ContT>
    Rm_cvref<ContT> split_if(ContT&& cont, Unary_predicate_for_cont<ContT> auto pred)
    {
        return split_if<Rm_cvref<ContT>, ContT>(FORWARD(cont), std::move(pred));
    }

    template <Container OutputCont, Container ContT>
    OutputCont split_if(ContT&& cont, Unary_predicate_for_cont<ContT> auto pred)
    {
        return split_if<OutputCont>(begin(cont), end(cont), std::move(pred));
    }

    template <Container ContT>
    Rm_cvref<ContT> inplace_split_if(ContT&& cont, Unary_predicate_for_cont<ContT> auto pred)
    {
        return inplace_split_if<Rm_cvref<ContT>, ContT>(FORWARD(cont), std::move(pred));
    }

    template <Container OutputCont, Container ContT>
    OutputCont inplace_split_if(ContT&& cont, Unary_predicate_for_cont<ContT> auto pred)
    {
        if (empty(cont)) return {};
        auto last = end(cont);
        auto first = std::find_if(++begin(cont), last, std::move(pred));
        OutputCont d_cont = split_if<OutputCont>(first, last, std::move(pred));
        cont.erase(first, last);
        return d_cont;
    }

    template <Container ContT>
    ContT& reverse(ContT& cont)
    {
        std::reverse(begin(cont), end(cont));
        return cont;
    }

    template <Container ContT>
    ContT reverse(ContT&& cont)
    {
        return std::move(reverse(cont));
    }

    template <Container ContT>
    ContT reverse_copy(const ContT& cont)
    {
        ContT d_cont;
        if constexpr (requires { d_cont.reserve(1); }) d_cont.reserve(size(cont));
        std::reverse_copy(cbegin(cont), cend(cont), back_inserter(d_cont));
        return d_cont;
    }

    template <input_iterator ItT, indirect_binary_predicate<ItT, ItT> P>
    bool is_sorted(ItT first, ItT last, indirectly_unary_invocable<ItT> auto conv, P pred)
    {
        return std::is_sorted(first, last, [&conv, &pred](const auto& e1, const auto& e2){
            return pred(conv(e1), conv(e2));
        });
    }

    template <Container ContT, Binary_predicate_for_cont<ContT> P>
    bool is_sorted(const ContT& cont, P pred)
    {
        return std::is_sorted(cbegin(cont), cend(cont), std::move(pred));
    }

    template <Container ContT, Binary_predicate_for_cont<ContT> P>
    bool is_sorted(const ContT& cont, Unary_invocable_for_cont<ContT> auto conv, P pred)
    {
        return is_sorted(cbegin(cont), cend(cont), std::move(conv), std::move(pred));
    }

    template <input_iterator ItT, indirect_binary_predicate<ItT, ItT> P>
    void sort(ItT first, ItT last, indirectly_unary_invocable<ItT> auto conv, P pred)
    {
        std::sort(first, last, [&conv, &pred](const auto& e1, const auto& e2){
            return pred(conv(e1), conv(e2));
        });
    }

    template <Container ContT, Binary_predicate_for_cont<ContT> P>
    ContT& sort(ContT& cont, P pred)
    {
        std::sort(begin(cont), end(cont), std::move(pred));
        return cont;
    }

    template <Container ContT, Binary_predicate_for_cont<ContT> P>
    ContT sort(ContT&& cont, P pred)
    {
        return std::move(sort(cont, std::move(pred)));
    }

    template <Container ContT, Binary_predicate_for_cont<ContT> P>
    ContT& sort(ContT& cont, Unary_invocable_for_cont<ContT> auto conv, P pred)
    {
        sort(begin(cont), end(cont), std::move(conv), std::move(pred));
        return cont;
    }

    template <Container ContT, Binary_predicate_for_cont<ContT> P>
    ContT sort(ContT&& cont, Unary_invocable_for_cont<ContT> auto conv, P pred)
    {
        return std::move(sort(cont, std::move(conv), std::move(pred)));
    }

    template <input_iterator ItT, indirect_binary_predicate<ItT, ItT> P>
    void stable_sort(ItT first, ItT last, indirectly_unary_invocable<ItT> auto conv, P pred)
    {
        std::stable_sort(first, last, [&conv, &pred](const auto& e1, const auto& e2){
            return pred(conv(e1), conv(e2));
        });
    }

    template <Container ContT, Binary_predicate_for_cont<ContT> P>
    ContT& stable_sort(ContT& cont, P pred)
    {
        std::stable_sort(begin(cont), end(cont), std::move(pred));
        return cont;
    }

    template <Container ContT, Binary_predicate_for_cont<ContT> P>
    ContT stable_sort(ContT&& cont, P pred)
    {
        return std::move(stable_sort(cont, std::move(pred)));
    }

    template <Container ContT, Binary_predicate_for_cont<ContT> P>
    ContT& stable_sort(ContT& cont, Unary_invocable_for_cont<ContT> auto conv, P pred)
    {
        stable_sort(begin(cont), end(cont), std::move(conv), std::move(pred));
        return cont;
    }

    template <Container ContT, Binary_predicate_for_cont<ContT> P>
    ContT stable_sort(ContT&& cont, Unary_invocable_for_cont<ContT> auto conv, P pred)
    {
        return std::move(stable_sort(cont, std::move(conv), std::move(pred)));
    }

    template <Container ContT, Binary_predicate_for_cont<ContT> P>
    ContT sort_copy(const ContT& cont, P pred)
    {
        ContT d_cont(cont);
        return sort(std::move(d_cont), std::move(pred));
    }

    template <Container ContT, Binary_predicate_for_cont<ContT> P>
    ContT sort_copy(const ContT& cont, Unary_invocable_for_cont<ContT> auto conv, P pred)
    {
        ContT d_cont(cont);
        return sort(std::move(d_cont), std::move(conv), std::move(pred));
    }

    template <input_iterator ItT, indirect_binary_predicate<ItT, ItT> P>
    constexpr const auto& min(ItT&& first, ItT&& last, P pred)
    {
        return *std::min_element(first, last, std::move(pred));
    }

    namespace aux {
        template <bool minV, input_iterator ItT, indirectly_unary_invocable<ItT> ConvF,
                  Binary_predicate_for_indirectly_invocable<ItT, ConvF> P>
        auto minmax_impl(ItT first, ItT last, ConvF conv, P pred)
        {
            expect(first != last, "Cannot take min/max from empty range.");
            auto minmax_v = conv(FORWARD(*first));

            const auto cond_f = [&pred, &minmax_v](auto& v){
                if constexpr (minV) return pred(v, minmax_v);
                else return pred(minmax_v, v);
            };

            ++first;
            for (; first != last; ++first) {
                auto v = conv(FORWARD(*first));
                if (cond_f(v)) {
                    minmax_v = std::move(v);
                }
            }
            return minmax_v;
        }
    }

    template <input_iterator ItT, indirectly_unary_invocable<ItT> ConvF,
              Binary_predicate_for_indirectly_invocable<ItT, ConvF> P>
    auto min(ItT first, ItT last, ConvF conv, P pred)
    {
        return aux::minmax_impl<true>(first, last, std::move(conv), std::move(pred));
    }

    template <Container ContT, Binary_predicate_for_cont<ContT> P>
    constexpr const auto& min(ContT&& cont, P pred)
    {
        return min(begin(cont), end(cont), std::move(pred));
    }

    template <Container ContT, Unary_invocable_for_cont<ContT> ConvF,
              Binary_predicate_for_cont_invocable<ContT, ConvF> P>
    auto min(ContT&& cont, ConvF conv, P pred)
    {
        return FORWARD(min(begin(cont), end(cont), std::move(conv), std::move(pred)));
    }

    template <input_iterator ItT, indirect_binary_predicate<ItT, ItT> P>
    constexpr const auto& max(ItT&& first, ItT&& last, P pred)
    {
        return *std::max_element(first, last, std::move(pred));
    }

    template <input_iterator ItT, indirectly_unary_invocable<ItT> ConvF,
              Binary_predicate_for_indirectly_invocable<ItT, ConvF> P>
    auto max(ItT first, ItT last, ConvF conv, P pred)
    {
        return aux::minmax_impl<false>(first, last, std::move(conv), std::move(pred));
    }

    template <Container ContT, Binary_predicate_for_cont<ContT> P>
    constexpr const auto& max(ContT&& cont, P pred)
    {
        return max(begin(cont), end(cont), std::move(pred));
    }

    template <Container ContT, Unary_invocable_for_cont<ContT> ConvF,
              Binary_predicate_for_cont_invocable<ContT, ConvF> P>
    auto max(ContT&& cont, ConvF conv, P pred)
    {
        return FORWARD(max(begin(cont), end(cont), std::move(conv), std::move(pred)));
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace tomaqa::util {
    template <Container ContT, Binary_invocable BinF>
    typename Container_f<ContT, BinF>::Ret
    Container_f<ContT, BinF>::operator ()(const Cont& cont) const
    {
        using util::accumulate;
        return accumulate(++cbegin(cont), cend(cont), cont.front(), *this);
    }

    template <Container ContT, Binary_invocable BinF>
    typename Container_f<ContT, BinF>::Ret
    Container_f<ContT, BinF>::operator ()(const Cont& cont,
                                          Unary_invocable_for_cont<Cont> auto conv) const
    {
        using util::accumulate;
        return accumulate(++cbegin(cont), cend(cont), conv(cont.front()),
                          [this, &conv](auto&& lhs, auto&& rhs) -> Ret {
            return invoke(*this, FORWARD(lhs), conv(FORWARD(rhs)));
        });
    }
}
