#pragma once

#include "tomaqa/util/alg.hpp"

namespace tomaqa::util {
    template <typename ContT, typename UnF, typename BreakPred = False_pred>
        UnF for_each_rec_pre(ContT&&, UnF, BreakPred = false_pred);
    template <typename ContT, typename UnF, typename BreakPred = False_pred>
        UnF for_each_rec_post(ContT&&, UnF, BreakPred = false_pred);

    template <typename ContT, typename UnPred, typename UnF,
              typename ConvRecF, typename BreakPred = False_pred>
        UnF for_each_if_rec_pre(ContT&&, UnPred, UnF, ConvRecF,
                                BreakPred = false_pred);
    template <typename ContT, typename UnPred, typename UnF,
              typename ConvRecF, typename BreakPred = False_pred>
        UnF for_each_if_rec_post(ContT&&, UnPred, UnF, ConvRecF,
                                 BreakPred = false_pred);

    template <typename ContT, typename UnPred, typename UnThenF,
              typename ConvRecF, typename UnElseF,
              typename BreakPred = False_pred>
        UnThenF for_each_ite_rec_pre(ContT&&, UnPred, UnThenF, ConvRecF,
                                     UnElseF, BreakPred = false_pred);
    template <typename ContT, typename UnPred,
              typename ConvRecF, typename UnElseF,
              typename BreakPred = False_pred>
        void for_each_else_rec_pre(ContT&&, UnPred, ConvRecF, UnElseF,
                                   BreakPred = false_pred);
    template <typename ContT, typename UnPred, typename UnThenF,
              typename ConvRecF, typename UnElseF,
              typename BreakPred = False_pred>
        UnThenF for_each_ite_rec_post(ContT&&, UnPred, UnThenF, ConvRecF,
                                      UnElseF, BreakPred = false_pred);
    template <typename ContT, typename UnPred,
              typename ConvRecF, typename UnElseF,
              typename BreakPred = False_pred>
        void for_each_else_rec_post(ContT&&, UnPred, ConvRecF, UnElseF,
                                    BreakPred = false_pred);
}

#include "tomaqa/util/alg/rec.inl"
