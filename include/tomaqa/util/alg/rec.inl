#pragma once

namespace tomaqa::util {
    template <typename ContT, typename UnF, typename BreakPred>
    UnF for_each_rec_pre(ContT&& cont, UnF f, BreakPred break_pred)
    {
        for_each(FORWARD(cont), [&f, &break_pred](auto&& e){
            f(e);
            if (break_pred(e)) return;
            for_each_rec_pre(e, f, break_pred);
        });
        return f;
    }

    template <typename ContT, typename UnF, typename BreakPred>
    UnF for_each_rec_post(ContT&& cont, UnF f, BreakPred break_pred)
    {
        for_each(FORWARD(cont), [&f, &break_pred](auto&& e){
            for_each_rec_post(e, f, break_pred);
            f(e);
            if (break_pred(e)) return;
        });
        return f;
    }

    template <typename ContT, typename UnPred, typename UnF,
              typename ConvRecF, typename BreakPred>
    UnF for_each_if_rec_pre(ContT&& cont, UnPred pred, UnF f,
                            ConvRecF conv_rec, BreakPred break_pred)
    {
        for_each_if(FORWARD(cont), pred, [&](auto&& e){
            f(e);
            if (!pred(e) || break_pred(e)) return;
            for_each_if_rec_pre(conv_rec(FORWARD(e)),
                                pred, f, conv_rec, break_pred);
        });
        return f;
    }

    template <typename ContT, typename UnPred, typename UnF,
              typename ConvRecF, typename BreakPred>
    UnF for_each_if_rec_post(ContT&& cont, UnPred pred, UnF f,
                             ConvRecF conv_rec, BreakPred break_pred)
    {
        for_each_if(FORWARD(cont), pred, [&](auto&& e){
            for_each_if_rec_post(conv_rec(e), pred, f, conv_rec, break_pred);
            f(e);
            if (break_pred(e)) return;
        });
        return f;
    }

    template <typename ContT, typename UnPred, typename UnThenF,
              typename ConvRecF, typename UnElseF, typename BreakPred>
    UnThenF for_each_ite_rec_pre(ContT&& cont,
                                 UnPred pred, UnThenF then_f,
                                 ConvRecF conv_rec, UnElseF else_f,
                                 BreakPred break_pred)
    {
        auto then_f_ = [&](auto&& e){
            then_f(e);
            if (!pred(e) || break_pred(e)) return;
            for_each_ite_rec_pre(conv_rec(FORWARD(e)), pred, then_f,
                                 conv_rec, else_f, break_pred);
        };
        for_each_ite(FORWARD(cont), pred, std::move(then_f_), else_f);
        return then_f;
    }

    template <typename ContT, typename UnPred,
              typename ConvRecF, typename UnElseF, typename BreakPred>
    void for_each_else_rec_pre(ContT&& cont, UnPred pred,
                               ConvRecF conv_rec, UnElseF else_f,
                               BreakPred break_pred)
    {
        auto then_f = [&](auto&& e){
            if (break_pred(e)) return;
            for_each_else_rec_pre(conv_rec(FORWARD(e)), pred,
                                  conv_rec, else_f, break_pred);
        };
        for_each_ite(FORWARD(cont), pred, std::move(then_f), else_f);
    }

    template <typename ContT, typename UnPred, typename UnThenF,
              typename ConvRecF, typename UnElseF, typename BreakPred>
    UnThenF for_each_ite_rec_post(ContT&& cont,
                                  UnPred pred, UnThenF then_f,
                                  ConvRecF conv_rec, UnElseF else_f,
                                  BreakPred break_pred)
    {
        auto then_f_ = [&](auto&& e){
            for_each_ite_rec_post(conv_rec(e), pred, then_f,
                                  conv_rec, else_f, break_pred);
            then_f(e);
            if (break_pred(e)) return;
        };
        for_each_ite(FORWARD(cont), pred, std::move(then_f_), else_f);
        return then_f;
    }

    template <typename ContT, typename UnPred,
              typename ConvRecF, typename UnElseF, typename BreakPred>
    void for_each_else_rec_post(ContT&& cont, UnPred pred,
                                ConvRecF conv_rec, UnElseF else_f,
                                BreakPred break_pred)
    {
        auto then_f = [&](auto&& e){
            for_each_else_rec_post(conv_rec(e), pred, conv_rec, else_f,
                                   break_pred);
            if (break_pred(e)) return;
        };
        for_each_ite(FORWARD(cont), pred, std::move(then_f), else_f);
    }
}
