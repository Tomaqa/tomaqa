#pragma once

#include "tomaqa/util.hpp"

#include <array>

namespace tomaqa::util {
    using std::array;

    using std::to_array;

    template <typename T, size_t n1, size_t n2>
    constexpr array<T, n1+n2> array_cat(const array<T, n1>&, const array<T, n2>&);

    template <typename T, size_t nV>
    String to_string(const array<T, nV>&);
}

#include "tomaqa/util/array.inl"
