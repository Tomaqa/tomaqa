#pragma once

namespace tomaqa::util {
    template <typename T, size_t n1, size_t n2>
    constexpr array<T, n1+n2> array_cat(const array<T, n1>& lhs, const array<T, n2>& rhs)
    {
        array<T, n1+n2> res{};
        int j = 0;
        for (Idx lsize = lhs.size(), i = 0; i < lsize; ++i, ++j)
            res[j] = lhs[i];
        for (Idx rsize = rhs.size(), i = 0; i < rsize; ++i, ++j)
            res[j] = rhs[i];
        return res;
    }

    template <typename T, size_t nV>
    String to_string(const array<T, nV>& ary)
    {
        using tomaqa::to_string;
        String str;
        for (const auto& e : ary) {
            str += to_string(e) + " ";
        }
        return str;
    }
}
