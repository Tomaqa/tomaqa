#pragma once

#include "tomaqa/util.hpp"

#include "tomaqa/util/optional.hpp"

namespace tomaqa::util {
    template <Container B, typename = Value_t<B>> class Associative_mixin;
    template <Container> class Map_mixin;
}

namespace tomaqa::util {
    template <Container B, typename ValueT>
    class Associative_mixin : public Inherit<B> {
    public:
        using Inherit = tomaqa::Inherit<B>;
        using Base = typename Inherit::Parent;

        using Key = typename Inherit::key_type;
        using Value = ValueT;

        using Inherit::Inherit;
        using Base::operator =;

        using Base::insert;
        void insert(const Base&);
        void insert(Base&&);
    };

    template <Container B>
    class Map_mixin : public Inherit<Associative_mixin<B, typename B::mapped_type>> {
    public:
        using Inherit = tomaqa::Inherit<Associative_mixin<B, typename B::mapped_type>>;
        using typename Inherit::Base;

        using typename Inherit::Key;
        using typename Inherit::Value;
        using Pair = typename Inherit::value_type;

        using Inherit::Inherit;
        using Inherit::Parent::operator =;

        const Value& cat(const Key&) const;
        const Value& at(const Key& key) const                                   { return cat(key); }
        Value& at(const Key&);

        using Inherit::operator [];
        const Value& operator [](const Key&) const;

        using Inherit::find;
        const Value* cfind_l(const Key&) const;
        Value* find_l(const Key&);

        using Inherit::extract;
        Optional<Value> extract_value(const Key&) noexcept;
    };
}

#include "tomaqa/util/associative.inl"
