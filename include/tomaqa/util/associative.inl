#pragma once

namespace tomaqa::util {
    template <Container B, typename ValueT>
    void Associative_mixin<B, ValueT>::insert(const Base& rhs)
    {
        insert(rhs.cbegin(), rhs.cend());
    }

    template <Container B, typename ValueT>
    void Associative_mixin<B, ValueT>::insert(Base&& rhs)
    {
        insert(move_iterator(rhs.begin()), move_iterator(rhs.end()));
    }

    ////////////////////////////////////////////////////////////////

    template <Container B>
    const typename Map_mixin<B>::Value& Map_mixin<B>::cat(const Key& key) const
    try {
        return Inherit::at(key);
    }
    catch (const std::out_of_range&) {
        THROW("Key not found: ") + to_string(key);
    }

    template <Container B>
    typename Map_mixin<B>::Value& Map_mixin<B>::at(const Key& key)
    {
        return const_cast<Value&>(cat(key));
    }

    template <Container B>
    const typename Map_mixin<B>::Value&
    Map_mixin<B>::operator [](const Key& key) const
    {
        assert(!_prefer_assert_over_throw_ || this->contains(key));
        return cat(key);
    }

    template <Container B>
    const typename Map_mixin<B>::Value* Map_mixin<B>::cfind_l(const Key& key) const
    {
        auto it = find(key);
        if (it == this->end()) return nullptr;
        return &it->second;
    }

    template <Container B>
    typename Map_mixin<B>::Value* Map_mixin<B>::find_l(const Key& key)
    {
        return const_cast<Value*>(cfind_l(key));
    }

    template <Container B>
    Optional<typename Map_mixin<B>::Value>
    Map_mixin<B>::extract_value(const Key& key) noexcept
    {
        auto node_handle = extract(key);
        if (empty(node_handle)) return {};
        return move(node_handle.mapped());
    }
}
