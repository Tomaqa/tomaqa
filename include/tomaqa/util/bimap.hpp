#pragma once

#include "tomaqa/util.hpp"

namespace tomaqa::util {
    template <typename Key1T, typename Key2T,
              template<typename...> typename Map1T, template<typename...> typename Map2T = Map1T>
    class Bimap;
}

#include "tomaqa/util/bimap/detail.hpp"

namespace tomaqa::util {
    template <typename Key1T, typename Key2T,
              template<typename...> typename Map1T, template<typename...> typename Map2T>
    class Bimap
        : public Inherit<detail::Bimap<Key1T, Key2T, Map1T, Map2T,
                                       convertible_to<Key1T, Key2T>, convertible_to<Key2T, Key1T>>,
                         Bimap<Key1T, Key2T, Map1T, Map2T>> {
    public:
        using Inherit =
            tomaqa::Inherit<detail::Bimap<Key1T, Key2T, Map1T, Map2T,
                                         convertible_to<Key1T, Key2T>, convertible_to<Key2T, Key1T>>,
                            Bimap<Key1T, Key2T, Map1T, Map2T>>;
        using typename Inherit::This;

        using Inherit::Inherit;
    };
}

#include "tomaqa/util/bimap.inl"
