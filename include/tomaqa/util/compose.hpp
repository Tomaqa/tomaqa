#pragma once

#include "tomaqa/util.hpp"

namespace tomaqa::util {
    template <typename T>
    concept Composable = requires (T t, ostream& os) { {t.compose(os)} -> same_as<ostream&>; };

    template <Composable> struct Compose_proxy;
}

namespace tomaqa::util {
    template <Composable T>
    struct Compose_proxy {
        const T& t;

        friend ostream& operator <<(ostream& os, const Compose_proxy& rhs)
                                                                       { return rhs.t.compose(os); }
    };
}
