#pragma once

#include "tomaqa/util/optional.hpp"

namespace tomaqa::util {
    class Flag;
}

namespace tomaqa::util {
    class Flag : public Inherit<Optional<bool>, Flag> {
    public:
        static constexpr int true_value = true;
        static constexpr int false_value = false;
        static constexpr int unknown_value = -1;
        static constexpr int invalid_value = unknown_value;

        using Inherit::Inherit;
        using Inherit::Parent::operator =;
        Flag()                                                                            = default;
        constexpr Flag(int) noexcept;
        Flag& operator =(int) noexcept;

        /// Beware, `is_true == !is_false' does *not* hold!
        constexpr bool is_true() const noexcept                      { return valid() && cvalue(); }
        constexpr bool is_false() const noexcept                    { return valid() && !cvalue(); }

        Flag flip() const& noexcept;
        Flag& flip()& noexcept;
        Flag&& flip()&& noexcept                                            { return move(flip()); }

        constexpr operator bool() const noexcept                               { return is_true(); }
        constexpr bool operator !() const noexcept                      { return !operator bool(); }
        explicit inline operator int() const noexcept;
        template <typename T> operator T() const                                           = delete;

        inline Flag operator &(Flag) const noexcept;
        inline Flag operator |(Flag) const noexcept;
        inline Flag operator ^(Flag) const noexcept;
        Flag& operator &=(Flag) noexcept;
        Flag& operator |=(Flag) noexcept;
        Flag& operator ^=(Flag) noexcept;

        using Inherit::equals;
        constexpr bool equals(const This&) const noexcept;
        constexpr bool equals(int) const noexcept;
    };
}

namespace tomaqa::util {
    Flag operator &(Flag, Integral auto) noexcept;
    Flag operator |(Flag, Integral auto) noexcept;
    Flag operator ^(Flag, Integral auto) noexcept;
    Flag operator &(Integral auto, Flag) noexcept;
    Flag operator |(Integral auto, Flag) noexcept;
    Flag operator ^(Integral auto, Flag) noexcept;
}

#include "tomaqa/util/flag.inl"

namespace tomaqa::util {
    constexpr Flag true_flag{true};
    constexpr Flag false_flag{false};
    constexpr Flag invalid_flag;
    constexpr Flag unknown;
}
