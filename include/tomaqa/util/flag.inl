#pragma once

namespace tomaqa::util {
    constexpr Flag::Flag(int val) noexcept
        : Flag((val == unknown_value) ? Flag() : Flag(bool(val)))
    { }

    Flag::operator int() const noexcept
    {
        return (unknown()) ? unknown_value : int(cvalue());
    }

    Flag Flag::operator &(Flag rhs) const noexcept
    {
        return rhs &= *this;
    }

    Flag Flag::operator |(Flag rhs) const noexcept
    {
        return rhs |= *this;
    }

    Flag Flag::operator ^(Flag rhs) const noexcept
    {
        return rhs ^= *this;
    }

    constexpr bool Flag::equals(const This& rhs) const noexcept
    {
        return Inherit::equals(rhs);
    }

    constexpr bool Flag::equals(int val) const noexcept
    {
        return equals(Flag(val));
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace tomaqa::util {
    Flag operator &(Flag flag, Integral auto x) noexcept
    {
        return flag & Flag(x);
    }

    Flag operator |(Flag flag, Integral auto x) noexcept
    {
        return flag | Flag(x);
    }

    Flag operator ^(Flag flag, Integral auto x) noexcept
    {
        return flag ^ Flag(x);
    }

    Flag operator &(Integral auto x, Flag flag) noexcept
    {
        return move(flag) & x;
    }

    Flag operator |(Integral auto x, Flag flag) noexcept
    {
        return move(flag) | x;
    }

    Flag operator ^(Integral auto x, Flag flag) noexcept
    {
        return move(flag) ^ x;
    }
}
