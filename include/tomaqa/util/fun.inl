#pragma once

namespace tomaqa::util {
    decltype(auto) bind_this(Member_function_pointer auto f, Class auto&& this_, auto&&... args)
    {
        return bind_front(f, FORWARD(this_), FORWARD(args)...);
    }
}
