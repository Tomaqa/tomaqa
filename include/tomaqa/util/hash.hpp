#pragma once

#include "tomaqa/util/associative.hpp"

#include <unordered_set>
#include <unordered_map>

namespace tomaqa::util {
    template<typename T, typename H>
    concept Hashable_with = Invocable_r<H&, size_t, T>;
    template<typename T> concept Hashable = Hashable_with<T, std::hash<T>>;

    template <Hashable KeyT, typename ValueT = void, invocable<KeyT> = std::hash<KeyT>>
    class Hash;

    //+ template <typename> class Hash_object;
}

namespace tomaqa::util {
    template <Hashable KeyT, typename ValueT, invocable<KeyT> HashT>
    class Hash : public Inherit<Map_mixin<std::unordered_map<KeyT, ValueT, HashT>>,
                                Hash<KeyT, ValueT, HashT>> {
    public:
        using Inherit = tomaqa::Inherit<Map_mixin<std::unordered_map<KeyT, ValueT, HashT>>,
                                       Hash<KeyT, ValueT, HashT>>;
        using typename Inherit::This;
        using typename Inherit::Base;

        using Inherit::Inherit;
        using Inherit::Parent::operator =;

        String to_string() const;
    };

    //+ cannot select custom hash function
    template <Hashable KeyT>
    class Hash<KeyT> : public Inherit<Associative_mixin<std::unordered_set<KeyT>>, Hash<KeyT>> {
    public:
        using Inherit = tomaqa::Inherit<Associative_mixin<std::unordered_set<KeyT>>, Hash<KeyT>>;
        using typename Inherit::This;
        using typename Inherit::Base;

        using Inherit::Inherit;
        using Inherit::Parent::operator =;

        String to_string() const;
    };

    /*+ .. the idea is to automatically provide `hash` specialization
        for classes that have `hash` member function
    template <typename T>
    class Hash_object : public Object<T> {
    public:
        size_t hash() const;
    protected:
        Hash_object()                                               = default;
        ~Hash_object()                                              = default;
        Hash_object(const Hash_object&)                             = default;
        Hash_object& operator =(const Hash_object&)                 = default;
        Hash_object(Hash_object&&)                                  = default;
        Hash_object& operator =(Hash_object&&)                      = default;
    };
    */
}

/*+
namespace std {
    template <typename T> struct hash<tomaqa::util::Hash_object<T>>;
}
*/

#include "tomaqa/util/hash.inl"
