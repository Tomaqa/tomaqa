#pragma once

namespace tomaqa::util {
    template <Hashable KeyT, typename ValueT, invocable<KeyT> HashT>
    String Hash<KeyT, ValueT, HashT>::to_string() const
    {
        using tomaqa::to_string;
        String str;
        for (const auto& e : *this) {
            str += to_string(e) + ", ";
        }
        return str;
    }

    ////////////////////////////////////////////////////////////////

    template <Hashable KeyT>
    String Hash<KeyT>::to_string() const
    {
        using tomaqa::to_string;
        String str;
        for (const auto& e : *this) {
            str += to_string(e) + " ";
        }
        return str;
    }

    ////////////////////////////////////////////////////////////////

    /*+
    template <typename T>
    size_t Hash_object<T>::hash() const
    {
        return This::ccast().hash();
    }
    */
}

/*+
namespace std {
    template <typename T>
    struct hash<tomaqa::util::Hash_object<T>>
    {
        size_t operator ()(const tomaqa::util::Hash_object<T>& object) const
        {
            return object.hash();
        }
    };
}
*/
