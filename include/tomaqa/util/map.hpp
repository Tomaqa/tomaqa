#pragma once

#include "tomaqa/util/associative.hpp"

#include <map>

namespace tomaqa::util {
    template <typename KeyT, typename ValueT, relation<KeyT, KeyT> = less<>> class Map;
}

namespace tomaqa::util {
    template <typename KeyT, typename ValueT, relation<KeyT, KeyT> CmpT>
    class Map : public Inherit<Map_mixin<std::map<KeyT, ValueT, CmpT>>, Map<KeyT, ValueT, CmpT>> {
    public:
        using Inherit = tomaqa::Inherit<Map_mixin<std::map<KeyT, ValueT, CmpT>>, Map<KeyT, ValueT, CmpT>>;
        using typename Inherit::This;
        using typename Inherit::Base;

        using Inherit::Inherit;
        using Inherit::Parent::operator =;

        String to_string() const;
    };
}

#include "tomaqa/util/map.inl"
