#pragma once

namespace tomaqa::util {
    template <typename KeyT, typename ValueT, relation<KeyT, KeyT> CmpT>
    String Map<KeyT, ValueT, CmpT>::to_string() const
    {
        using tomaqa::to_string;
        String str;
        for (const auto& e : *this) {
            str += to_string(e) + ", ";
        }
        return str;
    }
}
