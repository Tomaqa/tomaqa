#pragma once

#include "tomaqa/util.hpp"

#include "tomaqa/util/optional.hpp"

namespace tomaqa::util {
    template <typename T>
    concept Addable = requires (T t) { {t+t} -> convertible_to<T>; {t+=t} -> convertible_to<T&>; };
    template <typename T>
    concept Subtractable = requires (T t) { {t-t} -> convertible_to<T>; {t-=t} -> convertible_to<T&>; };
    template <typename T>
    concept Multipliable = requires (T t) { {t*t} -> convertible_to<T>; {t*=t} -> convertible_to<T&>; };
    template <typename T>
    concept Divisible = requires (T t) { {t/t} -> convertible_to<T>; {t/=t} -> convertible_to<T&>; };

    template <typename T>
    concept Precision_c = requires {
        {T::rel_eps()} -> floating_point;
        {T::abs_eps()} -> floating_point;
    };

    namespace precision {
        struct Tiny;
        struct Low;
        struct Medium;
        struct High;
        struct Huge;
        struct Max;
    }

    template <size_t n>
    using Int_tp = conditional_t<n == 1, int8_t,
                   conditional_t<n == 2, int16_t,
                   conditional_t<n <= 4, int32_t,
                   conditional_t<n <= 8, int64_t,
                   intmax_t>>>>;
    template <size_t n>
    using Uint_tp = conditional_t<n == 1, uint8_t,
                    conditional_t<n == 2, uint16_t,
                    conditional_t<n <= 4, uint32_t,
                    conditional_t<n <= 8, uint64_t,
                    uintmax_t>>>>;
    template <size_t n>
    using Float_tp = conditional_t<n <= 4, float,
                     conditional_t<n <= 8, double,
                     long double>>;

    template <typename T, typename U, typename P>
    concept Has_apx_equals_with = Precision_c<P>
                               && requires (T t, U u) { t.template apx_equals<P>(u); };
    template <typename T, typename P>
    concept Has_apx_equals = Has_apx_equals_with<T, T, P>;
    template <typename T, typename U, typename P>
    concept Has_apx_compare_with = Precision_c<P>
                                && requires (T t, U u) { t.template apx_compare<P>(u); };
    template <typename T, typename P>
    concept Has_apx_compare = Has_apx_compare_with<T, T, P>;

    template <Precision_c = precision::Medium> struct Apx_equal;
    template <Precision_c = precision::Medium> struct Apx_compare;
    template <Precision_c = precision::Medium> struct Apx_less;
    template <Precision_c = precision::Medium> struct Apx_greater;
    template <Precision_c = precision::Medium> struct Apx_less_equal;
    template <Precision_c = precision::Medium> struct Apx_greater_equal;

    template <Precision_c = precision::Medium>
    constexpr bool apx_equal(const Integral auto&, const Integral auto&);
    template <Precision_c = precision::Medium>
    constexpr bool apx_equal(const floating_point auto&, const floating_point auto&);
    template <Precision_c = precision::Medium>
    constexpr bool apx_equal(const floating_point auto&, const Integral auto&);
    template <Precision_c = precision::Medium>
    constexpr bool apx_equal(const Integral auto&, const floating_point auto&);
    template <Precision_c = precision::Medium, typename T, typename U = T>
    constexpr bool apx_equal(const pair<T, U>&, const pair<T, U>&);
    template <Precision_c = precision::Medium, typename... Args>
    constexpr bool apx_equal(const tuple<Args...>&, const tuple<Args...>&);
    template <Precision_c = precision::Medium, typename T, typename U = T>
    constexpr bool apx_equal(const Optional<T>&, const Optional<U>&);
    //+ support also tag dispatching to allow using virtual functions as well
    template <Precision_c P = precision::Medium, Class T, typename U = T>
    requires Has_apx_equals_with<T, U, P>
    constexpr bool apx_equal(const T&, const U&);
    template <Precision_c = precision::Medium>
    constexpr partial_ordering apx_compare(const Integral auto&, const Integral auto&);
    template <Precision_c = precision::Medium>
    constexpr partial_ordering apx_compare(const floating_point auto&, const floating_point auto&);
    template <Precision_c = precision::Medium>
    constexpr partial_ordering apx_compare(const floating_point auto&, const Integral auto&);
    template <Precision_c = precision::Medium>
    constexpr partial_ordering apx_compare(const Integral auto&, const floating_point auto&);
    template <Precision_c = precision::Medium, typename T, typename U = T>
    constexpr partial_ordering apx_compare(const pair<T, U>&, const pair<T, U>&);
    template <Precision_c = precision::Medium, typename... Args>
    constexpr partial_ordering apx_compare(const tuple<Args...>&, const tuple<Args...>&);
    template <Precision_c = precision::Medium, typename T, typename U = T>
    constexpr partial_ordering apx_compare(const Optional<T>&, const Optional<U>&);
    template <Precision_c P = precision::Medium, Class T, typename U = T>
    requires Has_apx_compare_with<T, U, P>
    constexpr partial_ordering apx_compare(const T&, const U&);

    template <typename T, typename U, typename P>
    concept Apx_comparable_with = Precision_c<P>
                               && requires (T t, U u) { apx_compare<P>(t, u); };
    template <typename T, typename P>
    concept Apx_comparable = Apx_comparable_with<T, T, P>;

    template <Precision_c P = precision::Medium, typename T, typename U = T>
    requires Apx_comparable_with<T, U, P>
    constexpr bool apx_less(const T&, const U&);
    template <Precision_c P = precision::Medium, typename T, typename U = T>
    requires Apx_comparable_with<T, U, P>
    constexpr bool apx_greater(const T&, const U&);
    template <Precision_c P = precision::Medium, typename T, typename U = T>
    requires Apx_comparable_with<T, U, P>
    constexpr bool apx_less_equal(const T&, const U&);
    template <Precision_c P = precision::Medium, typename T, typename U = T>
    requires Apx_comparable_with<T, U, P>
    constexpr bool apx_greater_equal(const T&, const U&);
}

namespace tomaqa::util::precision {
    struct Tiny {
        using Higher = Low;

        static consteval int rel_eps_exp() noexcept                                   { return -1; }
        static consteval int abs_eps_exp() noexcept                                   { return -1; }
        static consteval float_t rel_eps() noexcept                                 { return 1e-1; }
        static consteval float_t abs_eps() noexcept                                 { return 1e-1; }
    };

    struct Low {
        using Lower = Tiny;
        using Higher = Medium;

        static consteval int rel_eps_exp() noexcept                                   { return -3; }
        static consteval int abs_eps_exp() noexcept                                   { return -3; }
        static consteval float_t rel_eps() noexcept                                 { return 1e-3; }
        static consteval float_t abs_eps() noexcept                                 { return 1e-3; }
    };

    struct Medium {
        using Lower = Low;
        using Higher = High;

        static consteval int rel_eps_exp() noexcept                 { return Low::rel_eps_exp()*2; }
        static consteval int abs_eps_exp() noexcept                 { return Low::abs_eps_exp()*2; }
        static consteval float_t rel_eps() noexcept        { return Low::rel_eps()*Low::rel_eps(); }
        static consteval float_t abs_eps() noexcept        { return Low::abs_eps()*Low::abs_eps(); }
    };

    struct High {
        using Lower = Medium;
        using Higher = Huge;

        static consteval int rel_eps_exp() noexcept
                                              { return Medium::rel_eps_exp() + Low::rel_eps_exp(); }
        static consteval int abs_eps_exp() noexcept
                                              { return Medium::abs_eps_exp() + Low::abs_eps_exp(); }
        static consteval float_t rel_eps() noexcept     { return Medium::rel_eps()*Low::rel_eps(); }
        static consteval float_t abs_eps() noexcept     { return Medium::abs_eps()*Low::abs_eps(); }
    };

    struct Huge {
        using Lower = High;
        using Higher = Max;

        static consteval int rel_eps_exp() noexcept              { return Medium::rel_eps_exp()*2; }
        static consteval int abs_eps_exp() noexcept              { return Medium::abs_eps_exp()*2; }
        static consteval float_t rel_eps() noexcept  { return Medium::rel_eps()*Medium::rel_eps(); }
        static consteval float_t abs_eps() noexcept  { return Medium::abs_eps()*Medium::abs_eps(); }
    };

    struct Max {
        using Lower = Huge;
        using Higher = Max;

        static consteval float_t rel_eps() noexcept                                    { return 0; }
        static consteval float_t abs_eps() noexcept                                    { return 0; }
    };
}

namespace tomaqa::util {
    template <Precision_c P>
    struct Apx_equal {
        constexpr bool operator ()(auto&& a, auto&& b) const
                                                    { return apx_equal<P>(FORWARD(a), FORWARD(b)); }
    };
    template <Precision_c P>
    struct Apx_compare {
        constexpr partial_ordering operator ()(auto&& a, auto&& b) const
                                                  { return apx_compare<P>(FORWARD(a), FORWARD(b)); }
    };
    template <Precision_c P>
    struct Apx_less {
        constexpr bool operator ()(auto&& a, auto&& b) const
                                                     { return apx_less<P>(FORWARD(a), FORWARD(b)); }
    };
    template <Precision_c P>
    struct Apx_greater {
        constexpr bool operator ()(auto&& a, auto&& b) const
                                                  { return apx_greater<P>(FORWARD(a), FORWARD(b)); }
    };
    template <Precision_c P>
    struct Apx_less_equal {
        constexpr bool operator ()(auto&& a, auto&& b) const
                                               { return apx_less_equal<P>(FORWARD(a), FORWARD(b)); }
    };
    template <Precision_c P>
    struct Apx_greater_equal {
        constexpr bool operator ()(auto&& a, auto&& b) const
                                            { return apx_greater_equal<P>(FORWARD(a), FORWARD(b)); }
    };
}

#include "tomaqa/util/numeric.inl"

namespace tomaqa::util {
    static_assert(Addable<int>);
    static_assert(Addable<double>);
    static_assert(Addable<String>);
    static_assert(Subtractable<int>);
    static_assert(Subtractable<double>);
    static_assert(Multipliable<int>);
    static_assert(Multipliable<double>);
    static_assert(Divisible<int>);
    static_assert(Divisible<double>);

    static_assert(Precision_c<precision::Tiny>);
    static_assert(Precision_c<precision::Low>);
    static_assert(Precision_c<precision::Medium>);
    static_assert(Precision_c<precision::High>);
    static_assert(Precision_c<precision::Huge>);
    static_assert(Precision_c<precision::Max>);
}
