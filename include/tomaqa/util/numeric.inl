#pragma once

#include "tomaqa/util/alg.hpp"

namespace tomaqa::util {
    template <Precision_c>
    constexpr bool apx_equal(const Integral auto& a, const Integral auto& b)
    {
        return a == b;
    }

    template <Precision_c P>
    constexpr bool apx_equal(const floating_point auto& a, const floating_point auto& b)
    {
        using Longer_float = conditional_t<(sizeof(a) > sizeof(b)), DECAY(a), DECAY(b)>;

        if (a == b) return true;
        if constexpr (is_same_v<P, precision::Max>) return false;
        else {
            if (!isfinite(a) || !isfinite(b)) return false;
            const auto diff = abs(a - b);
            if (diff <= P::abs_eps()) return true;
            return diff <= P::rel_eps()*max(abs(Longer_float(a)), abs(Longer_float(b)));
        }
    }

    template <Precision_c P>
    constexpr bool apx_equal(const floating_point auto& a, const Integral auto& b)
    {
        using Float = DECAY(a);
        return apx_equal<P>(a, Float(b));
    }

    template <Precision_c P>
    constexpr bool apx_equal(const Integral auto& a, const floating_point auto& b)
    {
        return apx_equal<P>(b, a);
    }

    template <Precision_c P, typename T, typename U>
    constexpr bool apx_equal(const pair<T, U>& a, const pair<T, U>& b)
    {
        return apx_equal<P>(a.first, b.first)
            && apx_equal<P>(a.second, b.second);
    }

    template <Precision_c P, typename... Args>
    constexpr bool apx_equal(const tuple<Args...>& a, const tuple<Args...>& b)
    {
        return equal(a, b, Apx_equal<P>());
    }

    template <Precision_c P, typename T, typename U>
    constexpr bool apx_equal(const Optional<T>& a, const Optional<U>& b)
    {
        if (a.invalid() || b.invalid()) return a.invalid() && b.invalid();
        return apx_equal<P>(a.cvalue(), b.cvalue());
    }

    template <Precision_c P, Class T, typename U>
    requires Has_apx_equals_with<T, U, P>
    constexpr bool apx_equal(const T& a, const U& b)
    {
        return a.template apx_equals<P>(b);
    }

    template <Precision_c>
    constexpr partial_ordering apx_compare(const Integral auto& a, const Integral auto& b)
    {
        return a <=> b;
    }

    template <Precision_c P>
    constexpr partial_ordering apx_compare(const floating_point auto& a, const floating_point auto& b)
    {
        partial_ordering cmp = a <=> b;
        if (cmp == partial_ordering::unordered) return cmp;

        if (apx_equal<P>(a, b)) return partial_ordering::equivalent;

        assert(cmp != partial_ordering::equivalent);
        return cmp;
    }

    template <Precision_c P>
    constexpr partial_ordering apx_compare(const floating_point auto& a, const Integral auto& b)
    {
        using Float = DECAY(a);
        return apx_compare<P>(a, Float(b));
    }

    template <Precision_c P>
    constexpr partial_ordering apx_compare(const Integral auto& a, const floating_point auto& b)
    {
        return apx_compare<P>(b, a);
    }

    template <Precision_c P, typename T, typename U>
    constexpr partial_ordering apx_compare(const pair<T, U>& a, const pair<T, U>& b)
    {
        if (auto cmp = apx_compare<P>(a.first, b.first); !is_eq(cmp)) return cmp;
        return apx_compare<P>(a.second, b.second);
    }

    template <Precision_c P, typename... Args>
    constexpr partial_ordering apx_compare(const tuple<Args...>& a, const tuple<Args...>& b)
    {
        partial_ordering cmp = partial_ordering::equivalent;
        none_of(a, b, [&cmp](auto& arg1, auto& arg2){
            if (auto cmp_ = apx_compare<P>(arg1, arg2); !is_eq(cmp_)) {
                cmp = cmp_;
                return true;
            }
            return false;
        });
        return cmp;
    }

    template <Precision_c P, typename T, typename U>
    constexpr partial_ordering apx_compare(const Optional<T>& a, const Optional<U>& b)
    {
        if (a.invalid() || b.invalid()) return a <=> b;
        return apx_compare<P>(a.cvalue(), b.cvalue());
    }

    template <Precision_c P, Class T, typename U>
    requires Has_apx_compare_with<T, U, P>
    constexpr partial_ordering apx_compare(const T& a, const U& b)
    {
        return a.template apx_compare<P>(b);
    }

    template <Precision_c P, typename T, typename U>
    requires Apx_comparable_with<T, U, P>
    constexpr bool apx_less(const T& a, const U& b)
    {
        return is_lt(apx_compare<P>(a, b));
    }

    template <Precision_c P, typename T, typename U>
    requires Apx_comparable_with<T, U, P>
    constexpr bool apx_greater(const T& a, const U& b)
    {
        return is_gt(apx_compare<P>(a, b));
    }

    template <Precision_c P, typename T, typename U>
    requires Apx_comparable_with<T, U, P>
    constexpr bool apx_less_equal(const T& a, const U& b)
    {
        return is_lteq(apx_compare<P>(a, b));
    }

    template <Precision_c P, typename T, typename U>
    requires Apx_comparable_with<T, U, P>
    constexpr bool apx_greater_equal(const T& a, const U& b)
    {
        return is_gteq(apx_compare<P>(a, b));
    }
}
