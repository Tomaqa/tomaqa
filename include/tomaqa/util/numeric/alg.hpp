#pragma once

#include "tomaqa/util/numeric.hpp"

#include <numeric>

namespace tomaqa::util {
    template <Precision_c = precision::Medium>
    bool apx_equal(const Container auto&, const Container auto&);

    template <Addable T, Container ContT> requires convertible_to<Value_t<ContT>, T>
    T accumulate(const ContT&, T init = {});
    template <typename T, Container ContT, Invocable_r<T, T, T> OpF>
    requires convertible_to<Value_t<ContT>, T>
    T accumulate(const ContT&, T init, OpF);

    template <Addable T, Container ContT> requires convertible_to<Value_t<ContT>, T>
    T reduce(const ContT&, T init = {});
    template <typename T, Container ContT, Invocable_r<T, T, T> OpF>
    requires convertible_to<Value_t<ContT>, T>
    T reduce(const ContT&, T init, OpF);
}

#include "tomaqa/util/numeric/alg.inl"
