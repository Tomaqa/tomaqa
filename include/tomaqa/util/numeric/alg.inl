#pragma once

#include "tomaqa/util/alg.hpp"

namespace tomaqa::util {
    template <Precision_c P>
    bool apx_equal(const Container auto& a, const Container auto& b)
    {
        return equal(a, b, [](auto& l, auto& r){
            return apx_equal<P>(l, r);
        });
    }

    template <Addable T, Container ContT> requires convertible_to<Value_t<ContT>, T>
    T accumulate(const ContT& cont, T init)
    {
        return std::accumulate(cbegin(cont), cend(cont), std::move(init));
    }

    template <typename T, Container ContT, Invocable_r<T, T, T> OpF>
    requires convertible_to<Value_t<ContT>, T>
    T accumulate(const ContT& cont, T init, OpF op)
    {
        return std::accumulate(cbegin(cont), cend(cont), std::move(init), std::move(op));
    }

    template <Addable T, Container ContT> requires convertible_to<Value_t<ContT>, T>
    T reduce(const ContT& cont, T init)
    {
        return std::reduce(cbegin(cont), cend(cont), std::move(init));
    }

    template <typename T, Container ContT, Invocable_r<T, T, T> OpF>
    requires convertible_to<Value_t<ContT>, T>
    T reduce(const ContT& cont, T init, OpF op)
    {
        return std::reduce(cbegin(cont), cend(cont), std::move(init), std::move(op));
    }
}
