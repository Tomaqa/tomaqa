#pragma once

#include "tomaqa/util.hpp"

#include <optional>

namespace tomaqa::util {
    template <typename> class Optional;
}

namespace tomaqa::util {
    template <typename T>
    class Optional : public Inherit<std::optional<T>, Optional<T>> {
    public:
        using Inherit = tomaqa::Inherit<std::optional<T>, Optional<T>>;
        using typename Inherit::This;
        using Base = typename Inherit::Parent;

        using Value = Value_t<Base>;

        using Inherit::Inherit;
        using Base::operator =;
        Optional()                                                                        = default;
        template <Class T_ = This, constructible_from<typename T_::Base> U = Value>
        constexpr Optional(U&&);
        template <Class T_ = This, constructible_from<typename T_::Base> U = Value>
        constexpr Optional& operator =(U&&);
        template <Class T_ = This, Derived_from<T_> U> constexpr Optional(U&&);
        template <Class T_ = This, Derived_from<T_> U> constexpr Optional& operator =(U&&);

        using Inherit::operator *;
        using Inherit::operator ->;
        constexpr const auto& cvalue() const noexcept;
        constexpr const auto& value() const& noexcept                           { return cvalue(); }
        constexpr auto& value()& noexcept;
        constexpr auto&& value()&& noexcept;
        constexpr const auto& cvalue_check() const;
        constexpr const auto& value_check() const&                        { return cvalue_check(); }
        constexpr auto& value_check()&;
        constexpr auto&& value_check()&&;

        using Inherit::has_value;
        constexpr bool valid() const noexcept                                { return has_value(); }
        constexpr bool invalid() const noexcept                                 { return !valid(); }
        constexpr bool unknown() const noexcept                                { return invalid(); }
        explicit constexpr operator bool() const noexcept                        { return valid(); }
        constexpr bool operator !() const noexcept                             { return invalid(); }

        using Inherit::value_or;
        constexpr auto value_or_default() const&;
        constexpr auto value_or_default()&&;

        using Inherit::reset;
        void invalidate() noexcept                                                      { reset(); }
        void forget() noexcept                                                          { reset(); }

        //+ `operator <` is not well defined, it should probably consider `<=>` semantics ...
        constexpr bool equals(const This&) const noexcept;
        constexpr bool equals(const Value&) const noexcept;

        String to_string() const;
    };
}

#include "tomaqa/util/optional.inl"
