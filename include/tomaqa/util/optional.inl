#pragma once

namespace tomaqa::util {
    template <typename T>
    template <Class T_, constructible_from<typename T_::Base> U>
    constexpr Optional<T>::Optional(U&& val)
        : Inherit(FORWARD(val))
    { }

    template <typename T>
    template <Class T_, constructible_from<typename T_::Base> U>
    constexpr Optional<T>& Optional<T>::operator =(U&& val)
    {
        Base::operator =(FORWARD(val));
        return *this;
    }

    template <typename T>
    template <Class T_, Derived_from<T_> U>
    constexpr Optional<T>::Optional(U&& rhs)
        : Inherit(static_cast<Base&&>(rhs))
    { }

    template <typename T>
    template <Class T_, Derived_from<T_> U>
    constexpr Optional<T>& Optional<T>::operator =(U&& rhs)
    {
        Base::operator =(static_cast<Base&&>(rhs));
        return *this;
    }

    template <typename T>
    constexpr const auto& Optional<T>::cvalue() const noexcept
    {
        assert(valid());
        return operator *();
    }

    template <typename T>
    constexpr auto& Optional<T>::value()& noexcept
    {
        assert(valid());
        return operator *();
    }

    template <typename T>
    constexpr auto&& Optional<T>::value()&& noexcept
    {
        assert(valid());
        return move(*this).operator *();
    }

    template <typename T>
    constexpr const auto& Optional<T>::cvalue_check() const
    {
        return Inherit::value();
    }

    template <typename T>
    constexpr auto& Optional<T>::value_check()&
    {
        return Inherit::value();
    }

    template <typename T>
    constexpr auto&& Optional<T>::value_check()&&
    {
        return move(Inherit::value());
    }

    template <typename T>
    constexpr auto Optional<T>::value_or_default() const&
    {
        return value_or(Value());
    }

    template <typename T>
    constexpr auto Optional<T>::value_or_default()&&
    {
        return move(*this).value_or(Value());
    }

    template <typename T>
    constexpr bool Optional<T>::equals(const This& rhs) const noexcept
    {
        return std::operator ==(static_cast<const Base&>(*this), static_cast<const Base&>(rhs));
    }

    template <typename T>
    constexpr bool Optional<T>::equals(const Value& val) const noexcept
    {
        return std::operator ==(static_cast<const Base&>(*this), val);
    }

    template <typename T>
    String Optional<T>::to_string() const
    {
        using tomaqa::to_string;
        return valid() ? to_string(cvalue()) : "?";
    }
}
