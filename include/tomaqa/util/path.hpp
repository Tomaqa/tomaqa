#pragma once

#include "tomaqa/util.hpp"

#include <filesystem>

namespace tomaqa::util {
    struct Path;

    namespace filesystem = std::filesystem;

    using filesystem::absolute;
    using filesystem::relative;
    using filesystem::canonical;

    Path pwd();
    Path root();

    template <Base_of<Path> P, typename T>
    requires requires (Rm_cvref<P>& p, T t) { {p+=t} -> Base_of<Path>; }
    auto operator +(P&&, T&&);
}

namespace tomaqa::util {
    struct Path : Inherit<filesystem::path, Path> {
        using Inherit::Inherit;

        using Inherit::is_absolute;
        using Inherit::is_relative;

        using Inherit::has_filename;
        using Inherit::has_extension;
        using Inherit::filename;
        using Inherit::extension;
        bool has_dirname() const;
        Path dirname() const;

        String to_string() const&                         { return string(); }
    };
}

#include "tomaqa/util/path.inl"