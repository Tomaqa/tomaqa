#pragma once

namespace tomaqa::util {
    template <Base_of<Path> P, typename T>
    requires requires (Rm_cvref<P>& p, T t) { {p+=t} -> Base_of<Path>; }
    auto operator +(P&& p, T&& rhs)
    {
        auto p_(FORWARD(p));
        return p_ += FORWARD(rhs);
    }
}