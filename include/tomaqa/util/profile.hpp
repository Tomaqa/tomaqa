#pragma once

#include "tomaqa/util.hpp"

#include "tomaqa/util/optional.hpp"
#include "tomaqa/util/scope_guard.hpp"

namespace tomaqa::util::profile::conf {
    template <typename T>
    concept Has_parent = Class<T> && requires { requires Class<typename T::Parent>; };

    struct Empty {
        static constexpr const char* name = "";
    };
    struct Main {
        static constexpr const char* name = "main";
    };
}

namespace tomaqa::util::profile::measure {
    class Base;
    template <Class> class Enable_mixin;
}

namespace tomaqa::util::profile {
    class Base;
    template <Class ConfT> class Tp;
    template <Class> class Lap_mixin;

    using Duration = double_t;

    using Measure = measure::Enable_mixin<measure::Base>;
    using Main = Lap_mixin<Tp<profile::conf::Main>>;
}

namespace tomaqa::util {
    using Profile = profile::Tp<profile::conf::Empty>;
}

namespace tomaqa::util::profile::measure {
    class Base {
    public:
        const auto& cstart_time() const noexcept                             { return _start_time; }

        inline bool started() const noexcept;

        inline void start();

        inline Duration get_duration() const;
        inline void set_duration(Duration&) const;
        inline void inc_duration(Duration&) const;
        inline void inc_duration(Duration&, long& counter) const;
    protected:
        using Time = Duration;

        auto& start_time() noexcept                                          { return _start_time; }
    private:
        template <typename T = Dummy>
        void inc_duration_impl(Duration&, T& counter = rdummy) const;

        Time _start_time{};
    };

    template <Class B>
    class Enable_mixin : public Inherit<B> {
    public:
        using Inherit = tomaqa::Inherit<B>;

        using Inherit::Inherit;
        explicit Enable_mixin(bool enabled_)                                : _enabled(enabled_) { }

        bool enabled() const noexcept                                           { return _enabled; }
        void enable() noexcept                                                  { _enabled = true; }
        void disable() noexcept                                                { _enabled = false; }

        inline void start();

        inline Optional<Duration> get_duration() const;
        inline void set_duration(Duration&) const;
        inline void inc_duration(Duration&) const;
        inline void inc_duration(Duration&, long& counter) const;
    private:
        bool _enabled{};
    };
}

namespace tomaqa::util::profile {
    class Base : public Inherit<Measure> {
    public:
        using Inherit::Inherit;
        Base()                                                                            = default;
        ~Base()                                                                           = default;
        Base(const Base&)                                                                 = default;
        Base& operator =(const Base&)                                                     = default;
        Base(Base&&)                                                                      = default;
        Base& operator =(Base&&)                                                          = default;

        const auto& ctotal_duration() const noexcept                     { return _total_duration; }
        const auto& ccount() const noexcept                                       { return _count; }

        inline bool finished() const noexcept;

        template <bool incCount = true>
        inline void finish();

        template <bool incCount = true>
        inline auto make_scope_guard();

        inline Base& operator +=(const Base&) noexcept;
    protected:
        auto& total_duration() noexcept                                  { return _total_duration; }
        auto& count() noexcept                                                    { return _count; }

        inline auto make_scope_guard_impl(invocable auto);
    private:
        Duration _total_duration{};
        long _count{};
    };

    namespace aux {
        template <Class ConfT>
        class Tp : public Inherit<Base, Tp<ConfT>> {
        public:
            using Inherit = tomaqa::Inherit<Base, Tp<ConfT>>;
            using typename Inherit::This;

            using Conf = ConfT;

            static_assert(requires { Conf::name; });

            static constexpr bool has_parent_v = conf::Has_parent<Conf>;

            using Inherit::Inherit;

            static String name();
        protected:
            static String to_string_impl(const Derived_from<This> auto&);
            static String duration_to_string(const String& name_, Duration dur);
            template <Class ParentT>
            static String rel_duration_to_string(Duration, const ParentT&);
        };
    }

    template <Class ConfT>
    class Tp : public Inherit<aux::Tp<ConfT>, Tp<ConfT>> {
    public:
        using Inherit = tomaqa::Inherit<aux::Tp<ConfT>, Tp<ConfT>>;

        using Inherit::Inherit;

        String to_string() const;
    };

    template <conf::Has_parent ConfT>
    class Tp<ConfT> : public Inherit<aux::Tp<ConfT>, Tp<ConfT>> {
    public:
        using Inherit = tomaqa::Inherit<aux::Tp<ConfT>, Tp<ConfT>>;
        using typename Inherit::This;

        using typename Inherit::Conf;

        using Parent = typename Conf::Parent;

        Tp()                                                                               = delete;
        ~Tp()                                                                             = default;
        Tp(const Tp&)                                                                     = default;
        Tp& operator =(const Tp&)                                                         = default;
        Tp(Tp&&)                                                                          = default;
        Tp& operator =(Tp&&)                                                              = default;
        explicit Tp(const Parent&);

        const auto& cparent() const                                           { return *_parent_l; }

        String to_string() const;
    private:
        const Parent* _parent_l;
    };

    template <Class B>
    class Lap_mixin : public Inherit<B, Lap_mixin<B>> {
    public:
        using Inherit = tomaqa::Inherit<B, Lap_mixin<B>>;
        using typename Inherit::This;

        using Inherit::Inherit;

        inline const auto& clap_duration(Idx) const;

        inline size_t n_laps() const noexcept;
        inline bool no_laps() const noexcept;

        inline void lap();

        template <bool incCount = true>
        inline void finish();

        template <bool incCount = true>
        inline auto make_scope_guard();

        String to_string() const;
    protected:
        using Lap_measure = measure::Base;
        using Lap_measures = Vector<Lap_measure>;
        using Lap_durations = Vector<Duration>;

        const auto& clap_measures() const noexcept                         { return _lap_measures; }
        auto& lap_measures() noexcept                                      { return _lap_measures; }
        inline const auto& clap_measure(Idx) const;
        inline auto& lap_measure(Idx);
        const auto& clap_durations() const noexcept                       { return _lap_durations; }
        auto& lap_durations() noexcept                                    { return _lap_durations; }
        inline auto& lap_duration(Idx);

        inline void add_lap_duration();
    private:
        Lap_measures _lap_measures{};
        Lap_durations _lap_durations{};
    };
}

#include "tomaqa/util/profile.inl"
