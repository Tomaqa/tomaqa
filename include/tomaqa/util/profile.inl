#pragma once

#include <omp.h>

namespace tomaqa::util::profile::measure {
    bool Base::started() const noexcept
    {
        return cstart_time() != 0;
    }

    void Base::start()
    {
        start_time() = omp_get_wtime();
    }

    Duration Base::get_duration() const
    {
        assert(started());
        const Time finish_time = omp_get_wtime();
        return finish_time - cstart_time();
    }

    void Base::set_duration(Duration& duration) const
    {
        duration = get_duration();
    }

    void Base::inc_duration(Duration& duration) const
    {
        inc_duration_impl(duration);
    }

    void Base::inc_duration(Duration& duration, long& counter) const
    {
        inc_duration_impl(duration, counter);
    }

    template <typename T>
    void Base::inc_duration_impl(Duration& duration, T& counter) const
    {
        duration += get_duration();
        if constexpr (!is_dummy_v<T>) ++counter;
    }

    ////////////////////////////////////////////////////////////////

    template <Class B>
    void Enable_mixin<B>::start()
    {
        if (!enabled()) return;
        Inherit::start();
    }

    template <Class B>
    Optional<Duration> Enable_mixin<B>::get_duration() const
    {
        if (!enabled()) return {};
        return Inherit::get_duration();
    }

    template <Class B>
    void Enable_mixin<B>::set_duration(Duration& duration) const
    {
        if (!enabled()) return;
        Inherit::set_duration(duration);
    }

    template <Class B>
    void Enable_mixin<B>::inc_duration(Duration& duration) const
    {
        if (!enabled()) return;
        Inherit::inc_duration(duration);
    }

    template <Class B>
    void Enable_mixin<B>::inc_duration(Duration& duration, long& counter) const
    {
        if (!enabled()) return;
        Inherit::inc_duration(duration, counter);
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace tomaqa::util::profile {
    bool Base::finished() const noexcept
    {
        return ctotal_duration() != 0;
    }

    template <bool incCount>
    void Base::finish()
    {
        if constexpr (incCount) inc_duration(total_duration(), count());
        else inc_duration(total_duration());
    }

    template <bool incCount>
    auto Base::make_scope_guard()
    {
        return make_scope_guard_impl([this]{
            finish<incCount>();
        });
    }

    auto Base::make_scope_guard_impl(invocable auto f)
    {
        start();
        return Scope_guard(std::move(f));
    }

    Base& Base::operator +=(const Base& rhs) noexcept
    {
        if (!enabled()) return *this;

        total_duration() += rhs.ctotal_duration();
        count() += rhs.ccount();

        return *this;
    }

    ////////////////////////////////////////////////////////////////

    namespace aux {
        template <Class ConfT>
        String Tp<ConfT>::name()
        {
            return Conf::name;
        }

        template <Class ConfT>
        String Tp<ConfT>::to_string_impl(const Derived_from<This> auto& that_)
        {
            using tomaqa::to_string;

            const auto dur = that_.ctotal_duration();
            const auto cnt = that_.ccount();
            String str;
            str += duration_to_string(name(), dur);
            if constexpr (has_parent_v) {
                //+ what if it is entirely part of just a parent's lap (e.g. main-body)
                str += " (" + rel_duration_to_string(dur, that_.cparent()) + ")";
            }
            if constexpr (!is_same_v<Conf, conf::Main>) {
                str += "\n";
                str += name() + " count: " + to_string(cnt) + " x (avg: " + to_string(dur/cnt) + ")";
            }

            return str;
        }

        template <Class ConfT>
        String Tp<ConfT>::duration_to_string(const String& name_, Duration dur)
        {
            using tomaqa::to_string;

            return name_ + " duration: " + to_string(dur) + " s";
        }

        template <Class ConfT>
        template <Class ParentT>
        String Tp<ConfT>::rel_duration_to_string(Duration dur, const ParentT& par)
        {
            using tomaqa::to_string;

            const auto par_dur = par.ctotal_duration();
            return to_string((dur/par_dur)*100) + " % of " + par.name();
        }
    }

    ////////////////////////////////////////////////////////////////

    template <Class ConfT>
    String Tp<ConfT>::to_string() const
    {
        return Inherit::to_string_impl(*this);
    }

    ////////////////////////////////////////////////////////////////

    template <conf::Has_parent ConfT>
    Tp<ConfT>::Tp(const Parent& parent_)
        : Inherit(parent_.enabled()), _parent_l(&parent_)
    { }

    template <conf::Has_parent ConfT>
    String Tp<ConfT>::to_string() const
    {
        return Inherit::to_string_impl(*this);
    }

    ////////////////////////////////////////////////////////////////

    template <Class B>
    const auto& Lap_mixin<B>::clap_measure(Idx idx) const
    {
        if (idx > 0) return clap_measures()[idx-1];

        assert(this->enabled());
        return static_cast<const Lap_measure&>(*this);
    }

    template <Class B>
    auto& Lap_mixin<B>::lap_measure(Idx idx)
    {
        return const_cast<Lap_measure&>(clap_measure(idx));
    }

    template <Class B>
    const auto& Lap_mixin<B>::clap_duration(Idx idx) const
    {
        if (idx == 0 && no_laps()) return this->ctotal_duration();
        return clap_durations()[idx];
    }

    template <Class B>
    auto& Lap_mixin<B>::lap_duration(Idx idx)
    {
        return const_cast<Duration&>(clap_duration(idx));
    }

    template <Class B>
    size_t Lap_mixin<B>::n_laps() const noexcept
    {
        return size(clap_durations());
    }

    template <Class B>
    bool Lap_mixin<B>::no_laps() const noexcept
    {
        return empty(clap_durations());
    }

    template <Class B>
    void Lap_mixin<B>::add_lap_duration()
    {
        assert(this->enabled());

        auto& lms = lap_measures();
        auto& ldurs = lap_durations();
        if (empty(lms)) ldurs.push_back(measure::Base::get_duration());
        else ldurs.push_back(lms.back().get_duration());

        assert(!no_laps());
    }

    template <Class B>
    void Lap_mixin<B>::lap()
    {
        if (!this->enabled()) return;

        add_lap_duration();

        Lap_measure new_m;
        new_m.start();
        lap_measures().push_back(std::move(new_m));
    }

    template <Class B>
    template <bool incCount>
    void Lap_mixin<B>::finish()
    {
        Inherit::template finish<incCount>();

        assert(size(clap_measures()) == n_laps());
        if (no_laps()) return;
        add_lap_duration();
        assert(size(clap_measures())+1 == n_laps());
    }

    template <Class B>
    template <bool incCount>
    auto Lap_mixin<B>::make_scope_guard()
    {
        return This::make_scope_guard_impl([this]{
            finish<incCount>();
        });
    }

    template <Class B>
    String Lap_mixin<B>::to_string() const
    {
        using tomaqa::to_string;

        String str = Inherit::to_string();
        const int n_laps_ = n_laps();
        assert((n_laps_ == 0) == no_laps());
        if (n_laps_ == 0) return str;

        for (Idx idx = 0; idx < n_laps_; ++idx) {
            auto& dur = clap_duration(idx);
            str += "\n" + This::duration_to_string(This::name() + " lap#" + to_string(idx+1), dur)
                 + " (" + This::rel_duration_to_string(dur, *this) + ")";
        }

        return str;
    }
}
