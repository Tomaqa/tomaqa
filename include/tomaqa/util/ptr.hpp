#pragma once

#include <memory>

#define MAKE_UNIQUE(obj) make_unique<DECAY(obj)>(obj)
#define MAKE_SHARED(obj) make_shared<DECAY(obj)>(obj)

namespace tomaqa::util {
    template <Class> class Smart_ptr_mixin;

    template <typename> struct Unique_ptr;
    template <typename> struct Shared_ptr;

    template <typename T> concept Smart_ptr = requires { typename Rm_ref<T>::Smart_ptr_mixin; };

    using std::make_unique;
    using std::make_shared;

    /// Copies the pointer if it is not unique
    void maybe_shallow_copy(Smart_ptr auto&);
    void maybe_deep_copy(Smart_ptr auto&);
}

namespace tomaqa::util {
    template <Class B>
    class Smart_ptr_mixin : public Inherit<B, Smart_ptr_mixin<B>> {
    public:
        using Inherit = tomaqa::Inherit<B, Smart_ptr_mixin<B>>;
        using typename Inherit::This;
        using Base = typename Inherit::Parent;

        using Inherit::Inherit;

        bool equals(const This&) const noexcept;

        String to_string() const;
    };

    template <typename T>
    struct Unique_ptr : Inherit<Smart_ptr_mixin<std::unique_ptr<T>>, Unique_ptr<T>> {
        using Inherit = tomaqa::Inherit<Smart_ptr_mixin<std::unique_ptr<T>>, Unique_ptr<T>>;
        using typename Inherit::This;

        using Inherit::Inherit;

        bool unique() const noexcept                                                { return true; }
    };

    template <typename T>
    struct Shared_ptr : Inherit<Smart_ptr_mixin<std::shared_ptr<T>>, Shared_ptr<T>> {
        using Inherit = tomaqa::Inherit<Smart_ptr_mixin<std::shared_ptr<T>>, Shared_ptr<T>>;
        using typename Inherit::This;

        using Inherit::Inherit;

        bool unique() const noexcept;
    };
}

#include "tomaqa/util/ptr.inl"
