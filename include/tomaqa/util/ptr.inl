#pragma once

namespace tomaqa::util {
    template <Class B>
    bool Smart_ptr_mixin<B>::equals(const This& rhs) const noexcept
    {
        return this->cparent() == rhs.cparent();
    }

    template <Class B>
    String Smart_ptr_mixin<B>::to_string() const
    {
        using tomaqa::to_string;
        return to_string(**this);
    }

    ////////////////////////////////////////////////////////////////

    template <typename T>
    bool Shared_ptr<T>::unique() const noexcept
    {
        /// Only approximation in multithreaded environment !
        return This::use_count() == 1;
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace tomaqa::util {
    void maybe_shallow_copy(Smart_ptr auto& ptr)
    {
        if (ptr.unique()) return;
        ptr = ptr->shallow_copy_to_ptr();
    }

    void maybe_deep_copy(Smart_ptr auto& ptr)
    {
        if (ptr.unique()) return;
        ptr = ptr->deep_copy_to_ptr();
    }
}
