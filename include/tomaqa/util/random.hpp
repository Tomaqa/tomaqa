#pragma once

#include "tomaqa/util.hpp"

#include <random>

namespace tomaqa::util {
    using Pseudo_random = std::default_random_engine;
    using True_random = std::random_device;

    inline Pseudo_random pseudo_random{};
    inline True_random true_random{};

    template <input_iterator ItT, invocable RandomT = Pseudo_random>
    void shuffle(ItT first, ItT last, RandomT&& = {});
    template <invocable RandomT = Pseudo_random>
    void shuffle(Container auto&, RandomT&& = {});
}

#include "tomaqa/util/random.inl"
