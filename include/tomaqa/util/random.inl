#pragma once

namespace tomaqa::util {
    template <input_iterator ItT, invocable RandomT>
    void shuffle(ItT first, ItT last, RandomT&& rnd)
    {
        std::shuffle(first, last, FORWARD(rnd));
    }

    template <invocable RandomT>
    void shuffle(Container auto& cont, RandomT&& rnd)
    {
        util::shuffle(begin(cont), end(cont), FORWARD(rnd));
    }
}
