#pragma once

#include "tomaqa/util/union.hpp"

namespace tomaqa::util {
    template <typename> class Ref;
}

namespace tomaqa::util {
    template <typename T>
    class Ref : protected Inherit<Union<Rm_cvref<T>, Add_ptr<Rm_ref<T>>>, Ref<T>> {
    public:
        using Inherit = tomaqa::Inherit<Union<Rm_cvref<T>, Add_ptr<Rm_ref<T>>>, Ref<T>>;
        using typename Inherit::This;

        using Item = Rm_cvref<T>;

        static constexpr bool is_const_v = tomaqa::is_const_v<Rm_ref<T>>;

        Ref()                                                                             = default;
        ~Ref()                                                                            = default;
        Ref(const Ref&)                                                                   = default;
        Ref& operator =(const Ref&)                                                       = default;
        Ref(Ref&&)                                                                        = default;
        Ref& operator =(Ref&&)                                                            = default;
        //? made non-explicit, is it safe?
        constexpr Ref(auto&& i) requires requires { &i; };
        constexpr Ref(Item&&);
        constexpr Ref(const Item&&);
        constexpr Ref(Tag<Item>);
        template <Class T_ = This, typename U> requires (!is_base_of_v<T_, Rm_ref<U>>)
        constexpr Ref& operator =(U&&);

        constexpr const T& citem() const                                { return item_impl(*this); }
        constexpr const T& item() const&                                         { return citem(); }
        constexpr T& item()&                                            { return item_impl(*this); }
        constexpr T&& item()&&                                              { return move(item()); }

        using Inherit::valid;
        using Inherit::invalid;
        constexpr bool is_owner() const noexcept;

        String to_string() const&;
        String to_string()&&;

        using Inherit::equals;
    protected:
        using Ptr = Add_ptr<Item>;
    private:
        static constexpr auto& item_impl(auto&&);

        static String to_string_impl(auto&&);
    };
}

#include "tomaqa/util/ref.inl"
