#pragma once

namespace tomaqa::util {
    template <typename T>
    constexpr Ref<T>::Ref(auto&& i) requires requires { &i; }
        : Inherit(&i)
    { }

    template <typename T>
    constexpr Ref<T>::Ref(Item&& i)
        : Inherit(std::move(i))
    { }

    template <typename T>
    constexpr Ref<T>::Ref(const Item&& i)
        : Inherit(std::move(i))
    { }

    template <typename T>
    constexpr Ref<T>::Ref(Tag<Item>)
        : Ref(Item())
    { }

    template <typename T>
    template <Class T_, typename U> requires (!is_base_of_v<T_, Rm_ref<U>>)
    constexpr Ref<T>& Ref<T>::operator =(U&& i)
    {
        if constexpr (is_same_v<U&&, Item&&> || is_same_v<U&&, const Item&&>) {
            Inherit::Parent::operator =(std::move(i));
        }
        else if constexpr (requires { &i; }) {
            Inherit::Parent::operator =(&i);
        }
        else static_assert(false_tp<U>);
        return *this;
    }

    template <typename T>
    constexpr auto& Ref<T>::item_impl(auto&& r)
    {
        return r.visit([](auto& arg) -> Forward_as_ref<T, decltype(arg)> {
            if constexpr (is_ptr_v<Rm_ref<decltype(arg)>>) return *arg;
            else return arg;
        });
    }

    template <typename T>
    constexpr bool Ref<T>::is_owner() const noexcept
    {
        return This::template valid<Item>();
    }

    template <typename T>
    String Ref<T>::to_string() const&
    {
        return to_string_impl(*this);
    }

    template <typename T>
    String Ref<T>::to_string()&&
    {
        return to_string_impl(std::move(*this));
    }

    template <typename T>
    String Ref<T>::to_string_impl(auto&& r)
    {
        using tomaqa::to_string;
        return to_string(item_impl(FORWARD(r)));
    }
}
