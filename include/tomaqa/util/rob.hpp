#pragma once

#include "tomaqa/util.hpp"

/** Serves to access private members of a third-party class
Example of usage:

namespace tomaqa::util::rob {
    struct Rob_var1_tag : Member_variable_tag_base<class1, int> {
        friend Type access(Rob_var1_tag);
    };
    constexpr Rob_var1_tag rob_var1_tag;

    template struct Access<Rob_var1_tag, &class1::var1>;
}

...

    cls1.*access(util::rob::var1_tag) = 5;

in the case of a member function:

    (cls1.*access(util::rob::foo1_tag))(arg1, arg2);
*/

namespace tomaqa::util::rob {
    template <Class TagT, typename TagT::Type> struct Access;

    template <Class, typename VarT> struct Member_variable_tag_base;
    template <Class, typename RetT = void, typename... Args> struct Member_function_tag_base;
}

namespace tomaqa::util::rob {
    template <Class TagT, typename TagT::Type mV>
    struct Access {
        friend typename TagT::Type access(TagT)                                       { return mV; }
    };

    template <Class C, typename VarT>
    struct Member_variable_tag_base {
        using Type = VarT C::*;
        // `friend Type access(Member_variable_tag_base);` is not used here directly
        // because friend declarations inside tp. class require tp. declaration
    };

    template <Class C, typename RetT, typename... Args>
    struct Member_function_tag_base {
        using Type = RetT (C::*)(Args...);
    };
}
