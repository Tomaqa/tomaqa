#pragma once

#include "tomaqa/util/alg.hpp"

namespace tomaqa::util {
    template <typename StreamT, typename FstreamT>
    void Run::set_stream_ptr(Stream_ptr<StreamT>& s_ptr, FstreamT& fs, const Path& path,
                             Stream_ptr<StreamT> std_s_ptr, const String& err_msg)
    {
        if (!path.empty()) return set_stream_ptr(s_ptr, fs, path, err_msg);

        s_ptr = std_s_ptr;
        check_stream_ptr(s_ptr, err_msg);
    }

    template <typename StreamT, typename FstreamT>
    void Run::set_stream_ptr(Stream_ptr<StreamT>& s_ptr, FstreamT& fs, const Path& path,
                             const String& err_msg)
    {
        //+ why it is not recognized automatically?
        //+ (e.g. clang has `is_same` requirement, but not `is_base_of` ...)
        fs = FstreamT(path.cparent());
        s_ptr = &fs;
        check_stream_ptr(s_ptr, err_msg);
    }

    template <typename StreamT>
    void Run::check_stream_ptr(const Stream_ptr<StreamT>& s_ptr, const String& err_msg)
    {
        expect(s_ptr && s_ptr->good(), err_msg);
    }
}
