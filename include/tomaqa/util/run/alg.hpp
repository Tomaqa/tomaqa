#pragma once

#include "tomaqa/util/run.hpp"

namespace tomaqa::util {
    Path common_dirname(const Path&, const Path& = pwd());
}
