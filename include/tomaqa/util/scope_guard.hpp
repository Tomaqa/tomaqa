#pragma once

#include "tomaqa/util.hpp"

#include "tomaqa/util/fun.hpp"

namespace tomaqa::util {
    //+ see 'std::experimental::scope_exit'
    template <invocable = Lazy<>> class [[nodiscard]] Scope_guard;
}

namespace tomaqa::util {
    template <invocable F>
    class Scope_guard {
    public:
        Scope_guard()                                               = default;
        ~Scope_guard();
        Scope_guard(const Scope_guard&)                              = delete;
        Scope_guard& operator =(const Scope_guard&)                  = delete;
        Scope_guard(Scope_guard&&)                                  = default;
        Scope_guard& operator =(Scope_guard&&)                      = default;
        Scope_guard(F guard_);

        //! it does not work if the lambda has captures ???
        void set_guard(F guard_);
        void unset_guard();
    private:
        F _guard{};
    };
}

#include "tomaqa/util/scope_guard.inl"
