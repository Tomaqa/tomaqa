#pragma once

namespace tomaqa::util {
    template <invocable F>
    Scope_guard<F>::~Scope_guard()
    {
        if constexpr (requires { bool(_guard); }) {
            if (_guard) _guard();
        }
        else _guard();
    }

    template <invocable F>
    Scope_guard<F>::Scope_guard(F guard_)
        : _guard(std::move(guard_))
    { }

    template <invocable F>
    void Scope_guard<F>::set_guard(F guard_)
    {
        _guard = std::move(guard_);
    }

    template <invocable F>
    void Scope_guard<F>::unset_guard()
    {
        set_guard(nullptr);
    }
}
