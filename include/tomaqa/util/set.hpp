#pragma once

#include "tomaqa/util/associative.hpp"

#include <set>

namespace tomaqa::util {
    template <typename KeyT, relation<KeyT, KeyT> = less<>> class Set;
    template <typename KeyT, relation<KeyT, KeyT> = less<>> class Multiset;
}

namespace tomaqa::util {
    template <typename KeyT, relation<KeyT, KeyT> CmpT>
    class Set : public Inherit<Associative_mixin<std::set<KeyT, CmpT>>, Set<KeyT, CmpT>> {
    public:
        using Inherit = tomaqa::Inherit<Associative_mixin<std::set<KeyT, CmpT>>, Set<KeyT, CmpT>>;
        using typename Inherit::This;
        using typename Inherit::Base;

        using Inherit::Inherit;
        using Inherit::Parent::operator =;

        String to_string() const;
    };

    //+ not tested etc. ...
    template <typename KeyT, relation<KeyT, KeyT> CmpT>
    class Multiset : public Inherit<Associative_mixin<std::multiset<KeyT, CmpT>>, Multiset<KeyT, CmpT>> {
    public:
        using Inherit = tomaqa::Inherit<Associative_mixin<std::multiset<KeyT, CmpT>>, Multiset<KeyT, CmpT>>;
        using typename Inherit::This;
        using typename Inherit::Base;

        using Inherit::Inherit;
        using Inherit::Parent::operator =;

        String to_string() const;
    };
}

#include "tomaqa/util/set.inl"
