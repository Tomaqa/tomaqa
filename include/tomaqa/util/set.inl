#pragma once

namespace tomaqa::util {
    template <typename KeyT, relation<KeyT, KeyT> CmpT>
    String Set<KeyT, CmpT>::to_string() const
    {
        using tomaqa::to_string;
        String str;
        for (const auto& e : *this) {
            str += to_string(e) + " ";
        }
        return str;
    }

    ////////////////////////////////////////////////////////////////

    template <typename KeyT, relation<KeyT, KeyT> CmpT>
    String Multiset<KeyT, CmpT>::to_string() const
    {
        using tomaqa::to_string;
        String str;
        for (const auto& e : *this) {
            str += to_string(e) + " ";
        }
        return str;
    }
}
