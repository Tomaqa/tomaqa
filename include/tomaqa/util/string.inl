#pragma once

#include "tomaqa/util/optional.hpp"

namespace tomaqa::util {
    template <Arithmetic Arg>
    Optional<Arg> to_value(String_c auto&& str) noexcept
    {
        Arg value;
        if (get_value<Arg>(value, FORWARD(str))) return value;
        return {};
    }

    template <Arithmetic Arg>
    Arg to_value_check(String_c auto&& str)
    {
        auto opt = to_value<Arg>(FORWARD(str));
        expect(opt.valid(),
               "Conversion from string to <"s + typeid(Arg).name()
               + "> value failed: " + str);
        return move(opt).value();
    }
}
