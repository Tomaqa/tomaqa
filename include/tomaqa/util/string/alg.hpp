#pragma once

#include "tomaqa/util/string.hpp"

namespace tomaqa::util {
    bool contains(String_view, String_view) noexcept;
    bool contains(String_view, char) noexcept;

    Vector<String> split(String, char delim);
    pair<String, String> split_to_pair(String, char delim);

    inline String extract_first(String_view, char delim);
    inline String extract_first(String_view, char delim1, char delim2);
    inline String extract_first(istream&, char delim);
    String extract_first(istream&, char delim1, char delim2);
    inline Vector<String> extract_all(String_view, char delim);
    inline Vector<String> extract_all(String_view, char delim1, char delim2);
    inline Vector<String> extract_all(istream&, char delim);
    Vector<String> extract_all(istream&, char delim1, char delim2);

    inline String::size_type replace_first(String&, char delim, String_view with, String::size_type pos = 0);
    String::size_type replace_first(String&, char delim1, char delim2, String_view with, String::size_type pos = 0);
    inline void replace_all(String&, char delim, String_view with, String::size_type pos = 0);
    void replace_all(String&, char delim1, char delim2, String_view with, String::size_type pos = 0);

    String repeat(char, int n);
    String repeat(String_view, int n);
}

#include "tomaqa/util/string/alg.inl"
