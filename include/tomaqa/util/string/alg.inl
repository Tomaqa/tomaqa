#pragma once

#include <sstream>

namespace tomaqa::util {
    String extract_first(String_view vw, char delim)
    {
        return extract_first(move(vw), delim, delim);
    }

    String extract_first(String_view vw, char delim1, char delim2)
    {
        istringstream iss(move(vw));
        return extract_first(iss, delim1, delim2);
    }

    String extract_first(istream& is, char delim)
    {
        return extract_first(is, delim, delim);
    }

    Vector<String> extract_all(String_view vw, char delim)
    {
        return extract_all(move(vw), delim, delim);
    }

    Vector<String> extract_all(String_view vw, char delim1, char delim2)
    {
        istringstream iss(move(vw));
        return extract_all(iss, delim1, delim2);
    }

    Vector<String> extract_all(istream& is, char delim)
    {
        return extract_all(is, delim, delim);
    }

    String::size_type replace_first(String& str, char delim, String_view with, String::size_type pos)
    {
        return replace_first(str, delim, delim, move(with), pos);
    }

    void replace_all(String& str, char delim, String_view with, String::size_type pos)
    {
        replace_all(str, delim, delim, move(with), pos);
    }
}
