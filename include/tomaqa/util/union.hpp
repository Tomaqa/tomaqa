#pragma once

#include "tomaqa/util.hpp"

#include "tomaqa/util/optional.hpp"

#include <variant>

namespace tomaqa::util {
    template <typename...> class Union;

    template <typename T> concept Union_c = requires { typename Rm_ref<T>::Union_t; };

    template <typename T> constexpr bool valid(const Union_c auto&) noexcept;

    template <typename T> constexpr auto&& get(Union_c auto&&);
    template <typename T> constexpr const T* get_l(const Union_c auto&) noexcept;
    template <typename T> constexpr T* get_l(Union_c auto&) noexcept;

    template <typename T> Optional<T> to(Union_c auto&&) noexcept;
    template <typename T> Optional<T> to(Union_c auto&) noexcept                           = delete;
    template <typename T> T to_check(Union_c auto&&);
    template <typename T> T to_check(Union_c auto&)                                        = delete;
}

namespace tomaqa::util {
    /// Composition is used instead of inheritance,
    /// because `std::visit' works reliably only directly with `std::variant'
    template <typename... Ts>
    class Union : public Static<Union<Ts...>> {
    public:
        using typename Static<Union<Ts...>>::This;
        using Base = std::variant<Ts...>;
        using Union_t = This;

        Union()                                                                           = default;
        ~Union()                                                                          = default;
        Union(const Union&)                                                               = default;
        Union& operator =(const Union&)                                                   = default;
        Union(Union&&)                                                                    = default;
        Union& operator =(Union&&)                                                        = default;
        constexpr Union(auto&&...);
        constexpr Union& operator =(const Base&);
        constexpr Union& operator =(Base&&);
        // Include no derivate, neither private nor protected
        template <Union_c T_ = This, typename T> requires (!is_base_of_v<T_, Rm_ref<T>>)
        Union& operator =(T&&);
        void swap(Union&) noexcept;

        constexpr size_t index() const noexcept;
        constexpr bool valid() const noexcept                                 { return !invalid(); }
        constexpr bool invalid() const noexcept;
        template <typename T> constexpr bool valid() const noexcept;

        template <typename T> constexpr const T& cget() const;
        template <typename T> constexpr const T& get() const&                  { return cget<T>(); }
        template <typename T> constexpr T& get()&;
        template <typename T> constexpr T&& get()&&                  { return std::move(get<T>()); }
        template <size_t idx> constexpr const auto& cget() const;
        template <size_t idx> constexpr const auto& get() const&             { return cget<idx>(); }
        template <size_t idx> constexpr auto& get()&;
        template <size_t idx> constexpr auto&& get()&&             { return std::move(get<idx>()); }
        template <typename T> constexpr const T* cget_l() const noexcept;
        template <typename T> constexpr const T* get_l() const noexcept      { return cget_l<T>(); }
        template <typename T> constexpr T* get_l() noexcept;
        template <size_t idx> constexpr auto cget_l() const noexcept;
        template <size_t idx> constexpr auto get_l() const noexcept        { return cget_l<idx>(); }
        template <size_t idx> constexpr auto get_l() noexcept;

        template <typename T> Optional<T> to() const& noexcept;
        template <typename T> Optional<T> to()&& noexcept;
        template <typename T> T to_check() const&;
        template <typename T> T to_check()&&;

        template <typename T> T& emplace(auto&&...);

        template <Union_c... OthersT>
        constexpr decltype(auto) cvisit(auto&& f, OthersT&&...) const;
        template <Union_c... OthersT>
        constexpr decltype(auto) visit(auto&& f, OthersT&&... others) const
                                                  { return cvisit(FORWARD(f), FORWARD(others)...); }
        template <Union_c... OthersT>
        constexpr decltype(auto) visit(auto&& f, OthersT&&...);

        constexpr bool equals(const This&) const noexcept;

        String to_string() const&;
        String to_string()&&;
    protected:
        const auto& cvar() const noexcept                                           { return _var; }
        const auto& var() const& noexcept                                         { return cvar(); }
        auto& var()& noexcept                                                       { return _var; }
        auto&& var()&& noexcept                                         { return std::move(var()); }
    private:
        template <typename T> static auto&& get_impl(auto&&);
        template <size_t idx> static auto&& get_impl(auto&&);

        template <typename T, bool checkV> static auto to_impl(Union_c auto&&) noexcept(!checkV);

        template <Union_c... UnionsT>
        static constexpr decltype(auto) visit_impl(auto&& f, UnionsT&&...);

        static String to_string_impl(auto&& f, Union_c auto&&);

        Base _var{};
    };
}

#include "tomaqa/util/union.inl"
