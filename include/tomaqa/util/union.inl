#pragma once

namespace tomaqa::util {
    template <typename... Ts>
    constexpr Union<Ts...>::Union(auto&&... args)
        : _var(FORWARD(args)...)
    { }

    template <typename... Ts>
    constexpr Union<Ts...>& Union<Ts...>::operator =(const Base& var_)
    {
        _var = var_;
        return *this;
    }

    template <typename... Ts>
    constexpr Union<Ts...>& Union<Ts...>::operator =(Base&& var_)
    {
        _var = std::move(var_);
        return *this;
    }

    template <typename... Ts>
    template <Union_c T_, typename T> requires (!is_base_of_v<T_, Rm_ref<T>>)
    Union<Ts...>& Union<Ts...>::operator =(T&& arg)
    {
        _var = FORWARD(arg);
        return *this;
    }

    template <typename... Ts>
    void Union<Ts...>::swap(Union& rhs) noexcept
    {
        _var.swap(rhs._var);
    }

    template <typename... Ts>
    constexpr size_t Union<Ts...>::index() const noexcept
    {
        return cvar().index();
    }

    template <typename... Ts>
    constexpr bool Union<Ts...>::invalid() const noexcept
    {
        return cvar().valueless_by_exception();
    }

    template <typename... Ts>
    template <typename T>
    constexpr bool Union<Ts...>::valid() const noexcept
    {
        return std::holds_alternative<T>(cvar());
    }

    template <typename... Ts>
    template <typename T>
    constexpr const T& Union<Ts...>::cget() const
    {
        assert(valid());
        return get_impl<T>(cvar());
    }

    template <typename... Ts>
    template <typename T>
    constexpr T& Union<Ts...>::get()&
    {
        assert(valid());
        return get_impl<T>(var());
    }

    template <typename... Ts>
    template <size_t idx>
    constexpr const auto& Union<Ts...>::cget() const
    {
        assert(valid());
        return get_impl<idx>(cvar());
    }

    template <typename... Ts>
    template <size_t idx>
    constexpr auto& Union<Ts...>::get()&
    {
        assert(valid());
        return get_impl<idx>(var());
    }

    template <typename... Ts>
    template <typename T>
    auto&& Union<Ts...>::get_impl(auto&& v)
    try {
        return std::get<T>(FORWARD(v));
    }
    catch (const std::bad_variant_access&) {
        THROW("Attempt to get invalid type of Union: ")
              + typeid(Rm_cvref<T>).name();
    }

    template <typename... Ts>
    template <size_t idx>
    auto&& Union<Ts...>::get_impl(auto&& v)
    {
        return get_impl<std::variant_alternative_t<idx, decltype(v)>>(FORWARD(v));
    }

    template <typename... Ts>
    template <typename T>
    constexpr const T* Union<Ts...>::cget_l() const noexcept
    {
        return std::get_if<T>(&cvar());
    }

    template <typename... Ts>
    template <typename T>
    constexpr T* Union<Ts...>::get_l() noexcept
    {
        return std::get_if<T>(&var());
    }

    template <typename... Ts>
    template <size_t idx>
    constexpr auto Union<Ts...>::cget_l() const noexcept
    {
        return std::get_if<idx>(&cvar());
    }

    template <typename... Ts>
    template <size_t idx>
    constexpr auto Union<Ts...>::get_l() noexcept
    {
        return std::get_if<idx>(&var());
    }

    template <typename... Ts>
    template <typename T>
    Optional<T> Union<Ts...>::to() const& noexcept
    {
        return to_impl<T, false>(*this);
    }

    template <typename... Ts>
    template <typename T>
    Optional<T> Union<Ts...>::to()&& noexcept
    {
        return to_impl<T, false>(std::move(*this));
    }

    template <typename... Ts>
    template <typename T>
    T Union<Ts...>::to_check() const&
    {
        return to_impl<T, true>(*this);
    }

    template <typename... Ts>
    template <typename T>
    T Union<Ts...>::to_check()&&
    {
        return to_impl<T, true>(std::move(*this));
    }

    template <typename... Ts>
    template <typename T, bool checkV>
    auto Union<Ts...>::to_impl(Union_c auto&& u) noexcept(!checkV)
    {
        using RetT = conditional_t<checkV, T, Optional<T>>;
        static constexpr auto f = [](auto&& arg) constexpr -> RetT {
            using Arg = decltype(arg);
            if constexpr (constructible_from<T, Arg>) return FORWARD(arg);
            else if constexpr (!checkV) return {};
            else THROW("Conversion of Union value<")
                 + typeid(Arg).name() + "> to <"
                 + typeid(T).name() + "> failed";
        };

        if constexpr (!checkV) return visit_impl(f, FORWARD(u));
        else try {
            return visit_impl(f, FORWARD(u));
        }
        catch (const Error& err) {
            if constexpr (checkV) throw err + ": "s + u.to_string();
            else assert(false);
        }
    }

    template <typename... Ts>
    template <typename T>
    T& Union<Ts...>::emplace(auto&&... args)
    {
        return var().template emplace<T>(FORWARD(args)...);
    }

    template <typename... Ts>
    template <Union_c... OthersT>
    constexpr decltype(auto)
    Union<Ts...>::cvisit(auto&& f, OthersT&&... others) const
    {
        assert(valid());
        return visit_impl(FORWARD(f), *this, FORWARD(others)...);
    }

    template <typename... Ts>
    template <Union_c... OthersT>
    constexpr decltype(auto)
    Union<Ts...>::visit(auto&& f, OthersT&&... others)
    {
        assert(valid());
        return visit_impl(FORWARD(f), *this, FORWARD(others)...);
    }

    template <typename... Ts>
    template <Union_c... UnionsT>
    constexpr decltype(auto)
    Union<Ts...>::visit_impl(auto&& f, UnionsT&&... unions)
    {
        constexpr auto var_f = [](auto&& u) -> auto& { return u.var(); };

        return std::visit(FORWARD(f), var_f(FORWARD(unions)...));
    }

    template <typename... Ts>
    constexpr bool Union<Ts...>::equals(const This& rhs) const noexcept
    {
        return cvar() == rhs.cvar();
    }

    template <typename... Ts>
    String Union<Ts...>::to_string() const&
    {
        using tomaqa::to_string;
        constexpr auto f = [](const auto& arg){ return to_string(arg); };
        return to_string_impl(f, *this);
    }

    template <typename... Ts>
    String Union<Ts...>::to_string()&&
    {
        using tomaqa::to_string;
        constexpr auto f = [](auto&& arg){ return to_string(std::move(arg)); };
        return to_string_impl(f, std::move(*this));
    }

    template <typename... Ts>
    String Union<Ts...>::to_string_impl(auto&& f, Union_c auto&& u)
    {
        if (u.invalid()) return "<invalid>";
        return visit_impl(FORWARD(f), FORWARD(u));
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace tomaqa::util {
    template <typename T>
    constexpr bool valid(const Union_c auto& u) noexcept
    {
        return u.template valid<T>();
    }

    template <typename T>
    constexpr auto&& get(Union_c auto&& u)
    {
        return FORWARD(u).template get<T>();
    }

    template <typename T>
    constexpr const T* get_l(const Union_c auto& u) noexcept
    {
        return u.template get_l<T>();
    }

    template <typename T>
    constexpr T* get_l(Union_c auto& u) noexcept
    {
        return u.template get_l<T>();
    }

    template <typename T>
    Optional<T> to(Union_c auto&& u) noexcept
    {
        return FORWARD(u).template to<T>();
    }

    template <typename T>
    T to_check(Union_c auto&& u)
    {
        return FORWARD(u).template to_check<T>();
    }
}
