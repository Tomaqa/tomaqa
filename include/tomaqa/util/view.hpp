#pragma once

#include "tomaqa/util.hpp"

#include "tomaqa/util/hash.hpp"

namespace tomaqa::util {
    template <typename V> concept View_c = requires { typename Rm_ref<V>::View_t; };
    template <typename V> concept Unique_view_c = View_c<V> && requires { &Rm_ref<V>::insert; };
}

namespace tomaqa::util::view {
    // Cannot use `View_c` for the CRTP argument - it is not known yet
    template <Class, typename T, template<typename...> typename AccCont = Vector>
    requires ((Container<T> || indirectly_readable<T>) && Container<AccCont<int>>)
    class Crtp;
    template <View_c> class Unique_mixin;

    template <typename T>
    concept Has_push_front = requires (T& t) { t.push_front(Value_t<T>());  t.pop_front(); };
}

namespace tomaqa::util {
    template <typename T, template<typename...> typename AccCont = Vector>
    requires ((Container<T> || indirectly_readable<T>) && Container<AccCont<int>>)
    class View;
    template <typename T, template<typename...> typename AccCont = Vector>
    requires ((Container<T> || indirectly_readable<T>) && Container<AccCont<int>>)
    class Unique_view;
}

#include "tomaqa/util/view/detail.hpp"

namespace tomaqa::util::view {
    template <Class V, Container ContT, template<typename...> typename AccCont>
    requires Container<AccCont<int>>
    class Crtp<V, ContT, AccCont> : public Inherit<detail::Cont_crtp<V, ContT, AccCont>> {
    public:
        using Inherit = tomaqa::Inherit<detail::Cont_crtp<V, ContT, AccCont>>;

        using Inherit::Inherit;
    };

    template <Class V, indirectly_readable P, template<typename...> typename AccCont>
    requires Container<AccCont<int>>
    class Crtp<V, P, AccCont>
        : public Inherit<Cond_mixin<Has_push_front<AccCont<P>>, detail::Front_mixin,
                         detail::Mixin<detail::Crtp<V, P, AccCont>>>> {
    public:
        using Inherit = tomaqa::Inherit<Cond_mixin<Has_push_front<AccCont<P>>, detail::Front_mixin,
                                       detail::Mixin<detail::Crtp<V, P, AccCont>>>>;

        using Inherit::Inherit;
    };

    template <View_c B>
    class Unique_mixin : public Inherit<B, Unique_mixin<B>> {
    public:
        using Inherit = tomaqa::Inherit<B, Unique_mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using typename Inherit::That;

        using typename Inherit::Accessor;
        using typename Inherit::Accessors;

        using Inherit::Inherit;

        bool contains(const Accessor&) const noexcept;
        template <Unique_view_c T = This, same_as<typename T::Ptr> P> requires T::random_access_v
        bool contains(P) const noexcept;

        void reserve(size_t);

        void clear();

        bool insert(Accessor);

        void merge(Unique_view_c auto&&);
        template <Class A, Unique_view_c T = This>
        requires same_as<Rm_cvref<A>, typename T::Accessors_set>
        void merge(A&&);
        template <Class A, Unique_view_c T = This>
        requires same_as<Rm_cvref<A>, typename T::Accessors>
        void merge(A&&);
    protected:
        friend Base;
        friend typename Inherit::Cont_mixin;
        template <View_c> friend class detail::Front_mixin;

        using Accessors_set = Hash<Accessor>;

        void swap_impl(That&) noexcept;

        const auto& caccessors_set() const noexcept                       { return _accessors_set; }
        auto& accessors_set() noexcept                                    { return _accessors_set; }

        /// Cannot use for `push_front_impl` (it may not be available)
        using Inherit::push_back_impl;
        void push_back_impl(Accessor);
        void pop_back_impl();
        template <Has_push_front = This> void push_front_impl(auto&&);
        template <Has_push_front = This> void pop_front_impl();
    private:
        Accessors_set _accessors_set{};
    };
}

namespace tomaqa::util {
    template <typename T, template<typename...> typename AccCont>
    requires ((Container<T> || indirectly_readable<T>) && Container<AccCont<int>>)
    class View final : public Inherit<view::Crtp<View<T, AccCont>, T, AccCont>> {
    public:
        using Inherit = tomaqa::Inherit<view::Crtp<View<T, AccCont>, T, AccCont>>;
        using typename Inherit::This;

        using Inherit::Inherit;
    };

    template <typename T, template<typename...> typename AccCont>
    requires ((Container<T> || indirectly_readable<T>) && Container<AccCont<int>>)
    class Unique_view final
        : public Inherit<view::Unique_mixin<view::Crtp<Unique_view<T, AccCont>, T, AccCont>>> {
    public:
        using Inherit =
            tomaqa::Inherit<view::Unique_mixin<view::Crtp<Unique_view<T, AccCont>, T, AccCont>>>;
        using typename Inherit::This;

        using Inherit::Inherit;
    };
}

#include "tomaqa/util/view.inl"

namespace tomaqa::util {
    static_assert(input_iterator<typename View<int*>::iterator>);
    static_assert(input_iterator<typename View<int*>::const_iterator>);
    static_assert(input_iterator<typename View<Vector<int>>::iterator>);
    static_assert(input_iterator<typename View<Vector<int>>::const_iterator>);
}
