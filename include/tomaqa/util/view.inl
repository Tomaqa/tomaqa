#pragma once

#include "tomaqa/util/alg.hpp"

namespace tomaqa::util::view {
    template <View_c B>
    void Unique_mixin<B>::swap_impl(That& rhs) noexcept
    {
        Inherit::swap_impl(rhs);
        accessors_set().swap(rhs.accessors_set());
    }

    template <View_c B>
    bool Unique_mixin<B>::contains(const Accessor& acc) const noexcept
    {
        return caccessors_set().contains(acc);
    }

    template <View_c B>
    template <Unique_view_c T, same_as<typename T::Ptr> P> requires T::random_access_v
    bool Unique_mixin<B>::contains(P ptr) const noexcept
    {
        return contains(This::to_accessor(ptr));
    }

    template <View_c B>
    void Unique_mixin<B>::reserve(size_t size_)
    {
        Inherit::reserve(size_);
        accessors_set().reserve(size_);
    }

    template <View_c B>
    void Unique_mixin<B>::clear()
    {
        Inherit::clear();
        accessors_set().clear();
    }

    template <View_c B>
    void Unique_mixin<B>::push_back_impl(Accessor acc)
    {
        accessors_set().insert(acc);
        Inherit::push_back_impl(std::move(acc));
    }

    template <View_c B>
    void Unique_mixin<B>::pop_back_impl()
    {
        //+ use multiset?
        accessors_set().erase(This::caccessors().back());
        Inherit::pop_back_impl();
    }

    template <View_c B>
    template <Has_push_front T>
    void Unique_mixin<B>::push_front_impl(auto&& arg)
    {
        if constexpr (is_same_v<DECAY(arg), Accessor>) {
            accessors_set().insert(arg);
        }
        Inherit::push_front_impl(FORWARD(arg));
    }

    template <View_c B>
    template <Has_push_front T>
    void Unique_mixin<B>::pop_front_impl()
    {
        //+ use multiset?
        accessors_set().erase(This::caccessors().front());
        Inherit::pop_front_impl();
    }

    template <View_c B>
    bool Unique_mixin<B>::insert(Accessor acc)
    {
        auto [_, inserted] = accessors_set().insert(acc);
        if (inserted) Inherit::push_back_impl(std::move(acc));
        return inserted;
    }

    template <View_c B>
    void Unique_mixin<B>::merge(Unique_view_c auto&& rhs)
    {
        accessors_set().merge(FORWARD(rhs.accessors_set()));
        append(This::accessors(), FORWARD(rhs.accessors()));
    }

    template <View_c B>
    template <Class A, Unique_view_c T>
    requires same_as<Rm_cvref<A>, typename T::Accessors_set>
    void Unique_mixin<B>::merge(A&& rhs)
    {
        append(This::accessors(), rhs);
        accessors_set().merge(FORWARD(rhs));
    }

    template <View_c B>
    template <Class A, Unique_view_c T>
    requires same_as<Rm_cvref<A>, typename T::Accessors>
    void Unique_mixin<B>::merge(A&& rhs)
    {
        std::copy(rhs.begin(), rhs.end(), inserter(accessors_set()));
        append(This::accessors(), FORWARD(rhs));
    }
}

#include "tomaqa/util/view/detail.inl"
