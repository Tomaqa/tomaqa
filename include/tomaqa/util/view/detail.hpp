#pragma once

#include "tomaqa/core/meta.hpp"

namespace tomaqa::util::view::detail {
    template <Class V, indirectly_readable PtrT,
              template<typename...> typename AccCont,
              indirectly_readable CPtrT = const PtrT, typename AccT = PtrT, bool randomAccess = false>
    class Crtp : public Static<Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>>,
                 public tomaqa::Crtp<V>,
                 public Container_types<Rm_ptr<PtrT>> {
    public:
        using typename Static<Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>>::This;
        using typename tomaqa::Crtp<V>::That;
        using Base = This;
        using View_t = That;

        struct const_iterator;
        struct iterator;

        using Const_ptr = CPtrT;
        using Ptr = PtrT;

        using Accessor = AccT;
        template <typename AccT_> using Accessors_tp = AccCont<AccT_>;
        using Accessors = Accessors_tp<Accessor>;

        static_assert(Container<Accessors>);

        using Const_access_iterator = typename Accessors::const_iterator;
        using Access_iterator = typename Accessors::iterator;

        static constexpr bool random_access_v = randomAccess;

        Crtp()                                                                            = default;
        ~Crtp()                                                                           = default;
        Crtp(const Crtp&)                                                                 = default;
        Crtp& operator =(const Crtp&)                                                     = default;
        Crtp(Crtp&&)                                                                      = default;
        Crtp& operator =(Crtp&&)                                                          = default;
        void swap(That&) noexcept;

        const_iterator make_const_iterator(Const_access_iterator) const noexcept;
        iterator make_iterator(Access_iterator) noexcept;

        const auto& caccessors() const noexcept                               { return _accessors; }
        auto& accessors() noexcept                                            { return _accessors; }
        const auto& caccessor(Idx idx) const noexcept                  { return caccessors()[idx]; }
        auto& accessor(Idx idx) noexcept                                { return accessors()[idx]; }

        size_t size() const noexcept                             { return std::size(caccessors()); }
        bool empty() const noexcept                             { return std::empty(caccessors()); }

        Const_access_iterator cacc_begin() const noexcept      { return std::cbegin(caccessors()); }
        Const_access_iterator cacc_end() const noexcept          { return std::cend(caccessors()); }
        Const_access_iterator acc_begin() const noexcept                    { return cacc_begin(); }
        Const_access_iterator acc_end() const noexcept                        { return cacc_end(); }
        Access_iterator acc_begin() noexcept                     { return std::begin(accessors()); }
        Access_iterator acc_end() noexcept                         { return std::end(accessors()); }

        const_iterator cbegin() const noexcept;
        const_iterator cend() const noexcept;
        const_iterator begin() const noexcept                                   { return cbegin(); }
        const_iterator end() const noexcept                                       { return cend(); }
        iterator begin() noexcept;
        iterator end() noexcept;
        const auto& cfront() const noexcept                                    { return *cbegin(); }
        const auto& cback() const noexcept                                     { return *--cend(); }
        const auto& front() const noexcept                                      { return cfront(); }
        const auto& back() const noexcept                                        { return cback(); }
        auto& front() noexcept                                                  { return *begin(); }
        auto& back() noexcept                                                   { return *--end(); }

        const auto& caccess(const Accessor&) const noexcept;
        auto& access(const Accessor&) noexcept;

        const auto& operator [](Idx) const noexcept;
        auto& operator [](Idx) noexcept;

        void reserve(size_t size_)                                   { accessors().reserve(size_); }
        void resize(size_t size_)                                     { accessors().resize(size_); }

        void clear()                                                        { accessors().clear(); }

        void push_back(Accessor);
        void pop_back();

        String to_string() const&;
    protected:
        template <input_iterator AccIt> class Iterator_crtp;

        void swap_impl(That&) noexcept;

        void push_back_impl(Accessor);
        void pop_back_impl();
    private:
        Accessors _accessors{};
    };

    template <View_c B, Container ContT>
    class Cont_mixin : public Inherit<B, Cont_mixin<B, ContT>> {
    public:
        using Inherit = tomaqa::Inherit<B, Cont_mixin<B, ContT>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using typename Inherit::That;

        using Cont = Rm_cvref<ContT>;

        using Const_it = typename Cont::const_iterator;
        using It = conditional_t<is_const_v<Cont>, Const_it, typename Cont::iterator>;

        using Inherit::Inherit;

        using Inherit::push_back;
        void push_back(input_iterator auto);

        template <input_iterator ItT> void assign(ItT first, ItT last, size_t);
        template <input_iterator ItT> void assign(ItT first, ItT last);
        void assign(Container auto&&);
    protected:
        using Inherit::push_back_impl;
        void push_back_impl(input_iterator auto);
    };

    template <View_c B, bool randomAccess = false>
    class Mixin : public Inherit<B> {
    public:
        using Inherit = tomaqa::Inherit<B>;
        using typename Inherit::That;
        using typename Inherit::Base;

        using typename Inherit::Ptr;

        static_assert(!Inherit::random_access_v);

        using typename Inherit::Accessor;

        static_assert(is_same_v<Accessor, Ptr>);

        using typename Inherit::Const_access_iterator;
        using typename Inherit::Access_iterator;

        using typename Inherit::const_iterator;
        using typename Inherit::iterator;

        using Inherit::Inherit;
    protected:
        friend Base;

        template <input_iterator AccIt> class Iterator_base;

        const_iterator make_const_iterator_impl(Const_access_iterator it) const noexcept
                                                                                      { return it; }
        iterator make_iterator_impl(Access_iterator it) noexcept                      { return it; }

        const auto& caccess_impl(const Accessor& acc) const noexcept                { return *acc; }
        auto& access_impl(const Accessor& acc) noexcept                             { return *acc; }
    };

    template <View_c B>
    class Mixin<B, true> : public Inherit<B> {
    public:
        using Inherit = tomaqa::Inherit<B>;
        using typename Inherit::That;
        using typename Inherit::Base;

        using typename Inherit::Cont;

        using typename Inherit::It;

        static_assert(Inherit::random_access_v);

        using typename Inherit::Ptr;

        using typename Inherit::Accessor;

        using typename Inherit::Const_access_iterator;
        using typename Inherit::Access_iterator;

        using typename Inherit::const_iterator;
        using typename Inherit::iterator;

        using Inherit::Inherit;
        Mixin()                                                                           = default;
        Mixin(const Mixin&)                                                                = delete;
        Mixin& operator =(const Mixin&)                                                    = delete;
        Mixin(Mixin&&)                                                                    = default;
        Mixin& operator =(Mixin&&)                                                        = default;
        explicit Mixin(Cont&);

        void connect(Cont&);

        using Inherit::push_back;
        void push_back(Ptr);
    protected:
        friend Base;
        friend typename Inherit::Cont_mixin;

        template <input_iterator AccIt> class Iterator_base;

        void swap_impl(That&) noexcept;

        const_iterator make_const_iterator_impl(Const_access_iterator) const noexcept;
        iterator make_iterator_impl(Access_iterator) noexcept;

        const auto& ccont_l() const noexcept                                     { return _cont_l; }
        auto& cont_l() noexcept                                                  { return _cont_l; }
        const auto& ccont() const noexcept                                    { return *ccont_l(); }
        const auto& cont() const noexcept                                        { return ccont(); }
        auto& cont() noexcept                                                  { return *cont_l(); }

        const auto& caccess_impl(const Accessor&) const noexcept;
        auto& access_impl(const Accessor& acc) noexcept;

        //+ not implemented for iterators, need to distinguish const/non-const
        Accessor to_accessor(Ptr) const noexcept;

        using Inherit::push_back_impl;
        void push_back_impl(input_iterator auto);
    private:
        /// (zero-initialization)
        Cont* _cont_l{};
    };

    template <View_c B>
    class Front_mixin : public Inherit<B> {
    public:
        using Inherit = tomaqa::Inherit<B>;
        using typename Inherit::That;
        using typename Inherit::Base;

        using typename Inherit::Accessor;

        using Inherit::Inherit;

        void push_front(Accessor);
        void push_front(input_iterator auto);
        void push_front(indirectly_readable auto);
        void pop_front();
    protected:
        void push_front_impl(Accessor);
        void push_front_impl(input_iterator auto);
        void pop_front_impl();
    };

    template <typename V, Container ContT, template<typename...> typename AccCont,
              typename C = Rm_cvref<ContT>,
              bool randomAccess = random_access_iterator<typename C::iterator>,
              indirectly_readable PtrT = typename C::pointer,
              typename AccT = conditional_t<!randomAccess, PtrT, typename C::size_type>>
        using Cont_crtp =
            Cond_mixin<Has_push_front<AccCont<AccT>>, Front_mixin,
            Mixin<Cont_mixin<Crtp<V, PtrT, AccCont, typename C::const_pointer, AccT, randomAccess>,
                             ContT>, randomAccess>
            >;
}

namespace tomaqa::util::view::detail {
    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    template <input_iterator AccIt>
    class Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::Iterator_crtp
        : public Inherit<AccIt>,
          public tomaqa::Crtp<typename V::template Iterator_base<AccIt>> {
    public:
        using Inherit = tomaqa::Inherit<AccIt>;

        using value_type = typename Crtp::value_type;
        using reference = typename Crtp::reference;
        using pointer = typename Crtp::pointer;
        using size_type = typename Crtp::size_type;

        using Inherit::Inherit;
    protected:
        const auto& cderef() const noexcept;
        // Also const because this object is not modified
        auto& deref() const noexcept;
    };

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    struct Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator
        : public Inherit<typename V::template Iterator_base<Const_access_iterator>,
                         const_iterator> {
        using Inherit = tomaqa::Inherit<typename V::template Iterator_base<Const_access_iterator>,
                                        const_iterator>;
        using typename Inherit::This;

        using Inherit::Inherit;

        const auto& operator *() const noexcept                        { return Inherit::cderef(); }

        const_iterator& operator ++() noexcept;
        const_iterator operator ++(int) noexcept;
        const_iterator operator +(int) const noexcept;
        const_iterator& operator --() noexcept;
        const_iterator operator --(int) noexcept;
        const_iterator operator -(int) const noexcept;
    };

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    struct Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator
        : public Inherit<typename V::template Iterator_base<Access_iterator>,
                         iterator> {
        using Inherit = tomaqa::Inherit<typename V::template Iterator_base<Access_iterator>,
                                        iterator>;
        using typename Inherit::This;

        using Inherit::Inherit;

        auto& operator *() const noexcept                               { return Inherit::deref(); }

        iterator& operator ++() noexcept;
        iterator operator ++(int) noexcept;
        iterator operator +(int) const noexcept;
        iterator& operator --() noexcept;
        iterator operator --(int) noexcept;
        iterator operator -(int) const noexcept;
    };

    template <View_c B, bool randomAccess>
    template <input_iterator AccIt>
    class Mixin<B, randomAccess>::Iterator_base
        : public tomaqa::Inherit<typename B::template Iterator_crtp<AccIt>> {
    public:
        using Inherit = tomaqa::Inherit<typename B::template Iterator_crtp<AccIt>>;

        using Inherit::Inherit;
    protected:
        friend typename Inherit::Iterator_crtp;

        const auto& cderef() const noexcept;
        auto& deref() const noexcept;
    };

    template <View_c B>
    template <input_iterator AccIt>
    class Mixin<B, true>::Iterator_base
        : public tomaqa::Inherit<typename B::template Iterator_crtp<AccIt>> {
    public:
        using Inherit = tomaqa::Inherit<typename B::template Iterator_crtp<AccIt>>;
        using typename Inherit::Parent;

        using Cont_link = conditional_t<is_same_v<AccIt, typename Mixin::Access_iterator>,
                                        Cont*, const Cont*>;

        Iterator_base()                                                                   = default;
        ~Iterator_base()                                                                  = default;
        Iterator_base(const Iterator_base&)                                               = default;
        Iterator_base& operator =(const Iterator_base&)                                   = default;
        Iterator_base(Iterator_base&&)                                                    = default;
        Iterator_base& operator =(Iterator_base&&)                                        = default;
        Iterator_base(Parent, Cont_link);
    protected:
        friend typename Inherit::Iterator_crtp;

        const auto& ccont_l() const noexcept                                     { return _cont_l; }
        auto& cont_l() const noexcept                                            { return _cont_l; }

        const auto& cderef() const noexcept;
        auto& deref() const noexcept;
    private:
        /// (zero-initialization)
        Cont_link _cont_l{};
    };
}
