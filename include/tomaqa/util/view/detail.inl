#pragma once

#include "tomaqa/util/alg.hpp"

namespace tomaqa::util::view::detail {
    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    void Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::swap(That& rhs) noexcept
    {
        This::that().swap_impl(rhs);
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    void Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::swap_impl(That& rhs) noexcept
    {
        accessors().swap(rhs.accessors());
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::
        make_const_iterator(Const_access_iterator it) const noexcept
    {
        return This::cthat().make_const_iterator_impl(it);
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::
        make_iterator(Access_iterator it) noexcept
    {
        return This::that().make_iterator_impl(it);
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::cbegin() const noexcept
    {
        return make_const_iterator(cacc_begin());
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::cend() const noexcept
    {
        return make_const_iterator(cacc_end());
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::begin() noexcept
    {
        return make_iterator(acc_begin());
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::end() noexcept
    {
        return make_iterator(acc_end());
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    const auto& Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::
        caccess(const Accessor& acc) const noexcept
    {
        return This::cthat().caccess_impl(acc);
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    auto& Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::
        access(const Accessor& acc) noexcept
    {
        return This::that().access_impl(acc);
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    const auto&
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::operator [](Idx idx) const noexcept
    {
        return caccess(caccessor(idx));
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    auto&
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::operator [](Idx idx) noexcept
    {
        return access(accessor(idx));
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    void Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::push_back(Accessor acc)
    {
        This::that().push_back_impl(std::move(acc));
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    void Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::push_back_impl(Accessor acc)
    {
        accessors().push_back(std::move(acc));
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    void Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::pop_back()
    {
        This::that().pop_back_impl();
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    void Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::pop_back_impl()
    {
        accessors().pop_back();
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    String Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::to_string() const&
    {
        using tomaqa::to_string;
        String str;
        for (auto& e : This::cthat()) {
            str += to_string(e) + " ";
        }
        return str;
    }

    ////////////////////////////////////////////////////////////////

    template <View_c B, Container ContT>
    void Cont_mixin<B, ContT>::push_back(input_iterator auto it)
    {
        This::that().push_back_impl(it);
    }

    template <View_c B, Container ContT>
    void Cont_mixin<B, ContT>::push_back_impl(input_iterator auto it)
    {
        This::push_back(to_pointer(it));
    }

    template <View_c B, Container ContT>
    template <input_iterator ItT>
    void Cont_mixin<B, ContT>::assign(ItT first, ItT last, size_t size_)
    {
        This::clear();
        This::reserve(size_);
        for (auto it = first; it != last; ++it) push_back(it);
    }

    template <View_c B, Container ContT>
    template <input_iterator ItT>
    void Cont_mixin<B, ContT>::assign(ItT first, ItT last)
    {
        assign(first, last, last - first);
    }

    template <View_c B, Container ContT>
    void Cont_mixin<B, ContT>::assign(Container auto&& cont_)
    {
        assign(begin_tp(cont_), end_tp(cont_), std::size(cont_));
    }

    ////////////////////////////////////////////////////////////////

    template <View_c B>
    Mixin<B, true>::Mixin(Cont& cont_)
        : _cont_l(&cont_)
    { }

    template <View_c B>
    void Mixin<B, true>::swap_impl(That& rhs) noexcept
    {
        Inherit::swap_impl(rhs);
        std::swap(_cont_l, rhs._cont_l);
    }

    template <View_c B>
    void Mixin<B, true>::connect(Cont& cont_)
    {
        cont_l() = &cont_;
    }

    template <View_c B>
    typename Mixin<B, true>::const_iterator
    Mixin<B, true>::make_const_iterator_impl(Const_access_iterator it) const noexcept
    {
        return {it, ccont_l()};
    }

    template <View_c B>
    typename Mixin<B, true>::iterator
    Mixin<B, true>::make_iterator_impl(Access_iterator it) noexcept
    {
        return {it, cont_l()};
    }

    template <View_c B>
    const auto& Mixin<B, true>::caccess_impl(const Accessor& acc) const noexcept
    {
        return ccont()[acc];
    }

    template <View_c B>
    auto& Mixin<B, true>::access_impl(const Accessor& acc) noexcept
    {
        return cont()[acc];
    }

    template <View_c B>
    typename Mixin<B, true>::Accessor Mixin<B, true>::to_accessor(Ptr ptr) const noexcept
    {
        /// Unreliable in general compared to the impl. with `distance`
        return ptr - &this->caccess(0);
    }

    template <View_c B>
    void Mixin<B, true>::push_back(Ptr ptr)
    {
        this->that().push_back(to_accessor(ptr));
    }

    template <View_c B>
    void Mixin<B, true>::push_back_impl(input_iterator auto it)
    {
        Accessor acc = std::distance(std::begin(cont()), it);
        this->push_back(std::move(acc));
    }

    ////////////////////////////////////////////////////////////////

    template <View_c B>
    void Front_mixin<B>::push_front(Accessor acc)
    {
        this->that().push_front_impl(std::move(acc));
    }

    template <View_c B>
    void Front_mixin<B>::push_front_impl(Accessor acc)
    {
        this->accessors().push_front(std::move(acc));
    }

    template <View_c B>
    void Front_mixin<B>::push_front(input_iterator auto it)
    {
        this->that().push_front_impl(it);
    }

    template <View_c B>
    void Front_mixin<B>::push_front_impl(input_iterator auto it)
    {
        if constexpr (Inherit::random_access_v) {
            Accessor acc = std::distance(std::begin(this->cont()), it);
            this->push_front(std::move(acc));
        }
        else this->push_front(to_pointer(it));
    }

    template <View_c B>
    void Front_mixin<B>::push_front(indirectly_readable auto ptr)
    {
        this->that().push_front(to_accessor(ptr));
    }

    template <View_c B>
    void Front_mixin<B>::pop_front()
    {
        this->that().pop_front_impl();
    }

    template <View_c B>
    void Front_mixin<B>::pop_front_impl()
    {
        this->accessors().pop_front();
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace tomaqa::util::view::detail {
    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    template <input_iterator AccIt>
    const auto&
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::Iterator_crtp<AccIt>::cderef() const noexcept
    {
        return This::cthat().cderef();
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    template <input_iterator AccIt>
    auto&
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::Iterator_crtp<AccIt>::deref() const noexcept
    {
        return This::cthat().deref();
    }

    ////////////////////////////////////////////////////////////////

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator&
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator::operator ++() noexcept
    {
        Inherit::operator ++();
        return *this;
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator::operator ++(int) noexcept
    {
        auto it = Inherit::operator ++(0);
        if constexpr (!Crtp::random_access_v) return it;
        else return {it, This::ccont_l()};
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator::
        operator +(int n) const noexcept
    {
        /// It may not be implemented as a member function
        auto it = static_cast<const Inherit&>(*this) + n;
        if constexpr (!Crtp::random_access_v) return it;
        else return {it, This::ccont_l()};
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator&
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator::operator --() noexcept
    {
        Inherit::operator --();
        return *this;
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator::operator --(int) noexcept
    {
        auto it = Inherit::operator --(0);
        if constexpr (!Crtp::random_access_v) return it;
        else return {it, This::ccont_l()};
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator::
        operator -(int n) const noexcept
    {
        /// It may not be implemented as a member function
        auto it = static_cast<const Inherit&>(*this) - n;
        if constexpr (!Crtp::random_access_v) return it;
        else return {it, This::ccont_l()};
    }

    ////////////////////////////////////////////////////////////////

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator&
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator::operator ++() noexcept
    {
        Inherit::operator ++();
        return *this;
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator::operator ++(int) noexcept
    {
        auto it = Inherit::operator ++(0);
        if constexpr (!Crtp::random_access_v) return it;
        else return {it, This::ccont_l()};
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator::operator +(int n) const noexcept
    {
        /// It may not be implemented as a member function
        auto it = static_cast<const Inherit&>(*this) + n;
        if constexpr (!Crtp::random_access_v) return it;
        else return {it, This::ccont_l()};
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator&
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator::operator --() noexcept
    {
        Inherit::operator --();
        return *this;
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator::operator --(int) noexcept
    {
        auto it = Inherit::operator --(0);
        if constexpr (!Crtp::random_access_v) return it;
        else return {it, This::ccont_l()};
    }

    template <Class V, indirectly_readable PtrT, template<typename...> typename AccCont,
              indirectly_readable CPtrT, typename AccT, bool randomAccess>
    typename Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator
    Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator::operator -(int n) const noexcept
    {
        /// It may not be implemented as a member function
        auto it = static_cast<const Inherit&>(*this) - n;
        if constexpr (!Crtp::random_access_v) return it;
        else return {it, This::ccont_l()};
    }

    ////////////////////////////////////////////////////////////////

    template <View_c B, bool randomAccess>
    template <input_iterator AccIt>
    const auto& Mixin<B, randomAccess>::Iterator_base<AccIt>::cderef() const noexcept
    {
        return *Inherit::operator *();
    }

    template <View_c B, bool randomAccess>
    template <input_iterator AccIt>
    auto& Mixin<B, randomAccess>::Iterator_base<AccIt>::deref() const noexcept
    {
        return *Inherit::operator *();
    }

    ////////////////////////////////////////////////////////////////

    template <View_c B>
    template <input_iterator AccIt>
    Mixin<B, true>::Iterator_base<AccIt>::Iterator_base(Parent par, Cont_link cont_l_)
        : Inherit(par), _cont_l(cont_l_)
    { }

    template <View_c B>
    template <input_iterator AccIt>
    const auto& Mixin<B, true>::Iterator_base<AccIt>::cderef() const noexcept
    {
        const Accessor& acc = Inherit::operator *();
        return (*ccont_l())[acc];
    }

    template <View_c B>
    template <input_iterator AccIt>
    auto& Mixin<B, true>::Iterator_base<AccIt>::deref() const noexcept
    {
        const Accessor& acc = Inherit::operator *();
        return (*cont_l())[acc];
    }
}
