#include "tomaqa/test.hpp"
#include "tomaqa/math.hpp"
#include "tomaqa/math/alg.hpp"

namespace tomaqa::test {
    using namespace math;

    ////////////////////////////////////////////////////////////////

    template <floating_point FloatT, typename RationalT = void>
    void rational_test()
    {
        using Float = FloatT;
        using Rational = conditional_t<is_void_v<RationalT>, Rational_for<Float>, RationalT>;

        constexpr bool shrink = sizeof(typename Rational::int_type) < sizeof(Float);

        Vector<Rational> rat_arr;
        Vector<Float> f_arr;
        for (int n = 0; n <= 10000; ++n) {
            for (int d : {1, 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97}) {
                rat_arr.emplace_back(n, d);
            }
        }
        for (auto& rat : rat_arr) {
            const Float f = to_float<Rational, Float>(rat);
            expect(f == Float(rat.numerator())/rat.denominator(),
                   "Conversion of rational to float failed: "s + to_string(rat) + " != " + to_string(f));
            f_arr.push_back(f);
        }

        for (auto& f : f_arr) {
            const Rational rat = to_rational<Float, Rational>(f);
            const Float f2 = to_float<Rational, Float>(rat);
            expect(!shrink ? f2 == f : apx_equal<precision::High>(f2, f),
                   "Conversion of float to rational failed: "s + to_string(f) + (!shrink ? " != " : " !~ ") + to_string(rat) + " (" + to_string(f2) + ")");
        }
    }

    ////////////////////////////////////////////////////////////////

    template <floating_point FloatT>
    struct Apx_rational_input {
        using Float = FloatT;

        Float x;

        String to_string() const
        {
            using tomaqa::to_string;

            return to_string(x);
        }
    };

    template <floating_point FloatT>
    struct Apx_rational_output {
        using Rational = Rational_for<FloatT>;

        Rational rat;

        String to_string() const
        {
            using tomaqa::to_string;

            return to_string(rat);
        }
    };

    template <floating_point FloatT>
    bool operator ==(const Apx_rational_output<FloatT>& lhs, const Apx_rational_output<FloatT>& rhs) noexcept
    {
        return lhs.rat == rhs.rat;
    }

    template <floating_point FloatT, Side sideV, Precision_c P = precision::Medium>
    struct Apx_rational_case : Inherit<Case<Apx_rational_input<FloatT>, Apx_rational_output<FloatT>>> {
        using Inherit = tomaqa::Inherit<Case<Apx_rational_input<FloatT>, Apx_rational_output<FloatT>>>;

        using typename Inherit::Input;
        using typename Inherit::Output;

        using Float = typename Input::Float;
        using Rational = typename Output::Rational;

        static_assert(!is_same_v<P, precision::Max>);
        static_assert(!is_same_v<P, precision::Huge>);

        using Inherit::Inherit;

        Output result() override
        {
            auto& in = this->cinput();
            auto& [r] = in;

            return {to_apx_rational<sideV, P>(r)};
        }

        void finish() override
        {
            using Lower_p = typename P::Lower;

            Inherit::finish();

            auto& in = this->cinput();
            auto& [r] = in;

            auto& res = this->cresult();
            auto& [rat] = res;

            const auto rat_r = to_float(rat);
            assert(rat_r == (to_float<Rational, Float>(rat)));

            constexpr auto eps_ = P::abs_eps();
            constexpr int precision = -P::abs_eps_exp()*2;

            expect(apx_equal<Lower_p>(r, rat_r),
                   "The produced apx. rational is not close to the original float even with lower precision: "s
                   + to_string(rat_r, precision) + " !~~ " + to_string(r, precision));

            //? it should still work
            if constexpr (is_same_v<P, precision::High> && sizeof(Float) < 6) return;
            if constexpr (is_same_v<P, precision::Medium> && sizeof(Float) <= 4) return;

            const bool cond = invoke([r, rat_r]{
                constexpr float mult = 2;
                if constexpr (sideV == Side::right) return rat_r >= r && rat_r <= r + mult*eps_;
                else if constexpr (sideV == Side::left) return rat_r >= r - mult*eps_ && rat_r <= r;
                else return apx_equal<P>(r, rat_r);
            });

            expect(cond,
                   "The produced apx. rational is not close enough to the original float: "s
                   + to_string(rat_r, precision) + " !~ " + to_string(r, precision));
        }
    };

    ////////////////////////////////////////////////////////////////

    struct Interval_input {
        String oper;
        Interval a, b{};

        bool binary() const
        {
            return contains({"+", "-", "*", "/", "&", "|"}, oper);
        }

        String arity() const
        {
            return binary() ? "binary" : "unary";
        }

        String to_string() const
        {
            String str = a.to_string();
            if (!binary()) return oper + move(str);
            str += oper + b.to_string();
            return str;
        }
    };

    using Interval_output = Interval;

    struct Interval_case : Inherit<Case<Interval_input, Interval_output>> {
        using Inherit::Inherit;

        bool condition(const Output& expected_, const Output& result_) const override
        {
            return apx_equal(expected_, result_);
        }

        Output result() override
        {
            auto& in = cinput();
            auto& [oper, a, b] = in;

            auto check_f = [&](const String& oper1, const String& oper2, const Interval& i1, const Interval& i2){
                expect(i1 == i2,
                       oper1 + " and " + in.arity() + " " + oper2 + " does not give the same result: "
                       + i1.to_string() + " vs. " + i2.to_string());
                return i1;
            };

            if (!in.binary()) {
                if (oper == "middle") return a.middle();
                if (oper == "size") return a.size();
                if (oper == "radius") return a.radius();
                if (oper == "neg") {
                    auto cp = a;
                    return check_f(oper, "-", cp.neg(), -cp);
                }
            }

            if (oper == "+") {
                auto cp = a;
                return check_f("+=", oper, cp += b, a + b);
            }
            else if (oper == "-") {
                auto cp = a;
                return check_f("-=", oper, cp -= b, a - b);
            }
            else if (oper == "*") {
                auto cp = a;
                return check_f("*=", oper, cp *= b, a * b);
            }
            else if (oper == "/") {
                auto cp = a;
                return check_f("/=", oper, cp /= b, a / b);
            }
            else if (oper == "&") {
                auto cp = a;
                return check_f("&=", oper, cp &= b, a & b);
            }
            else if (oper == "|") {
                auto cp = a;
                return check_f("|=", oper, cp |= b, a | b);
            }

            THROW("Unknown interval "s + in.arity() + " operation: " + oper);
        }
    };

    ////////////////////////////////////////////////////////////////

    struct Quadratic_input {
        Float a, b, c;

        String to_string() const
        {
            return tomaqa::to_string(tuple<Float, Float, Float>(a, b, c));
        }
    };

    using Quadratic_output = Interval;

    template <bool lessV>
    struct Quadratic_case : Inherit<Case<Quadratic_input, Quadratic_output>> {
        using Inherit::Inherit;

        bool condition(const Output& expected_, const Output& result_) const override
        {
            return apx_equal(expected_, result_);
        }

        Output result() override
        {
            auto [a, b, c] = cinput();
            return quadratic_ineq<lessV>(a, b, c);
        }

        void finish() override
        {
            Inherit::finish();

            auto [a, b, c] = cinput();

            auto& res = cresult();

            if (a == 0) {
                const Output res_l = linear_ineq<lessV>(b, c);
                expect(res == res_l,
                       "Quadratic output differs from linear output in the linear case: "s
                       + res.to_string() + " != " + res_l.to_string());

                if (b == 0) {
                    const Output res_c = const_ineq<lessV>(c);
                    expect(res_l == res_c,
                           "Linear output differs from const output in the const case: "s
                           + res_l.to_string() + " != " + res_c.to_string());
                }
            }
        }
    };

    ////////////////////////////////////////////////////////////////
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

int main(int, const char*[])
try {
    using namespace tomaqa;
    using namespace tomaqa::test;
    using namespace tomaqa::math;
    using namespace std;
    using tomaqa::ignore;
    using math::nan;

    ////////////////////////////////////////////////////////////////

    rational_test<double>();
    rational_test<float>();
    rational_test<double, Rational_tp<int32_t>>();
    rational_test<float, Rational_tp<int64_t>>();

    ////////////////////////////////////////////////////////////////

    constexpr auto rat_f = []([[maybe_unused]] Rational rat_r, [[maybe_unused]] Rational rat_d){
        if constexpr (_release_) return rat_r;
        else return rat_d;
    };

    const String apx_rat_msg = "apx. float-to-rational";
    /*+
    Suite(apx_rat_msg + "<double, right, Huge>").test<Apx_rational_case<double, Side::right, precision::Huge>>({
        { {0},                                                                                      {} },
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {2273378, 1607521} } },
        { {sqrt3},                                                                                  { {978122, 564719} } },
        { {e},                                                                                      { {4904759, 1804360} } },
        { {pi},                                                                                     { {5419351, 1725033} } },
    });
    Suite(apx_rat_msg + "<double, right, High>").test<Apx_rational_case<double, Side::right, precision::High>>({
        { {0},                                                                                      {} },
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {66922, 47321} } },
        { {sqrt3},                                                                                  { {70226, 40545} } },
        { {e},                                                                                      { {49171, 18089} } },
        { {pi},                                                                                     { {104348, 33215} } },
    });
    */
    Suite(apx_rat_msg + "<double, right, Medium>").test<Apx_rational_case<double, Side::right, precision::Medium>>({
        { {0},                                                                                      {} },
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {1970, 1393} } },
        { {sqrt3},                                                                                  { {1351, 780} } },
        { {e},                                                                                      { {4178, 1537} } },
        { {pi},                                                                                     { {355, 113} } },
        { {precision::Low::abs_eps()},                                                              { {2, 1999} } },
        { {precision::Medium::abs_eps()},                                                           { {1, 500000} } },
        { {precision::High::abs_eps()},                                                             { {1, 999001} } },
        { {5*precision::Low::abs_eps()},                                                            { {25, 4999} } },
        { {5*precision::Medium::abs_eps()},                                                         { {1, 166667} } },
        { {5*precision::High::abs_eps()},                                                           { {1, 995025} } },
    });
    Suite(apx_rat_msg + "<double, right, Low>").test<Apx_rational_case<double, Side::right, precision::Low>>({
        { {0},                                                                                      {} },
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {58, 41} } },
        { {sqrt3},                                                                                  { {97, 56} } },
        { {e},                                                                                      { {87, 32} } },
        { {pi},                                                                                     { {355, 113} } },
        { {precision::Low::abs_eps()},                                                              { {1, 500} } },
        { {precision::Medium::abs_eps()},                                                           { {1, 1000} } },
        { {precision::High::abs_eps()},                                                             { {1, 1000} } },
        { {5*precision::Low::abs_eps()},                                                            { {1, 167} } },
        { {5*precision::Medium::abs_eps()},                                                         { {1, 996} } },
        { {5*precision::High::abs_eps()},                                                           { {1, 1000} } },
    });
    Suite(apx_rat_msg + "<double, right, Low>", true).test<Apx_rational_case<double, Side::right, precision::Low>>({
        { {0},                                                                                      { {1, int(pow(10, -precision::Low::abs_eps_exp()))} } },
        { {1},                                                                                      { {1+int(pow(10, -precision::Low::abs_eps_exp())), int(pow(10, -precision::Low::abs_eps_exp()))} } },
        { {2},                                                                                      { {2+int(pow(10, -precision::Low::abs_eps_exp())), int(pow(10, -precision::Low::abs_eps_exp()))} } },
    });
    /*+
    Suite(apx_rat_msg + "<double, left, Huge>").test<Apx_rational_case<double, Side::left, precision::Huge>>({
        //+ too slow convergence & mem usage:
        //+ { {0},                                                                                      {} },
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {1607521, 1136689} } },
        { {sqrt3},                                                                                  { {1694157, 978122} } },
        { {e},                                                                                      { {1084483, 398959} } },
        { {pi},                                                                                     { {4272943, 1360120} } },
    });
    Suite(apx_rat_msg + "<double, left, High>").test<Apx_rational_case<double, Side::left, precision::High>>({
        //+ too slow convergence & mem usage:
        //+ { {0},                                                                                      {} },
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {47321, 33461} } },
        { {sqrt3},                                                                                  { {51409, 29681} } },
        { {e},                                                                                      { {124288, 45723} } },
        { {pi},                                                                                     { {103993, 33102} } },
    });
    */
    Suite(apx_rat_msg + "<double, left, Medium>").test<Apx_rational_case<double, Side::left, precision::Medium>>({
        { {0},                                                                                      {} },
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {1393, 985} } },
        { {sqrt3},                                                                                  { {2340, 1351} } },
        { {e},                                                                                      { {2721, 1001} } },
        { {pi},                                                                                     { rat_f({21988, 6999}, {20568, 6547}) } },  //! optimization should not affect results
        { {precision::Low::abs_eps()},                                                              { {1, 1000} } },
        { {precision::Medium::abs_eps()},                                                           {} },
        { {precision::High::abs_eps()},                                                             {} },
        { {5*precision::Low::abs_eps()},                                                            { {1, 200} } },
        { {5*precision::Medium::abs_eps()},                                                         { {1, 200000} } },
        { {5*precision::High::abs_eps()},                                                           {} },
    });
    Suite(apx_rat_msg + "<double, left, Low>").test<Apx_rational_case<double, Side::left, precision::Low>>({
        { {0},                                                                                      {} },
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {41, 29} } },
        { {sqrt3},                                                                                  { {71, 41} } },
        { {e},                                                                                      { {106, 39} } },
        { {pi},                                                                                     { {201, 64} } },
        { {precision::Low::abs_eps()},                                                              {} },
        { {precision::Medium::abs_eps()},                                                           {} },
        { {precision::High::abs_eps()},                                                             {} },
        { {5*precision::Low::abs_eps()},                                                            { {1, 200} } },
        { {5*precision::Medium::abs_eps()},                                                         {} },
        { {5*precision::High::abs_eps()},                                                           {} },
    });
    Suite(apx_rat_msg + "<double, left, Low>", true).test<Apx_rational_case<double, Side::left, precision::Low>>({
        { {0},                                                                                      { {-1, int(pow(10, -precision::Low::abs_eps_exp()))} } },
        { {1},                                                                                      { {1-int(pow(10, -precision::Low::abs_eps_exp())), int(pow(10, -precision::Low::abs_eps_exp()))} } },
        { {2},                                                                                      { {2-int(pow(10, -precision::Low::abs_eps_exp())), int(pow(10, -precision::Low::abs_eps_exp()))} } },
    });
    /*+
    Suite(apx_rat_msg + "<double, center, Huge>").test<Apx_rational_case<double, Side::center, precision::Huge>>({
        //+ too slow convergence & mem usage:
        //+ { {0},                                                                                      {} },
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {1607521, 1136689} } },
        { {sqrt3},                                                                                  { {2672279, 1542841} } },
        { {e},                                                                                      { {1084483, 398959} } },
        { {pi},                                                                                     { {4272943, 1360120} } },
    });
    Suite(apx_rat_msg + "<double, center, High>").test<Apx_rational_case<double, Side::center, precision::High>>({
        //+ too slow convergence & mem usage:
        //+ { {0},                                                                                      {} },
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {47321, 33461} } },
        { {sqrt3},                                                                                  { {70226, 40545} } },
        { {e},                                                                                      { {49171, 18089} } },
        { {pi},                                                                                     { {104348, 33215} } },
    });
    */
    Suite(apx_rat_msg + "<double, center, Medium>").test<Apx_rational_case<double, Side::center, precision::Medium>>({
        { {0},                                                                                      {} },
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {1393, 985} } },
        { {sqrt3},                                                                                  { {2340, 1351} } },
        { {e},                                                                                      { {2721, 1001} } },
        { {pi},                                                                                     { {355, 113} } },
    });
    Suite(apx_rat_msg + "<double, center, Low>").test<Apx_rational_case<double, Side::center, precision::Low>>({
        { {0},                                                                                      {} },
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {41, 29} } },
        { {sqrt3},                                                                                  { {71, 41} } },
        { {e},                                                                                      { {87, 32} } },
        { {pi},                                                                                     { {267, 85} } },
    });
    /*+
    Suite(apx_rat_msg + "<float, right, Huge>").test<Apx_rational_case<float, Side::right, precision::Huge>>({
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {1393, 985} } },
        { {sqrt3},                                                                                  { {989, 571} } },
        { {e},                                                                                      { {1264, 465} } },
        { {pi},                                                                                     { {355, 113} } },
    });
    Suite(apx_rat_msg + "<float, right, Huge>", true).test<Apx_rational_case<float, Side::right, precision::Huge>>({
        { {0},                                                                                      ignore },
    });
    Suite(apx_rat_msg + "<float, right, High>").test<Apx_rational_case<float, Side::right, precision::High>>({
        { {0},                                                                                      {} },
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {1393, 985} } },
        { {sqrt3},                                                                                  { {989, 571} } },
        { {e},                                                                                      { {1264, 465} } },
        { {pi},                                                                                     { {355, 113} } },
    });
    */
    Suite(apx_rat_msg + "<float, right, Medium>").test<Apx_rational_case<float, Side::right, precision::Medium>>({
        { {0},                                                                                      {} },
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {577, 408} } },
        { {sqrt3},                                                                                  { {1351, 780} } },
        { {e},                                                                                      { {1264, 465} } },
        { {pi},                                                                                     { {355, 113} } },
    });
    Suite(apx_rat_msg + "<float, right, Low>").test<Apx_rational_case<float, Side::right, precision::Low>>({
        { {0},                                                                                      {} },
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {58, 41} } },
        { {sqrt3},                                                                                  { {97, 56} } },
        { {e},                                                                                      { {87, 32} } },
        { {pi},                                                                                     { {377, 120} } },
    });
    /*+
    Suite(apx_rat_msg + "<float, left, Huge>").test<Apx_rational_case<float, Side::left, precision::Huge>>({
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {1393, 985} } },
        { {sqrt3},                                                                                  { {989, 571} } },
        { {e},                                                                                      { {1264, 465} } },
        { {pi},                                                                                     { {355, 113} } },
    });
    Suite(apx_rat_msg + "<float, left, Huge>", true).test<Apx_rational_case<float, Side::left, precision::Huge>>({
        { {0},                                                                                      ignore },
    });
    Suite(apx_rat_msg + "<float, left, High>").test<Apx_rational_case<float, Side::left, precision::High>>({
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {1393, 985} } },
        { {sqrt3},                                                                                  { {989, 571} } },
        { {e},                                                                                      { {1264, 465} } },
        { {pi},                                                                                     { {355, 113} } },
    });
    Suite(apx_rat_msg + "<float, left, High>", true).test<Apx_rational_case<float, Side::left, precision::High>>({
        { {0},                                                                                      ignore },
    });
    */
    Suite(apx_rat_msg + "<float, left, Medium>").test<Apx_rational_case<float, Side::left, precision::Medium>>({
        { {0},                                                                                      {} },
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {816, 577} } },
        { {sqrt3},                                                                                  { {989, 571} } },
        { {e},                                                                                      { {1264, 465} } },
        { {pi},                                                                                     { {355, 113} } },
    });
    Suite(apx_rat_msg + "<float, left, Low>").test<Apx_rational_case<float, Side::left, precision::Low>>({
        { {0},                                                                                      {} },
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {41, 29} } },
        { {sqrt3},                                                                                  { {71, 41} } },
        { {e},                                                                                      { {106, 39} } },
        { {pi},                                                                                     { {201, 64} } },
    });
    /*+
    Suite(apx_rat_msg + "<float, center, Huge>").test<Apx_rational_case<float, Side::center, precision::Huge>>({
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {1393, 985} } },
        { {sqrt3},                                                                                  { {989, 571} } },
        { {e},                                                                                      { {1264, 465} } },
        { {pi},                                                                                     { {355, 113} } },
    });
    Suite(apx_rat_msg + "<float, center, Huge>", true).test<Apx_rational_case<float, Side::center, precision::Huge>>({
        { {0},                                                                                      ignore },
    });
    Suite(apx_rat_msg + "<float, center, High>").test<Apx_rational_case<float, Side::center, precision::High>>({
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {1393, 985} } },
        { {sqrt3},                                                                                  { {989, 571} } },
        { {e},                                                                                      { {1264, 465} } },
        { {pi},                                                                                     { {355, 113} } },
    });
    Suite(apx_rat_msg + "<float, center, High>", true).test<Apx_rational_case<float, Side::center, precision::High>>({
        { {0},                                                                                      ignore },
    });
    */
    Suite(apx_rat_msg + "<float, center, Medium>").test<Apx_rational_case<float, Side::center, precision::Medium>>({
        { {0},                                                                                      {} },
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {1970, 1393} } },
        { {sqrt3},                                                                                  { {989, 571} } },
        { {e},                                                                                      { {1264, 465} } },
        { {pi},                                                                                     { {355, 113} } },
    });
    Suite(apx_rat_msg + "<float, center, Low>").test<Apx_rational_case<float, Side::center, precision::Low>>({
        { {0},                                                                                      {} },
        { {1},                                                                                      { {1} } },
        { {2},                                                                                      { {2} } },
        { {-1},                                                                                     { {-1} } },
        { {-2},                                                                                     { {-2} } },
        { {sqrt2},                                                                                  { {41, 29} } },
        { {sqrt3},                                                                                  { {71, 41} } },
        { {e},                                                                                      { {87, 32} } },
        { {pi},                                                                                     { {267, 85} } },
    });

    ////////////////////////////////////////////////////////////////

    const String int_msg = "interval operations";
    Suite(int_msg).test<Interval_case>({
        { { "middle", inf_interval },                                                               0. },
        { { "middle", pos_inf_interval },                                                           inf },
        { { "middle", neg_inf_interval },                                                           -inf },
        { { "middle", 0. },                                                                         0. },
        { { "middle", 1. },                                                                         1. },
        { { "middle", -1. },                                                                        -1. },
        { { "middle", {0.,2.} },                                                                    1. },
        { { "middle", {-3.,0.} },                                                                   -1.5 },
        { { "middle", {1.,2.} },                                                                    1.5 },
        { { "middle", {-3.,-1.} },                                                                  -2. },
        { { "middle", {-3.,1.} },                                                                   -1. },
        { { "size", inf_interval },                                                                 inf },
        { { "size", pos_inf_interval },                                                             inf },
        { { "size", neg_inf_interval },                                                             inf },
        { { "size", 0. },                                                                           0. },
        { { "size", 1. },                                                                           0. },
        { { "size", -1. },                                                                          0. },
        { { "size", {0.,2.} },                                                                      2. },
        { { "size", {-3.,0.} },                                                                     3. },
        { { "size", {1.,2.} },                                                                      1. },
        { { "size", {-3.,-1.} },                                                                    2. },
        { { "size", {-3.,1.} },                                                                     4. },
        { { "radius", inf_interval },                                                               inf },
        { { "radius", pos_inf_interval },                                                           inf },
        { { "radius", neg_inf_interval },                                                           inf },
        { { "radius", 0. },                                                                         0. },
        { { "radius", 1. },                                                                         0. },
        { { "radius", -1. },                                                                        0. },
        { { "radius", {0.,2.} },                                                                    1. },
        { { "radius", {-3.,0.} },                                                                   1.5 },
        { { "radius", {1.,2.} },                                                                    0.5 },
        { { "radius", {-3.,-1.} },                                                                  1. },
        { { "radius", {-3.,1.} },                                                                   2. },
        { { "neg", inf_interval },                                                                  inf_interval },
        { { "neg", pos_inf_interval },                                                              neg_inf_interval },
        { { "neg", neg_inf_interval },                                                              pos_inf_interval },
        { { "neg", 0. },                                                                            0. },
        { { "neg", 1. },                                                                            -1. },
        { { "neg", -1. },                                                                           1. },
        { { "neg", {0.,2.} },                                                                       {-2.,0.} },
        { { "neg", {-3.,0.} },                                                                      {0.,3.} },
        { { "neg", {1.,2.} },                                                                       {-2.,-1.} },
        { { "neg", {-3.,-1.} },                                                                     {1.,3.} },
        { { "neg", {-3.,1.} },                                                                      {-1.,3.} },
        { { "+", inf_interval, inf_interval },                                                      inf_interval },
        { { "+", inf_interval, pos_inf_interval },                                                  inf_interval },
        { { "+", inf_interval, neg_inf_interval },                                                  inf_interval },
        { { "+", inf_interval, 0. },                                                                inf_interval },
        { { "+", inf_interval, 1.5 },                                                               inf_interval },
        { { "+", inf_interval, -1.2 },                                                              inf_interval },
        { { "+", inf_interval, {0.,3.} },                                                           inf_interval },
        { { "+", inf_interval, {-4.,0.} },                                                          inf_interval },
        { { "+", inf_interval, {1.5,2.5} },                                                         inf_interval },
        { { "+", inf_interval, {-3.5,-1.} },                                                        inf_interval },
        { { "+", inf_interval, {-3.,1.5} },                                                         inf_interval },
        { { "+", pos_inf_interval, inf_interval },                                                  inf_interval },
        { { "+", pos_inf_interval, pos_inf_interval },                                              pos_inf_interval },
        { { "+", pos_inf_interval, neg_inf_interval },                                              inf_interval },
        { { "+", pos_inf_interval, 0. },                                                            pos_inf_interval },
        { { "+", pos_inf_interval, 1.5 },                                                           {1.5,inf} },
        { { "+", pos_inf_interval, -1.2 },                                                          {-1.2,inf} },
        { { "+", pos_inf_interval, {0.,3.} },                                                       pos_inf_interval },
        { { "+", pos_inf_interval, {-4.,0.} },                                                      {-4.,inf} },
        { { "+", pos_inf_interval, {1.5,2.5} },                                                     {1.5,inf} },
        { { "+", pos_inf_interval, {-3.5,-1.} },                                                    {-3.5,inf} },
        { { "+", pos_inf_interval, {-3.,1.5} },                                                     {-3.,inf} },
        { { "+", neg_inf_interval, inf_interval },                                                  inf_interval },
        { { "+", neg_inf_interval, pos_inf_interval },                                              inf_interval },
        { { "+", neg_inf_interval, neg_inf_interval },                                              neg_inf_interval },
        { { "+", neg_inf_interval, 0. },                                                            neg_inf_interval },
        { { "+", neg_inf_interval, 1.5 },                                                           {-inf,1.5} },
        { { "+", neg_inf_interval, -1.2 },                                                          {-inf,-1.2} },
        { { "+", neg_inf_interval, {0.,3.} },                                                       {-inf,3.} },
        { { "+", neg_inf_interval, {-4.,0.} },                                                      neg_inf_interval },
        { { "+", neg_inf_interval, {1.5,2.5} },                                                     {-inf,2.5} },
        { { "+", neg_inf_interval, {-3.5,-1.} },                                                    {-inf,-1.} },
        { { "+", neg_inf_interval, {-3.,1.5} },                                                     {-inf,1.5} },
        { { "+", 0., inf_interval },                                                                inf_interval },
        { { "+", 0., pos_inf_interval },                                                            pos_inf_interval },
        { { "+", 0., neg_inf_interval },                                                            neg_inf_interval },
        { { "+", 0., 0. },                                                                          0. },
        { { "+", 0., 1.5 },                                                                         1.5 },
        { { "+", 0., -1.2 },                                                                        -1.2 },
        { { "+", 0., {0.,3.} },                                                                     {0.,3.} },
        { { "+", 0., {-4.,0.} },                                                                    {-4.,0.} },
        { { "+", 0., {1.5,2.5} },                                                                   {1.5,2.5} },
        { { "+", 0., {-3.5,-1.} },                                                                  {-3.5,-1.} },
        { { "+", 0., {-3.,1.5} },                                                                   {-3.,1.5} },
        { { "+", 1., inf_interval },                                                                inf_interval },
        { { "+", 1., pos_inf_interval },                                                            {1.,inf} },
        { { "+", 1., neg_inf_interval },                                                            {-inf,1.} },
        { { "+", 1., 0. },                                                                          1. },
        { { "+", 1., 1.5 },                                                                         2.5 },
        { { "+", 1., -1.2 },                                                                        -0.2 },
        { { "+", 1., {0.,3.} },                                                                     {1.,4.} },
        { { "+", 1., {-4.,0.} },                                                                    {-3.,1.} },
        { { "+", 1., {1.5,2.5} },                                                                   {2.5,3.5} },
        { { "+", 1., {-3.5,-1.} },                                                                  {-2.5,-0.} },
        { { "+", 1., {-3.,1.5} },                                                                   {-2.,2.5} },
        { { "+", -1., inf_interval },                                                               inf_interval },
        { { "+", -1., pos_inf_interval },                                                           {-1.,inf} },
        { { "+", -1., neg_inf_interval },                                                           {-inf,-1.} },
        { { "+", -1., 0. },                                                                         -1. },
        { { "+", -1., 1.5 },                                                                        0.5 },
        { { "+", -1., -1.2 },                                                                       -2.2 },
        { { "+", -1., {0.,3.} },                                                                    {-1.,2.} },
        { { "+", -1., {-4.,0.} },                                                                   {-5.,-1.} },
        { { "+", -1., {1.5,2.5} },                                                                  {0.5,1.5} },
        { { "+", -1., {-3.5,-1.} },                                                                 {-4.5,-2.} },
        { { "+", -1., {-3.,1.5} },                                                                  {-4.,0.5} },
        { { "+", {0.,2.}, inf_interval },                                                           inf_interval },
        { { "+", {0.,2.}, pos_inf_interval },                                                       pos_inf_interval },
        { { "+", {0.,2.}, neg_inf_interval },                                                       {-inf,2.} },
        { { "+", {0.,2.}, 0. },                                                                     {0.,2.} },
        { { "+", {0.,2.}, 1.5 },                                                                    {1.5,3.5} },
        { { "+", {0.,2.}, -1.2 },                                                                   {-1.2,0.8} },
        { { "+", {0.,2.}, {0.,3.} },                                                                {0.,5.} },
        { { "+", {0.,2.}, {-4.,0.} },                                                               {-4.,2.} },
        { { "+", {0.,2.}, {1.5,2.5} },                                                              {1.5,4.5} },
        { { "+", {0.,2.}, {-3.5,-1.} },                                                             {-3.5,1.} },
        { { "+", {0.,2.}, {-3.,1.5} },                                                              {-3.,3.5} },
        { { "+", {-3.,0.}, inf_interval },                                                          inf_interval },
        { { "+", {-3.,0.}, pos_inf_interval },                                                      {-3.,inf} },
        { { "+", {-3.,0.}, neg_inf_interval },                                                      neg_inf_interval },
        { { "+", {-3.,0.}, 0. },                                                                    {-3.,0.} },
        { { "+", {-3.,0.}, 1.5 },                                                                   {-1.5,1.5} },
        { { "+", {-3.,0.}, -1.2 },                                                                  {-4.2,-1.2} },
        { { "+", {-3.,0.}, {0.,3.} },                                                               {-3.,3.} },
        { { "+", {-3.,0.}, {-4.,0.} },                                                              {-7.,0.} },
        { { "+", {-3.,0.}, {1.5,2.5} },                                                             {-1.5,2.5} },
        { { "+", {-3.,0.}, {-3.5,-1.} },                                                            {-6.5,-1.} },
        { { "+", {-3.,0.}, {-3.,1.5} },                                                             {-6.,1.5} },
        { { "+", {1.,2.}, inf_interval },                                                           inf_interval },
        { { "+", {1.,2.}, pos_inf_interval },                                                       {1.,inf} },
        { { "+", {1.,2.}, neg_inf_interval },                                                       {-inf,2.} },
        { { "+", {1.,2.}, 0. },                                                                     {1.,2.} },
        { { "+", {1.,2.}, 1.5 },                                                                    {2.5,3.5} },
        { { "+", {1.,2.}, -1.2 },                                                                   {-0.2,0.8} },
        { { "+", {1.,2.}, {0.,3.} },                                                                {1.,5.} },
        { { "+", {1.,2.}, {-4.,0.} },                                                               {-3.,2.} },
        { { "+", {1.,2.}, {1.5,2.5} },                                                              {2.5,4.5} },
        { { "+", {1.,2.}, {-3.5,-1.} },                                                             {-2.5,1.} },
        { { "+", {1.,2.}, {-3.,1.5} },                                                              {-2.,3.5} },
        { { "+", {-3.,-1.}, inf_interval },                                                         inf_interval },
        { { "+", {-3.,-1.}, pos_inf_interval },                                                     {-3.,inf} },
        { { "+", {-3.,-1.}, neg_inf_interval },                                                     {-inf,-1.} },
        { { "+", {-3.,-1.}, 0. },                                                                   {-3.,-1.} },
        { { "+", {-3.,-1.}, 1.5 },                                                                  {-1.5,0.5} },
        { { "+", {-3.,-1.}, -1.2 },                                                                 {-4.2,-2.2} },
        { { "+", {-3.,-1.}, {0.,3.} },                                                              {-3.,2.} },
        { { "+", {-3.,-1.}, {-4.,0.} },                                                             {-7.,-1.} },
        { { "+", {-3.,-1.}, {1.5,2.5} },                                                            {-1.5,1.5} },
        { { "+", {-3.,-1.}, {-3.5,-1.} },                                                           {-6.5,-2.} },
        { { "+", {-3.,-1.}, {-3.,1.5} },                                                            {-6.,0.5} },
        { { "+", {-3.,1.}, inf_interval },                                                          inf_interval },
        { { "+", {-3.,1.}, pos_inf_interval },                                                      {-3.,inf} },
        { { "+", {-3.,1.}, neg_inf_interval },                                                      {-inf,1.} },
        { { "+", {-3.,1.}, 0. },                                                                    {-3.,1.} },
        { { "+", {-3.,1.}, 1.5 },                                                                   {-1.5,2.5} },
        { { "+", {-3.,1.}, -1.2 },                                                                  {-4.2,-0.2} },
        { { "+", {-3.,1.}, {0.,3.} },                                                               {-3.,4.} },
        { { "+", {-3.,1.}, {-4.,0.} },                                                              {-7.,1.} },
        { { "+", {-3.,1.}, {1.5,2.5} },                                                             {-1.5,3.5} },
        { { "+", {-3.,1.}, {-3.5,-1.} },                                                            {-6.5,0.} },
        { { "+", {-3.,1.}, {-3.,1.5} },                                                             {-6.,2.5} },
        { { "-", inf_interval, inf_interval },                                                      inf_interval },
        { { "-", inf_interval, pos_inf_interval },                                                  inf_interval },
        { { "-", inf_interval, neg_inf_interval },                                                  inf_interval },
        { { "-", inf_interval, 0. },                                                                inf_interval },
        { { "-", inf_interval, 1.5 },                                                               inf_interval },
        { { "-", inf_interval, -1.2 },                                                              inf_interval },
        { { "-", inf_interval, {0.,3.} },                                                           inf_interval },
        { { "-", inf_interval, {-4.,0.} },                                                          inf_interval },
        { { "-", inf_interval, {1.5,2.5} },                                                         inf_interval },
        { { "-", inf_interval, {-3.5,-1.} },                                                        inf_interval },
        { { "-", inf_interval, {-3.,1.5} },                                                         inf_interval },
        { { "-", pos_inf_interval, inf_interval },                                                  inf_interval },
        { { "-", pos_inf_interval, pos_inf_interval },                                              inf_interval },
        { { "-", pos_inf_interval, neg_inf_interval },                                              pos_inf_interval },
        { { "-", pos_inf_interval, 0. },                                                            pos_inf_interval },
        { { "-", pos_inf_interval, 1.5 },                                                           {-1.5,inf} },
        { { "-", pos_inf_interval, -1.2 },                                                          {1.2,inf} },
        { { "-", pos_inf_interval, {0.,3.} },                                                       {-3.,inf} },
        { { "-", pos_inf_interval, {-4.,0.} },                                                      pos_inf_interval },
        { { "-", pos_inf_interval, {1.5,2.5} },                                                     {-2.5,inf} },
        { { "-", pos_inf_interval, {-3.5,-1.} },                                                    {1.,inf} },
        { { "-", pos_inf_interval, {-3.,1.5} },                                                     {-1.5,inf} },
        { { "-", neg_inf_interval, inf_interval },                                                  inf_interval },
        { { "-", neg_inf_interval, pos_inf_interval },                                              neg_inf_interval },
        { { "-", neg_inf_interval, neg_inf_interval },                                              inf_interval },
        { { "-", neg_inf_interval, 0. },                                                            neg_inf_interval },
        { { "-", neg_inf_interval, 1.5 },                                                           {-inf,-1.5} },
        { { "-", neg_inf_interval, -1.2 },                                                          {-inf,1.2} },
        { { "-", neg_inf_interval, {0.,3.} },                                                       neg_inf_interval },
        { { "-", neg_inf_interval, {-4.,0.} },                                                      {-inf,4.} },
        { { "-", neg_inf_interval, {1.5,2.5} },                                                     {-inf,-1.5} },
        { { "-", neg_inf_interval, {-3.5,-1.} },                                                    {-inf,3.5} },
        { { "-", neg_inf_interval, {-3.,1.5} },                                                     {-inf,3.} },
        { { "-", 0., inf_interval },                                                                inf_interval },
        { { "-", 0., pos_inf_interval },                                                            neg_inf_interval },
        { { "-", 0., neg_inf_interval },                                                            pos_inf_interval },
        { { "-", 0., 0. },                                                                          0. },
        { { "-", 0., 1.5 },                                                                         -1.5 },
        { { "-", 0., -1.2 },                                                                        1.2 },
        { { "-", 0., {0.,3.} },                                                                     {-3.,0.} },
        { { "-", 0., {-4.,0.} },                                                                    {0.,4.} },
        { { "-", 0., {1.5,2.5} },                                                                   {-2.5,-1.5} },
        { { "-", 0., {-3.5,-1.} },                                                                  {1.,3.5} },
        { { "-", 0., {-3.,1.5} },                                                                   {-1.5,3.} },
        { { "-", 1., inf_interval },                                                                inf_interval },
        { { "-", 1., pos_inf_interval },                                                            {-inf,1.} },
        { { "-", 1., neg_inf_interval },                                                            {1.,inf} },
        { { "-", 1., 0. },                                                                          1. },
        { { "-", 1., 1.5 },                                                                         -0.5 },
        { { "-", 1., -1.2 },                                                                        2.2 },
        { { "-", 1., {0.,3.} },                                                                     {-2.,1.} },
        { { "-", 1., {-4.,0.} },                                                                    {1.,5.} },
        { { "-", 1., {1.5,2.5} },                                                                   {-1.5,-0.5} },
        { { "-", 1., {-3.5,-1.} },                                                                  {2.,4.5} },
        { { "-", 1., {-3.,1.5} },                                                                   {-0.5,4.} },
        { { "-", -1., inf_interval },                                                               inf_interval },
        { { "-", -1., pos_inf_interval },                                                           {-inf,-1.} },
        { { "-", -1., neg_inf_interval },                                                           {-1.,inf} },
        { { "-", -1., 0. },                                                                         -1. },
        { { "-", -1., 1.5 },                                                                        -2.5 },
        { { "-", -1., -1.2 },                                                                       0.2 },
        { { "-", -1., {0.,3.} },                                                                    {-4.,-1.} },
        { { "-", -1., {-4.,0.} },                                                                   {-1.,3.} },
        { { "-", -1., {1.5,2.5} },                                                                  {-3.5,-2.5} },
        { { "-", -1., {-3.5,-1.} },                                                                 {0.,2.5} },
        { { "-", -1., {-3.,1.5} },                                                                  {-2.5,2.} },
        { { "-", {0.,2.}, inf_interval },                                                           inf_interval },
        { { "-", {0.,2.}, pos_inf_interval },                                                       {-inf,2.} },
        { { "-", {0.,2.}, neg_inf_interval },                                                       pos_inf_interval },
        { { "-", {0.,2.}, 0. },                                                                     {0.,2.} },
        { { "-", {0.,2.}, 1.5 },                                                                    {-1.5,0.5} },
        { { "-", {0.,2.}, -1.2 },                                                                   {1.2,3.2} },
        { { "-", {0.,2.}, {0.,3.} },                                                                {-3.,2.} },
        { { "-", {0.,2.}, {-4.,0.} },                                                               {0.,6.} },
        { { "-", {0.,2.}, {1.5,2.5} },                                                              {-2.5,0.5} },
        { { "-", {0.,2.}, {-3.5,-1.} },                                                             {1.,5.5} },
        { { "-", {0.,2.}, {-3.,1.5} },                                                              {-1.5,5.} },
        { { "-", {-3.,0.}, inf_interval },                                                          inf_interval },
        { { "-", {-3.,0.}, pos_inf_interval },                                                      neg_inf_interval },
        { { "-", {-3.,0.}, neg_inf_interval },                                                      {-3.,inf} },
        { { "-", {-3.,0.}, 0. },                                                                    {-3.,0.} },
        { { "-", {-3.,0.}, 1.5 },                                                                   {-4.5,-1.5} },
        { { "-", {-3.,0.}, -1.2 },                                                                  {-1.8,1.2} },
        { { "-", {-3.,0.}, {0.,3.} },                                                               {-6.,0.} },
        { { "-", {-3.,0.}, {-4.,0.} },                                                              {-3.,4.} },
        { { "-", {-3.,0.}, {1.5,2.5} },                                                             {-5.5,-1.5} },
        { { "-", {-3.,0.}, {-3.5,-1.} },                                                            {-2.,3.5} },
        { { "-", {-3.,0.}, {-3.,1.5} },                                                             {-4.5,3.} },
        { { "-", {1.,2.}, inf_interval },                                                           inf_interval },
        { { "-", {1.,2.}, pos_inf_interval },                                                       {-inf,2.} },
        { { "-", {1.,2.}, neg_inf_interval },                                                       {1.,inf} },
        { { "-", {1.,2.}, 0. },                                                                     {1.,2.} },
        { { "-", {1.,2.}, 1.5 },                                                                    {-0.5,0.5} },
        { { "-", {1.,2.}, -1.2 },                                                                   {2.2,3.2} },
        { { "-", {1.,2.}, {0.,3.} },                                                                {-2.,2.} },
        { { "-", {1.,2.}, {-4.,0.} },                                                               {1.,6.} },
        { { "-", {1.,2.}, {1.5,2.5} },                                                              {-1.5,0.5} },
        { { "-", {1.,2.}, {-3.5,-1.} },                                                             {2.,5.5} },
        { { "-", {1.,2.}, {-3.,1.5} },                                                              {-0.5,5.} },
        { { "-", {-3.,-1.}, inf_interval },                                                         inf_interval },
        { { "-", {-3.,-1.}, pos_inf_interval },                                                     {-inf,-1.} },
        { { "-", {-3.,-1.}, neg_inf_interval },                                                     {-3.,inf} },
        { { "-", {-3.,-1.}, 0. },                                                                   {-3.,-1.} },
        { { "-", {-3.,-1.}, 1.5 },                                                                  {-4.5,-2.5} },
        { { "-", {-3.,-1.}, -1.2 },                                                                 {-1.8,0.2} },
        { { "-", {-3.,-1.}, {0.,3.} },                                                              {-6.,-1.} },
        { { "-", {-3.,-1.}, {-4.,0.} },                                                             {-3.,3.} },
        { { "-", {-3.,-1.}, {1.5,2.5} },                                                            {-5.5,-2.5} },
        { { "-", {-3.,-1.}, {-3.5,-1.} },                                                           {-2.,2.5} },
        { { "-", {-3.,-1.}, {-3.,1.5} },                                                            {-4.5,2.} },
        { { "-", {-3.,1.}, inf_interval },                                                          inf_interval },
        { { "-", {-3.,1.}, pos_inf_interval },                                                      {-inf,1.} },
        { { "-", {-3.,1.}, neg_inf_interval },                                                      {-3.,inf} },
        { { "-", {-3.,1.}, 0. },                                                                    {-3.,1.} },
        { { "-", {-3.,1.}, 1.5 },                                                                   {-4.5,-0.5} },
        { { "-", {-3.,1.}, -1.2 },                                                                  {-1.8,2.2} },
        { { "-", {-3.,1.}, {0.,3.} },                                                               {-6.,1.} },
        { { "-", {-3.,1.}, {-4.,0.} },                                                              {-3.,5.} },
        { { "-", {-3.,1.}, {1.5,2.5} },                                                             {-5.5,-0.5} },
        { { "-", {-3.,1.}, {-3.5,-1.} },                                                            {-2.,4.5} },
        { { "-", {-3.,1.}, {-3.,1.5} },                                                             {-4.5,4.} },
        { { "&", {}, {} },                                                                          {} },
        { { "&", {}, inf_interval },                                                                {} },
        { { "&", {}, pos_inf_interval },                                                            {} },
        { { "&", {}, neg_inf_interval },                                                            {} },
        { { "&", {}, 0. },                                                                          {} },
        { { "&", {}, 1.5 },                                                                         {} },
        { { "&", {}, -1.2 },                                                                        {} },
        { { "&", {}, {0.,3.} },                                                                     {} },
        { { "&", {}, {-4.,0.} },                                                                    {} },
        { { "&", {}, {1.5,2.5} },                                                                   {} },
        { { "&", {}, {-3.5,-1.} },                                                                  {} },
        { { "&", {}, {-3.,1.5} },                                                                   {} },
        { { "&", inf_interval, {} },                                                                {} },
        { { "&", inf_interval, inf_interval },                                                      inf_interval },
        { { "&", inf_interval, pos_inf_interval },                                                  pos_inf_interval },
        { { "&", inf_interval, neg_inf_interval },                                                  neg_inf_interval },
        { { "&", inf_interval, 0. },                                                                0. },
        { { "&", inf_interval, 1.5 },                                                               1.5 },
        { { "&", inf_interval, -1.2 },                                                              -1.2 },
        { { "&", inf_interval, {0.,3.} },                                                           {0.,3.} },
        { { "&", inf_interval, {-4.,0.} },                                                          {-4.,0.} },
        { { "&", inf_interval, {1.5,2.5} },                                                         {1.5,2.5} },
        { { "&", inf_interval, {-3.5,-1.} },                                                        {-3.5,-1.} },
        { { "&", inf_interval, {-3.,1.5} },                                                         {-3.,1.5} },
        { { "&", pos_inf_interval, {} },                                                            {} },
        { { "&", pos_inf_interval, inf_interval },                                                  pos_inf_interval },
        { { "&", pos_inf_interval, pos_inf_interval },                                              pos_inf_interval },
        { { "&", pos_inf_interval, neg_inf_interval },                                              0. },
        { { "&", pos_inf_interval, 0. },                                                            0. },
        { { "&", pos_inf_interval, 1.5 },                                                           1.5 },
        { { "&", pos_inf_interval, -1.2 },                                                          {} },
        { { "&", pos_inf_interval, {0.,3.} },                                                       {0.,3.} },
        { { "&", pos_inf_interval, {-4.,0.} },                                                      0. },
        { { "&", pos_inf_interval, {1.5,2.5} },                                                     {1.5,2.5} },
        { { "&", pos_inf_interval, {-3.5,-1.} },                                                    {} },
        { { "&", pos_inf_interval, {-3.,1.5} },                                                     {0.,1.5} },
        { { "&", neg_inf_interval, {} },                                                            {} },
        { { "&", neg_inf_interval, inf_interval },                                                  neg_inf_interval },
        { { "&", neg_inf_interval, pos_inf_interval },                                              0. },
        { { "&", neg_inf_interval, neg_inf_interval },                                              neg_inf_interval },
        { { "&", neg_inf_interval, 0. },                                                            0. },
        { { "&", neg_inf_interval, 1.5 },                                                           {} },
        { { "&", neg_inf_interval, -1.2 },                                                          -1.2 },
        { { "&", neg_inf_interval, {0.,3.} },                                                       0. },
        { { "&", neg_inf_interval, {-4.,0.} },                                                      {-4.,0.} },
        { { "&", neg_inf_interval, {1.5,2.5} },                                                     {} },
        { { "&", neg_inf_interval, {-3.5,-1.} },                                                    {-3.5,-1.} },
        { { "&", neg_inf_interval, {-3.,1.5} },                                                     {-3.,0.} },
        { { "&", 0., {} },                                                                          {} },
        { { "&", 0., inf_interval },                                                                0. },
        { { "&", 0., pos_inf_interval },                                                            0. },
        { { "&", 0., neg_inf_interval },                                                            0. },
        { { "&", 0., 0. },                                                                          0. },
        { { "&", 0., 1.5 },                                                                         {} },
        { { "&", 0., -1.2 },                                                                        {} },
        { { "&", 0., {0.,3.} },                                                                     0. },
        { { "&", 0., {-4.,0.} },                                                                    0. },
        { { "&", 0., {1.5,2.5} },                                                                   {} },
        { { "&", 0., {-3.5,-1.} },                                                                  {} },
        { { "&", 0., {-3.,1.5} },                                                                   0. },
        { { "&", 1., {} },                                                                          {} },
        { { "&", 1., inf_interval },                                                                1. },
        { { "&", 1., pos_inf_interval },                                                            1. },
        { { "&", 1., neg_inf_interval },                                                            {} },
        { { "&", 1., 0. },                                                                          {} },
        { { "&", 1., 1.5 },                                                                         {} },
        { { "&", 1., -1.2 },                                                                        {} },
        { { "&", 1., {0.,3.} },                                                                     1. },
        { { "&", 1., {-4.,0.} },                                                                    {} },
        { { "&", 1., {1.5,2.5} },                                                                   {} },
        { { "&", 1., {-3.5,-1.} },                                                                  {} },
        { { "&", 1., {-3.,1.5} },                                                                   1. },
        { { "&", -1., {} },                                                                         {} },
        { { "&", -1., inf_interval },                                                               -1. },
        { { "&", -1., pos_inf_interval },                                                           {} },
        { { "&", -1., neg_inf_interval },                                                           -1. },
        { { "&", -1., 0. },                                                                         {} },
        { { "&", -1., 1.5 },                                                                        {} },
        { { "&", -1., -1.2 },                                                                       {} },
        { { "&", -1., {0.,3.} },                                                                    {} },
        { { "&", -1., {-4.,0.} },                                                                   -1. },
        { { "&", -1., {1.5,2.5} },                                                                  {} },
        { { "&", -1., {-3.5,-1.} },                                                                 -1. },
        { { "&", -1., {-3.,1.5} },                                                                  -1. },
        { { "&", {0.,2.}, {} },                                                                     {} },
        { { "&", {0.,2.}, inf_interval },                                                           {0.,2.} },
        { { "&", {0.,2.}, pos_inf_interval },                                                       {0.,2.} },
        { { "&", {0.,2.}, neg_inf_interval },                                                       0. },
        { { "&", {0.,2.}, 0. },                                                                     0. },
        { { "&", {0.,2.}, 1.5 },                                                                    1.5 },
        { { "&", {0.,2.}, -1.2 },                                                                   {} },
        { { "&", {0.,2.}, {0.,3.} },                                                                {0.,2.} },
        { { "&", {0.,2.}, {-4.,0.} },                                                               0. },
        { { "&", {0.,2.}, {1.5,2.5} },                                                              {1.5,2.} },
        { { "&", {0.,2.}, {-3.5,-1.} },                                                             {} },
        { { "&", {0.,2.}, {-3.,1.5} },                                                              {0.,1.5} },
        { { "&", {-3.,0.}, {} },                                                                    {} },
        { { "&", {-3.,0.}, inf_interval },                                                          {-3.,0.} },
        { { "&", {-3.,0.}, pos_inf_interval },                                                      0. },
        { { "&", {-3.,0.}, neg_inf_interval },                                                      {-3.,0.} },
        { { "&", {-3.,0.}, 0. },                                                                    0. },
        { { "&", {-3.,0.}, 1.5 },                                                                   {} },
        { { "&", {-3.,0.}, -1.2 },                                                                  -1.2 },
        { { "&", {-3.,0.}, {0.,3.} },                                                               0. },
        { { "&", {-3.,0.}, {-4.,0.} },                                                              {-3.,0.} },
        { { "&", {-3.,0.}, {1.5,2.5} },                                                             {} },
        { { "&", {-3.,0.}, {-3.5,-1.} },                                                            {-3.,-1.} },
        { { "&", {-3.,0.}, {-3.,1.5} },                                                             {-3.,0.} },
        { { "&", {1.,2.}, {} },                                                                     {} },
        { { "&", {1.,2.}, inf_interval },                                                           {1.,2.} },
        { { "&", {1.,2.}, pos_inf_interval },                                                       {1.,2.} },
        { { "&", {1.,2.}, neg_inf_interval },                                                       {} },
        { { "&", {1.,2.}, 0. },                                                                     {} },
        { { "&", {1.,2.}, 1.5 },                                                                    1.5 },
        { { "&", {1.,2.}, -1.2 },                                                                   {} },
        { { "&", {1.,2.}, {0.,3.} },                                                                {1.,2.} },
        { { "&", {1.,2.}, {-4.,0.} },                                                               {} },
        { { "&", {1.,2.}, {1.5,2.5} },                                                              {1.5,2.} },
        { { "&", {1.,2.}, {-3.5,-1.} },                                                             {} },
        { { "&", {1.,2.}, {-3.,1.5} },                                                              {1.,1.5} },
        { { "&", {-3.,-1.}, {} },                                                                   {} },
        { { "&", {-3.,-1.}, inf_interval },                                                         {-3.,-1.} },
        { { "&", {-3.,-1.}, pos_inf_interval },                                                     {} },
        { { "&", {-3.,-1.}, neg_inf_interval },                                                     {-3.,-1.} },
        { { "&", {-3.,-1.}, 0. },                                                                   {} },
        { { "&", {-3.,-1.}, 1.5 },                                                                  {} },
        { { "&", {-3.,-1.}, -1.2 },                                                                 -1.2 },
        { { "&", {-3.,-1.}, {0.,3.} },                                                              {} },
        { { "&", {-3.,-1.}, {-4.,0.} },                                                             {-3.,-1.} },
        { { "&", {-3.,-1.}, {1.5,2.5} },                                                            {} },
        { { "&", {-3.,-1.}, {-3.5,-1.} },                                                           {-3.,-1.} },
        { { "&", {-3.,-1.}, {-3.,1.5} },                                                            {-3.,-1.} },
        { { "&", {-3.,1.}, {} },                                                                    {} },
        { { "&", {-3.,1.}, inf_interval },                                                          {-3.,1.} },
        { { "&", {-3.,1.}, pos_inf_interval },                                                      {0.,1.} },
        { { "&", {-3.,1.}, neg_inf_interval },                                                      {-3.,0.} },
        { { "&", {-3.,1.}, 0. },                                                                    0. },
        { { "&", {-3.,1.}, 1.5 },                                                                   {} },
        { { "&", {-3.,1.}, -1.2 },                                                                  -1.2 },
        { { "&", {-3.,1.}, {0.,3.} },                                                               {0.,1.} },
        { { "&", {-3.,1.}, {-4.,0.} },                                                              {-3.,0.} },
        { { "&", {-3.,1.}, {1.5,2.5} },                                                             {} },
        { { "&", {-3.,1.}, {-3.5,-1.} },                                                            {-3.,-1.} },
        { { "&", {-3.,1.}, {-3.,1.5} },                                                             {-3.,1.} },
        { { "|", {}, {} },                                                                          {} },
        { { "|", {}, inf_interval },                                                                inf_interval },
        { { "|", {}, pos_inf_interval },                                                            pos_inf_interval },
        { { "|", {}, neg_inf_interval },                                                            neg_inf_interval },
        { { "|", {}, 0. },                                                                          0. },
        { { "|", {}, 1.5 },                                                                         1.5 },
        { { "|", {}, -1.2 },                                                                        -1.2 },
        { { "|", {}, {0.,3.} },                                                                     {0.,3.} },
        { { "|", {}, {-4.,0.} },                                                                    {-4.,0.} },
        { { "|", {}, {1.5,2.5} },                                                                   {1.5,2.5} },
        { { "|", {}, {-3.5,-1.} },                                                                  {-3.5,-1.} },
        { { "|", {}, {-3.,1.5} },                                                                   {-3.,1.5} },
        { { "|", inf_interval, {} },                                                                inf_interval },
        { { "|", inf_interval, inf_interval },                                                      inf_interval },
        { { "|", inf_interval, pos_inf_interval },                                                  inf_interval },
        { { "|", inf_interval, neg_inf_interval },                                                  inf_interval },
        { { "|", inf_interval, 0. },                                                                inf_interval },
        { { "|", inf_interval, 1.5 },                                                               inf_interval },
        { { "|", inf_interval, -1.2 },                                                              inf_interval },
        { { "|", inf_interval, {0.,3.} },                                                           inf_interval },
        { { "|", inf_interval, {-4.,0.} },                                                          inf_interval },
        { { "|", inf_interval, {1.5,2.5} },                                                         inf_interval },
        { { "|", inf_interval, {-3.5,-1.} },                                                        inf_interval },
        { { "|", inf_interval, {-3.,1.5} },                                                         inf_interval },
        { { "|", pos_inf_interval, {} },                                                            pos_inf_interval },
        { { "|", pos_inf_interval, inf_interval },                                                  inf_interval },
        { { "|", pos_inf_interval, pos_inf_interval },                                              pos_inf_interval },
        { { "|", pos_inf_interval, neg_inf_interval },                                              inf_interval },
        { { "|", pos_inf_interval, 0. },                                                            pos_inf_interval },
        { { "|", pos_inf_interval, 1.5 },                                                           pos_inf_interval },
        { { "|", pos_inf_interval, -1.2 },                                                          {-1.2,inf} },
        { { "|", pos_inf_interval, {0.,3.} },                                                       pos_inf_interval },
        { { "|", pos_inf_interval, {-4.,0.} },                                                      {-4.,inf} },
        { { "|", pos_inf_interval, {1.5,2.5} },                                                     pos_inf_interval },
        { { "|", pos_inf_interval, {-3.5,-1.} },                                                    {-3.5,inf} },
        { { "|", pos_inf_interval, {-3.,1.5} },                                                     {-3.,inf} },
        { { "|", neg_inf_interval, {} },                                                            neg_inf_interval },
        { { "|", neg_inf_interval, inf_interval },                                                  inf_interval },
        { { "|", neg_inf_interval, pos_inf_interval },                                              inf_interval },
        { { "|", neg_inf_interval, neg_inf_interval },                                              neg_inf_interval },
        { { "|", neg_inf_interval, 0. },                                                            neg_inf_interval },
        { { "|", neg_inf_interval, 1.5 },                                                           {-inf,1.5} },
        { { "|", neg_inf_interval, -1.2 },                                                          neg_inf_interval },
        { { "|", neg_inf_interval, {0.,3.} },                                                       {-inf,3.} },
        { { "|", neg_inf_interval, {-4.,0.} },                                                      neg_inf_interval },
        { { "|", neg_inf_interval, {1.5,2.5} },                                                     {-inf,2.5} },
        { { "|", neg_inf_interval, {-3.5,-1.} },                                                    neg_inf_interval },
        { { "|", neg_inf_interval, {-3.,1.5} },                                                     {-inf,1.5} },
        { { "|", 0., {} },                                                                          0. },
        { { "|", 0., inf_interval },                                                                inf_interval },
        { { "|", 0., pos_inf_interval },                                                            pos_inf_interval },
        { { "|", 0., neg_inf_interval },                                                            neg_inf_interval },
        { { "|", 0., 0. },                                                                          0. },
        { { "|", 0., 1.5 },                                                                         {0.,1.5} },
        { { "|", 0., -1.2 },                                                                        {-1.2,0.} },
        { { "|", 0., {0.,3.} },                                                                     {0.,3.} },
        { { "|", 0., {-4.,0.} },                                                                    {-4.,0.} },
        { { "|", 0., {1.5,2.5} },                                                                   {0.,2.5} },
        { { "|", 0., {-3.5,-1.} },                                                                  {-3.5,0.} },
        { { "|", 0., {-3.,1.5} },                                                                   {-3.,1.5} },
        { { "|", 1., {} },                                                                          1. },
        { { "|", 1., inf_interval },                                                                inf_interval },
        { { "|", 1., pos_inf_interval },                                                            pos_inf_interval },
        { { "|", 1., neg_inf_interval },                                                            {-inf,1.} },
        { { "|", 1., 0. },                                                                          {0.,1.} },
        { { "|", 1., 1.5 },                                                                         {1.,1.5} },
        { { "|", 1., -1.2 },                                                                        {-1.2,1.} },
        { { "|", 1., {0.,3.} },                                                                     {0.,3.} },
        { { "|", 1., {-4.,0.} },                                                                    {-4.,1.} },
        { { "|", 1., {1.5,2.5} },                                                                   {1.,2.5} },
        { { "|", 1., {-3.5,-1.} },                                                                  {-3.5,1.} },
        { { "|", 1., {-3.,1.5} },                                                                   {-3.,1.5} },
        { { "|", -1., {} },                                                                         -1. },
        { { "|", -1., inf_interval },                                                               inf_interval },
        { { "|", -1., pos_inf_interval },                                                           {-1.,inf} },
        { { "|", -1., neg_inf_interval },                                                           neg_inf_interval },
        { { "|", -1., 0. },                                                                         {-1.,0.} },
        { { "|", -1., 1.5 },                                                                        {-1.,1.5} },
        { { "|", -1., -1.2 },                                                                       {-1.2,-1.} },
        { { "|", -1., {0.,3.} },                                                                    {-1.,3.} },
        { { "|", -1., {-4.,0.} },                                                                   {-4.,0.} },
        { { "|", -1., {1.5,2.5} },                                                                  {-1.,2.5} },
        { { "|", -1., {-3.5,-1.} },                                                                 {-3.5,-1.} },
        { { "|", -1., {-3.,1.5} },                                                                  {-3.,1.5} },
        { { "|", {0.,2.}, {} },                                                                     {0.,2.} },
        { { "|", {0.,2.}, inf_interval },                                                           inf_interval },
        { { "|", {0.,2.}, pos_inf_interval },                                                       pos_inf_interval },
        { { "|", {0.,2.}, neg_inf_interval },                                                       {-inf,2.} },
        { { "|", {0.,2.}, 0. },                                                                     {0.,2.} },
        { { "|", {0.,2.}, 1.5 },                                                                    {0.,2.} },
        { { "|", {0.,2.}, -1.2 },                                                                   {-1.2,2.} },
        { { "|", {0.,2.}, {0.,3.} },                                                                {0.,3.} },
        { { "|", {0.,2.}, {-4.,0.} },                                                               {-4.,2.} },
        { { "|", {0.,2.}, {1.5,2.5} },                                                              {0.,2.5} },
        { { "|", {0.,2.}, {-3.5,-1.} },                                                             {-3.5,2.} },
        { { "|", {0.,2.}, {-3.,1.5} },                                                              {-3.,2.} },
        { { "|", {-3.,0.}, {} },                                                                    {-3.,0.} },
        { { "|", {-3.,0.}, inf_interval },                                                          inf_interval },
        { { "|", {-3.,0.}, pos_inf_interval },                                                      {-3.,inf} },
        { { "|", {-3.,0.}, neg_inf_interval },                                                      neg_inf_interval },
        { { "|", {-3.,0.}, 0. },                                                                    {-3.,0.} },
        { { "|", {-3.,0.}, 1.5 },                                                                   {-3.,1.5} },
        { { "|", {-3.,0.}, -1.2 },                                                                  {-3.,0.} },
        { { "|", {-3.,0.}, {0.,3.} },                                                               {-3.,3.} },
        { { "|", {-3.,0.}, {-4.,0.} },                                                              {-4.,0.} },
        { { "|", {-3.,0.}, {1.5,2.5} },                                                             {-3.,2.5} },
        { { "|", {-3.,0.}, {-3.5,-1.} },                                                            {-3.5,0.} },
        { { "|", {-3.,0.}, {-3.,1.5} },                                                             {-3.,1.5} },
        { { "|", {1.,2.}, {} },                                                                     {1.,2.} },
        { { "|", {1.,2.}, inf_interval },                                                           inf_interval },
        { { "|", {1.,2.}, pos_inf_interval },                                                       pos_inf_interval },
        { { "|", {1.,2.}, neg_inf_interval },                                                       {-inf,2.} },
        { { "|", {1.,2.}, 0. },                                                                     {0.,2.} },
        { { "|", {1.,2.}, 1.5 },                                                                    {1.,2.} },
        { { "|", {1.,2.}, -1.2 },                                                                   {-1.2,2.} },
        { { "|", {1.,2.}, {0.,3.} },                                                                {0.,3.} },
        { { "|", {1.,2.}, {-4.,0.} },                                                               {-4.,2.} },
        { { "|", {1.,2.}, {1.5,2.5} },                                                              {1.,2.5} },
        { { "|", {1.,2.}, {-3.5,-1.} },                                                             {-3.5,2.} },
        { { "|", {1.,2.}, {-3.,1.5} },                                                              {-3.,2.} },
        { { "|", {-3.,-1.}, {} },                                                                   {-3.,-1.} },
        { { "|", {-3.,-1.}, inf_interval },                                                         inf_interval },
        { { "|", {-3.,-1.}, pos_inf_interval },                                                     {-3.,inf} },
        { { "|", {-3.,-1.}, neg_inf_interval },                                                     neg_inf_interval },
        { { "|", {-3.,-1.}, 0. },                                                                   {-3.,0.} },
        { { "|", {-3.,-1.}, 1.5 },                                                                  {-3.,1.5} },
        { { "|", {-3.,-1.}, -1.2 },                                                                 {-3.,-1.} },
        { { "|", {-3.,-1.}, {0.,3.} },                                                              {-3.,3.} },
        { { "|", {-3.,-1.}, {-4.,0.} },                                                             {-4.,0.} },
        { { "|", {-3.,-1.}, {1.5,2.5} },                                                            {-3.,2.5} },
        { { "|", {-3.,-1.}, {-3.5,-1.} },                                                           {-3.5,-1.} },
        { { "|", {-3.,-1.}, {-3.,1.5} },                                                            {-3.,1.5} },
        { { "|", {-3.,1.}, {} },                                                                    {-3.,1.} },
        { { "|", {-3.,1.}, inf_interval },                                                          inf_interval },
        { { "|", {-3.,1.}, pos_inf_interval },                                                      {-3.,inf} },
        { { "|", {-3.,1.}, neg_inf_interval },                                                      {-inf,1.} },
        { { "|", {-3.,1.}, 0. },                                                                    {-3.,1.} },
        { { "|", {-3.,1.}, 1.5 },                                                                   {-3.,1.5} },
        { { "|", {-3.,1.}, -1.2 },                                                                  {-3.,1.} },
        { { "|", {-3.,1.}, {0.,3.} },                                                               {-3.,3.} },
        { { "|", {-3.,1.}, {-4.,0.} },                                                              {-4.,1.} },
        { { "|", {-3.,1.}, {1.5,2.5} },                                                             {-3.,2.5} },
        { { "|", {-3.,1.}, {-3.5,-1.} },                                                            {-3.5,1.} },
        { { "|", {-3.,1.}, {-3.,1.5} },                                                             {-3.,1.5} },
    });

    Suite(int_msg, true).test<Interval_case>({
        { { "neg", {} },                                                                            ignore },
        { { "+", {}, {} },                                                                          ignore },
        { { "+", {}, inf_interval },                                                                ignore },
        { { "+", {}, pos_inf_interval },                                                            ignore },
        { { "+", {}, neg_inf_interval },                                                            ignore },
        { { "+", {}, 0. },                                                                          ignore },
        { { "+", {}, 1. },                                                                          ignore },
        { { "+", {}, -1. },                                                                         ignore },
        { { "+", {}, {0.,2.} },                                                                     ignore },
        { { "+", {}, {-3.,0.} },                                                                    ignore },
        { { "+", {}, {1.,2.} },                                                                     ignore },
        { { "+", {}, {-3.,-1.} },                                                                   ignore },
        { { "+", {}, {-3.,1.} },                                                                    ignore },
        { { "+", inf_interval, {} },                                                                ignore },
        { { "+", pos_inf_interval, {} },                                                            ignore },
        { { "+", neg_inf_interval, {} },                                                            ignore },
        { { "+", 0., {} },                                                                          ignore },
        { { "+", 1., {} },                                                                          ignore },
        { { "+", -1., {} },                                                                         ignore },
        { { "+", {0.,2.}, {} },                                                                     ignore },
        { { "+", {-3.,0.}, {} },                                                                    ignore },
        { { "+", {1.,2.}, {} },                                                                     ignore },
        { { "+", {-3.,-1.}, {} },                                                                   ignore },
        { { "+", {-3.,1.}, {} },                                                                    ignore },
        { { "-", {}, {} },                                                                          ignore },
        { { "-", {}, 0. },                                                                          ignore },
        { { "-", {}, 1. },                                                                          ignore },
        { { "-", {}, -1. },                                                                         ignore },
        { { "-", {}, {0.,2.} },                                                                     ignore },
        { { "-", {}, {-3.,0.} },                                                                    ignore },
        { { "-", {}, {1.,2.} },                                                                     ignore },
        { { "-", {}, {-3.,-1.} },                                                                   ignore },
        { { "-", {}, {-3.,1.} },                                                                    ignore },
        { { "-", inf_interval, {} },                                                                ignore },
        { { "-", pos_inf_interval, {} },                                                            ignore },
        { { "-", neg_inf_interval, {} },                                                            ignore },
        { { "-", 0., {} },                                                                          ignore },
        { { "-", 1., {} },                                                                          ignore },
        { { "-", -1., {} },                                                                         ignore },
        { { "-", {0.,2.}, {} },                                                                     ignore },
        { { "-", {-3.,0.}, {} },                                                                    ignore },
        { { "-", {1.,2.}, {} },                                                                     ignore },
        { { "-", {-3.,-1.}, {} },                                                                   ignore },
        { { "-", {-3.,1.}, {} },                                                                    ignore },
    });

    ////////////////////////////////////////////////////////////////

    const Float _sqrt2_2 = sqrt2/2;

    const auto quad_msg_f = [](bool less){
        return "quadratic "s + (less ? "<=" : ">=") + " inequations";
    };

    Suite(quad_msg_f(true)).test<Quadratic_case<true>>({
        { { 0., 0., 0. },                                                                           inf_interval },
        { { 0., 0., 1. },                                                                           {} },
        { { 0., 0., -1. },                                                                          inf_interval },
        { { 0., 1., 0. },                                                                           neg_inf_interval },
        { { 0., 1., 1. },                                                                           {-inf,-1.} },
        { { 0., 1., -1. },                                                                          {-inf,1.} },
        { { 0., -1., 0. },                                                                          pos_inf_interval },
        { { 0., -1., 1. },                                                                          {1.,inf} },
        { { 0., -1., -1. },                                                                         {-1.,inf} },
        { { 0., 2., 0. },                                                                           neg_inf_interval },
        { { 0., 2., 1. },                                                                           {-inf,-0.5} },
        { { 0., 2., -1. },                                                                          {-inf,0.5} },
        { { 1., 0., 0. },                                                                           0. },
        { { 1., 0., 1. },                                                                           {} },
        { { 1., 0., -1. },                                                                          {-1.,1.} },
        { { 1., 1., 0. },                                                                           {-1.,0.} },
        { { 1., 1., 1. },                                                                           {} },
        { { 1., 1., -1. },                                                                          {(-1.-sqrt(5))/2,(-1.+sqrt(5))/2} },
        { { 1., -1., 0. },                                                                          {0.,1.} },
        { { 1., -1., 1. },                                                                          {} },
        { { 1., -1., -1. },                                                                         {(1.-sqrt(5))/2,(1.+sqrt(5))/2} },
        { { 1., 2., 0. },                                                                           {-2.,0.} },
        { { 1., 2., 1. },                                                                           -1. },
        { { 1., 2., -1. },                                                                          {-1.-sqrt2,-1.+sqrt2} },
        { { 2., 0., 0. },                                                                           0. },
        { { 2., 0., 1. },                                                                           {} },
        { { 2., 0., -1. },                                                                          {-_sqrt2_2,_sqrt2_2} },
        { { 2., 1., 0. },                                                                           {-0.5,0.} },
        { { 2., 1., 1. },                                                                           {} },
        { { 2., 1., -1. },                                                                          {-1.,0.5} },
        { { 2., -1., 0. },                                                                          {0.,0.5} },
        { { 2., -1., 1. },                                                                          {} },
        { { 2., -1., -1. },                                                                         {-0.5,1.} },
        { { 2., 2., 0. },                                                                           {-1.,0.} },
        { { 2., 2., 1. },                                                                           {} },
        { { 2., 2., -1. },                                                                          {(-1.-sqrt3)/2,(-1.+sqrt3)/2} },
        { { 0., 0., inf },                                                                          {} },
        { { 0., 0., -inf },                                                                         inf_interval },
        { { 0., 1., inf },                                                                          {} },
        { { 0., 1., -inf },                                                                         inf_interval },
        { { 1., 1., inf },                                                                          {} },
        { { 1., 1., -inf },                                                                         inf_interval },
        { { inf, 0., 1. },                                                                          {} },
        { { inf, 0., inf },                                                                         {} },
        { { inf, 1., 1. },                                                                          {} },
        { { inf, 1., inf },                                                                         {} },
    });

    Suite(quad_msg_f(true), true).test<Quadratic_case<true>>({
        { { 1., inf, 0. },                                                                          ignore },
        { { 1., inf, 1. },                                                                          ignore },
        { { 1., inf, -1. },                                                                         ignore },
        { { 1., inf, inf },                                                                         ignore },
        { { 1., inf, -inf },                                                                        ignore },
        { { 1., -inf, 0. },                                                                         ignore },
        { { 1., -inf, 1. },                                                                         ignore },
        { { 1., -inf, -1. },                                                                        ignore },
        { { 1., -inf, inf },                                                                        ignore },
        { { 1., -inf, -inf },                                                                       ignore },
        { { inf, 0., 0. },                                                                          ignore },
        { { inf, 0., -1. },                                                                         ignore },
        { { inf, 0., -inf },                                                                        ignore },
        { { inf, 1., 0. },                                                                          ignore },
        { { inf, 1., -1. },                                                                         ignore },
        { { inf, 1., -inf },                                                                        ignore },
        { { inf, inf, 0. },                                                                         ignore },
        { { inf, inf, 1. },                                                                         ignore },
        { { inf, inf, -1. },                                                                        ignore },
        { { inf, inf, inf },                                                                        ignore },
        { { inf, inf, -inf },                                                                       ignore },
        { { inf, -inf, 0. },                                                                        ignore },
        { { inf, -inf, 1. },                                                                        ignore },
        { { inf, -inf, -1. },                                                                       ignore },
        { { inf, -inf, inf },                                                                       ignore },
        { { inf, -inf, -inf },                                                                      ignore },
        { { 0., 0., nan },                                                                          ignore },
        { { 0., 1., nan },                                                                          ignore },
        { { 1., 0., nan },                                                                          ignore },
        { { 1., 1., nan },                                                                          ignore },
        { { 0., nan, 0. },                                                                          ignore },
        { { 0., nan, 1. },                                                                          ignore },
        { { 0., nan, nan },                                                                         ignore },
        { { 1., nan, 0. },                                                                          ignore },
        { { 1., nan, 1. },                                                                          ignore },
        { { 1., nan, nan },                                                                         ignore },
        { { nan, 0., 0. },                                                                          ignore },
        { { nan, 0., 1. },                                                                          ignore },
        { { nan, 0., nan },                                                                         ignore },
        { { nan, 1., 0. },                                                                          ignore },
        { { nan, 1., 1. },                                                                          ignore },
        { { nan, 1., nan },                                                                         ignore },
        { { nan, nan, 0. },                                                                         ignore },
        { { nan, nan, 1. },                                                                         ignore },
        { { nan, nan, nan },                                                                        ignore },
        { { nan, nan, 0. },                                                                         ignore },
        { { nan, nan, 1. },                                                                         ignore },
        { { nan, nan, nan },                                                                        ignore },
    });

    Suite(quad_msg_f(false)).test<Quadratic_case<false>>({
        { { 0., 0., 0. },                                                                           inf_interval },
        { { 0., 0., 1. },                                                                           inf_interval },
        { { 0., 0., -1. },                                                                          {} },
        { { 0., 1., 0. },                                                                           pos_inf_interval },
        { { 0., 1., 1. },                                                                           {-1.,inf} },
        { { 0., 1., -1. },                                                                          {1.,inf} },
        { { 0., -1., 0. },                                                                          neg_inf_interval },
        { { 0., -1., 1. },                                                                          {-inf,1.} },
        { { 0., -1., -1. },                                                                         {-inf,-1.} },
        { { 0., 2., 0. },                                                                           pos_inf_interval },
        { { 0., 2., 1. },                                                                           {-0.5,inf} },
        { { 0., 2., -1. },                                                                          {0.5,inf} },
        { { -1., 0., 0. },                                                                          0. },
        { { -1., 0., 1. },                                                                          {-1.,1.} },
        { { -1., 0., -1. },                                                                         {} },
        { { -1., 1., 0. },                                                                          {0.,1.} },
        { { -1., 1., 1. },                                                                          {(1.-sqrt(5))/2,(1.+sqrt(5))/2} },
        { { -1., 1., -1. },                                                                         {} },
        { { -1., -1., 0. },                                                                         {-1.,0.} },
        { { -1., -1., 1. },                                                                         {(-1.-sqrt(5))/2,(-1.+sqrt(5))/2} },
        { { -1., -1., -1. },                                                                        {} },
        { { -1., 2., 0. },                                                                          {0.,2.} },
        { { -1., 2., 1. },                                                                          {1.-sqrt2,1.+sqrt2} },
        { { -1., 2., -1. },                                                                         1. },
        { { -2., 0., 0. },                                                                          0. },
        { { -2., 0., 1. },                                                                          {-_sqrt2_2,_sqrt2_2} },
        { { -2., 0., -1. },                                                                         {} },
        { { -2., 1., 0. },                                                                          {0.,0.5} },
        { { -2., 1., 1. },                                                                          {-0.5,1.} },
        { { -2., 1., -1. },                                                                         {} },
        { { -2., -1., 0. },                                                                         {-0.5,0.} },
        { { -2., -1., 1. },                                                                         {-1,0.5} },
        { { -2., -1., -1. },                                                                        {} },
        { { -2., 2., 0. },                                                                          {0.,1.} },
        { { -2., 2., 1. },                                                                          {(1.-sqrt3)/2,(1.+sqrt3)/2} },
        { { -2., 2., -1. },                                                                         {} },
        { { 0., 0., inf },                                                                          inf_interval },
        { { 0., 0., -inf },                                                                         {} },
        { { 0., 1., inf },                                                                          inf_interval },
        { { 0., 1., -inf },                                                                         {} },
        { { -1., 1., inf },                                                                         inf_interval },
        { { -1., 1., -inf },                                                                        {} },
        { { -inf, 0., -1. },                                                                        {} },
        { { -inf, 0., -inf },                                                                       {} },
        { { -inf, 1., -1. },                                                                        {} },
        { { -inf, 1., -inf },                                                                       {} },
    });

    Suite(quad_msg_f(false), true).test<Quadratic_case<false>>({
        { { -1., inf, 0. },                                                                         ignore },
        { { -1., inf, 1. },                                                                         ignore },
        { { -1., inf, -1. },                                                                        ignore },
        { { -1., inf, inf },                                                                        ignore },
        { { -1., inf, -inf },                                                                       ignore },
        { { -1., -inf, 0. },                                                                        ignore },
        { { -1., -inf, 1. },                                                                        ignore },
        { { -1., -inf, -1. },                                                                       ignore },
        { { -1., -inf, inf },                                                                       ignore },
        { { -1., -inf, -inf },                                                                      ignore },
        { { -inf, 0., 0. },                                                                         ignore },
        { { -inf, 0., 1. },                                                                         ignore },
        { { -inf, 0., inf },                                                                        ignore },
        { { -inf, 1., 0. },                                                                         ignore },
        { { -inf, 1., 1. },                                                                         ignore },
        { { -inf, 1., inf },                                                                        ignore },
        { { -inf, inf, 0. },                                                                        ignore },
        { { -inf, inf, 1. },                                                                        ignore },
        { { -inf, inf, -1. },                                                                       ignore },
        { { -inf, inf, inf },                                                                       ignore },
        { { -inf, inf, -inf },                                                                      ignore },
        { { -inf, -inf, 0. },                                                                       ignore },
        { { -inf, -inf, 1. },                                                                       ignore },
        { { -inf, -inf, -1. },                                                                      ignore },
        { { -inf, -inf, inf },                                                                      ignore },
        { { -inf, -inf, -inf },                                                                     ignore },
        { { 0., 0., nan },                                                                          ignore },
        { { 0., 1., nan },                                                                          ignore },
        { { -1., 0., nan },                                                                         ignore },
        { { -1., 1., nan },                                                                         ignore },
        { { 0., nan, 0. },                                                                          ignore },
        { { 0., nan, 1. },                                                                          ignore },
        { { 0., nan, nan },                                                                         ignore },
        { { -1., nan, 0. },                                                                         ignore },
        { { -1., nan, 1. },                                                                         ignore },
        { { -1., nan, nan },                                                                        ignore },
        { { nan, 0., 0. },                                                                          ignore },
        { { nan, 0., 1. },                                                                          ignore },
        { { nan, 0., nan },                                                                         ignore },
        { { nan, 1., 0. },                                                                          ignore },
        { { nan, 1., 1. },                                                                          ignore },
        { { nan, 1., nan },                                                                         ignore },
        { { nan, nan, 0. },                                                                         ignore },
        { { nan, nan, 1. },                                                                         ignore },
        { { nan, nan, nan },                                                                        ignore },
        { { nan, nan, 0. },                                                                         ignore },
        { { nan, nan, 1. },                                                                         ignore },
        { { nan, nan, nan },                                                                        ignore },
    });

    ////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    cout << endl << "Success." << endl;
    return 0;
}
catch (const tomaqa::Error& e) {
    std::cout << e << std::endl;
    throw;
}
