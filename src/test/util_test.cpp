#include "tomaqa/test.hpp"
#include "tomaqa/util/fun.hpp"
#include "tomaqa/util/alg.hpp"
#include "tomaqa/util/flag.hpp"
#include "tomaqa/util/view.hpp"
#include "tomaqa/util/ref.hpp"
#include "tomaqa/util/string/alg.hpp"
#include "tomaqa/util/numeric/alg.hpp"

#include <list>
#include <deque>

namespace tomaqa::test {
    using namespace util;
    using tomaqa::to_string;

    ////////////////////////////////////////////////////////////////

    template <typename Arg>
    struct Fun_case : Inherit<Case<Vector<Arg>, Arg>> {
        using Inherit = tomaqa::Inherit<Case<Vector<Arg>, Arg>>;
        using typename Inherit::This;

        using Inherit::Inherit;

        using typename Inherit::Input;
        using typename Inherit::Output;

        Output result() override
        {
            auto f = Container_f<Input>(plus<>());
            auto ret = f(This::cinput());
            auto ret2 = f(This::cinput(), [](auto arg){ return arg*2; });
            expect(ret*2 == ret2,
                   "Comparison with result invoked with conversion function failed: "s
                   + "expected: " + to_string(ret*2) + ", got: " + to_string(ret2));

            return ret;
        }
    };

    ////////////////////////////////////////////////////////////////

    struct A {
        static bool pred(bool arg)
        {
            return arg;
        }

        bool member_pred(bool arg) const
        {
            return arg;
        }
    };

    template <typename F>
    auto negate(F f)
    {
        return wrap(move(f), [](bool arg){ return !arg; });
    }

    struct Alg_case : Inherit<Void_case<>> {
        void do_stuff() override
        {
            expect(A::pred(true) && A().member_pred(true)
                   && negate(&A::pred)(false) && negate(&A::member_pred)(A(), false),
                   "Wrong...");
        }
    };

    ////////////////////////////////////////////////////////////////

    struct Flag_output {
        bool valid;
        bool is_true;
    };

    String to_string(const Flag_output& rhs)
    {
        return "valid?"s + to_string(rhs.valid) + " true?" + to_string(rhs.is_true);
    }

    struct Flag_case : Inherit<Case<Flag, Flag_output>> {
        using Inherit::Inherit;

        bool condition(const Output& expected_, const Output& result_) const override
        {
            return expected_.valid == result_.valid
                && expected_.is_true == result_.is_true;
        }

        Output result() override
        {
            const Flag& flag = cinput();
            return {flag.valid(), flag.is_true()};
        }

        void finish() override
        {
            Inherit::finish();

            const Flag& flag = cinput();

            const bool eb(flag);
            const int ei(flag);
            expect(flag.is_true() == eb, "Explicit conversion to bool failed!");
            expect(flag == Flag(ei), "Explicit conversion to int and back failed!");
            expect(Flag(eb) == invoke([eb]() -> Flag { return eb; }), "Explicit conversion to bool and back via return failed!");
            expect(Flag(ei) == invoke([ei]() -> Flag { return ei; }), "Explicit conversion to int and back via return failed!");

            const bool ib = flag;
            expect(flag.is_true() == ib, "Implicit conversion to bool failed!");
            expect(ib == invoke([flag]() -> bool { return flag; }), "Implicit conversion to bool via return failed!");
            expect(Flag(ib) == invoke([ib]() -> Flag { return ib; }), "Implicit conversion to bool and back via return failed!");

            expect(eb == ib, "Explicit and implicit conversions to bool mismatch!");

            expect(!flag == !flag.is_true(), "Negation failed!");
            expect(flag != !flag, "Inequality failed!");
            expect(!(flag == !flag), "Negation of equality failed!");

            expect((flag == true) == (flag == true_flag), "Comparison with 'true' failed!");
            expect((flag == false) == (flag == false_flag), "Comparison with 'false' failed!");
            expect((flag == Flag::true_value) == (flag == true_flag), "Comparison with 'true_value' failed!");
            expect((flag == Flag::false_value) == (flag == false_flag), "Comparison with 'false_value' failed!");
            expect((flag == Flag::unknown_value) == (flag == unknown), "Comparison with 'unknown_value' failed!");
        }
    };

    ////////////////////////////////////////////////////////////////

    template <typename T>
    struct List : Inherit<std::list<T>, List<T>> {
        using Inherit = tomaqa::Inherit<std::list<T>, List<T>>;

        using Inherit::Inherit;

        String to_string() const&
        {
            using tomaqa::to_string;
            String str;
            for_each(*this, [&str](auto& elem){
                str += to_string(elem) + " ";
            });
            return str;
        }
    };

    template <typename T> using Aux_view = View<T>;
    template <typename T> using Aux_deque_view = View<T, std::deque>;
    template <typename T> using Aux_uniq_view = Unique_view<T>;
    template <typename T> using Aux_uniq_deque_view = Unique_view<T, std::deque>;

    template <typename C, template<typename> typename ViewT = Aux_view>
    struct View_case : Inherit<Case<C, C, Vector<Idx>>> {
        using Inherit = tomaqa::Inherit<Case<C, C, Vector<Idx>>>;
        using typename Inherit::This;

        using typename Inherit::Params;
        using typename Inherit::Output;

        using View = ViewT<C>;

        static constexpr bool unique = Unique_view_c<View>;
        static constexpr bool has_front = view::Has_push_front<View>;

        using Inherit::Inherit;

        Output result() override
        {
            auto& cont = This::input();

            auto& idxs = This::cparams();

            cout << cont << "[ " << idxs << "] =? " << This::cexpected() << endl;

            View tmp;
            View v(move(tmp));
            View v2;
            if constexpr (View::random_access_v) {
                v.connect(cont);
                v2.connect(cont);
            }
            tmp = move(v);
            swap(tmp, v);
            util::View<Value_t<C>*, View::template Accessors_tp> ptrs;

            for_each(idxs, [&](Idx idx){
                bool inserted = true;
                auto it = std::next(begin(cont), idx);
                auto& elem = *it;

                auto arg = invoke([idx, &elem]{
                    if constexpr (View::random_access_v) return idx;
                    else return &elem;
                });

                if constexpr (unique) {
                    assert(contains(v.caccessors(), arg) == v.contains(arg));
                    inserted &= v.insert(arg);
                    assert(v.contains(arg));
                }
                else v.push_back(arg);
                assert(contains(v.caccessors(), arg));

                if (inserted) {
                    v2.push_back(it);
                    ptrs.push_back(&elem);
                    if constexpr (has_front) {
                        v.push_front(arg);
                        v2.push_front(it);
                        ptrs.push_front(&elem);
                    }
                }
            });

            auto test_it_f = [&v](auto it){
                expect(*it == v.front(),
                       "First element accessed via iterator does not match with the view: "s
                       + to_string(it) + " != " + to_string(v.front()));
            };

            typename View::const_iterator cit;
            typename View::iterator it;
            if (!empty(v)) {
                cit = v.cbegin();
                test_it_f(cit);
                ++cit; --cit; cit++; cit--;
                test_it_f(cit);
                cit+1; cit-1;
                test_it_f(cit);

                it = v.begin();
                test_it_f(it);
                ++it; --it; it++; it--;
                test_it_f(it);
                it+1; it-1;
                test_it_f(it);
            }

            it = v.begin();
            for (Idx i = 0; i < v.size(); ++i, ++it) {
                expect(*it == v[i],
                       "Element accessed via iterator differs from accessing via idx "s
                       + to_string(i) + ": " + to_string(*it) + " != " + to_string(v[i]));
            }

            expect(equal(v, v2),
                   "The view does not correspond to view from iterators: "s
                   + v.to_string() + " != " + v2.to_string());

            expect(equal(v, ptrs),
                   "The view does not correspond to view of pointers: "s
                   + v.to_string() + " != " + to_string(ptrs));

            return to<Output>(v);
        }
    };

    ////////////////////////////////////////////////////////////////

    /// Ref_case

    struct Struct {
        Struct() { ++def_counter; }
        Struct(const Struct&) { ++copy_counter; }
        Struct& operator =(const Struct&) { ++copy_counter; return *this; }
        Struct(Struct&&) { ++move_counter; }
        Struct& operator =(Struct&&) { ++move_counter; return *this; }

        static void check_counters(int def_cnt, int cp_cnt, int mv_cnt)
        {
            expect(def_counter == def_cnt && copy_counter == cp_cnt && move_counter == mv_cnt,
                   "Struct counters mismatch: expected: "s
                   + to_string(def_cnt) + "x default, "
                   + to_string(cp_cnt) + "x copy, "
                   + to_string(mv_cnt) + "x move, got: "
                   + to_string(def_counter) + ", " + to_string(copy_counter) + ", " + to_string(move_counter));

            def_counter = copy_counter = move_counter = 0;
        }

        static inline int def_counter{};
        static inline int copy_counter{};
        static inline int move_counter{};
    };

    ////////////////////////////////////////////////////////////////

    struct String_params {
        String with;
        char delim1;
        char delim2{};
    };

    struct String_case : Inherit<Case<String, Vector<String>, String_params>> {
        using Inherit::Inherit;

        using typename Inherit::Input;
        using typename Inherit::Output;
        using typename Inherit::Params;

        Output result() override
        {
            const String& str = This::cinput();
            auto& par = This::cparams();

            Output out;

            out.push_back(extract_first(str, par.delim1));
            out.push_back(accumulate<String>(extract_all(str, par.delim1), "", [](auto str1, auto str2){
                return move(str1) + "_" + move(str2);
            }));
            if (par.delim2) {
                out.push_back(extract_first(str, par.delim1, par.delim2));
                out.push_back(accumulate<String>(extract_all(str, par.delim1, par.delim2), "", [](auto str1, auto str2){
                    return move(str1) + "_" + move(str2);
                }));
            }

            String aux = str;
            replace_first(aux, par.delim1, par.with);
            out.push_back(move(aux));
            aux = str;
            replace_all(aux, par.delim1, par.with);
            out.push_back(move(aux));
            if (par.delim2) {
                aux = str;
                replace_first(aux, par.delim1, par.delim2, par.with);
                out.push_back(move(aux));
                aux = str;
                replace_all(aux, par.delim1, par.delim2, par.with);
                out.push_back(move(aux));
            }

            return out;
        }
    };

    ////////////////////////////////////////////////////////////////
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

int main(int, const char*[])
try {
    using namespace tomaqa;
    using namespace tomaqa::test;
    using namespace std;

    ////////////////////////////////////////////////////////////////

    const String fun_msg = "functions accepting vectors of values";
    Suite(fun_msg).test<Fun_case<int>>({
        {{1},                                       {1},                    },
        {{1, 2},                                    {3},                    },
        {{1, 2, 3},                                 {6},                    },
        {{1, 2, 3, 4, 5},                           {15},                   },
    });

    ////////////////////////////////////////////////////////////////

    const String alg_msg = "algorithms";
    Suite(alg_msg).test<Alg_case>({
        {},
    });

    ////////////////////////////////////////////////////////////////

    const String flag_msg = "constructing of Flags";
    Suite(flag_msg).test<Flag_case>({
        {{},                                        {false, false},         },
        {true_flag,                                 {true, true},           },
        {false_flag,                                {true, false},          },
        {unknown,                                   {false, false},         },
        {true,                                      {true, true},           },
        {false,                                     {true, false},          },
        {Flag::true_value,                          {true, true},           },
        {Flag::false_value,                         {true, false},          },
        {Flag::unknown_value,                       {false, false},         },
    });

    Suite(flag_msg, true).test<Flag_case>({
        {{},                                        {false, true},          },
    });

    ////////////////////////////////////////////////////////////////

    using V = Vector<int>;
    using L = List<int>;

    auto view_msg_f = [](bool uniq = false, String tp_name = ""){
        return "constructing of "s + (uniq ? "Unique_v" : "V") + "iews" + tp_name + " of ";
    };
    Suite(view_msg_f() + "Vector<int>").test<View_case<V>>({
        {{},                       V{},                  {},                },
        {{1},                      V{},                  {},                },
        {{1},                      V{1},                 {0},               },
        {{1},                      V{1, 1},              {0, 0},            },
        {{1, 2},                   V{},                  {},                },
        {{1, 2},                   V{1},                 {0},               },
        {{1, 2},                   V{2},                 {1},               },
        {{1, 2},                   V{1, 1},              {0, 0},            },
        {{1, 2},                   V{1, 2},              {0, 1},            },
        {{1, 2},                   V{2, 1},              {1, 0},            },
        {{1, 2},                   V{2, 2},              {1, 1},            },
        {{1, 2, 3},                V{},                  {},                },
        {{1, 2, 3},                V{1},                 {0},               },
        {{1, 2, 3},                V{2},                 {1},               },
        {{1, 2, 3},                V{3},                 {2},               },
        {{1, 2, 3},                V{1, 1},              {0, 0},            },
        {{1, 2, 3},                V{1, 2},              {0, 1},            },
        {{1, 2, 3},                V{1, 3},              {0, 2},            },
        {{1, 2, 3},                V{2, 1},              {1, 0},            },
        {{1, 2, 3},                V{2, 2},              {1, 1},            },
        {{1, 2, 3},                V{2, 3},              {1, 2},            },
        {{1, 2, 3},                V{3, 1},              {2, 0},            },
        {{1, 2, 3},                V{3, 2},              {2, 1},            },
        {{1, 2, 3},                V{3, 3},              {2, 2},            },
        {{1, 2, 3},                V{1, 1, 1},           {0, 0, 0},         },
        {{1, 2, 3},                V{1, 1, 2},           {0, 0, 1},         },
        {{1, 2, 3},                V{1, 1, 3},           {0, 0, 2},         },
        {{1, 2, 3},                V{1, 2, 1},           {0, 1, 0},         },
        {{1, 2, 3},                V{1, 2, 2},           {0, 1, 1},         },
        {{1, 2, 3},                V{1, 2, 3},           {0, 1, 2},         },
        {{1, 2, 3},                V{1, 3, 1},           {0, 2, 0},         },
        {{1, 2, 3},                V{1, 3, 2},           {0, 2, 1},         },
        {{1, 2, 3},                V{1, 3, 3},           {0, 2, 2},         },
        {{1, 2, 3},                V{2, 1, 1},           {1, 0, 0},         },
        {{1, 2, 3},                V{2, 1, 2},           {1, 0, 1},         },
        {{1, 2, 3},                V{2, 1, 3},           {1, 0, 2},         },
        {{1, 2, 3},                V{2, 2, 1},           {1, 1, 0},         },
        {{1, 2, 3},                V{2, 2, 2},           {1, 1, 1},         },
        {{1, 2, 3},                V{2, 2, 3},           {1, 1, 2},         },
        {{1, 2, 3},                V{2, 3, 1},           {1, 2, 0},         },
        {{1, 2, 3},                V{2, 3, 2},           {1, 2, 1},         },
        {{1, 2, 3},                V{2, 3, 3},           {1, 2, 2},         },
        {{1, 2, 3},                V{3, 1, 1},           {2, 0, 0},         },
        {{1, 2, 3},                V{3, 1, 2},           {2, 0, 1},         },
        {{1, 2, 3},                V{3, 1, 3},           {2, 0, 2},         },
        {{1, 2, 3},                V{3, 2, 1},           {2, 1, 0},         },
        {{1, 2, 3},                V{3, 2, 2},           {2, 1, 1},         },
        {{1, 2, 3},                V{3, 2, 3},           {2, 1, 2},         },
        {{1, 2, 3},                V{3, 3, 1},           {2, 2, 0},         },
        {{1, 2, 3},                V{3, 3, 2},           {2, 2, 1},         },
        {{1, 2, 3},                V{3, 3, 3},           {2, 2, 2},         },
    });

    Suite(view_msg_f() + "List<int>").test<View_case<L>>({
        {{},                       L{},                  {},                },
        {{1},                      L{},                  {},                },
        {{1},                      L{1},                 {0},               },
        {{1},                      L{1, 1},              {0, 0},            },
        {{1, 2},                   L{},                  {},                },
        {{1, 2},                   L{1},                 {0},               },
        {{1, 2},                   L{2},                 {1},               },
        {{1, 2},                   L{1, 1},              {0, 0},            },
        {{1, 2},                   L{1, 2},              {0, 1},            },
        {{1, 2},                   L{2, 1},              {1, 0},            },
        {{1, 2},                   L{2, 2},              {1, 1},            },
        {{1, 2, 3},                L{},                  {},                },
        {{1, 2, 3},                L{1},                 {0},               },
        {{1, 2, 3},                L{2},                 {1},               },
        {{1, 2, 3},                L{3},                 {2},               },
        {{1, 2, 3},                L{1, 1},              {0, 0},            },
        {{1, 2, 3},                L{1, 2},              {0, 1},            },
        {{1, 2, 3},                L{1, 3},              {0, 2},            },
        {{1, 2, 3},                L{2, 1},              {1, 0},            },
        {{1, 2, 3},                L{2, 2},              {1, 1},            },
        {{1, 2, 3},                L{2, 3},              {1, 2},            },
        {{1, 2, 3},                L{3, 1},              {2, 0},            },
        {{1, 2, 3},                L{3, 2},              {2, 1},            },
        {{1, 2, 3},                L{3, 3},              {2, 2},            },
        {{1, 2, 3},                L{1, 1, 1},           {0, 0, 0},         },
        {{1, 2, 3},                L{1, 1, 2},           {0, 0, 1},         },
        {{1, 2, 3},                L{1, 1, 3},           {0, 0, 2},         },
        {{1, 2, 3},                L{1, 2, 1},           {0, 1, 0},         },
        {{1, 2, 3},                L{1, 2, 2},           {0, 1, 1},         },
        {{1, 2, 3},                L{1, 2, 3},           {0, 1, 2},         },
        {{1, 2, 3},                L{1, 3, 1},           {0, 2, 0},         },
        {{1, 2, 3},                L{1, 3, 2},           {0, 2, 1},         },
        {{1, 2, 3},                L{1, 3, 3},           {0, 2, 2},         },
        {{1, 2, 3},                L{2, 1, 1},           {1, 0, 0},         },
        {{1, 2, 3},                L{2, 1, 2},           {1, 0, 1},         },
        {{1, 2, 3},                L{2, 1, 3},           {1, 0, 2},         },
        {{1, 2, 3},                L{2, 2, 1},           {1, 1, 0},         },
        {{1, 2, 3},                L{2, 2, 2},           {1, 1, 1},         },
        {{1, 2, 3},                L{2, 2, 3},           {1, 1, 2},         },
        {{1, 2, 3},                L{2, 3, 1},           {1, 2, 0},         },
        {{1, 2, 3},                L{2, 3, 2},           {1, 2, 1},         },
        {{1, 2, 3},                L{2, 3, 3},           {1, 2, 2},         },
        {{1, 2, 3},                L{3, 1, 1},           {2, 0, 0},         },
        {{1, 2, 3},                L{3, 1, 2},           {2, 0, 1},         },
        {{1, 2, 3},                L{3, 1, 3},           {2, 0, 2},         },
        {{1, 2, 3},                L{3, 2, 1},           {2, 1, 0},         },
        {{1, 2, 3},                L{3, 2, 2},           {2, 1, 1},         },
        {{1, 2, 3},                L{3, 2, 3},           {2, 1, 2},         },
        {{1, 2, 3},                L{3, 3, 1},           {2, 2, 0},         },
        {{1, 2, 3},                L{3, 3, 2},           {2, 2, 1},         },
        {{1, 2, 3},                L{3, 3, 3},           {2, 2, 2},         },
    });

    Suite(view_msg_f(false, "<deque>") + "Vector<int>").test<View_case<V, Aux_deque_view>>({
        {{},                       V{},                  {},                },
        {{1},                      V{},                  {},                },
        {{1},                      V{1, 1},              {0},               },
        {{1},                      V{1, 1, 1, 1},        {0, 0},            },
        {{1, 2},                   V{},                  {},                },
        {{1, 2},                   V{1, 1},              {0},               },
        {{1, 2},                   V{2, 2},              {1},               },
        {{1, 2},                   V{1, 1, 1, 1},        {0, 0},            },
        {{1, 2},                   V{2, 1, 1, 2},        {0, 1},            },
        {{1, 2},                   V{1, 2, 2, 1},        {1, 0},            },
        {{1, 2},                   V{2, 2, 2, 2},        {1, 1},            },
        {{1, 2, 3},                V{},                  {},                },
        {{1, 2, 3},                V{1, 1},              {0},               },
        {{1, 2, 3},                V{2, 2},              {1},               },
        {{1, 2, 3},                V{3, 3},              {2},               },
        {{1, 2, 3},                V{1, 1, 1, 1},        {0, 0},            },
        {{1, 2, 3},                V{2, 1, 1, 2},        {0, 1},            },
        {{1, 2, 3},                V{3, 1, 1, 3},        {0, 2},            },
        {{1, 2, 3},                V{1, 2, 2, 1},        {1, 0},            },
        {{1, 2, 3},                V{2, 2, 2, 2},        {1, 1},            },
        {{1, 2, 3},                V{3, 2, 2, 3},        {1, 2},            },
        {{1, 2, 3},                V{1, 3, 3, 1},        {2, 0},            },
        {{1, 2, 3},                V{2, 3, 3, 2},        {2, 1},            },
        {{1, 2, 3},                V{3, 3, 3, 3},        {2, 2},            },
        {{1, 2, 3},                V{1, 1, 1, 1, 1, 1},  {0, 0, 0},         },
        {{1, 2, 3},                V{2, 1, 1, 1, 1, 2},  {0, 0, 1},         },
        {{1, 2, 3},                V{3, 1, 1, 1, 1, 3},  {0, 0, 2},         },
        {{1, 2, 3},                V{1, 2, 1, 1, 2, 1},  {0, 1, 0},         },
        {{1, 2, 3},                V{2, 2, 1, 1, 2, 2},  {0, 1, 1},         },
        {{1, 2, 3},                V{3, 2, 1, 1, 2, 3},  {0, 1, 2},         },
        {{1, 2, 3},                V{1, 3, 1, 1, 3, 1},  {0, 2, 0},         },
        {{1, 2, 3},                V{2, 3, 1, 1, 3, 2},  {0, 2, 1},         },
        {{1, 2, 3},                V{3, 3, 1, 1, 3, 3},  {0, 2, 2},         },
        {{1, 2, 3},                V{1, 1, 2, 2, 1, 1},  {1, 0, 0},         },
        {{1, 2, 3},                V{2, 1, 2, 2, 1, 2},  {1, 0, 1},         },
        {{1, 2, 3},                V{3, 1, 2, 2, 1, 3},  {1, 0, 2},         },
        {{1, 2, 3},                V{1, 2, 2, 2, 2, 1},  {1, 1, 0},         },
        {{1, 2, 3},                V{2, 2, 2, 2, 2, 2},  {1, 1, 1},         },
        {{1, 2, 3},                V{3, 2, 2, 2, 2, 3},  {1, 1, 2},         },
        {{1, 2, 3},                V{1, 3, 2, 2, 3, 1},  {1, 2, 0},         },
        {{1, 2, 3},                V{2, 3, 2, 2, 3, 2},  {1, 2, 1},         },
        {{1, 2, 3},                V{3, 3, 2, 2, 3, 3},  {1, 2, 2},         },
        {{1, 2, 3},                V{1, 1, 3, 3, 1, 1},  {2, 0, 0},         },
        {{1, 2, 3},                V{2, 1, 3, 3, 1, 2},  {2, 0, 1},         },
        {{1, 2, 3},                V{3, 1, 3, 3, 1, 3},  {2, 0, 2},         },
        {{1, 2, 3},                V{1, 2, 3, 3, 2, 1},  {2, 1, 0},         },
        {{1, 2, 3},                V{2, 2, 3, 3, 2, 2},  {2, 1, 1},         },
        {{1, 2, 3},                V{3, 2, 3, 3, 2, 3},  {2, 1, 2},         },
        {{1, 2, 3},                V{1, 3, 3, 3, 3, 1},  {2, 2, 0},         },
        {{1, 2, 3},                V{2, 3, 3, 3, 3, 2},  {2, 2, 1},         },
        {{1, 2, 3},                V{3, 3, 3, 3, 3, 3},  {2, 2, 2},         },
    });

    Suite(view_msg_f(true) + "Vector<int>").test<View_case<V, Aux_uniq_view>>({
        {{},                       V{},                  {},                },
        {{1},                      V{},                  {},                },
        {{1},                      V{1},                 {0},               },
        {{1},                      V{1},                 {0, 0},            },
        {{1, 2},                   V{},                  {},                },
        {{1, 2},                   V{1},                 {0},               },
        {{1, 2},                   V{2},                 {1},               },
        {{1, 2},                   V{1},                 {0, 0},            },
        {{1, 2},                   V{1, 2},              {0, 1},            },
        {{1, 2},                   V{2, 1},              {1, 0},            },
        {{1, 2},                   V{2},                 {1, 1},            },
        {{1, 2, 3},                V{},                  {},                },
        {{1, 2, 3},                V{1},                 {0},               },
        {{1, 2, 3},                V{2},                 {1},               },
        {{1, 2, 3},                V{3},                 {2},               },
        {{1, 2, 3},                V{1},                 {0, 0},            },
        {{1, 2, 3},                V{1, 2},              {0, 1},            },
        {{1, 2, 3},                V{1, 3},              {0, 2},            },
        {{1, 2, 3},                V{2, 1},              {1, 0},            },
        {{1, 2, 3},                V{2},                 {1, 1},            },
        {{1, 2, 3},                V{2, 3},              {1, 2},            },
        {{1, 2, 3},                V{3, 1},              {2, 0},            },
        {{1, 2, 3},                V{3, 2},              {2, 1},            },
        {{1, 2, 3},                V{3},                 {2, 2},            },
        {{1, 2, 3},                V{1},                 {0, 0, 0},         },
        {{1, 2, 3},                V{1, 2},              {0, 0, 1},         },
        {{1, 2, 3},                V{1, 3},              {0, 0, 2},         },
        {{1, 2, 3},                V{1, 2},              {0, 1, 0},         },
        {{1, 2, 3},                V{1, 2},              {0, 1, 1},         },
        {{1, 2, 3},                V{1, 2, 3},           {0, 1, 2},         },
        {{1, 2, 3},                V{1, 3},              {0, 2, 0},         },
        {{1, 2, 3},                V{1, 3, 2},           {0, 2, 1},         },
        {{1, 2, 3},                V{1, 3},              {0, 2, 2},         },
        {{1, 2, 3},                V{2, 1},              {1, 0, 0},         },
        {{1, 2, 3},                V{2, 1},              {1, 0, 1},         },
        {{1, 2, 3},                V{2, 1, 3},           {1, 0, 2},         },
        {{1, 2, 3},                V{2, 1},              {1, 1, 0},         },
        {{1, 2, 3},                V{2},                 {1, 1, 1},         },
        {{1, 2, 3},                V{2, 3},              {1, 1, 2},         },
        {{1, 2, 3},                V{2, 3, 1},           {1, 2, 0},         },
        {{1, 2, 3},                V{2, 3},              {1, 2, 1},         },
        {{1, 2, 3},                V{2, 3},              {1, 2, 2},         },
        {{1, 2, 3},                V{3, 1},              {2, 0, 0},         },
        {{1, 2, 3},                V{3, 1, 2},           {2, 0, 1},         },
        {{1, 2, 3},                V{3, 1},              {2, 0, 2},         },
        {{1, 2, 3},                V{3, 2, 1},           {2, 1, 0},         },
        {{1, 2, 3},                V{3, 2},              {2, 1, 1},         },
        {{1, 2, 3},                V{3, 2},              {2, 1, 2},         },
        {{1, 2, 3},                V{3, 1},              {2, 2, 0},         },
        {{1, 2, 3},                V{3, 2},              {2, 2, 1},         },
        {{1, 2, 3},                V{3},                 {2, 2, 2},         },
    });

    Suite(view_msg_f(true) + "List<int>").test<View_case<L, Aux_uniq_view>>({
        {{},                       L{},                  {},                },
        {{1},                      L{},                  {},                },
        {{1},                      L{1},                 {0},               },
        {{1},                      L{1},                 {0, 0},            },
        {{1, 2},                   L{},                  {},                },
        {{1, 2},                   L{1},                 {0},               },
        {{1, 2},                   L{2},                 {1},               },
        {{1, 2},                   L{1},                 {0, 0},            },
        {{1, 2},                   L{1, 2},              {0, 1},            },
        {{1, 2},                   L{2, 1},              {1, 0},            },
        {{1, 2},                   L{2},                 {1, 1},            },
        {{1, 2, 3},                L{},                  {},                },
        {{1, 2, 3},                L{1},                 {0},               },
        {{1, 2, 3},                L{2},                 {1},               },
        {{1, 2, 3},                L{3},                 {2},               },
        {{1, 2, 3},                L{1},                 {0, 0},            },
        {{1, 2, 3},                L{1, 2},              {0, 1},            },
        {{1, 2, 3},                L{1, 3},              {0, 2},            },
        {{1, 2, 3},                L{2, 1},              {1, 0},            },
        {{1, 2, 3},                L{2},                 {1, 1},            },
        {{1, 2, 3},                L{2, 3},              {1, 2},            },
        {{1, 2, 3},                L{3, 1},              {2, 0},            },
        {{1, 2, 3},                L{3, 2},              {2, 1},            },
        {{1, 2, 3},                L{3},                 {2, 2},            },
        {{1, 2, 3},                L{1},                 {0, 0, 0},         },
        {{1, 2, 3},                L{1, 2},              {0, 0, 1},         },
        {{1, 2, 3},                L{1, 3},              {0, 0, 2},         },
        {{1, 2, 3},                L{1, 2},              {0, 1, 0},         },
        {{1, 2, 3},                L{1, 2},              {0, 1, 1},         },
        {{1, 2, 3},                L{1, 2, 3},           {0, 1, 2},         },
        {{1, 2, 3},                L{1, 3},              {0, 2, 0},         },
        {{1, 2, 3},                L{1, 3, 2},           {0, 2, 1},         },
        {{1, 2, 3},                L{1, 3},              {0, 2, 2},         },
        {{1, 2, 3},                L{2, 1},              {1, 0, 0},         },
        {{1, 2, 3},                L{2, 1},              {1, 0, 1},         },
        {{1, 2, 3},                L{2, 1, 3},           {1, 0, 2},         },
        {{1, 2, 3},                L{2, 1},              {1, 1, 0},         },
        {{1, 2, 3},                L{2},                 {1, 1, 1},         },
        {{1, 2, 3},                L{2, 3},              {1, 1, 2},         },
        {{1, 2, 3},                L{2, 3, 1},           {1, 2, 0},         },
        {{1, 2, 3},                L{2, 3},              {1, 2, 1},         },
        {{1, 2, 3},                L{2, 3},              {1, 2, 2},         },
        {{1, 2, 3},                L{3, 1},              {2, 0, 0},         },
        {{1, 2, 3},                L{3, 1, 2},           {2, 0, 1},         },
        {{1, 2, 3},                L{3, 1},              {2, 0, 2},         },
        {{1, 2, 3},                L{3, 2, 1},           {2, 1, 0},         },
        {{1, 2, 3},                L{3, 2},              {2, 1, 1},         },
        {{1, 2, 3},                L{3, 2},              {2, 1, 2},         },
        {{1, 2, 3},                L{3, 1},              {2, 2, 0},         },
        {{1, 2, 3},                L{3, 2},              {2, 2, 1},         },
        {{1, 2, 3},                L{3},                 {2, 2, 2},         },
    });

    Suite(view_msg_f(true, "<deque>") + "Vector<int>").test<View_case<V, Aux_uniq_deque_view>>({
        {{},                       V{},                  {},                },
        {{1},                      V{},                  {},                },
        {{1},                      V{1, 1},              {0},               },
        {{1},                      V{1, 1},              {0, 0},            },
        {{1, 2},                   V{},                  {},                },
        {{1, 2},                   V{1, 1},              {0},               },
        {{1, 2},                   V{2, 2},              {1},               },
        {{1, 2},                   V{1, 1},              {0, 0},            },
        {{1, 2},                   V{2, 1, 1, 2},        {0, 1},            },
        {{1, 2},                   V{1, 2, 2, 1},        {1, 0},            },
        {{1, 2},                   V{2, 2},              {1, 1},            },
        {{1, 2, 3},                V{},                  {},                },
        {{1, 2, 3},                V{1, 1},              {0},               },
        {{1, 2, 3},                V{2, 2},              {1},               },
        {{1, 2, 3},                V{3, 3},              {2},               },
        {{1, 2, 3},                V{1, 1},              {0, 0},            },
        {{1, 2, 3},                V{2, 1, 1, 2},        {0, 1},            },
        {{1, 2, 3},                V{3, 1, 1, 3},        {0, 2},            },
        {{1, 2, 3},                V{1, 2, 2, 1},        {1, 0},            },
        {{1, 2, 3},                V{2, 2},              {1, 1},            },
        {{1, 2, 3},                V{3, 2, 2, 3},        {1, 2},            },
        {{1, 2, 3},                V{1, 3, 3, 1},        {2, 0},            },
        {{1, 2, 3},                V{2, 3, 3, 2},        {2, 1},            },
        {{1, 2, 3},                V{3, 3},              {2, 2},            },
        {{1, 2, 3},                V{1, 1},              {0, 0, 0},         },
        {{1, 2, 3},                V{2, 1, 1, 2},        {0, 0, 1},         },
        {{1, 2, 3},                V{3, 1, 1, 3},        {0, 0, 2},         },
        {{1, 2, 3},                V{2, 1, 1, 2},        {0, 1, 0},         },
        {{1, 2, 3},                V{2, 1, 1, 2},        {0, 1, 1},         },
        {{1, 2, 3},                V{3, 2, 1, 1, 2, 3},  {0, 1, 2},         },
        {{1, 2, 3},                V{3, 1, 1, 3},        {0, 2, 0},         },
        {{1, 2, 3},                V{2, 3, 1, 1, 3, 2},  {0, 2, 1},         },
        {{1, 2, 3},                V{3, 1, 1, 3},        {0, 2, 2},         },
        {{1, 2, 3},                V{1, 2, 2, 1},        {1, 0, 0},         },
        {{1, 2, 3},                V{1, 2, 2, 1},        {1, 0, 1},         },
        {{1, 2, 3},                V{3, 1, 2, 2, 1, 3},  {1, 0, 2},         },
        {{1, 2, 3},                V{1, 2, 2, 1},        {1, 1, 0},         },
        {{1, 2, 3},                V{2, 2},              {1, 1, 1},         },
        {{1, 2, 3},                V{3, 2, 2, 3},        {1, 1, 2},         },
        {{1, 2, 3},                V{1, 3, 2, 2, 3, 1},  {1, 2, 0},         },
        {{1, 2, 3},                V{3, 2, 2, 3},        {1, 2, 1},         },
        {{1, 2, 3},                V{3, 2, 2, 3},        {1, 2, 2},         },
        {{1, 2, 3},                V{1, 3, 3, 1},        {2, 0, 0},         },
        {{1, 2, 3},                V{2, 1, 3, 3, 1, 2},  {2, 0, 1},         },
        {{1, 2, 3},                V{1, 3, 3, 1},        {2, 0, 2},         },
        {{1, 2, 3},                V{1, 2, 3, 3, 2, 1},  {2, 1, 0},         },
        {{1, 2, 3},                V{2, 3, 3, 2},        {2, 1, 1},         },
        {{1, 2, 3},                V{2, 3, 3, 2},        {2, 1, 2},         },
        {{1, 2, 3},                V{1, 3, 3, 1},        {2, 2, 0},         },
        {{1, 2, 3},                V{2, 3, 3, 2},        {2, 2, 1},         },
        {{1, 2, 3},                V{3, 3},              {2, 2, 2},         },
    });

    ////////////////////////////////////////////////////////////////

    Struct stru;
    Ref<Struct> ref(stru);
    expect(&ref.item() == &stru && !ref.is_owner(), "Ref<Struct>(Struct&) failed!");
    Struct::check_counters(1, 0, 0);

    const Struct cstru;
    Ref<const Struct> crefc(cstru);
    expect(&crefc.item() == &cstru && !crefc.is_owner(), "Ref<const Struct>(const Struct&) failed!");
    Struct::check_counters(1, 0, 0);

    /// Ref<Struct> refc(cstru);
    Ref<const Struct> cref(stru);
    expect(&cref.item() == &stru && !cref.is_owner(), "Ref<const Struct>(Struct&) failed!");
    Struct::check_counters(0, 0, 0);

    Struct stru_cp(stru);
    Ref<Struct> refr(move(stru_cp));
    expect(&refr.item() != &stru && &refr.item() != &stru_cp && refr.is_owner(), "Ref<Struct>(Struct&&) failed!");
    Struct::check_counters(0, 1, 1);

    Ref<Struct> refv{Struct()};
    expect(refv.is_owner(), "Ref<Struct>(Struct) failed!");
    Struct::check_counters(1, 0, 1);

    const Struct cstru_cp(cstru);
    Ref<const Struct> crefrc(move(cstru_cp));
    expect(&crefrc.item() != &cstru && &crefrc.item() != &cstru_cp && crefrc.is_owner(), "Ref<const Struct>(const Struct&&) failed!");
    Struct::check_counters(0, 2, 0);

    Struct stru_cp2(stru);
    Ref<const Struct> crefr(move(stru_cp2));
    expect(&crefr.item() != &stru && &crefr.item() != &stru_cp2 && crefr.is_owner(), "Ref<const Struct>(Struct&&) failed!");
    Struct::check_counters(0, 1, 1);

    Ref<const Struct> crefv{Struct()};
    expect(crefv.is_owner(), "Ref<const Struct>(Struct) failed!");
    Struct::check_counters(1, 0, 1);

    const Struct cstru_cp2(cstru);
    Ref<Struct> refrc(move(cstru_cp2));
    expect(&refrc.item() != &cstru && &refrc.item() != &cstru_cp2 && refrc.is_owner(), "Ref<Struct>(const Struct&&) failed!");
    Struct::check_counters(0, 2, 0);


    Struct stru2;
    ref = stru2;
    expect(&ref.item() == &stru2 && !ref.is_owner(), "Ref<Struct> = Struct& failed!");
    Struct::check_counters(1, 0, 0);

    const Struct cstru2;
    crefc = cstru2;
    expect(&crefc.item() == &cstru2 && !crefc.is_owner(), "Ref<const Struct> = const Struct& failed!");
    Struct::check_counters(1, 0, 0);

    cref = stru2;
    expect(&cref.item() == &stru2 && !cref.is_owner(), "Ref<const Struct> = Struct& failed!");
    Struct::check_counters(0, 0, 0);

    Struct stru_cp3(stru);
    refr = move(stru_cp3);
    expect(&refr.item() != &stru && &refr.item() != &stru_cp && &refr.item() != &stru_cp3 && refr.is_owner(), "Ref<Struct> = Struct&& failed!");
    Struct::check_counters(0, 1, 1);

    refv = Struct();
    expect(refv.is_owner(), "Ref<Struct> = Struct failed!");
    Struct::check_counters(1, 0, 1);

    const Struct cstru_cp3(cstru);
    crefrc = move(cstru_cp3);
    expect(&crefrc.item() != &cstru && &crefrc.item() != &cstru_cp3 && crefrc.is_owner(), "Ref<const Struct> = const Struct&& failed!");
    Struct::check_counters(0, 2, 0);

    Struct stru_cp4(stru);
    crefr = move(stru_cp4);
    expect(&crefr.item() != &stru && &crefr.item() != &stru_cp2 && &crefr.item() != &stru_cp4 && crefr.is_owner(), "Ref<const Struct> = Struct&& failed!");
    Struct::check_counters(0, 1, 1);

    crefv = Struct();
    expect(crefv.is_owner(), "Ref<const Struct> = Struct failed!");
    Struct::check_counters(1, 0, 1);

    const Struct cstru_cp4(cstru);
    refrc = move(cstru_cp4);
    expect(&refrc.item() != &cstru && &refrc.item() != &cstru_cp4 && refrc.is_owner(), "Ref<Struct> = const Struct&& failed!");
    Struct::check_counters(0, 2, 0);

    ////////////////////////////////////////////////////////////////

    const String str_msg = "string algorithms";
    Suite(str_msg).test<String_case>({
        { "Hello",                 {"", "", "Hello", "Hello"},                     {"world", '|'} },
        { "Hello",                 {"", "", "Helworldlo", "Helworldlo"},           {"world", 'l'} },
        { "Helworldlo",            {"wor", "_wor_d", "HelXXldlo", "HelXXlXXlo"},      {"XX", 'l'} },
        { "var|1|",                {"1", "_1", "var|2|", "var|2|"},                    {"2", '|'} },
        { "var|1|0",               {"1", "_1", "var|2|0", "var|2|0"},                  {"2", '|'} },
        { "var|1|0|",              {"1", "_1_0", "var|2|0|", "var|2|2|"},              {"2", '|'} },
        { "var<1|",                {"", "", "", "", "var<1|", "var<1|", "var<1|", "var<1|"},        {"2", '<', '>'} },
        { "var>1<",                {"", "", "", "", "var>1<", "var>1<", "var>1<", "var>1<"},        {"2", '<', '>'} },
        { "var<1>",                {"", "", "1", "_1", "var<1>", "var<1>", "var<2>", "var<2>"},     {"2", '<', '>'} },
        { "var<1>0>",              {"", "", "1", "_1", "var<1>0>", "var<1>0>", "var<2>0>", "var<2>0>"}, {"2", '<', '>'} },
        { "var<1<0>",              {"1", "_1", "1<0", "_1<0", "var<2<0>", "var<2<0>", "var<2>", "var<2>"}, {"2", '<', '>'} },
        { "var<<1>",               {"", "", "<1", "_<1", "var<2<1>", "var<2<1>", "var<2>", "var<2>"}, {"2", '<', '>'} },
        { "var<1><0>",             {"1>", "_1>", "1", "_1_0", "var<2<0>", "var<2<0>", "var<2><0>", "var<2><2>"}, {"2", '<', '>'} },
        { "var<1>-<0>",            {"1>-", "_1>-", "1", "_1_0", "var<2<0>", "var<2<0>", "var<2>-<0>", "var<2>-<2>"}, {"2", '<', '>'} },
    });

    ////////////////////////////////////////////////////////////////

    Vector<int> v{0, 1, 2, 3, -3, -2, -1};

    auto numeric_check_f = [&v](int res, int exp, const String& fun_str){
        expect(res == exp,
               fun_str + "("s + to_string(v) + "): expected: "
               + to_string(exp) + ", got: " + to_string(res));
    };

    numeric_check_f(max(v), 3, "max");
    numeric_check_f(min(v), -3, "min");
    numeric_check_f(max(v, identity{}), 3, "max-identity{}");
    numeric_check_f(min(v, identity{}), -3, "min-identity{}");
    numeric_check_f(max(v, greater<>()), -3, "max-reverse");
    numeric_check_f(min(v, greater<>()), 3, "min-reverse");
    numeric_check_f(max(v, identity{}, greater<>()), -3, "max-identity{}-reverse");
    numeric_check_f(min(v, identity{}, greater<>()), 3, "min-identity{}-reverse");
    numeric_check_f(max(v, [](auto& i){ return i+1; }), 4, "max+1");
    numeric_check_f(min(v, [](auto& i){ return i-1; }), -4, "min-1");
    numeric_check_f(max(v, [](auto& i){ return i+1; }, greater<>()), -2, "max+1-reverse");
    numeric_check_f(min(v, [](auto& i){ return i-1; }, greater<>()), 2, "min-1-reverse");

    ////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    cout << endl << "Success." << endl;
    return 0;
}
catch (const tomaqa::Error& e) {
    std::cout << e << std::endl;
    throw;
}
