#include "tomaqa/core.hpp"
#include "tomaqa/core/error.hpp"

namespace tomaqa {
    Error Error::operator +(const Error& rhs) const
    {
        return {cmsg() + rhs.cmsg()};
    }

    Error& Error::operator +=(const Error& rhs)
    {
        msg() += rhs.cmsg();
        return *this;
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace tomaqa {
    ostream& operator <<(ostream& os, const Error& rhs)
    {
        if (!rhs._printed) {
            if (!empty(rhs.cmsg())) os << rhs.to_string();
            else os << "<empty error message>";
            rhs._printed = true;
        }
        return os;
    }
}
