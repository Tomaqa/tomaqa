#include "tomaqa/core.hpp"
#include "tomaqa/core/object.hpp"

namespace tomaqa::detail {
    void Dynamic_base_base::virtual_init()
    try {
        try { check_pre_init(); }
        catch (const Error& err) {
            recover_pre_init();
            THROW("Checking the pre-conditions failed: ") + err;
        }
        try { init(); }
        catch (const Error& err) {
            recover_init();
            THROW("The initialization failed: ") + err;
        }
        _initialized = true;
        try { check_post_init(); }
        catch (const Error& err) {
            recover_post_init();
            THROW("Checking the post-conditions failed: ") + err;
        }
    }
    catch (const Error& err) {
        recover_virtual_init();
        throw "At '"s + to_string() + "':\n" + err;
    }

    void Dynamic_base_base::maybe_virtual_init()
    {
        if (!initialized()) virtual_init();
    }

    String Dynamic_base_base::to_string() const&
    {
        if (!initialized()) return "<uninitialized>";
        return "<undefined to_string>";
    }
}
