#include "tomaqa/core.hpp"
#include "tomaqa/core/string.hpp"

#include <istream>
#include <iomanip>

namespace tomaqa {
    String to_string(floating_point auto arg, int precision)
    {
        ostringstream oss;
        oss << std::setprecision(precision) << std::fixed << arg;
        return oss.str();
    }

    template String to_string(float, int precision);
    template String to_string(double, int precision);

    String to_string(char arg)
    {
        return String(1, arg);
    }

    //+ This is clumsy and not properly tested
    String to_string(istream& is)
    {
        String str;
        int size_ = istream_remain_size(is);
        if (size_ <= 0) size_ = 32;
        str.reserve(size_);

        str.assign(std::istreambuf_iterator<char>(is),
                   std::istreambuf_iterator<char>());
        return str;
    }

    String to_string(istream& is, streampos end_pos)
    {
        const streampos pos = is.tellg();
        const int size_ = end_pos - pos;
        return to_string(is, size_);
    }

    String to_string(istream& is, streamsize size_)
    {
        if (size_ <= 0) return "";
        String str;
        str.resize(size_);
        is.read(str.data(), size_);
        return str;
    }

    String to_string(const Ignore&)
    {
        return "<ignore>";
    }

    String to_string(partial_ordering ord)
    {
        if (is_eq(ord)) return " == ";
        else if (is_lt(ord)) return " < ";
        else if (is_lteq(ord)) return " <= ";
        else if (is_gt(ord)) return " > ";
        else if (is_gteq(ord)) return " >= ";
        else return " ?=? ";
    }

    int istream_remain_size(istream& is)
    {
        streampos pos = is.tellg();
        if (pos < 0) return -1;
        is.seekg(0, std::ios::end);
        auto size_ = is.tellg() - pos;
        is.seekg(pos);
        return size_;
    }
}
