#include "tomaqa/math.hpp"

#include "tomaqa/util/scope_guard.hpp"

#include <boost/math/tools/simple_continued_fraction.hpp>

#include <cfenv>

namespace tomaqa::math {
    using boost::math::tools::simple_continued_fraction;
    using boost::math::tools::simple_continued_fraction_coefficients;

    template <floating_point FloatT, Rational_c RationalT>
    RationalT to_rational(FloatT x)
    {
        using tomaqa::to_string;

        using Int = typename RationalT::int_type;

        using limits = math::limits<FloatT>;
        using int_limits = math::limits<Int>;
        constexpr auto max_exp = limits::max_exponent-1;
        constexpr auto max_int_exp = int_limits::digits-1;
        constexpr auto digits = min(limits::digits, max_int_exp);

        static_assert(max_exp > max_int_exp);

        expect(isfinite(x),
               "Only conversion of finite floating-point values to rational numbers are supported, got: "s
               + to_string(x));

        Rm_const<decltype(digits)> exp_;

        assert((x == 0) == (frexp(x, &exp_) == 0));
        if (x == 0) return 0;

        double_t numerator = frexp(x, &exp_);
        assert(abs(numerator) >= 0.5 && abs(numerator) < 1);
        const auto diff_exp = digits - exp_;
        const auto cmp = diff_exp <=> 0;
        if (is_eq(cmp)) {
            numerator = ldexp(numerator, digits);
            return lround(numerator);
        }
        if (is_lt(cmp)) {
            numerator = ldexp(numerator, exp_);
            return lround(numerator);
        }

        assert(is_gt(cmp));
        if (diff_exp <= max_int_exp) {
            numerator = ldexp(numerator, digits);
            Int denominator = 1L << diff_exp;
            return {lround(numerator), denominator};
        }

        numerator = ldexp(numerator, exp_ + max_int_exp);
        constexpr Int denominator = 1L << max_int_exp;
        return {lround(numerator), denominator};
    }

    template Rational_tp<int64_t> to_rational(double);
    template Rational_tp<int32_t> to_rational(double);
    template Rational_tp<int64_t> to_rational(float);
    template Rational_tp<int32_t> to_rational(float);

    namespace {
        template <Side sideV = Side::center, floating_point FloatT>
        void check_bounds(FloatT x, FloatT lhs, FloatT rhs)
        {
            assert(lhs <= rhs);
            if (sideV != Side::right) assert(lhs <= x);
            if (sideV != Side::left) assert(rhs >= x);
            if (sideV == Side::right) assert(lhs >= x);
            if (sideV == Side::left) assert(rhs <= x);
        }

        template <Side sideV, Precision_c P, int multV = 1, floating_point FloatT>
        pair<FloatT, FloatT> eps_bounds_of(FloatT x)
        {
            // It may be difficult for `simple_continued_fraction` without the rounding
            // in the case of higher precisions
            static constexpr auto round_f = [](FloatT x_){
                assert(sideV != Side::right || std::fegetround() == FE_UPWARD);
                assert(sideV != Side::left || std::fegetround() == FE_DOWNWARD);
                FloatT y = float(x_);
                assert(sideV != Side::right || y >= x_);
                assert(sideV != Side::left || y <= x_);
                return y;
            };

            FloatT ix;
            if (modf(x, &ix) == 0) return {x, x};

            Scope_guard sg;
            if constexpr (sideV == Side::right || sideV == Side::left) {
                const int prev_round = std::fegetround();
                sg.set_guard([prev_round]{
                    [[maybe_unused]] const int res = std::fesetround(prev_round);
                    assert(res == 0);
                });

                [[maybe_unused]] int res;
                if constexpr (sideV == Side::right) res = std::fesetround(FE_UPWARD);
                else res = std::fesetround(FE_DOWNWARD);
                assert(res == 0);
            }

            constexpr auto eps_ = multV*P::abs_eps();
            static_assert(eps_ > 0);

            // `{x + eps_, x + mult*eps_}` etc. seems to be contraproductive
            if constexpr (sideV == Side::right)
                return {round_f(x), round_f(x + eps_)};
            else if constexpr (sideV == Side::left)
                return {round_f(x - eps_), round_f(x)};
            else if constexpr (sideV == Side::center)
                return {round_f(x - eps_/2), round_f(x + eps_/2)};
            else static_assert(false_tp<decltype(sideV)>);
        }

        template <Side sideV, Precision_c P, floating_point FloatT>
        pair<FloatT, FloatT> apx_rational_float_bounds(FloatT x)
        {
            const auto [lhs, rhs] = eps_bounds_of<sideV, P>(x);

            if constexpr (_debug_) {
                check_bounds<sideV>(x, lhs, rhs);

                FloatT ix;
                if (modf(x, &ix) == 0) {
                    assert(x == ix);
                    assert(lhs == x || rhs == x);
                }
            }

            return {lhs, rhs};
        }

        template <floating_point FloatT, Rational_c RationalT, integral IntT>
        RationalT to_rational(Container auto&& coefs)
        {
            auto scf = simple_continued_fraction<FloatT, IntT>(FORWARD(coefs));
            return boost::math::tools::to_rational<RationalT>(move(scf));
        }
    }

    template <Side sideV, Precision_c P, floating_point FloatT, Rational_c RationalT>
    RationalT to_apx_rational(FloatT x)
    {
        static_assert(!is_same_v<P, precision::Max>);

        using Int = Int_tp<sizeof(FloatT)>;

        const auto [lhs, rhs] = apx_rational_float_bounds<sideV, P, FloatT>(x);

        auto scf_coefs1 = simple_continued_fraction_coefficients<FloatT, Int>(lhs);
        if (lhs == rhs) return to_rational<FloatT, RationalT, Int>(move(scf_coefs1));
        auto scf_coefs2 = simple_continued_fraction_coefficients<FloatT, Int>(rhs);

        const size_t max_size = min(scf_coefs1.size(), scf_coefs2.size());
        scf_coefs1.resize(max_size);
        for (Idx i = 0; i < max_size; ++i) {
            Int& c1 = scf_coefs1[i];
            const Int c2 = scf_coefs2[i];
            if (c1 == c2) continue;

            c1 = min(c1, c2) + 1;
            scf_coefs1.resize(i+1);
            break;
        }

        if constexpr (_debug_ && sizeof(FloatT) > sizeof(float)) {
            const auto y = to_float<RationalT, FloatT>(to_rational<FloatT, RationalT, Int>(scf_coefs1));
            constexpr int mult = 2;
            // it still may not be true since here it uses specific rounding modes
            // which later are not necessarily used ...
            const auto [lhs_, rhs_] = eps_bounds_of<sideV, P, mult>(x);
            check_bounds(y, lhs_, rhs_);
        }

        return to_rational<FloatT, RationalT, Int>(move(scf_coefs1));
    }

    template Rational_tp<int64_t> to_apx_rational<Side::left, precision::Low>(double);
    template Rational_tp<int32_t> to_apx_rational<Side::left, precision::Low>(double);
    template Rational_tp<int64_t> to_apx_rational<Side::left, precision::Low>(float);
    template Rational_tp<int32_t> to_apx_rational<Side::left, precision::Low>(float);
    template Rational_tp<int64_t> to_apx_rational<Side::left, precision::Medium>(double);
    template Rational_tp<int32_t> to_apx_rational<Side::left, precision::Medium>(double);
    template Rational_tp<int64_t> to_apx_rational<Side::left, precision::Medium>(float);
    template Rational_tp<int32_t> to_apx_rational<Side::left, precision::Medium>(float);
    template Rational_tp<int64_t> to_apx_rational<Side::left, precision::High>(double);
    template Rational_tp<int32_t> to_apx_rational<Side::left, precision::High>(double);
    template Rational_tp<int64_t> to_apx_rational<Side::left, precision::High>(float);
    template Rational_tp<int32_t> to_apx_rational<Side::left, precision::High>(float);
    template Rational_tp<int64_t> to_apx_rational<Side::right, precision::Low>(double);
    template Rational_tp<int32_t> to_apx_rational<Side::right, precision::Low>(double);
    template Rational_tp<int64_t> to_apx_rational<Side::right, precision::Low>(float);
    template Rational_tp<int32_t> to_apx_rational<Side::right, precision::Low>(float);
    template Rational_tp<int64_t> to_apx_rational<Side::right, precision::Medium>(double);
    template Rational_tp<int32_t> to_apx_rational<Side::right, precision::Medium>(double);
    template Rational_tp<int64_t> to_apx_rational<Side::right, precision::Medium>(float);
    template Rational_tp<int32_t> to_apx_rational<Side::right, precision::Medium>(float);
    template Rational_tp<int64_t> to_apx_rational<Side::right, precision::High>(double);
    template Rational_tp<int32_t> to_apx_rational<Side::right, precision::High>(double);
    template Rational_tp<int64_t> to_apx_rational<Side::right, precision::High>(float);
    template Rational_tp<int32_t> to_apx_rational<Side::right, precision::High>(float);
    template Rational_tp<int64_t> to_apx_rational<Side::center, precision::Low>(double);
    template Rational_tp<int32_t> to_apx_rational<Side::center, precision::Low>(double);
    template Rational_tp<int64_t> to_apx_rational<Side::center, precision::Low>(float);
    template Rational_tp<int32_t> to_apx_rational<Side::center, precision::Low>(float);
    template Rational_tp<int64_t> to_apx_rational<Side::center, precision::Medium>(double);
    template Rational_tp<int32_t> to_apx_rational<Side::center, precision::Medium>(double);
    template Rational_tp<int64_t> to_apx_rational<Side::center, precision::Medium>(float);
    template Rational_tp<int32_t> to_apx_rational<Side::center, precision::Medium>(float);
    template Rational_tp<int64_t> to_apx_rational<Side::center, precision::High>(double);
    template Rational_tp<int32_t> to_apx_rational<Side::center, precision::High>(double);
    template Rational_tp<int64_t> to_apx_rational<Side::center, precision::High>(float);
    template Rational_tp<int32_t> to_apx_rational<Side::center, precision::High>(float);

    Float operator *(Vec lhs, Vec rhs) noexcept
    {
        return lhs.x*rhs.x + lhs.y*rhs.y;
    }

    Float angle(Vec vec)
    {
        return atan2(vec.y, vec.x);
    }

    Float dist(Vec vec)
    {
        return hypot(vec.x, vec.y);
    }

    Vec rotate(Vec vec, Float a)
    {
        const Float cosa = cos(a);
        const Float sina = sin(a);
        return {cosa*vec.x - sina*vec.y, sina*vec.x + cosa*vec.y};
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace tomaqa {
    String to_string(const math::Rational_c auto& x)
    {
        return to_string(x.numerator()) + "/" + to_string(x.denominator());
    }

    template String to_string(const math::Rational_for<double>&);
    template String to_string(const math::Rational_for<float>&);
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace tomaqa::math {
    String Coord::to_string() const
    {
        return to_string(-precision::Medium::abs_eps_exp());
    }

    String Coord::to_string(int precision) const
    {
        using tomaqa::to_string;

        return "["s + to_string(x, precision) + "," + to_string(y, precision) + "]";
    }

    ////////////////////////////////////////////////////////////////

    String Vec::to_string() const
    {
        return to_string(-precision::Medium::abs_eps_exp());
    }

    //+ parametrize with bracket symbol
    String Vec::to_string(int precision) const
    {
        using tomaqa::to_string;

        return "("s + to_string(x, precision) + "," + to_string(y, precision) + ")";
    }

    ////////////////////////////////////////////////////////////////

    Interval::Interval(Float pt)
        : Interval(pt, pt)
    { }

    Interval::Interval(Float lo, Float hi)
        : Inherit({lo, hi})
    {
        assert(valid());
        check_range();
    }

    Interval::Interval(Float lo, Float hi, Not_empty_tag)
        : Inherit({lo, hi})
    {
        if (lo > hi) std::swap(lower(), upper());
        assert(valid());
        // There still can be NaN
        check_range();
    }

    Interval::Interval(Float lo, Float hi, Not_point_tag)
        : Interval(lo, hi)
    {
        if (is_point()) invalidate();
    }

    bool Interval::valid_range() const noexcept
    {
        assert(valid());
        assert(!isnan(clower()) || !valid_range_impl());
        assert(!isnan(cupper()) || !valid_range_impl());
        return valid_range_impl();
    }

    bool Interval::valid_range_impl() const noexcept
    {
        assert(valid());
        return cupper() >= clower();
    }

    void Interval::check_range() const
    {
        expect(valid_range(), "Invalid interval range: "s + to_string());
    }

    void Interval::set_range(Float lo, Float hi) noexcept
    {
        lower() = lo;
        upper() = hi;
        maybe_invalidate();
    }

    void Interval::set_lower(Float lo) noexcept
    {
        lower() = lo;
        maybe_invalidate();
    }

    void Interval::set_upper(Float hi) noexcept
    {
        upper() = hi;
        maybe_invalidate();
    }

    void Interval::set_range_check(Float lo, Float hi)
    {
        lower() = lo;
        upper() = hi;
        check_range();
    }

    void Interval::set_lower_check(Float lo)
    {
        lower() = lo;
        check_range();
    }

    void Interval::set_upper_check(Float hi)
    {
        upper() = hi;
        check_range();
    }

    void Interval::inc_lower(Float lo) noexcept
    {
        lower() += lo;
        maybe_invalidate();
    }

    void Interval::inc_upper(Float hi) noexcept
    {
        upper() += hi;
        maybe_invalidate();
    }

    void Interval::mul_lower(Float lo) noexcept
    {
        lower() *= lo;
        maybe_invalidate();
    }

    void Interval::mul_upper(Float hi) noexcept
    {
        upper() *= hi;
        maybe_invalidate();
    }

    void Interval::inc_lower_check(Float lo)
    {
        lower() += lo;
        check_range();
    }

    void Interval::inc_upper_check(Float hi)
    {
        upper() += hi;
        check_range();
    }

    void Interval::mul_lower_check(Float lo)
    {
        lower() *= lo;
        check_range();
    }

    void Interval::mul_upper_check(Float hi)
    {
        upper() *= hi;
        check_range();
    }

    template <Precision_c P>
    bool Interval::apx_empty() const noexcept
    {
        if (empty()) return true;
        return is_apx_point<P>();
    }

    template <Precision_c P>
    bool Interval::is_apx_point() const noexcept
    {
        return valid() && apx_equal<P>(clower(), cupper());
    }

    template bool Interval::apx_empty<precision::Low>() const noexcept;
    template bool Interval::apx_empty<precision::Medium>() const noexcept;
    template bool Interval::apx_empty<precision::High>() const noexcept;
    template bool Interval::apx_empty<precision::Huge>() const noexcept;
    template bool Interval::apx_empty<precision::Max>() const noexcept;
    template bool Interval::is_apx_point<precision::Low>() const noexcept;
    template bool Interval::is_apx_point<precision::Medium>() const noexcept;
    template bool Interval::is_apx_point<precision::High>() const noexcept;
    template bool Interval::is_apx_point<precision::Huge>() const noexcept;
    template bool Interval::is_apx_point<precision::Max>() const noexcept;

    Float Interval::middle() const
    {
        expect(valid(), "Cannot take middle point of "s + to_string());

        assert(valid_range());
        if (infinite()) return 0.;
        // Probably faster than testing `is_point()` and returning `point()`
        return (clower() + cupper())/2.;
    }

    Float Interval::size() const noexcept
    {
        assert(invalid() == empty());
        if (empty()) return nan;

        const Float diff = cupper() - clower();
        assert(diff >= 0);
        return diff;
    }

    Float Interval::radius() const noexcept
    {
        return size()/2.;
    }

    Interval& Interval::neg()
    {
        expect(valid(), "Undefined operation: -"s + to_string());
        [[maybe_unused]] const auto cp = *this;

        Float tmp = clower();
        lower() = -cupper();
        upper() = -tmp;

        assert(valid_range());
        assert(equals(-1.*cp));
        return *this;
    }

    Interval& Interval::operator +=(Float a)
    {
        using tomaqa::to_string;
        expect(valid(), "Undefined operation: "s + This::to_string() + " + " + to_string(a));

        lower() += a;
        upper() += a;

        assert(valid_range());
        return *this;
    }

    Interval& Interval::operator -=(Float a)
    {
        return operator +=(-a);
    }

    Interval& Interval::operator *=(Float a) noexcept
    {
        using tomaqa::swap;

        if (empty()) return *this;

        auto& l = lower();
        auto& u = upper();
        l *= a;
        u *= a;
        if (a < 0) swap(l, u);

        assert(valid_range());
        return *this;
    }

    Interval& Interval::operator /=(Float a) noexcept
    {
        return operator *=(1./a);
    }

    Interval& Interval::operator +=(Interval rhs)
    {
        expect(valid() && rhs.valid(),
               "Undefined operation: "s + to_string() + " + " + rhs.to_string());

        lower() += rhs.clower();
        upper() += rhs.cupper();

        assert(valid_range());
        return *this;
    }

    Interval& Interval::operator -=(Interval rhs)
    {
        return operator +=(move(rhs.neg()));
    }

    Interval& Interval::operator *=(Interval)
    {
        NOT_IMPLEMENTED_YET;
    }

    Interval& Interval::operator /=(Interval)
    {
        NOT_IMPLEMENTED_YET;
    }

    Interval& Interval::operator &=(Interval rhs) noexcept
    {
        if (empty()) return *this;
        if (rhs.empty()) return operator =(move(rhs));

        lower() = max(clower(), rhs.clower());
        upper() = min(cupper(), rhs.cupper());

        maybe_invalidate();
        return *this;
    }

    Interval& Interval::operator |=(Interval rhs) noexcept
    {
        if (rhs.empty()) return *this;
        if (empty()) return operator =(move(rhs));

        lower() = min(clower(), rhs.clower());
        upper() = max(cupper(), rhs.cupper());

        assert(valid_range());
        return *this;
    }

    bool Interval::contains(Float a) const noexcept
    {
        if (empty()) return false;
        return a >= clower() && a <= cupper();
    }

    bool Interval::contains(Interval rhs) const noexcept
    {
        if (empty() || rhs.empty()) return false;
        return rhs.clower() >= clower() && rhs.cupper() <= cupper();
    }

    void Interval::maybe_invalidate() noexcept
    {
        assert(valid());
        if (!valid_range()) invalidate();
        assert(invalid() || valid_range());
        assert(invalid() == empty());
    }

    bool Interval::equals(Float a) const noexcept
    {
        if (invalid()) return false;
        return point() == a;
    }

    partial_ordering Interval::compare(Float a) const
    {
        return compare(Interval(a));
    }

    partial_ordering Interval::compare(const Interval& rhs) const
    {
        if (equals(rhs)) return partial_ordering::equivalent;

        expect(valid() && rhs.valid(),
               "Undefined operation: "s + to_string() + " <=> " + rhs.to_string());

        if (cupper() < rhs.clower()) return partial_ordering::less;
        if (clower() > rhs.cupper()) return partial_ordering::greater;
        return partial_ordering::unordered;
    }

    template <Precision_c P>
    partial_ordering Interval::apx_compare(const Interval& rhs) const
    {
        if (apx_equals<P>(rhs)) return partial_ordering::equivalent;

        expect(valid() && rhs.valid(),
               "Undefined operation: "s + to_string() + " <~> " + rhs.to_string());

        if (apx_less<P>(cupper(), rhs.clower()))
            return partial_ordering::less;
        if (apx_greater<P>(clower(), rhs.cupper()))
            return partial_ordering::greater;
        return partial_ordering::unordered;
    }

    template partial_ordering Interval::apx_compare<precision::Low>(const Interval&) const;
    template partial_ordering Interval::apx_compare<precision::Medium>(const Interval&) const;
    template partial_ordering Interval::apx_compare<precision::High>(const Interval&) const;
    template partial_ordering Interval::apx_compare<precision::Huge>(const Interval&) const;
    template partial_ordering Interval::apx_compare<precision::Max>(const Interval&) const;

    String Interval::to_string() const
    {
        return to_string(-precision::Medium::abs_eps_exp());
    }

    String Interval::to_string(int precision) const
    {
        using tomaqa::to_string;

        assert(invalid() == empty());
        if (empty()) return "[]";
        if (infinite()) return "[inf]";
        if (is_point()) return "["s + to_string(point(), precision) + "]";
        return "["s + to_string(clower(), precision) + "," + to_string(cupper(), precision) + "]";
    }
}
