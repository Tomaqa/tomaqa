#include "tomaqa/math/alg.hpp"

namespace tomaqa::math {
    template <bool lessV>
    Interval const_ineq(Float c)
    {
        if (isnan(c)) return nan;

        const bool cmp_val = invoke([c]{
            const auto res = c <=> 0;
            if constexpr (lessV) return is_lteq(res);
            else return is_gteq(res);
        });
        if (cmp_val) return inf_interval;

        return {};
    }

    template <bool lessV>
    Interval linear_ineq(Float b, Float c)
    {
        if (b == 0) return const_ineq<lessV>(c);

        const Float div = -c/b;
        const bool negative = b < 0;
        auto res = invoke([div, negative]() -> Interval {
            if (lessV ^ negative) return {-inf, div};
            else return {div, inf};
        });

        if (res.is_point()) return {};
        return res;
    }

    template <bool lessV>
    Interval quadratic_ineq(Float a, Float b, Float c)
    {
        assert(!lessV || a >= 0 || isnan(a));
        assert(lessV || a <= 0 || isnan(a));
        if (a == 0) return linear_ineq<lessV>(b, c);

        if (isfinite(a) && isfinite(b) && isfinite(c)) {
            auto [lo, hi] = quadratic_roots(a, b, c);
            if (isnan(lo)) {
                assert(isnan(hi));
                return {};
            }
            assert(lo <= hi);
            return {lo, hi};
        }

        const Float _2_a =  2*a;
        const Float D = b*b - 2*_2_a*c;

        const auto res = D <=> 0;
        if (is_lt(res)) {
            return {};
        }

        if (is_eq(res)) {
            return -b/_2_a;
        }

        assert(isnan(D) || is_gt(res));
        const Float sqrt_D = sqrt(D);
        assert(isnan(D) || sqrt_D > 0);
        const Float lo = (-b - sqrt_D)/_2_a;
        const Float hi = (-b + sqrt_D)/_2_a;
        if constexpr (lessV) return {lo, hi};
        else return {hi, lo};
    }

    template Interval const_ineq<true>(Float);
    template Interval const_ineq<false>(Float);
    template Interval linear_ineq<true>(Float, Float);
    template Interval linear_ineq<false>(Float, Float);
    template Interval quadratic_ineq<true>(Float, Float, Float);
    template Interval quadratic_ineq<false>(Float, Float, Float);
}
