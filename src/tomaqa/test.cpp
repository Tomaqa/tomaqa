#include "tomaqa/test.hpp"

namespace tomaqa::test {
    Suite::Suite(String msg_, bool should_throw_)
        : _msg(move(msg_)), _should_throw(should_throw_)
    { }
}
