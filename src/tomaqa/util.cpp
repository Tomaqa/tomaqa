#include "tomaqa/util.hpp"

namespace tomaqa::util {
    ostream& operator <<(ostream& os, const Side& side)
    {
        using enum Side;
        switch (side) {
        case left: return (os << "<-");
        case right: return (os << "->");
        case center: return (os << "|");
        default: UNREACHABLE;
        }
    }

    void mask(Mask& mask_, Idx idx)
    {
        mask_[idx] = true;
    }

    void unmask(Mask& mask_, Idx idx)
    {
        mask_[idx] = false;
    }
}
