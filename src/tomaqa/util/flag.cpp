#include "tomaqa/util/flag.hpp"

namespace tomaqa::util {
    Flag& Flag::operator =(int val) noexcept
    {
        if (val == unknown_value) forget();
        else emplace(bool(val));
        return *this;
    }

    Flag Flag::flip() const& noexcept
    {
        return Flag(*this).flip();
    }

    Flag& Flag::flip()& noexcept
    {
        if (valid()) value() = !cvalue();
        return *this;
    }

    namespace {
        bool maybe_invalid(Flag& lhs, const Flag& rhs) noexcept
        {
            if (lhs.invalid()) return true;
            if (rhs.invalid()) {
                lhs.invalidate();
                return true;
            }
            return false;
        }
    }

    /// Less efficient than operations in `::Minisat::lbool`
    Flag& Flag::operator &=(Flag rhs) noexcept
    {
        if (maybe_invalid(*this, rhs)) return *this;

        auto& val = value();
        if (val) val = move(rhs).value();
        return *this;
    }

    Flag& Flag::operator |=(Flag rhs) noexcept
    {
        if (maybe_invalid(*this, rhs)) return *this;

        auto& val = value();
        if (!val) val = move(rhs).value();
        return *this;
    }

    Flag& Flag::operator ^=(Flag rhs) noexcept
    {
        if (maybe_invalid(*this, rhs)) return *this;

        value() ^= move(rhs).value();
        return *this;
    }
}
