#include "tomaqa/util/path.hpp"

namespace tomaqa::util {
    bool Path::has_dirname() const
    {
        return has_parent_path();
    }

    Path Path::dirname() const
    {
        return parent_path();
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace tomaqa::util {
    Path pwd()
    {
        return filesystem::current_path();
    }

    Path root()
    {
        return pwd().root_path();
    }
}
