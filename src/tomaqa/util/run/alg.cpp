#include "tomaqa/util/run/alg.hpp"

#include "tomaqa/util/alg.hpp"

namespace tomaqa::util {
    Path common_dirname(const Path& orig_path1, const Path& orig_path2)
    {
        /// Do not use `canonical`, it expands symlinks
        Path path1 = absolute(orig_path1);
        Path path2 = absolute(orig_path2);
        if (path1 > path2) {
            swap(path1, path2);
        }

        Path path;
        for_each_while(path1, cbegin(path2), [&path](const Path& fn1, const Path& fn2){
            if (fn1 != fn2) return false;
            path /= fn1;
            return true;
        });

        return path;
    }
}
