#include "tomaqa/util/string.hpp"

namespace tomaqa::util {
    C_str::~C_str()
    {
        clear();
    }

    C_str::C_str(const C_str& rhs)
        : C_str(rhs.cdata())
    { }

    C_str& C_str::operator =(const C_str& rhs)
    {
        if (this != &rhs) *this = rhs.cdata();
        return *this;
    }

    C_str::C_str(C_str&& rhs)
        : C_str(rhs._data)
    {
        rhs._data = nullptr;
    }

    C_str& C_str::operator =(C_str&& rhs)
    {
        if (this != &rhs) {
            *this = rhs._data;
            rhs._data = nullptr;
        }
        return *this;
    }

    C_str::C_str(Const_data data_)
        : C_str(strdup(data_))
    { }

    C_str::C_str(Data data_)
        : _data(data_)
    { }

    C_str& C_str::operator =(Const_data data_)
    {
        return operator =(strdup(data_));
    }

    C_str& C_str::operator =(Data data_)
    {
        clear();
        _data = data_;
        return *this;
    }

    C_str::C_str(const String& str)
        : C_str(str.c_str())
    { }

    C_str& C_str::operator =(const String& str)
    {
        return operator =(str.c_str());
    }

    void C_str::clear() noexcept
    {
        free(data());
        _data = nullptr;
    }

    String C_str::to_string() const
    {
        return cdata();
    }

    ////////////////////////////////////////////////////////////////

    String_view::String_view(const String& str)
        : String_view(str.operator std::string_view())
    { }

    bool String_view::equals(const This& rhs) const
    {
        return std::operator ==(*this, rhs);
    }

    String String_view::to_string() const
    {
        return String(*this);
    }
}
