#include "tomaqa/util/string/alg.hpp"

#include <sstream>

namespace tomaqa::util {
    bool contains(String_view view, String_view substr) noexcept
    {
        return view.find(substr) != String_view::npos;
    }

    bool contains(String_view view, char c) noexcept
    {
        return view.find(c) != String_view::npos;
    }

    Vector<String> split(String str, char delim)
    {
        Vector<String> tokens;
        istringstream iss(move(str));
        for (String token; getline(iss, token, delim);) {
            tokens.push_back(move(token));
        }
        return tokens;
    }

    pair<String, String> split_to_pair(String str, char delim)
    {
        String first, second;
        istringstream iss(move(str));
        getline(iss, first, delim);
        getline(iss, second);
        return {move(first), move(second)};
    }

    String extract_first(istream& is, char delim1, char delim2)
    {
        String out;
        getline(is, out, delim1);
        getline(is, out, delim2);
        if (!is.good()) return {};
        return out;
    }

    Vector<String> extract_all(istream& is, char delim1, char delim2)
    {
        Vector<String> tokens;
        while (is) {
            String str = extract_first(is, delim1, delim2);
            if (str.empty()) break;
            tokens.push_back(move(str));
            is.unget();
        }
        return tokens;
    }

    String::size_type replace_first(String& str, char delim1, char delim2, String_view with, String::size_type pos)
    {
        auto pos1 = str.find(delim1, pos);
        if (pos1 == String::npos) return String::npos;
        auto pos2 = str.find(delim2, pos1+1);
        if (pos2 == String::npos) return String::npos;

        const auto old_size = pos2-pos1-1;
        const auto new_size = with.size();
        str.replace(pos1+1, old_size, with.data(), new_size);
        return pos2 + (new_size-old_size);
    }

    void replace_all(String& str, char delim1, char delim2, String_view with, String::size_type pos)
    {
        while ((pos = replace_first(str, delim1, delim2, with, pos)) != String::npos);
    }

    String repeat(char c, int n)
    {
        return String(n, c);
    }

    String repeat(String_view view, int n)
    {
        if (empty(view) || n == 0) return {};
        if (n == 1) return view;

        if (size(view) == 1) return repeat(view[0], n);

        String res = view;
        res.reserve(n*size(view));
        int m = 2;
        for (; m <= n; m *= 2) res += res;
        res.append(res.c_str(), (n-m/2)*size(view));
        return res;
    }
}
