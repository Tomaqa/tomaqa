#!/bin/sh

NAME=`sed -rn 's/PROJ_NAME.*:= *(.*)$/\1/p' <config.mk`

find src/ -type f \( -name '*.cpp' -o -name '*.c' \) -exec g++ -std=c++20 -c -Iinclude -Isrc -MM {} -MT {} \; | sed 's|^[^/]*/\([^.]*\)\.[^:]*:|$(BUILD_DIR)/\1.o:|' >.depend
