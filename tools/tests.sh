#!/bin/bash

ERR_F=`mktemp` || exit
ok=1

for t in "./test/"*/*; do
   printf -- "%s ... " "$t"
	"$t" &>"$ERR_F" || {
		printf -- "Error!\n"
		cat "$ERR_F"
		ok=0
		break
	}
	printf "OK\n"
done

(( $ok )) && printf -- "Success.\n"

rm "$ERR_F"

exit $ok
